<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Custom errors
Route::get('/access-denied', [PageController::class, 'accessDenied'])->name('access-denied');

// Pages
Route::get('/', [PageController::class, 'welcome'])->name('welcome');
Route::get('/find-employees', [EmployeeController::class, 'index'])->name('find-employees');
Route::get('/why-quopro', [PageController::class, 'whyQuopro'])->name('why-quopro');
Route::get('/contact', [PageController::class, 'contact'])->name('contact');
Route::get('/growpro', [PageController::class, 'growpro'])->name('growpro');
Route::get('/privacy-policy', [PageController::class, 'privacyPolicy'])->name('privacy-policy');
Route::get('/terms-and-conditions', [PageController::class, 'tandcs'])->name('tandcs');
Route::get('/tips/profile-description', [PageController::class, 'profileDescription'])->name('profile-description');

// Services pages
Route::get('/services', [PageController::class, 'services'])->name('services');

// Signup
Route::get('/signup', [PageController::class, 'signup'])->name('signup');
Route::get('/signup/employer', [PageController::class, 'signupEmployer'])->name('signup.employer');
Route::get('/signup/employee', [PageController::class, 'signupEmployee'])->name('signup.employee');

// Blog routes
Route::get('/blog', [PageController::class, 'blog'])->name('blog');
Route::get('/blog/get', [PageController::class, 'getPosts'])->name('get-posts');
Route::get('/blog/{date}/{slug}', [PageController::class, 'blogShow'])->name('blog-single');

// Employees routes
Route::get('/employees/get', [EmployeeController::class, 'getEmployees'])->name('get-employees');
Route::get('/jobs/get', [JobController::class, 'getJobs'])->name('get-jobs');
Route::get('/jobs/get/all', [JobController::class, 'getAllJobs'])->name('get-all-jobs');
Route::get('/employees/favourite/{employee}', [EmployeeController::class, 'favourite'])->name('employee-favourite');
Route::get('/employees/unfavourite/{employee}', [EmployeeController::class, 'unfavourite'])->name('employee-unfavourite');
Route::get('/employees/enquire/{employee}', [EmployeeController::class, 'enquire'])->name('employee-favourite');
Route::get('/employees/similar/{employee}', [EmployeeController::class, 'similar'])->name('similar-employees');

// Sign up
Route::get('/sign-up', [PageController::class, 'signup'])->name('sign-up');
Route::post('/check-personal-details', [AccountController::class, 'checkPersonalDetails'])->name('check-personal-details');
Route::post('/check-address-details', [AccountController::class, 'checkAddressDetails'])->name('check-address-details');
Route::post('/check-employer-details', [AccountController::class, 'checkEmployerDetails'])->name('check-employer-details');
Route::post('/check-employee-details', [AccountController::class, 'checkEmployeeDetails'])->name('check-employee-details');
Route::post('/signup/employer', [EmployerController::class, 'store'])->name('signup-employer');
Route::post('/signup/employee', [EmployeeController::class, 'store'])->name('signup-employee');

// Industries routes
Route::get('/industries/get', [IndustryController::class, 'getIndustries'])->name('get-industries');
Route::get('/attributes/get', [EmployeeController::class, 'getAttributes'])->name('get-attributes');
// Send enquiry form
Route::post('/send-message', [SendMail::class, 'enquiry'])->name('send-message');

// Login handler
Route::get('/home', [HomeController::class, 'index'])->name('home');

//Avatars
Route::get('/avatars/get', [EmployeeController::class, 'getAvatars'])->name('get-avatars');

Route::get('/dashboard', function () {
	if(Auth::user()->role->id == 2){
		return redirect()->to('/employers/dashboard');
	}else if(Auth::user()->role->id == 3){
		return redirect()->to('/employees/dashboard');
	}else if(Auth::user()->role->id == 1){
		return redirect()->to('/nova');
	}else{
		return redirect()->to('/logout');
	}
})->middleware(['auth'])->name('dashboard');

// Employer middlware
Route::group(['middleware' => 'App\Http\Middleware\EmployerMiddleware'], function(){

	Route::get('/employers/{any}', [SpaController::class, 'employers'])->where('any', '.*');
	Route::get('/favourites/get/{limit}', [EmployerController::class, 'favourites'])->name('get-favourites');
	Route::get('/enquiries/get/{limit}', [EmployerController::class, 'enquiries'])->name('get-enquiries');
	Route::post('/employer/update', [EmployerController::class, 'update'])->name('employer-update');
	Route::get('/account/specific-employer', [AccountController::class, 'specific'])->name('get-specific-account');
	

});

// Employer middlware
Route::group(['middleware' => 'App\Http\Middleware\ApprovedMiddleware'], function(){
	
	Route::get('/find-employees/view/{employee}', [EmployeeController::class, 'show'])->name('show-employee');

});

// Employee middlware
Route::group(['middleware' => 'App\Http\Middleware\EmployeeMiddleware'], function(){

	Route::get('/employees/{any}', [SpaController::class, 'employees'])->where('any', '.*');
	Route::post('/employee/update', [EmployeeController::class, 'update'])->name('employee-update');
	Route::get('/account/specific-employee', [AccountController::class, 'specific'])->name('get-specific-account');

});

Route::post('/account/avatar/update', [AccountController::class, 'avatar'])->middleware(['auth'])->name('update-avatar');
Route::post('/account/update', [AccountController::class, 'update'])->middleware(['auth'])->name('update-account');
Route::post('/account/password/update', [AccountController::class, 'updatePassword'])->middleware(['auth'])->name('update-password');

Route::get('/logout', function(){
	Auth::logout();
	return redirect()->to('/login');
});

require __DIR__.'/auth.php';