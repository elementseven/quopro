<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
  use HasFactory;

  protected $fillable = [
    'employee_id','employer_id','status','created_at'
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
      'created_at' => 'datetime:Y-m-d',
  ];


  public function employee(){
    return $this->belongsTo('App\Models\Employee');
  }

	public function employer(){
    return $this->belongsTo('App\Models\Employer');
  }

}
