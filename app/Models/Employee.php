<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
  use HasFactory;

  protected $fillable = [
    'status', 'description', 'dob', 'approved', 'expected_salary', 'experience', 'working_pattern', 'job_title', 'growpro', 'job_id', 'user_id', 'avatar_id'
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
      'dob' => 'date',
  ];

	public function job(){
	  return $this->belongsTo('App\Models\Job');
	}

  public function user(){
    return $this->belongsTo('App\Models\User');
  }

	public function favourites(){
    return $this->belongsToMany('App\Models\Employer', 'favourites')->withPivot('employer_id','employee_id','created_at');
  }

  public function enquiries(){
    return $this->hasMany('App\Models\Enquiry');
  }

   public function attributes(){
        return $this->belongsToMany('App\Models\Attribute', 'attribute_employee')->withPivot('attribute_id');
    }

}
