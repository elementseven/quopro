<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'first_name', 'last_name', 'email', 'phone', 'password', 'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($user) {
            $user->full_name = $user->first_name . ' ' . $user->last_name;
        });
    }

    public function role(){
        return $this->belongsTo('App\Models\Role');
    }

    public function addresses(){
        return $this->hasMany('App\Models\Address');
    }

    public function employee(){
        return $this->hasOne('App\Models\Employee');
    }

    public function employer(){
        return $this->hasOne('App\Models\Employer');
    }

}
