<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Industry extends Model
{
  use HasFactory;

  protected $fillable = [
    'title','slug'
  ];

  protected static function boot()
  {
    parent::boot();
    static::saving(function ($industry) {
        $industry->slug = Str::slug($industry->title, "-");
    });
  }

  public function employers(){
    return $this->hasMany('App\Models\Employer');
  }
}
