<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
  use HasFactory;

  protected $fillable = [
    'title','slug'
  ];
  
  public function posts(){
    return $this->belongsToMany('App\Models\Post', 'blog_post')->withPivot('post_id');
  }
}
