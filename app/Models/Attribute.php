<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Support\Str;

class Attribute extends Model implements HasMedia
{
  use HasFactory;
  use InteractsWithMedia;

  protected $fillable = [
    'title','slug','image'
  ];

  protected static function boot()
  {
    parent::boot();
    static::saving(function ($attribute) {
        $attribute->slug = Str::slug($attribute->title, "-");
    });
  }

  public function registerMediaConversions(Media $media = null): void
  {
    $this->addMediaConversion('normal')->width(400);
    $this->addMediaConversion('webp')->width(400)->format('webp');
  }
  public function registerMediaCollections(): void
  {
    $this->addMediaCollection('attributes')->singleFile();
  }

    public function employees(){
        return $this->belongsToMany('App\Models\Employee', 'attribute_employee')->withPivot('employee_id');
    }

}