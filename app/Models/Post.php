<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Support\Str;
use Carbon\Carbon;

class Post extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($post) {
            $post->slug = Str::slug($post->title, "-");
        });
    }
    
    protected $casts = [
        'created_at' => 'datetime',
    ];

    public function getDate($value)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('Y-m-d');
    }

    public function getFancyDate($value)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('jS M Y');
    }
    
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')->crop('crop-center', 400, 400);
        $this->addMediaConversion('normal')->width(1170);
        $this->addMediaConversion('normal-webp')->width(1170)->format('webp');
        $this->addMediaConversion('featured')->crop('crop-center', 800, 534);
        $this->addMediaConversion('featured-webp')->crop('crop-center', 800, 534)->format('webp');
    }
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('blog')->singleFile();
    }


    public function blogs(){
        return $this->belongsToMany('App\Models\Blog', 'blog_post')->withPivot('blog_id');
    }


}
