<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employer extends Model
{
  use HasFactory;

	protected $fillable = [
    'status', 'name', 'description', 'industry_id', 'user_id'
  ];

  public function user(){
	  return $this->belongsTo('App\Models\User');
	}

	public function industry(){
	  return $this->belongsTo('App\Models\Industry');
	}

	public function favourites(){
    return $this->belongsToMany('App\Models\Employee', 'favourites')->withPivot('employer_id','employee_id','created_at');
  }

  public function enquiries(){
    return $this->hasMany('App\Models\Enquiry');
  }

}
