<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\InteractsWithMedia;

class Avatar extends Model implements HasMedia
{
  use HasFactory;
  use InteractsWithMedia;

	protected $fillable = [
    'title'
  ];

  public function registerMediaConversions(Media $media = null): void
  {
    $this->addMediaConversion('normal')->width(400);
    $this->addMediaConversion('webp')->width(400)->format('webp');
  }
  public function registerMediaCollections(): void
  {
    $this->addMediaCollection('avatar')->singleFile();
  }

  public function employees(){
    return $this->hasMany('App\Models\Employee');
  }

}
