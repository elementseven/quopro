<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Trix;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class Employee extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Employee::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    // public static $title = 'user.full_name';

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->user->full_name;
    }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $statuses = array('show'=>'Show','hide'=>'Hide');
        $workingpatterns = array('Full Time'=>'Full Time','Part Time'=>'Part Time');
        $approvals = array(1 =>'Approve',0 =>'Deny');
        return [
            BelongsTo::make('User','user'),
            Select::make('Status')->options($statuses)->sortable()->rules('required'),
            Select::make('Approve','approved')->options($approvals)->sortable()->rules('required'),
            BelongsTo::make('Job','job'),
            Text::make('Job Title')->sortable()->rules('required'),
            Date::make('Date of Birth','dob')->rules('required'),
            Text::make('Expected Salary','expected_salary')->onlyOnForms()->rules('required'),
            Text::make('Experience')->sortable()->rules('required'),
            Select::make('Working Pattern', 'working_pattern')->options($workingpatterns)->sortable()->rules('required'),
            Trix::make('Description')->sortable()->rules('required'),
            Boolean::make('Growpro','growpro')->nullable(),
            BelongsToMany::make('Favourited By','favourites', 'App\Nova\Employer'),
            BelongsToMany::make('Attributes','attributes', 'App\Nova\Attribute'),
            HasMany::make('Enquiries','enquiries')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
