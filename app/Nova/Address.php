<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Http\Requests\NovaRequest;

class Address extends Resource
{
    public static $displayInNavigation = false;
    
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Address::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'streetAddress';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'streetAddress',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $types = array('company'=> 'Company Address','personal'=>'Personal Address');
        return [

            Text::make('Street Address', 'streetAddress')
            ->help('First line of address')
            ->rules('required', 'string', 'max:255'),

            Text::make('Extended Address', 'extendedAddress')
            ->help('Apartment or suite number'),

            Text::make('city')
            ->help('City or town')
            ->rules('required', 'string', 'max:255'),

            Text::make('Postal Code','postalCode')
            ->help('Post or zip code')
            ->rules('required', 'string', 'max:255'),

            Text::make('Country')
            ->help('Country')
            ->onlyOnForms()
            ->rules('required', 'string', 'max:255'),

            Text::make('Region')
            ->help('State or Province'),

            Text::make('Company')
            ->help('Company Name'),

            Select::make('Type')
            ->options($types)
            ->sortable()
            ->rules('required')
            ->help('Select the type of address'),

            BelongsTo::make('User')->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
