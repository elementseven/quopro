<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class ApprovedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->user() != NULL && $request->user()->role->id == 2 && $request->user()->employer->status == 'Approved'){
            return $next($request);
        }else{
            return redirect()->to('/access-denied');
        }
    }
}
