<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class SpaController extends Controller
{
  public function employers()
  {
  	$user = Auth::user();
  	$user->employer = $user->employer;
    return view('employers')->with('user', $user);
  }
  public function employees()
  {
  	$user = Auth::user();
  	$user->employee = $user->employee;
    $user->avatar = $user->employee->job->getFirstMediaUrl('jobs','normal');
    return view('employees')->with('user', $user);
  }
}