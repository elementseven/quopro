<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\NewEmployer;
use App\Notifications\EmployerSignup;

use App\Models\Employer;
use App\Models\Employee;
use App\Models\Address;
use App\Models\User;

use Auth;
use Hash;

class EmployerController extends Controller
{
    /**
     * Display a listing of the favourites.
     *
     * @return \Illuminate\Http\Response
     */
    public function favourites($limit)
    {
        $employees = Employee::whereHas('favourites', function($q){
            $q->where('employer_id', Auth::user()->employer->id);
        })
        ->with('user.addresses','job')
        ->paginate($limit);

        foreach($employees as $e){
            $e->fav = false;
            if($e->favourites()->where('id', Auth::user()->employer->id)->exists()){
                $e->fav = true;
            }else{
                $e->fav = false;
            }
        }

        return $employees;
    }

    /**
     * Display a listing of the enquiries.
     *
     * @return \Illuminate\Http\Response
     */
    public function enquiries($limit)
    {
        $employees = Employee::with('user.addresses','job','enquiries')
        ->whereHas('enquiries', function($q){
            $q->where('employer_id', Auth::user()->employer->id);
        })
        
        ->paginate($limit);

        foreach($employees as $e){
            $e->fav = false;
            if($e->favourites()->where('id', Auth::user()->employer->id)->exists()){
                $e->fav = true;
            }else{
                $e->fav = false;
            }
        }

        return $employees;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'phone' => 'required|string',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
            'employer.name' => 'required|string|min:2',
            'employer.industry' => 'required|not_in:*',
            'employer.description' => 'required',
            'address.streetAddress' => 'required|string|max:255',
            'address.city' => 'required|string|max:255',
            'address.postalCode' => 'required|string|max:255',
            'address.country' => 'required|string|max:255'
        ]);

        Auth::login($user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'email' => $request->email,
            'role_id' => 2,
            'password' => Hash::make($request->password)
        ]));

        Address::create([
            'streetAddress' => $request->input('address.streetAddress'),
            'extendedAddress' => $request->input('address.extendedAddress'),
            'city' => $request->input('address.city'),
            'region' => $request->input('address.region'),
            'company' => $request->input('employer.name'),
            'postalCode' => $request->input('address.postalCode'),
            'country' => $request->input('address.country'),
            'type' => 'business',
            'user_id' => $user->id
        ]);

        $employer = Employer::create([
            'name' => $request->input('employer.name'),
            'industry_id' => $request->input('employer.industry'),
            'description' => $request->input('employer.description'),
            'status' => 'Awaiting Approval',
            'user_id' => $user->id
        ]);

        $admin = User::find(2);
        $admin->notify(new NewEmployer($employer));
        $user->notify(new EmployerSignup($employer));

        return response()->json(['success' => 'success', 200]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employer  $employer
     * @return \Illuminate\Http\Response
     */
    public function show(Employer $employer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employer  $employer
     * @return \Illuminate\Http\Response
     */
    public function edit(Employer $employer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employer  $employer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $employer = Auth::user()->employer;
        $employer->update([
            'name' => $request->input('name'),
            'industry_id' => $request->input('industry'),
            'description' => $request->input('description'),
        ]);
        return response()->json(['success' => 'success', 200]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employer  $employer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employer $employer)
    {
        //
    }
}
