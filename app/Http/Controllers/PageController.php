<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Category;
use App\Models\Post;
use App\Models\Blog;
use Auth;
use Mail;


class PageController extends Controller
{

  // Access denied page
  public function accessDenied(){
    return view('permission');
  }
  // Home page
  public function welcome(){
    $posts = Post::where('status','published')->orderBy('created_at','desc')->paginate(3);
    return view('welcome', compact('posts'));
  }

  // Sign Up Page
  public function signup(){
    if(Auth::check()){
      return redirect()->to('/login');
    }
    return view('signup.signup');
  }

  public function profileDescription(){
    return view('tips');
  }

  // Sign Up Employer Page
  public function signupEmployer(){
    return view('signup.employer');
  }

  // Sign Up Employee Page
  public function signupEmployee(){
    return view('signup.employee');
  }

  // Find Employees page
  public function findEmployees(){
    return view('findEmployees');
  }

  // Why QuoPro page
  public function whyQuopro(){
    return view('whyQuopro');
  }

  // Blog index page
  public function blog(){
    $categories = Blog::whereHas('posts')->orderBy('title','asc')->get();
    return view('blog.index', compact('categories'));
  }

  // Return json of blogs
  public function getPosts(Request $request){
    if($request->input('category') == '*'){
      $posts = Post::where([['title','LIKE','%'.$request->input('title').'%'],['status','!=','draft']])
      ->orderBy('created_at','desc')
      ->with('blogs')
      ->paginate($request->input('limit'));
    }else{
      $posts = Post::whereHas('blogs', function($q) use($request){
          $q->where('id', $request->input('category'));
      })
      ->where([['title','LIKE','%'.$request->input('title').'%'],['status','!=','draft']])
      ->orderBy('created_at','desc')
      ->with('blogs')
      ->paginate($request->input('limit'));
    }

    return $posts;
  }

  // Individual blog page
  public function blogShow($date, $slug){
    $post = Post::where('slug', $slug)->whereDate('created_at', $date)->where('status','published')->with('blogs')->first();
    $others = Post::where('id','!=', $post->id)->where('status', 'published')->orderBy('created_at','desc')->paginate(6);
    return view('blog.show',compact('post','others'));
  }

  // Growpro page
  public function growpro(){
    return view('growpro');
  }

  // Contact page
  public function contact(){
    return view('contact');
  }

  // Services - Index
  public function services(){
    return view('services.index');
  }

  // Terms & Conditions
  public function tandcs(){
    return view('tandcs');
  }

  // Privacy Policy
  public function privacyPolicy(){
    return view('privacyPolicy');
  }

}
