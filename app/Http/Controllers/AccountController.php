<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\User;
use Auth;
use Hash;

class AccountController extends Controller
{

	/**
   * Check user account details
   *
   * @return User
   */
	public function checkPersonalDetails(Request $request)
	{

		$this->validate($request,[
			'first_name' => 'required|string',
			'last_name' => 'required|string',
			'phone' => 'required|string',
			'email' => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|confirmed|min:8',
			'consent' => 'accepted'
		]);

		return response()->json(['success' => 'success', 200]);

	}

	/**
   * Check employer details
   *
   * @return User
   */
	public function checkEmployerDetails(Request $request){

		$this->validate($request,[
			'name' => 'required|string|min:2',
			'industry' => 'required|not_in:*',
			'description' => 'required'
		]);

		return response()->json(['success' => 'success', 200]);

	}

	/**
   * Check employee details
   *
   * @return User
   */
	public function checkEmployeeDetails(Request $request){

		$this->validate($request,[
			'job' => 'required|not_in:*',
			'job_title' => 'required',
			'salary' => 'required|not_in:*',
			'dob' => 'required',
			'experience' => 'required|not_in:*',
			'working_pattern' => 'required|not_in:*',
			'description' => 'required'
		]);

		return response()->json(['success' => 'success', 200]);

	}

	/**
   * Check user address details
   *
   * @return User
   */
	public function checkAddressDetails(Request $request)
	{

		$this->validate($request,[
      'streetAddress' => 'required|string|max:255',
      'city' => 'required|string|max:255',
      'postalCode' => 'required|string|max:255',
      'country' => 'required|string|max:255'
    ]);

    return response()->json(['success' => 'success', 200]);

	}

	/**
   * Upload an avatar image
   *
   * @return User
   */
	public function avatar(Request $request){
		$this->validate($request,[
        'image' => 'required'
    ]);
    if ($request->hasFile('image') && $request->file('image')->isValid() && $request->file('image')->getClientOriginalName() != "") {
        $user = Auth::user();
        $user->media->each->delete();
        $user->addMediaFromRequest('image')->toMediaCollection('avatars');
    }
    return 'success';
	}


	/**
   * Get a specific account and associated details
   *
   * @return User
   */
	public function specific(){
		$user = User::where('id',Auth::id())->with('employer','employee','addresses')->first();
		return $user;
	}

	/**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
	public function update(Request $request)
	{
		$user = Auth::user();
		if($request->input('email') != $user->email){
			$this->validate($request,[
				'first_name' => 'required|string',
				'last_name' => 'required|string',
				'phone' => 'required|string',
				'email' => 'required|string|email|max:255|unique:users'
			]);
			$updated = array(   
				'first_name' => $request->input('first_name'),
				'last_name' => $request->input('last_name'),
				'phone' => $request->input('phone'),
				'email' => $request->input('email')
			);
			$user->update($updated);
		}else{
			$this->validate($request,[
				'first_name' => 'required|string',
				'last_name' => 'required|string',
				'phone' => 'required|string'
			]);
			$updated = array(   
				'first_name' => $request->input('first_name'),
				'last_name' => $request->input('last_name'),
				'phone' => $request->input('phone'),
			);
			$user->update($updated);
		}
		return back();
	}

	/**
   * Update the users password.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
	public function updatePassword(Request $request){
		$user = Auth::user();
		$this->validate($request,[
			'current_password' => 'required|string',
			'password' => 'required|string|min:6|confirmed'
		]);
		$pwcheck = $this->check($user->password, $request->input('current_password'));
		if($pwcheck == true){
			$updated = array(
				'password' => bcrypt($request->input('password'))
			);
			$user->update($updated);
			return back();
		}
		return back();
	}

	// Function to check password
	private function check($password, $check){
    if(Hash::check($check, $password)) {
        return true;
    } else {
        return false;
    }
  }

}
