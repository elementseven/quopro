<?php

namespace App\Http\Controllers;

use App\Notifications\NewEnquiry;
use App\Notifications\NewEmployee;
use App\Notifications\EmployeeSignup;
use Illuminate\Http\Request;

use App\Models\Attribute;
use App\Models\Employee;
use App\Models\Enquiry;
use App\Models\Address;
use App\Models\Avatar;
use App\Models\User;

use Carbon\Carbon;
use Auth;
use Hash;

class EmployeeController extends Controller
{
    /**
     * Find employees index page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
      return view('employees.index');
    }

    /**
     * Return an array of employees.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAttributes(Request $request){
      $attributes = Attribute::get();
      return $attributes;
    }

    /**
     * Return an array of employees.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEmployees(Request $request){

      $job = $request->input('profession');
      $salary = $request->input('salary');
      $location = $request->input('location');
      $employer = null;
      if(Auth::check()){
        $employer = Auth::user()->employer;
      }
      $employees = Employee::where('approved', 1)->whereHas('job', function($q) use($job){
        if($job != '*'){
          $q->where('slug', $job);
        }
      })
      ->whereHas('user.addresses', function($q) use($location){
        $q->where('city','LIKE','%'.$location.'%');
      })
      ->when($salary != '*', function($q) use($salary){
        $q->where('expected_salary',$salary);
      })
      ->where('status', '!=', 'hide')
      ->with('user.addresses','job')
      ->orderBy('created_at','desc')
      ->paginate($request->input('limit'));
    
      foreach($employees as $e){
        $e->fav = false;
        if($employer != null){
          if($e->favourites()->where('id', $employer->id)->exists()){
            $e->fav = true;
          }else{
            $e->fav = false;
          }
        }
        $e->normal = $e->job->getFirstMediaUrl('jobs','normal');
        $e->webp = $e->job->getFirstMediaUrl('jobs','webp');
      }
      
      return $employees;
    }

    /**
     * Return an array of employees.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAvatars(Request $request){

      $avatars = Avatar::get();
    
      foreach($avatars as $a){
        $a->normal = $a->getFirstMediaUrl('avatar','normal');
        $a->webp = $a->getFirstMediaUrl('avatar','webp');
      }
      
      return $avatars;
    }

    /**
     * Return an array of similar employees.
     *
     * @return \Illuminate\Http\Response
     */
    public function similar(Employee $employee){
      $job = $employee->job->id;
      $employer = null;
      if(Auth::check()){
        $employer = Auth::user()->employer;
      }
      $employees = Employee::whereHas('job', function($q) use($job){
        $q->where('id', $job);
      })
      ->with('user.addresses','job','pic')
      ->where('id', '!=', $employee->id)
      ->where('status', '!=', 'draft')
      ->paginate(3);

      foreach($employees as $e){
        $e->fav = false;
        if($employer != null){
          if($e->favourites()->where('id', $employer->id)->exists()){
            $e->fav = true;
          }else{
            $e->fav = false;
          }
        }
        $e->pic->normal = $e->pic->getFirstMediaUrl('avatar','normal');
        $e->pic->webp = $e->pic->getFirstMediaUrl('avatar','webp');
      }

      return $employees;
    }

    /**
     * Return Favourite Employees as an array
     *
     * @return \Illuminate\Http\Response
     */
    public function favourites(){
      $employer = null;
      if(Auth::check()){
        $employer = Auth::user()->employer;
        $employees = $employer->favourites()->get();
        foreach($employees as $employee){
            $employee->fav = 1;
        }
      }
      return $employees;
    }

    /**
     * Add employee to favourites
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function favourite(Request $request, Employee $employee)
    {
      $employer = null;
      if(Auth::check()){
        $employer = Auth::user()->employer;
        if($employer != null){
          $employer->favourites()->attach($employee);
        }
      }
      return "success";
    }

    /**
     * Remove employee from favourites
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function unfavourite(Request $request, Employee $employee)
    {
      $employer = null;
      if(Auth::check()){
        $employer = Auth::user()->employer;
        if($employer != null){
          $employer->favourites()->detach($employee);
          return "success";
        }else{
          return "failure";
        }
      }
      
    }

    /**
     * Enquire about an employee
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function enquire(Request $request, Employee $employee)
    {
      $employer = null;
      if(Auth::check()){
        $employer = Auth::user()->employer;
        if($employer != null){
          $check = Enquiry::where(['employer_id' => $employer->id, 'employee_id' => $employee->id])->exists();
          if($check == false){
            $enquiry = Enquiry::create([
              'employer_id' => $employer->id,
              'employee_id' => $employee->id,
              'status' => 'New'
            ]);
            $admin = User::find(2);
            $admin->notify(new NewEnquiry($enquiry));
          }
        }
      }
      return "success";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
          'first_name' => 'required|string',
          'last_name' => 'required|string',
          'phone' => 'required|string',
          'email' => 'required|string|email|max:255|unique:users',
          'password' => 'required|string|min:8',
          'employee.job' => 'required',
          'employee.job_title' => 'required',
          'employee.dob' => 'required|not_in:*',
          'employee.description' => 'required',
          'employee.salary' => 'required|not_in:*',
          'employee.experience' => 'required|not_in:*',
          'employee.working_pattern' => 'required|not_in:*',
          'address.streetAddress' => 'required|string|max:255',
          'address.city' => 'required|string|max:255',
          'address.postalCode' => 'required|string|max:255',
          'address.country' => 'required|string|max:255'
      ]);

      $growpro = null;
      if(null !== $request->input('employee.growpro') ){
        $growpro = true;
      }

      Auth::login($user = User::create([
          'first_name' => $request->first_name,
          'last_name' => $request->last_name,
          'phone' => $request->phone,
          'email' => $request->email,
          'role_id' => 3,
          'password' => Hash::make($request->password)
      ]));

      Address::create([
          'streetAddress' => $request->input('address.streetAddress'),
          'extendedAddress' => $request->input('address.extendedAddress'),
          'city' => $request->input('address.city'),
          'region' => $request->input('address.region'),
          'postalCode' => $request->input('address.postalCode'),
          'country' => $request->input('address.country'),
          'type' => 'personal',
          'user_id' => $user->id
      ]);

      $employee = Employee::create([
        'job_id' => $request->input('employee.job'),
        'job_title' => $request->input('employee.job_title'),
        'expected_salary' => $request->input('employee.salary'),
        'experience' => $request->input('employee.experience'),
        'working_pattern' => $request->input('employee.working_pattern'),
        'dob' => Carbon::parse($request->input('employee.dob')),
        'description' => $request->input('employee.description'),
        'status' => 'show',
        'growpro' => $growpro,
        'user_id' => $user->id
      ]);

      if (!empty($request->input('employee.attributes'))) {
        foreach($request->input('employee.attributes') as $a){
          $employee->attributes()->attach($a);
        }
      }

      $admin = User::find(2);
      $admin->notify(new NewEmployee($employee));
      $user->notify(new EmployeeSignup($employee));

      return response()->json(['success' => 'success', 200]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
      $employee->with('user.addresses','job','pic');
      $employer = null;
      if(Auth::check()){
        $employer = Auth::user()->employer;
      }
      $employee->fav = false;
      if($employer != null){
        if($employee->favourites()->where('id', $employer->id)->exists()){
          $employee->fav = true;
        }else{
          $employee->fav = false;
        }
        $check = Enquiry::where(['employer_id' => $employer->id, 'employee_id' => $employee->id])->exists();
        if($check == true){
          $employee->enquired = 1;
        }else{
          $employee->enquired = 0;
        }
      }
      return view('employees.show')->with(['employee' => $employee]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the employees avatar.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function updateAvatar(Request $request)
    {
      $employee = Auth::user()->employee;
      $employee->update([
          'avatar_id' => $request->input('avatar'),
      ]);
      return response()->json(['success' => 'success', 200]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $employee = Auth::user()->employee;
      $employee->update([
          'dob' => Carbon::createFromFormat('Y-m-d', $request->input('date')),
          'job_id' => $request->input('job'),
          'job_title' => $request->input('job_title'),
          'expected_salary' => $request->input('expected_salary'),
          'experience' => $request->input('experience'),
          'working_pattern' => $request->input('working_pattern'),
          'status' => $request->input('status'),
          'description' => $request->input('description'),
      ]);

      if (!empty($request->input('attributes'))) {
        foreach($employee->attributes as $att){
          $employee->attributes()->detach($att);
        }
        $attributes = $request->input('attributes');

        foreach($attributes as $a){
            $employee->attributes()->attach($a);
        }
      }
      return response()->json(['success' => 'success', 200]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }
}
