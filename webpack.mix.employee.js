const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');
require('laravel-mix-purgecss');
const TargetsPlugin = require('targets-webpack-plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.babelConfig({
  plugins: ['@babel/plugin-syntax-dynamic-import']
});

mix.webpackConfig({
	plugins: [
	  new TargetsPlugin({
	    browsers: ['last 2 versions', 'chrome >= 41', 'IE 11'],
	  }),
	],
	output: {
    chunkFilename: 'js/[name].js?id=[chunkhash]',
  }
});

mix.js('resources/js/employees.js', 'public/employees/js').vue()
.sass('resources/sass/employees.scss', 'public/employees/css', {
  implementation: require('node-sass')
})
.purgeCss()
.minify('public/employees/css/employees.css')
.version();

mix.mergeManifest();