<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        'full_name' => "Element Seven",
    		'first_name' => "Element",
    		'last_name' => "Seven",
    		'email' => "hello@elementseven.co",
    		'email_verified_at' => Carbon::now(),
    		'password' => bcrypt('Rubix2018!'),
    		'role_id' => 1,
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
    	]);

    	DB::table('users')->insert([
        'full_name' => "Gerard Marley",
    		'first_name' => "Gerard",
    		'last_name' => "Marley",
    		'email' => "gerard@quoprorecruitment.com",
    		'email_verified_at' => Carbon::now(),
    		'password' => bcrypt('password'),
    		'role_id' => 1,
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
    	]);
    }
}
