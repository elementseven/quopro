<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categories')->insert([
        'title' => 'Other',
        'slug' => 'other',
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
      ]);
    }
}
