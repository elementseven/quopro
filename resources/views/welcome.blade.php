@php
$page = 'Homepage';
$pagetitle = 'QuoPro Recruitment';
$metadescription = 'Providing employers with more autonomy and control over their recruitment processes as hiring procedures are time-consuming, financially burdening and still aren’t guaranteed a successful hire.';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://quoprorecruitment.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="home-header" class="container-fluid full-height position-relative mob-pt-0 page-top">
  <img src="/img/graphics/wave-down.svg" class="menu-bg" alt="QuoPro Recruitment menu background"/>
  <div class="tk-blob home-top-bg d-none d-md-block" style="--time: 80s; --amount: 1; --fill: #56cbb9;">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 440.7 428.7" style="--time: 40s;">
      <defs>
        <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
          <stop offset="0%" style="stop-color:#8DC3FF;stop-opacity:1" />
          <stop offset="55%" style="stop-color:#005ABB;stop-opacity:1" />
          <stop offset="100%" style="stop-color:#3131b8;stop-opacity:1" />
        </linearGradient>
      </defs>
      <path fill="url(#grad1)" d="M410.6 78.8c36 52.5 36.1 126 19.2 194C412.9 340.7 379 403 330 421.9c-49 19-113.1-5.3-178.6-34C85.8 359.2 18.7 326.1 3.5 276.4-11.7 226.7 25 160.3 71.7 105.3 118.3 50.3 174.8 6.8 239 .7c64.1-6 135.7 25.5 171.6 78.1z"></path>
    </svg>
  </div>
  <img src="/img/graphics/mob-top.svg" class="mob-home-top d-md-none"/>
  <img src="/img/graphics/top-circle.svg" class="home-top-circle" alt="QuoPro Recruitment Northern Ireland graphic"/>
  <div class="row position-relative z-2">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 lg-pl-5 mob-px-3 text-center text-lg-left">
          <div class="d-table w-100 full-height">
            <div class="d-table-cell align-middle w-100 full-height">
              <img src="/img/graphics/top-1.svg" alt="QuoPro Recruitment graphic Northern Ireland" class="w-100 d-lg-none mb-4 px-4"/>
              <h1 class="page-top mb-5 ipad-mb-3"> Recruitment that doesn’t feel complicated</h1>
              <div class="d-block">
                  <p class="mob-mb-1 cursor-pointer" ><span id="scrollToEmployees" class="cursor-pointer"><b class="hover-underline hover-underline-primary">Search for an employee</b> <span class="circle-icon pulse animated dark-blue-bg ml-2"><i class="fa fa-arrow-down"></i></span></span></p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 d-none d-lg-block">
          <div class="d-table w-100 full-height">
            <div class="d-table-cell align-middle w-100 full-height" >
              <img src="/img/graphics/top-1.svg" alt="QuoPro Recruitment graphic Northern Ireland" class="w-100"/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div id="homeEmployees" class="container mt-5 pt-5" >
  <div class="row">
    @if($currentUser)
    <employees-search :user="{{$currentUser}}" :amount="6" class="d-none d-lg-block" :loadmorebool="false"></employees-search>
    <employees-search :user="{{$currentUser}}" :amount="3" class="d-lg-none" :loadmorebool="false"></employees-search>
    @else
    <employees-search :user="null" :amount="6" class="d-none d-lg-block" :loadmorebool="false"></employees-search>
    <employees-search :user="null" :amount="3" class="d-lg-none" :loadmorebool="false"></employees-search>
    @endif
  </div>
</div>
<div class="container pb-5 mob-pb-0 mob-px-4 position-relative z-2 text-center text-lg-left" >
  <div class="row mob-py-3">
    <div class="col-lg-6 mb-4 d-lg-none">
      <img data-src="/img/graphics/guiding-employers.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100 lazy"/>
    </div>
    <div class="col-lg-6 mob-mb-5">
      <div class="d-table w-100 h-100">
        <div class="d-table-cell align-middle w-100 h-100" >
          <h3 class="mb-4" >Guiding employers to the right employee</h3>
          <p class="mb-4" >Providing employers with more autonomy and control over their recruitment processes as hiring procedures are time-consuming, financially burdening and still aren’t guaranteed a successful hire.</p>
          <a href="{{route('sign-up')}}">
            <button class="btn btn-primary btn-icon" type="button" >Employer? Sign up <i class="fa fa-arrow-circle-right"></i></button>
          </a>
        </div>
      </div>
    </div>
    <div class="col-lg-6 pl-5 mob-px-3 mob-mt-5 d-none d-lg-block"  >
      <img data-src="/img/graphics/guiding-employers.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100 lazy"/>
    </div>
  </div>
</div>
<div class="container py-5 mob-py-0 position-relative">
  <img data-src="/img/graphics/circle.svg" class="home-middle-circle lazy" alt="QuoPro Recruitment graphic" data-scroll-speed="3" />
  <div class="row py-5 text-center justify-content-center">
    <div class="col-md-4 px-5 mob-px-3 mob-mb-5 ipadp-mb-5 ipadp-px-3">
      <img data-src="/img/graphics/clock.svg" alt="Save time with more control of the process" height="100" class="lazy"/>
      <p class="mt-3 mob-px-5"><b>Save time with more control of the recruitment process</b></p>
    </div>
    <div class="col-md-4 px-5 mob-px-3 mob-mb-5 ipadp-mb-5 ipadp-px-3">
      <img data-src="/img/graphics/money.svg" alt="Save time with more control of the process" height="100" class="lazy"/>
      <p class="mt-3 mob-px-5"><b>More cost effective than traditional recruitment</b></p>
    </div>
    <div class="col-md-4 px-5 mob-px-3 ipadp-px-3">
      <img data-src="/img/graphics/check.svg" alt="Save time with more control of the process" height="100" class="lazy" />
      <p class="mt-3 mob-px-5"><b>More control allows for higher success rates </b></p>
    </div>
  </div>
</div>
<div class="container py-5 mob-pb-0 mob-px-4 position-relative z-2 text-center text-lg-left" >
  <div class="row mob-py-3">
    <div class="col-lg-6 pr-5 mob-px-3 mb-4">
      <img data-src="/img/graphics/looking-for-employment.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100 lazy"/>
    </div>
    <div class="col-lg-6 mob-mb-5">
      <div class="d-table w-100 h-100">
        <div class="d-table-cell align-middle w-100 h-100">
          <h3 class="mb-4">Looking for employment?</h3>
          <p class="mb-2">We will help you gain employment, and will hopefully be able to provide opportunities to everyone. Sign-up and get started today with a free profile!</p>
          <p class="mb-4">School leaver, graduate or someone new to the working world and wanting to gain employment? Check out GrowPro.</p>
          <a href="{{route('sign-up')}}">
            <button class="btn btn-primary btn-icon" type="button">Sign up <i class="fa fa-arrow-circle-right"></i></button>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container py-5 mob-pb-0 mob-px-4 position-relative z-2 mb-5" >
  <div class="row justify-content-center text-center pt-5 mob-py-3">
    <div class="col-lg-8 pr-5 mob-px-3 ipadp-px-5">
      <h3>New to the working world?</h3>
      <p class="mb-5">We have you covered, GrowPro helps nurture and guide our younger less experienced candidates, readying them for the working world.</p>
      <img data-src="/img/logos/growpro.svg" alt="QuoPro GrowPro Recruitment Logo Northern Ireland" height="80" class="d-none d-lg-inline-block lazy" />
      <img data-src="/img/logos/growpro.svg" alt="QuoPro GrowPro Recruitment Logo Northern Ireland" class="w-100 mob-px-5 d-lg-none ipadp-px-5 lazy" />
      <div class="d-block mt-4">
        <a href="{{route('growpro')}}">
          <button class="btn btn-green btn-icon" type="button">Find out more <i class="fa fa-arrow-circle-right"></i></button>
        </a>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
  $("#scrollToEmployees").click(function() {
    $([document.documentElement, document.body]).animate({
        scrollTop: $("#homeEmployees").offset().top -100
    }, 500);
  });
</script>
@endsection