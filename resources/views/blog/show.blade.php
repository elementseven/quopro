@php
$page = 'Blog Post';
$pagetitle = $post->title . ' | QuoPro Recruitment';
$metadescription = $post->excerpt;
$pagetype = 'white';
$pagename = 'home';
$ogimage = 'https://quoprorecruitment.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid py-5 mw-100 py-5 position-relative">
  <img src="/img/graphics/top-circle.svg" class="home-top-circle swell" alt="QuoPro Recruitment Northern Ireland graphic">
  <div class="row">
    <div class="container pt-5">
      <div class="row">
        <div class="col-xl-10">
          <p class="text-light">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $post->created_at)->format('jS M Y')}}</p>
          <h1 class="blog-title">{{$post->title}}</h1>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container pb-5">
  <div class="row">
    <div class="col-xl-10 blog-body">
      {!!$post->body!!}
    </div>
    <div class="col-12 mt-5">
      <p class="mb-1 text-larger"><b>Share this article:</b></p>
      <p class="text-larger">
          <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb">
          <i class="fa fa-facebook"></i>
        </a>
        <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln">
          <i class="fa fa-linkedin ml-2"></i>
        </a>
        <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw">
          <i class="fa fa-twitter ml-2"></i>
        </a>
        <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa">
          <i class="fa fa-whatsapp ml-2"></i>
        </a>
      </p>
    </div>
  </div>
</div>
<div class="container pt-5 mob-px-3 position-relative z-2">
  <div class="row">
    <div class="col-12">
      <h2 class="mb-5">More blog posts</h2>
    </div> 
    @foreach($others as $key => $post)
    <div class="col-12 mb-5">
      <div class="blog-link">
        <a href="{{route('blog-single', ['slug' => $post->slug, 'date' => $post->getDate($post->created_at)])}}" >
          <p class="post-exerpt mb-1 text-light">{{$post->getFancyDate($post->created_at)}}</p>
          <h3 class="post-title text-dark mb-4">{{$post->title}}</h3>
          <p class="post-exerpt text-dark mb-4">{{$post->excerpt}}...</p>
          <p class="mb-0 text-primary pb-5"><b>Read more</b> <i class="fa fa-arrow-circle-right text-primary ml-1 read-more"></i></p>
        </a>  
      </div>
    </div>
    @endforeach
  </div>
</div>
<div class="container mb-5">
  <div class="row">
    <div class="col-12 mob-pt-0">
      <hr class="my-5 mob-my-0"/>
    </div>
  </div>
  <div class="row py-5 mob-mt-5">
    <div class="col-lg-6 d-lg-none mb-4">
      <img src="/img/graphics/hiringadvice.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100"/>
    </div>
    <div class="col-lg-6 mt-5 mob-mt-0 text-center text-lg-left mob-px-4">
      <h3 class="mb-3">Guiding employers to the right employee</h3>
      <p class="mb-4" >Providing employers with more autonomy and control over their recruitment processes as hiring procedures are time-consuming, financially burdening and still aren’t guaranteed a successful hire.</p>
      <a href="{{route('sign-up')}}">
        <button class="btn btn-primary btn-icon" type="button" >Employer? Sign up <i class="fa fa-arrow-circle-right"></i></button>
      </a>
    </div>
    <div class="col-lg-6 d-none d-lg-block pl-5 mt-5 pt-5">
      <img src="/img/graphics/hiringadvice.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100"/>
    </div>
  </div>
</div>
<div class="container py-5 mob-py-0 position-relative">
  <img data-src="/img/graphics/circle.svg" class="home-middle-circle lazy" alt="QuoPro Recruitment graphic" data-scroll-speed="3" />
  <div class="row py-5 text-center justify-content-center">
    <div class="col-md-4 px-5 mob-px-3 mob-mb-5 ipadp-mb-5 ipadp-px-3">
      <img data-src="/img/graphics/clock.svg" alt="Save time with more control of the process" height="100" class="lazy"/>
      <p class="mt-3 mob-px-5"><b>Save time with more control of the recruitment process</b></p>
    </div>
    <div class="col-md-4 px-5 mob-px-3 mob-mb-5 ipadp-mb-5 ipadp-px-3">
      <img data-src="/img/graphics/money.svg" alt="Save time with more control of the process" height="100" class="lazy"/>
      <p class="mt-3 mob-px-5"><b>More cost effective than traditional recruitment</b></p>
    </div>
    <div class="col-md-4 px-5 mob-px-3 ipadp-px-3">
      <img data-src="/img/graphics/check.svg" alt="Save time with more control of the process" height="100" class="lazy" />
      <p class="mt-3 mob-px-5"><b>More control allows for higher success rates </b></p>
    </div>
  </div>
</div>
@endsection