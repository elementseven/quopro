@php
$page = 'Blog';
$pagetitle = 'Blog | QuoPro Recruitment';
$metadescription = 'Read our recruitment blog';
$pagetype = 'white';
$pagename = 'home';
$ogimage = 'https://quoprorecruitment.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid py-5 position-relative">
  <img src="/img/graphics/top-circle.svg" class="home-top-circle swell" alt="QuoPro Recruitment Northern Ireland graphic">
</header>
@endsection
@section('content')
<div class="container">
  <div class="row">
    <blog-index :amount="12" :categories="{{$categories}}"></blog-index>
  </div>
</div>
<div class="container mb-5 pb-5 mob-pb-0 mob-mt-5">
  <div class="row pb-5">
    <div class="col-lg-6 d-lg-none mb-4">
      <img src="/img/graphics/hiringadvice.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100"/>
    </div>
    <div class="col-lg-6 mt-5 mob-mt-0 text-center text-lg-left mob-px-4">
      <h3 class="mb-3">Guiding employers to the right employee</h3>
      <p class="mb-4" >Providing employers with more autonomy and control over their recruitment processes as hiring procedures are time-consuming, financially burdening and still aren’t guaranteed a successful hire.</p>
      <a href="{{route('sign-up')}}">
        <button class="btn btn-primary btn-icon" type="button" >Employer? Sign up <i class="fa fa-arrow-circle-right"></i></button>
      </a>
    </div>
    <div class="col-lg-6 d-none d-lg-block pl-5 mt-5 pt-5">
      <img src="/img/graphics/hiringadvice.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100"/>
    </div>
  </div>
</div>
<div class="container py-5 mob-py-0 position-relative">
  <img data-src="/img/graphics/circle.svg" class="home-middle-circle lazy" alt="QuoPro Recruitment graphic" data-scroll-speed="3" />
  <div class="row py-5 text-center justify-content-center">
    <div class="col-md-4 px-5 mob-px-3 mob-mb-5 ipadp-mb-5 ipadp-px-3">
      <img data-src="/img/graphics/clock.svg" alt="Save time with more control of the process" height="100" class="lazy"/>
      <p class="mt-3 mob-px-5"><b>Save time with more control of the recruitment process</b></p>
    </div>
    <div class="col-md-4 px-5 mob-px-3 mob-mb-5 ipadp-mb-5 ipadp-px-3">
      <img data-src="/img/graphics/money.svg" alt="Save time with more control of the process" height="100" class="lazy"/>
      <p class="mt-3 mob-px-5"><b>More cost effective than traditional recruitment</b></p>
    </div>
    <div class="col-md-4 px-5 mob-px-3 ipadp-px-3">
      <img data-src="/img/graphics/check.svg" alt="Save time with more control of the process" height="100" class="lazy" />
      <p class="mt-3 mob-px-5"><b>More control allows for higher success rates </b></p>
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection