@php
$page = 'Sign up';
$pagetitle = 'Sign up - QuoPro Recruitment';
$metadescription = 'If you are looking for potential employees please select Employer Account, if you are looking for employment please select Employee Account. If you want to sign up for GrowPro sign up for an employee account.';
$pagetype = 'white';
$pagename = 'home';
$ogimage = 'https://quoprorecruitment.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')

@endsection
@section('content')
<img src="/img/graphics/top-circle.svg" class="home-top-circle" alt="QuoPro Recruitment Northern Ireland graphic"/>
<signup-index></signup-index>
@endsection