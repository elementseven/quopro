<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Employees Dashboard</title>
  <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link href="https://use.typekit.net/eqx6nas.css" rel="stylesheet">
</head>
<body>
    <div id="app">
    	<app :user="{{$user}}"></app>
    </div>
    <script>
        window.user = @json($currentUser);
    </script>
    <script src="{{ mix('employees/js/employees.js') }}"></script>
</body>
</html>