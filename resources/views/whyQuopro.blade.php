@php
$page = 'Why QuoPro';
$pagetitle = 'Why QuoPro - QuoPro Recruitment';
$metadescription = 'More importantly, why not?! Self-promotion and exposure all contribute to a successful job search. Companies brand themselves and so should professionals; you are worth just as much.';
$pagetype = 'white';
$pagename = 'home';
$ogimage = 'https://quoprorecruitment.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="whyquopro-header" class="container-fluid position-relative mob-pt-0 pt-5 page-top">
  <img src="/img/graphics/top-circle.svg" class="home-top-circle" alt="QuoPro Recruitment Northern Ireland graphic"/>
  <div class="row position-relative z-2 pt-5">
    <div class="container mt-5 mob-pt-5">
      <div class="row">
        <div class="col-lg-8 mob-px-5 text-center text-lg-left">
          <h1 class="page-top ipad-mb-3">Why QuoPro</h1>
          <p class="mt-4">For jobseekers, self-promotion and exposure all contribute to a successful job search. Companies brand themselves and so should professionals. We highlight your skills and professional attributes without any fear of bias, ensuring a fair and equitable hiring process for everyone. </p>
          <p>Working with us? Unlike anywhere else, you have access to a library of fully-active job seekers<br/><b>NO</b> passive candidates<br/><b>ALL</b> screened and qualified <br/>Just <b>ONE</b> click away</p>
        </div>
        <div class="col-lg-6 py-5 text-center text-lg-left">
          <div class="line"></div>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mb-5 pb-5">
  <div class="row pb-5">
    <div class="col-lg-6 d-lg-none mb-4">
      <img src="/img/graphics/hiringadvice.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100"/>
    </div>
    <div class="col-lg-6 mt-5 mob-mt-0 text-center text-lg-left mob-px-4">
      <h3 class="mb-3">Let us help you</h3>
      <p>We understand job seeking can be a complex, emotional, and time-consuming task, which is where we come in. The perfect decision doesn’t exist, but the perfect opportunity does – let us create it!</p>
      <p>Employers, recruiters, and candidates often exhaust themselves through ineffective and unreliable practices. Rather than focusing on the TIME you spend practicing, focus on the QUALITY of your practice. </p>
      <p class="mb-4">We work with all the above and everyone in between, solely to generate the building blocks for a successful career journey.</p>
      <a href="{{route('sign-up')}}">
        <button class="btn btn-primary btn-icon" type="button">Sign up <i class="fa fa-arrow-circle-right"></i></button>
      </a>
    </div>
    <div class="col-lg-6 d-none d-lg-block pl-5 mt-5 pt-5">
      <img src="/img/graphics/hiringadvice.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100"/>
    </div>
  </div>
  <div class="row">
    <div class="col-12 py-5 mob-py-0">
      <hr class="my-5"/>
    </div>
  </div>
  <div class="row py-5">
    <div class="col-lg-6 pr-5 mob-px-3 pt-5 mob-pt-0 mob-mb-4">
      <img src="/img/graphics/trustedbespokestaffing.svg" alt="QuoPro Recruitment graphic Northern Ireland" class="w-100"/>
    </div>
    <div class="col-lg-6 text-center text-lg-left mob-px-4">
      <h3 class="mb-3">What we do</h3>
      <p> We combine functionality with suitability, untangling the mess and removing the stress. Instantly connect with the future of your business. Simply search for the best and let us do the rest. No confusion, zero ambiguity – all candidates are open to opportunities.</p>
      <p class="mb-4">  Unable to find exactly what you want? Our trusted & tailored technique is personal to you. Employer or candidate, your business is our priority. </p>
      <a href="{{route('sign-up')}}">
        <button class="btn btn-primary btn-icon" type="button">Sign up <i class="fa fa-arrow-circle-right"></i></button>
      </a>
    </div>
  </div>
  <div class="row">
    <div class="col-12 py-5 mob-pt-0">
      <hr class="my-5"/>
    </div>
  </div>
  <div class="row justify-content-center pb-5 mob-pb-0">
    <div class="col-lg-8 text-center">
      <p class="text-larger mb-3"><b>"The success of a company is about the choices it makes…"</b></p>
      <p class="mb-4">The same applies to people. Make the right one.</p>
      <a href="{{route('sign-up')}}">
        <button class="btn btn-green btn-icon" type="button">Sign up <i class="fa fa-arrow-circle-right"></i></button>
      </a>
    </div>
  </div>
</div>
<div class="container py-5 mob-py-0 position-relative">
  <img src="/img/graphics/circle.svg" class="home-middle-circle" alt="QuoPro Recruitment graphic" data-scroll-speed="3" />
  <div class="row py-5 text-center justify-content-center">
    <div class="col-md-4 px-5 mob-px-3 mob-mb-5 ipadp-mb-5 ipadp-px-3">
      <img src="/img/graphics/clock.svg" alt="Save time with more control of the process" height="100"/>
      <p class="mt-3 mob-px-5"><b>Save time with more control of the recruitment process</b></p>
    </div>
    <div class="col-md-4 px-5 mob-px-3 mob-mb-5 ipadp-mb-5 ipadp-px-3">
      <img src="/img/graphics/money.svg" alt="Save time with more control of the process" height="100"/>
      <p class="mt-3 mob-px-5"><b>More cost effective than traditional recruitment</b></p>
    </div>
    <div class="col-md-4 px-5 mob-px-3 ipadp-px-3">
      <img src="/img/graphics/check.svg" alt="Save time with more control of the process" height="100"/>
      <p class="mt-3 mob-px-5"><b>More control allows for higher success rates </b></p>
    </div>
  </div>
</div>
@endsection
@section('scripts')

@endsection