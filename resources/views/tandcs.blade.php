@php
$page = 'Terms & Conditions';
$pagetitle = 'Terms & Conditions - QuoPro Recruitment';
$metadescription = 'Terms & Conditions';
$pagetype = 'white';
$pagename = 'home';
$ogimage = 'https://quoprorecruitment.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative mob-pt-0 pt-5 page-top">
	<img src="/img/graphics/top-circle.svg" class="home-top-circle" alt="QuoPro Recruitment Northern Ireland graphic"/>
	<div class="row position-relative z-2 pt-5">
		<div class="container mt-5 mob-pt-5">
			<div class="row">
				<div class="col-lg-8 lg-pl-5 mob-px-5 text-center text-lg-left">
					<h1 class="page-top ipad-mb-3">Terms & Conditions</h1>
					<p class="text-large mb-0"><strong>QuoPro Recruitment </strong></p>
					<p class="mb-0">whose registered office is at Titanic Suites 55-59 Adelaide St, Belfast BT2 8FE</p>
				</div>
				<div class="col-lg-6 py-5 text-center text-lg-left">
					<div class="line"></div>
				</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mb-5 pb-5">
	<div class="row pb-5">
		<div class="col-12">
			
			<p class="text-large mb-0"><strong>Terms valid as at April 2021</strong></p>
					<p class="text-large mb-0"><strong>Standard Terms of Business - QuoPro Recruitment </strong></p>
					<p class="text-large mb-0"><strong>Introduction of Candidates to Clients for Direct Employment/Engagement</strong></p>
			
			<p class="mt-5"><strong>1. Definitions</strong></p>
			<p>1.1 In these Terms -</p>
			<p>&ldquo;<strong>Client</strong>&rdquo; means the person, firm or corporate body to whom Employment Agency Introduces a Candidate;</p>
			<p>&ldquo;<strong>Candidate</strong>&rdquo; means the person Introduced by Employment Agency to Client including, but not limited to, any officer or employee of the Candidate if the Candidate is a limited company, any member or employee of the Candidate if the Candidate is a limited liability partnership, and members of Employment Agency&rsquo;s own staff;</p>
			<p>&ldquo;<strong>Conduct Regulations</strong>&rdquo; means the Conduct of Employment Agencies and Employment Businesses Regulations 2003 (as amended);</p>
			<p>&ldquo;<strong>Data Controller</strong>&rdquo; means (i) "data controller" in the Data Protection Act 1998 in respect of processing undertaken on or before 24 May 2018; and (b) "controller" in accordance with the General Data Protection Regulation (EU) 2016/679 in respect of processing undertaken on or after 25 May 2018;</p>
			<p>&ldquo;<strong>Data Protection Legislation</strong>&rdquo; means all applicable laws and regulations, as amended or updated from time to time, in the United Kingdom relating to data protection, the processing of personal data and privacy, including without limitation,(a) the Data Protection Act 1998; (b) (with effect from 25 May 2018) the General Data Protection Regulation (EU) 2016/679; (c) the Privacy and Electronic Communications (EC Directive) Regulations 2003 (as may be amended by the proposed Regulation on Privacy and Electronic Communications); and (d) any legislation that replaces or converts into United Kingdom law the General Data Protection Regulation (EU) 2016/679, the proposed Regulation on Privacy and Electronic Communications or any other law relating to data protection, the processing of personal data and privacy resulting from the United Kingdom leaving the European Union;</p>
			<p>&ldquo;<strong>Engagement</strong>&rdquo; means the engagement, employment or use of the Candidate by Client or by any Third Party to whom or to which the Candidate was Introduced by Client (whether with or without Employment Agency&rsquo;s knowledge or consent) on a permanent or temporary basis, whether under a contract of service or for services; under an agency, license, franchise or partnership agreement; or through any other engagement directly or through a limited company of which the Candidate is an officer or employee or through a limited liability partnership of which the Candidate is a member or employee; or indirectly through another company and &ldquo;<strong>Engages</strong>&rdquo; and &ldquo;<strong>Engaged</strong>&rdquo; will be construed accordingly;</p>
			<p>&ldquo;<strong>Introduction</strong>&rdquo; means -</p>
			<p>the passing of a curriculum vit&aelig; or information about the Candidate; or</p>
			<p>the interview of a Candidate in person or by telephone</p>
			<p>and the time of the Introduction will be taken to be the earlier of (a) and (b) above; and &ldquo;Introduced&rdquo; and &ldquo;Introduces&rdquo; will be construed accordingly;</p>
			<p>&ldquo;<strong>Personal Data</strong>&rdquo; means as set out in, and will be interpreted in accordance with Data Protection Legislation;</p>
			<p>&ldquo;<strong>Personal Data Breach</strong>&rdquo; means the accidental or unlawful destruction, loss, alteration, unauthorised disclosure of, or access to, Personal Data transmitted, stored or otherwise Processed in connection with these Terms or which relates to any Candidate;</p>
			<p>&ldquo;<strong>Process</strong>&rdquo; means as set out in, and will be interpreted in accordance with Data Protection Legislation and &ldquo;Processed&rdquo; and &ldquo;Processing&rdquo; will be construed accordingly;</p>
			<p>&ldquo;<strong>Remuneration</strong>&rdquo; includes base salary or fees, guaranteed and/or anticipated bonus and commission earnings, allowances, inducement payments, shift allowances, location weighting and call-out allowances, the benefit of a company car and all other payments or emoluments payable to or receivable by the Candidate for work (or for services where applicable) to be rendered to or on behalf of Client. Where Client provides a company car, a notional amount of &pound;5,000 will be added to the salary in order to calculate Employment Agency&rsquo;s fee;</p>
			<p>&ldquo;<strong>Terms</strong>&rdquo; means these terms of business as further defined within clause 2;</p>
			<p>&ldquo;<strong>Third Party</strong>&rdquo; means any company or person who is not Client. For the avoidance of doubt, subsidiary and associated companies of Client (as defined by s.1159 of the Companies Act 2006 and s.416 of the Income and Corporation Taxes Act 1988 respectively) are included (without limitation) within this definition; and</p>
			<p>&ldquo;<strong>Vacancy</strong>&rdquo; means a specific role/s, work or position that Client requests Employment Agency to submit person for consideration for such role/s work or position.</p>
			<p class="mt-5"><strong>2. These Terms</strong></p>
			<p>2.1 These Terms constitute the entire agreement between Employment Agency and Client in relation to the subject matter hereof and are deemed to be accepted by Client and to apply by virtue of (a) an Introduction to Client of a Candidate; or (b) the Engagement by Client of a Candidate; or (c) the passing of information about the Candidate by Client to any Third Party; or (d) Client&rsquo;s interview or request to interview a Candidate; or (e) Client&rsquo;s signature at the end of these Terms; or (f) any other written expressed acceptance of these Terms. For the avoidance of doubt, these Terms apply whether or not the Candidate is Engaged by Client for the same type of work and/or Vacancy as that for which the Introduction was originally effected.</p>
			<p>2.2 These Terms supersede all previous agreements between the parties in relation to the subject matter hereof.</p>
			<p>2.3 These Terms prevail over any other terms of business or purchase conditions put forward by Client save where expressly agreed otherwise by Employment Agency.</p>
			<p>2.4 Client authorises Employment Agency to act on its behalf in seeking a person to meet Client&rsquo;s requirements and, if Client so requests, shall advertise for such a person through such methods as are agreed with Client and at Client&rsquo;s expense.</p>
			<p>2.5 For the purposes of these Terms, Employment Agency acts as an employment agency as defined within the Conduct Regulations.</p>
			<p class="mt-5"><strong>3. Obligations of Employment Agency</strong></p>
			<p>3.1 Employment Agency shall use reasonable endeavours to introduce at least one suitable person to meet the requirements of Client for each Vacancy. Employment Agency cannot guarantee to find a suitable person for each Vacancy. Without prejudice to clause 3.2 below, Employment Agency shall use reasonable endeavours to ascertain that the information provided by Employment Agency to Client in respect of the Candidate is accurate.</p>
			<p>3.2 Employment Agency accepts no responsibility in respect of matters outside its knowledge and Client must satisfy itself as to the suitability of the Candidate.</p>
			<p class="mt-5"><strong>4. Client Obligations</strong></p>
			<p>4.1 Client shall satisfy itself as to the suitability of the Candidate. Client is responsible for obtaining work permits and/or such other permission to work as may be required, for the arrangement of medical examinations and/or investigations into the medical history of any Candidate, for criminal records and/or background checks and for satisfying other requirements, qualifications or permission required by the law and regulations of the country in which the Candidate is engaged to work.</p>
			<p>4.2 To enable Employment Agency to comply with its obligations under clause 3 Client undertakes to provide to Employment Agency details of the position which Client seeks to fill, including the type of work that the Candidate would be required to do; the location and hours of work; the experience, training, qualifications and any authorisation which Client considers necessary or which are required by law or any professional body for the Candidate to possess in order to work in the position; and any risks to health or safety known to Client and what steps Client has taken to prevent or control such risks.&nbsp;</p>
			<p>4.4 Client agrees that it shall inform Employment Agency of any information it has that suggests it would be detrimental to the interests of either Client or the Candidate for the Candidate to work in the position which Client seeks to fill.</p>
			<p>4.5 Client agrees to provide written notice to Employment Agency within 3 working days where it receives details of a Candidate from Employment Agency which it has already received from (a) another company; or (b) a person; or (c) the Candidate; or (d) any other source including (without limitation) from social media, job boards or advertisements placed by Client. Client further agrees that if no such notice is given by Client to Employment Agency then in the event of an Engagement of the Candidate by Client, howsoever arising, Client agrees to pay Employment Agency a fee in accordance with clause 5.2.</p>
			<p>4.6 Where Client does so notify Employment Agency in accordance with clause 4.5 above and whereupon Client provides evidence to Employment Agency that such receipt of details by Client is in direct relation to the Vacancy, Client will not be liable to pay Employment Agency a fee for that Candidate in respect of the Vacancy. Client acknowledges and agrees that where Client is unable to evidence such or freely admits their possession of the Candidate&rsquo;s details was not in relation to the Vacancy, Client agrees to pay Employment Agency&rsquo;s fee in accordance with clause 5.2.</p>
			<p>4.7 Client agrees to -</p>
			<p>a) notify Employment Agency as soon as possible (and in any event, no later than 7 days from the date of offer or from the date the Engagement takes effect; whichever is earlier) of any offer of an Engagement which it makes to the Candidate; and</p>
			<p>b) notify Employment Agency immediately when its offer of an Engagement to the Candidate has been accepted and to provide details of the Candidate&rsquo;s Remuneration to Employment Agency; and</p>
			<p>c) pay Employment Agency&rsquo;s fee within the period set out under clause 6.2.</p>
			<p>4.8 Client shall not, and shall not seek to cause Employment Agency to, unlawfully discriminate in relation to the services provided by Employment Agency to Client in connection with these Terms and shall disclose any and all information requested by Employment Agency in the event a Candidate makes a complaint to Employment Agency.</p>
			<p>4.9 Client warrants that it shall not, and shall procure that its employees and agents shall not, pass any information concerning a Candidate to any Third Party. Client acknowledges that Introductions of Candidates are confidential and that failure to comply with this clause 4.9 may cause Employment Agency to breach the Conduct Regulations and/or the Data Protection Legislation and accordingly, Client agrees to indemnify Employment Agency from any and all liability in connection with Client&rsquo;s breach of this clause 4.9.</p>
			<p class="mt-5"><strong>5. Charges/Fees</strong></p>
			<p>5.1 Where Client discloses to a Third Party any details regarding a Candidate and that Third Party subsequently Engages the Candidate within 12 months from the date of the Introduction, Client agrees to pay Employment Agency&rsquo;s fee as set out in clause 5.3. There is no entitlement to any rebate or refund to Client or to the Third Party in relation to fees paid in accordance with this clause 5.1.</p>
			<p>5.2 Client agrees to pay Employment Agency a fee calculated in accordance with clause 5.3 where it Engages, whether directly or indirectly, any Candidate within 12 months from the date of Employment Agency&rsquo;s Introduction.</p>
			<p>5.3 The fee will be calculated as a percentage of the Candidate&rsquo;s Remuneration applicable during the first 12 months of the Engagement (as set out in the table below) with percentages based on a Client system search and subsequent enquiry of candidate. Employment Agency will charge VAT on the fee where applicable.</p>
		</div>
		<div class="col-lg-8 py-5">
			<table class="table table-bordered" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td>
							<p class="mb-0"><strong>Candidate&rsquo;s Placement Remuneration</strong></p>
						</td>
						<td>
							<p class="mb-0"><strong>Percentage payable as the Fee</strong></p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="mb-0">GrowPro (school leavers/Students)</p>
						</td>
						<td>
							<p class="mb-0">8%</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="mb-0">QuoPro professionals &pound;20,000 to &pound;34,999</p>
						</td>
						<td>
							<p class="mb-0">10%</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="mb-0">QuoPro Professionals &pound;35,000 to &pound;49,999</p>
						</td>
						<td>
							<p class="mb-0">12%</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="mb-0">QuoPro Professionals &pound;50,000 to &pound;74,999</p>
						</td>
						<td>
							<p class="mb-0">14%</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="mb-0">Bespoke, tailored search</p>
						</td>
						<td>
							<p class="mb-0">Flexible upon agreement</p>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-12">
			<p>5.4 Where the amount of the actual Remuneration is not known or disclosed, Employment Agency will charge a fee calculated in accordance with clause 5.3 on the maximum level of remuneration applicable &ndash;</p>
			<p>a) for the Vacancy; or</p>
			<p>b) for the type of position the Candidate had been originally submitted by Employment Agency to Client for; or</p>
			<p>c) for a comparable position in the general marketplace.</p>
			<p>5.5 Where the Engagement is for a fixed term of less than 12 months, the fee in clause 5.3 will apply pro-rata. Where the Engagement is extended beyond the initial fixed term or where Client re-Engages the Candidate within 6 months from the date of planned or actual termination (as applicable) of the first Engagement, Client shall pay a further fee based on the Remuneration applicable for the period of Engagement following the initial fixed term period up to the termination of the second Engagement calculated in accordance with clause 5.3.</p>
			<p>5.6 Where Client withdraws an offer of an Engagement made to the Candidate, Client agrees to pay Employment Agency a minimum fee of 5 % of the Remuneration for the services provided by Employment Agency prior to Client&rsquo;s withdrawal. Client further agrees to indemnify and hold harmless Employment Agency from any all liability in connection with Client&rsquo;s withdrawal of such an offer.</p>
			<p>5.7 Charges incurred by Employment Agency at Client&rsquo;s written request in respect of advertising or any other matters will be charged to Client in addition to the fee and such charges will be payable whether or not the Candidate is Engaged.</p>
			<p>5.8 Client acknowledges that it has no right to set-off, withhold or deduct monies from sums due to Employment Agency under or in connection with these Terms.</p>
			<p class="mt-5"><strong>6. Invoices</strong></p>
			<p>6.1 Except in the circumstances set out in clause 5.1, 5.6 and 5.7 no fee is incurred by Client until the Candidate commences the Engagement; whereupon Employment Agency will render an invoice to Client for its fees.</p>
			<p>6.2 Employment Agency shall raise invoices in respect of the charges payable and Client agrees to pay the amount due within 30 days of the date of the invoice.</p>
			<p>6.3 All invoices will be deemed to be accepted in full by Client in accordance with the payment terms stated within clause 6.2 unless Client notifies Employment Agency, in writing within 5 days of receiving the invoice, stating the amount Client disputes and the reason Client disputes that amount. In the event Client does so notify Employment Agency that it wishes to dispute part of an invoice, Client agrees to pay the undisputed part of the invoice within the agreed payment terms and shall co-operate fully with Employment Agency to resolve the dispute as quickly as possible.</p>
			<p class="mt-5"><strong>7. Rebates</strong></p>
			<p>7.1 Where Client qualifies for a rebate in accordance with clause 7.2, and the employment of the Candidate is terminated by Client or by the Candidate within the time periods specified below, Client will be entitled to a rebate of the introduction fee as follows &ndash;</p>
		</div>
		<div class="col-lg-8 py-5">
			<table class="table table-bordered" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td>
							<p class="mb-0"><strong>Duration of Employment</strong></p>
						</td>
						<td>
							<p class="mb-0"><strong>Percentage payable of fee to be rebated</strong></p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="mb-0">Less than 1 week</p>
						</td>
						<td>
							<p class="mb-0">100%</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="mb-0">1 week to less than 2 weeks</p>
						</td>
						<td>
							<p class="mb-0">90%</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="mb-0">2 weeks to less than 3 weeks</p>
						</td>
						<td>
							<p class="mb-0">80%</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="mb-0">3 weeks to less than 4 weeks</p>
						</td>
						<td>
							<p class="mb-0">70%</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="mb-0">4 weeks to less than 5 weeks</p>
						</td>
						<td>
							<p class="mb-0">60%</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="mb-0">5 weeks to less than 6 weeks</p>
						</td>
						<td>
							<p class="mb-0">50%</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="mb-0">6 weeks to less than 7 weeks</p>
						</td>
						<td>
							<p class="mb-0">40%</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="mb-0">7 weeks to less than 8 weeks</p>
						</td>
						<td>
							<p class="mb-0">30%</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="mb-0">8 weeks to less than 9 weeks</p>
						</td>
						<td>
							<p class="mb-0">20%</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="mb-0">9 weeks to less than 10 weeks</p>
						</td>
						<td>
							<p class="mb-0">10%</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="mb-0">10 weeks or more</p>
						</td>
						<td>
							<p class="mb-0">nil</p>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-12">
			<p>7.2 The following conditions must be met for Client to qualify for a rebate -</p>
			<p>a) Client must notify Employment Agency that the Candidate&rsquo;s employment has ended within 7 days of the employment ending or within 7 days of notice being given to end the employment (whichever is earlier) together with a reason for the premature end of the employment;</p>
			<p>b) Employment Agency&rsquo;s invoice for the fee must have been paid within the payment terms in accordance with clause 6.2;</p>
			<p>c) the Candidate&rsquo;s employment is not terminated by reason of redundancy or re-organisation or change in strategy of Client;</p>
			<p>d) the Candidate&rsquo;s employment is not terminated by reason of poor performance prior to the completion of any induction or training period;</p>
			<p>e) if the Candidate&rsquo;s employment is terminated by reason of misconduct, rebate is only due where such misconduct was reasonably foreseeable by Employment Agency;</p>
			<p>f) the Candidate did not leave the employment because he/she reasonably believed that the nature of the actual work was substantially different from the information Client provided prior to the Candidate&rsquo;s acceptance of the employment;</p>
			<p>g) the Candidate did not leave the employment as a result of discrimination or other acts against the Candidate; and</p>
			<p>h) the Candidate was not at any time in the 12 months prior to the start of the employment employed or hired (whether on a permanent or contract basis, directly or indirectly) by Client.</p>
			<p>7.3 Where Client re-engages the Candidate on an employment, worker or indirect (via a Third Party) basis, Client agrees that any rebate paid to Client under clause 7.1 in respect of that Candidate, will be immediately repaid to Employment Agency by Client.</p>
			<p class="mt-5"><strong>8. Liability and Indemnity</strong></p>
			<p>8.1 Employment Agency shall use reasonable endeavours to ensure Candidate has the required standard of skill, experience and necessary qualifications as stated in the Vacancy; nevertheless, Employment Agency is not liable for any loss, expense, damage or delay arising from and in connection with any failure on the part of Employment Agency or of Candidate to evidence such to Client nor for any negligence whether wilful or otherwise, dishonesty, fraud, acts or omissions, misconduct or lack of skill, experience or qualifications of Candidate.</p>
			<p>8.2 Employment Agency is not liable for any indirect or consequential losses or damage including but not limited to; loss of profits, revenue, goodwill, anticipated savings or for claims by third parties arising out of Employment Agency's performance or failure to perform any of its obligations in these Terms.</p>
			<p>8.3 Notwithstanding clause 8.2 above, nothing in these Terms will be deemed to exclude or restrict any liability of Employment Agency to Client for personal injury, death or fraud directly caused by Employment Agency.</p>
			<p>8.4 Employment Agency shall not be liable for failure to perform its obligations under these Terms if such failure results by reason of any cause beyond its reasonable control.</p>
			<p>8.5 Client will indemnify and keep indemnified Employment Agency against any costs (including legal costs), claims or liabilities incurred directly or indirectly by Employment Agency arising out of or in connection with these Terms including (without limitation) as a result of -</p>
			<p>a) any breach of these Terms by Client or by its employees or agents;&nbsp;</p>
			<p>b) any breach by Client or by Third Party, or any of its employees or agents, of any applicable statutory provisions (including, without limitation, any statutory provisions prohibiting or restricting discrimination or other inequality of opportunity, immigration legislation, the Conduct Regulations and Data Protection Legislation); or</p>
			<p>c) any unauthorised disclosure of a Candidate details by Client or by Third Party, or any of its employees or agents.</p>
			<p>8.6 Save as required by law, the sole aggregate liability of Employment Agency arising out of or in connection with these Terms is limited to &pound;10,000.00.</p>
			<p class="mt-5"><strong>9. Termination</strong></p>
			<p>9.1 These Terms may be terminated by either party by giving to the other immediate notice in the event that either Employment Agency or Client goes into liquidation, becomes bankrupt or enters into an arrangement with creditors or has a receiver or administrator appointed or where Employment Agency has reasonable grounds to believe Client will not pay Employment Agency&rsquo;s invoice within the payment terms agreed within clause 6.2.</p>
			<p>9.2 These Terms may be terminated by either party for convenience by serving 3 months notice in writing.</p>
			<p>9.3 Without prejudice to any rights accrued prior to termination, the obligations within clauses 1, 4.9, 5, 6, 7.3, 8, 11, 12, 13, 14, 15 and 16 will remain in force beyond the cessation or other termination (howsoever arising) of these Terms.</p>
			<p class="mt-5"><strong>10. Equal Opportunities</strong></p>
			<p>10.1 Employment Agency is committed to equal opportunities and expects Client to comply with all anti-discrimination legislation as regards the selection and treatment of Candidates.</p>
			<p class="mt-5"><strong>11. Confidentiality</strong></p>
			<p>11.1 All information contained within these Terms will remain confidential and Client shall not divulge it to any Third Party save for its own employees and professional advisers and as may be required by law.</p>
			<p>11.2 Client shall not without the prior written consent of Employment Agency provide any information in respect of a Candidate to any Third Party whether for employment purposes or otherwise.</p>
			<p class="mt-5"><strong>12. Data Protection</strong></p>
			<p>12.1 For the purposes of this clause 12 "Data Subject" means as set out in, and will be interpreted in accordance with Data Protection Legislation. For the avoidance of doubt, Data Subject includes Candidate.</p>
			<p>12.2 The parties hereto acknowledge that Employment Agency is a Data Controller in respect of the Personal Data of Candidate and provides such Personal Data to Client in accordance with the Data Protection Legislation for the purposes anticipated by these Terms.</p>
			<p>12.3 The parties hereto acknowledge that Client is a Data Controller but the parties hereto are not Joint Controllers (as defined within Data Protection Legislation) save where a specific agreement is made to that effect between the parties hereto.</p>
			<p>12.4 The parties hereto warrant to each other that any Personal Data relating to a Data Subject, whether provided by Client, Employment Agency or by Candidate, will be used, Processed and recorded by the receiving party in accordance with Data Protection Legislation.</p>
			<p>12.5 The parties hereto will take appropriate technical and organisational measures to adequately protect all Personal Data against accidental loss, destruction or damage, alteration or disclosure.</p>
			<p>12.6 Client will -</p>
			<p>a) comply with the instruction of the Employment Agency as regards the transfer/sharing of data between the parties hereto. If Client requires Personal Data not already in its control to be provided by Employment Agency, Client will set out their legal basis for the request of such data and accept that Employment Agency may refuse to share/transfer such Personal Data where, in the reasonable opinion of Employment Agency, it does not comply with its obligations in accordance with Data Protection Legislation;&nbsp;</p>
			<p>b) not cause Employment Agency to breach any of their obligations under the Data Protection Legislation.</p>
			<p>12.7 In the event Client becomes aware of an actual or any reasonably suspected Personal Data Breach, it will immediately notify Employment Agency and will provide Employment Agency with a description of the Personal Data Breach, the categories of data that was the subject of the Personal Data Breach and the identity of each Data Subject affected and any other information the Employment Agency reasonably requests relating to the Personal Data Breach.</p>
			<p>12.8 In the event of a Personal Data Breach, Client will promptly (at its own expense) provide such information, assistance and cooperation and do such things as Employment Agency may request to -</p>
			<p>a) investigate and defend any claim or regulatory investigation;</p>
			<p>b) mitigate, remedy and/or rectify such breach; and</p>
			<p>c) prevent future breaches.</p>
			<p>and will provide Employment Agency with details in writing of all such steps taken.</p>
			<p>12.9 Client will not release or publish any filing, communication, notice, press release or report concerning any Personal Data Breach without the prior written approval of Employment Agency.</p>
			<p>12.10 Client agrees it will only Process Personal Data of Candidate for the agreed purpose that is introduction for a Vacancy pursuant to these Terms.</p>
			<p>12.11 Client will provide evidence of compliance with clause 12 upon request from Employment Agency.</p>
			<p>12.12 Client will indemnify and keep indemnified Employment Agency against any costs, claims or liabilities incurred directly or indirectly by Employment Agency arising out of or in connection with any failure to comply with clause 12.</p>
			<p class="mt-5"><strong>13. General</strong></p>
			<p>13.1 Any failure by the Employment Agency to enforce at any particular time any one or more of these Terms will not be deemed a waiver of such rights or of the right to enforce these Terms subsequently.</p>
			<p>13.2 Headings contained in these Terms are for reference purposes only and will not affect the intended meanings of the clauses to which they relate.</p>
			<p>13.3 No provision of these Terms will be enforceable by any person who is not a party to it pursuant to the Contract (Rights of Third Parties) Act 1999.</p>
			<p>13.4 If any provision, clause or part-clause of these Terms is held to be invalid, void, illegal or otherwise unenforceable by any judicial body, the remaining provisions of these Terms will remain in full force and effect to the extent permitted by law.</p>
			<p>13.5 Any reference to legislation, statute, act or regulation will include any revisions, re-enactments or amendments that may be made from time to time.</p>
			<p class="mt-5"><strong>14. Notices</strong></p>
			<p>14.1 Any notice required to be given under these Terms (including the delivery of any information or invoice) will be delivered by hand, sent by facsimile, e-mail or prepaid first class post to the recipient at its fax number or address specified in these Terms (or as otherwise notified from time to time to the sender by the recipient for the purposes of these Terms).</p>
			<p>14.2 Notices will be deemed to have been given and served -</p>
			<p>a) if delivered by hand, at the time of delivery if delivered before 5.00pm on a business day or in any other case at 10.00am on the next business day after the day of delivery; or&nbsp;</p>
			<p>b) if sent by facsimile or e-mail, at the time of despatch if despatched on a business day before 5.00 p.m. or in any other case at 10.00 a.m. on the next business day after the day of despatch, unless the transmission report indicates a faulty or incomplete transmission or, within the relevant business day, the recipient informs the sender that the facsimile or e-mail message was received in an incomplete or illegible form; or</p>
			<p>c) if sent by prepaid first class post, 48 hours from the time of posting.</p>
			<p class="mt-5"><strong>15. Variation</strong></p>
			<p>15.1 No variation or alteration of these Terms will be valid unless approved in writing by Client and Employment Agency.</p>
			<p class="mt-5"><strong>16. Applicable Law</strong></p>
			<p>16.1 These Terms will be construed in accordance with UK Law and the parties submit to the exclusive jurisdiction of the Courts of Northern Ireland</p>
		</div>
	</div>
</div>
@endsection
@section('scripts')

@endsection