@php
$page = 'Find Employees';
$pagetitle = 'Find Employees | QuoPro Recruitment';
$metadescription = 'Find employees with more autonomy and control over the recruitment processes. Hiring procedures are time-consuming, financially burdening and still aren’t guaranteed a successful hire.';
$pagetype = 'white';
$pagename = 'home';
$ogimage = 'https://quoprorecruitment.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid mt-5 pt-5">
  <img src="/img/graphics/top-circle.svg" class="home-top-circle" alt="QuoPro Recruitment Northern Ireland graphic">
  <div class="row position-relative z-2 pt-5">
    <div class="container">
      <div class="row">
        <div class="col-12 mb-4">
          <h1>Find Employees</h1>
          <p class="mb-0 text-small">If your current search is limited, don’t hesitate to <a href="/contact">contact us</a> directly.</p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container" >
  <div class="row">
    @if($currentUser)
    <employees-search :user="{{$currentUser}}" :amount="12" :loadmorebool="true"></employees-search>
    @else
    <employees-search :user="null" :amount="12" :loadmorebool="true"></employees-search>
    @endif
  </div>
</div>
<div class="container mb-5 pb-5 mob-pb-0 mob-mt-5">
  <div class="row pb-5">
    <div class="col-lg-6 d-lg-none mb-4">
      <img src="/img/graphics/hiringadvice.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100"/>
    </div>
    <div class="col-lg-6 mt-5 mob-mt-0 text-center text-lg-left mob-px-4">
      <h3 class="mb-3">Guiding employers to the right employee</h3>
      <p class="mb-4" >Providing employers with more autonomy and control over their recruitment processes as hiring procedures are time-consuming, financially burdening and still aren’t guaranteed a successful hire.</p>
      <a href="{{route('sign-up')}}">
        <button class="btn btn-primary btn-icon" type="button" >Employer? Sign up <i class="fa fa-arrow-circle-right"></i></button>
      </a>
    </div>
    <div class="col-lg-6 d-none d-lg-block pl-5 mt-5 pt-5">
      <img src="/img/graphics/hiringadvice.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100"/>
    </div>
  </div>
</div>
<div class="container py-5 mob-pt-0 mob-px-5">
  <div class="row py-5 text-center mob-px-5">
    <div class="col-lg-4 px-5 mob-px-3 mob-mb-5">
      <img src="/img/graphics/clock.svg" alt="Save time with more control of the process" height="100"/>
      <p class="mt-3"><b>Save time with more control of the recruitment process</b></p>
    </div>
    <div class="col-lg-4 px-5 mob-px-3 mob-mb-5">
      <img src="/img/graphics/money.svg" alt="Save time with more control of the process" height="100"/>
      <p class="mt-3"><b>More cost effective than traditional recruitment</b></p>
    </div>
    <div class="col-lg-4 px-5 mob-px-3">
      <img src="/img/graphics/check.svg" alt="Save time with more control of the process" height="100"/>
      <p class="mt-3"><b>More control allows for higher success rates </b></p>
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection