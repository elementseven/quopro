@php
$page = 'Profile';
$pagetitle = $employee->job->title . ' - ' . $employee->user->addresses[0]->city . ' | QuoPro Recruitment';
$metadescription = 'Find candidaties with experience as a ' . $employee->job->title . ' in ' . $employee->user->addresses[0]->city . ' using the QuoPro platform.';
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://quoprorecruitment.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="position-relative mob-pt-0 page-top">
	<div class="profile-top">
    <div class="container">
      <div class="row position-relative z-2">
        <div class="container">
          <div class="row">
            <div class="col-12 lg-pl-5 mob-px-3 text-left">
            	<div class="profile-title">
            		<div class="avatar bg-white">
    							<picture> 
    							  <source srcset="{{$employee->job->getFirstMediaUrl('jobs','webp')}}" type="image/webp"/> 
    							  <source srcset="{{$employee->job->getFirstMediaUrl('jobs','normal')}}" type="image/jpeg"/> 
    							  <img srcset="{{$employee->job->getFirstMediaUrl('jobs','normal')}}" type="image/jpeg" alt="{{$employee->job->title}}  -  {{$employee->user->addresses[0]->city}}"/>
    							</picture>
    						</div>
    						<div class="job-title">
    							<div class="row">
    								<div class="col-lg-8">
                      <p class="mb-0 text-white text-uppercase letter-spacing text-small">{{$employee->job->title}}</p>
              				<h1 class="mb-4 mob-mb-3 ipad-mb-3">{{$employee->job_title}}</h1>
              			</div>
              			<div class="col-lg-4">
              				<employee-toggle-favourite :employee="{{$employee}}" :user="{{$currentUser}}" :refresh="true" type="white-text"></employee-toggle-favourite>
              			</div>
              		</div>
              	</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mt-5">
  <div class="row pt-4 mob-pt-0">
    <div class="col-xl-10">
      <div class="row half_row"> 
      @foreach($employee->attributes as $att)
        <div class="col-lg-3 col-6 half_col">
          <p class="text-small d-md-inline mr-4 mb-0 " style="width:inherit;">
            <picture> 
              <source srcset="{{$att->getFirstMediaUrl('attributes','webp')}}" type="image/webp"/> 
              <source srcset="{{$att->getFirstMediaUrl('attributes','normal')}}" type="image/jpeg"/> 
              <img srcset="{{$att->getFirstMediaUrl('attributes','normal')}}" type="image/jpeg" width="40" height="40" alt="{{$att->title}}" class="d-inline" />
            </picture>
            <b>{{$att->title}}</b>
          </p>
        </div>
      @endforeach
      </div>
    </div>
  </div>
</div>
<employee-profile :employee="{{$employee}}" :user="{{$currentUser}}"></employee-profile>
<div class="container pb-5 mob-px-4 position-relative z-2" >
  <img src="/img/graphics/circle.svg" class="home-middle-circle" alt="QuoPro Recruitment graphic"  data-scroll-speed="3"/>
  <div class="row mob-py-3">
    <div class="col-lg-6">
      <div class="d-table w-100 h-100">
        <div class="d-table-cell align-middle w-100 h-100">
          <h3 class="mb-4">Guiding employers to the right employee</h3>
          <p class="mb-4">Providing employers with more autonomy and control over their recruitment processes as hiring procedures are time-consuming, financially burdening and still aren’t guaranteed a successful hire.</p>
          <a href="">
            <button class="btn btn-primary btn-icon" type="button">Employer? Sign up <i class="fa fa-arrow-circle-right"></i></button>
          </a>
        </div>
      </div>
    </div>
    <div class="col-lg-6 pl-5 mob-px-3">
      <img src="/img/graphics/guiding-employers.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100"/>
    </div>
  </div>
</div>
<div class="container py-5">
  <div class="row py-5 text-center">
    <div class="col-lg-4 px-5 mob-px-3">
      <img src="/img/graphics/clock.svg" alt="Save time with more control of the process" height="100"/>
      <p class="mt-3"><b>Save time with more control of the recruitment process</b></p>
    </div>
    <div class="col-lg-4 px-5 mob-px-3">
      <img src="/img/graphics/money.svg" alt="Save time with more control of the process" height="100"/>
      <p class="mt-3"><b>More cost effective than traditional recruitment</b></p>
    </div>
    <div class="col-lg-4 px-5 mob-px-3">
      <img src="/img/graphics/check.svg" alt="Save time with more control of the process" height="100"/>
      <p class="mt-3"><b>More control allows for higher success rates </b></p>
    </div>
  </div>
</div>
@endsection