
@php
$page = 'Login';
$pagetitle = 'Reset your password | QuoPro Recruitment';
$metadescription = 'Reset your password';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://quoprorecruitment.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="home-header" class="container-fluid full-height position-relative mob-pt-0 page-top mob-height-auto">
  <img src="/img/graphics/wave-down.svg" class="menu-bg" alt="QuoPro Recruitment menu background"/>
  <div class="tk-blob home-top-bg" style="--time: 80s; --amount: 1; --fill: #56cbb9;">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 440.7 428.7" style="--time: 40s;">
      <defs>
        <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
          <stop offset="0%" style="stop-color:#8DC3FF;stop-opacity:1" />
          <stop offset="55%" style="stop-color:#005ABB;stop-opacity:1" />
          <stop offset="100%" style="stop-color:#3131b8;stop-opacity:1" />
      </linearGradient>
  </defs>
  <path fill="url(#grad1)" d="M410.6 78.8c36 52.5 36.1 126 19.2 194C412.9 340.7 379 403 330 421.9c-49 19-113.1-5.3-178.6-34C85.8 359.2 18.7 326.1 3.5 276.4-11.7 226.7 25 160.3 71.7 105.3 118.3 50.3 174.8 6.8 239 .7c64.1-6 135.7 25.5 171.6 78.1z"></path>
</svg>
</div>
<img src="/img/graphics/top-circle.svg" class="home-top-circle" alt="QuoPro Recruitment Northern Ireland graphic">
<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo"></x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />
        <div class="container py-5 mt-5 mob-mt-0">
            <div class="row py-5 justify-content-center">
                <div class="col-lg-5">
                    <div class="card border-0 shadow px-5 mob-px-4 py-5 text-center">
                        <h1 class="mb-4">Reset your password</h1>
                        <form method="POST" action="{{ route('password.update') }}">
                            @csrf

                            <!-- Password Reset Token -->
                            <input type="hidden" name="token" value="{{ $request->route('token') }}">


                            <!-- Email Address -->
                            <div class="text-left">
                                <b><x-label for="email" class="text-left" :value="__('Email Address')" /></b>

                                <x-input id="email" class="form-control mt-1 w-full" type="email" name="email" :value="old('email', $request->email)" required autofocus placeholder="Email Address"/>
                            </div>

                            <!-- Password -->
                            <div class="text-left mt-4">
                                <b><x-label for="password" :value="__('New Password')" /></b>

                                <x-input id="password" class="form-control mt-1 w-full" type="password" name="password" required placeholder="New Password" />
                            </div>

                            <!-- Confirm Password -->
                            <div class="text-left mt-4">
                                <b><x-label for="password_confirmation" :value="__('Confirm New Password')" /></b>

                                <x-input id="password_confirmation" class="form-control mt-1 w-full" type="password" name="password_confirmation" required placeholder="Confirm New Password" />
                            </div>

                            <div class="flex items-center justify-end mt-4">
                                <x-button class="btn btn-primary mb-2">
                                    {{ __('Reset Password') }}
                                </x-button>
                                <!-- Session Status -->
                                <x-auth-session-status class="mb-4" :status="session('status')" />

                                <!-- Validation Errors -->
                                <x-auth-validation-errors class="mb-4" :errors="$errors" />
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </x-auth-card>
</x-guest-layout>

</header>
@endsection