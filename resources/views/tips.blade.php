@php
$page = 'QuoPro Tips';
$pagetitle = 'QuoPro Tips Profile description - QuoPro Recruitment';
$metadescription = 'QuoPro Tips';
$pagetype = 'white';
$pagename = 'home';
$ogimage = 'https://quoprorecruitment.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative mob-pt-0 pt-5 page-top">
	<img src="/img/graphics/top-circle.svg" class="home-top-circle" alt="QuoPro Recruitment Northern Ireland graphic"/>
	<div class="row position-relative z-2 pt-5">
		<div class="container mt-5 mob-pt-5">
			<div class="row">
				<div class="col-lg-8 lg-pl-5 mob-px-5 text-center text-lg-left">
					<h1 class="page-top ipad-mb-3 mb-3">QuoPro Tips</h1>
					<p class="text-large"><b>Writing your Profile description</b></p>
				</div>
				<div class="col-lg-6 py-5 text-center text-lg-left">
					<div class="line"></div>
				</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mb-5 pb-5">
	<div class="row pb-5">
		<div class="col-12">
			<ul class="mb-5">
				<li><p>The hardest request but most important – tell us about yourself! This is your first point of contact, so make an impact! Cover who you are, what you can offer and what your career goals are. </p></li>
				<li><p>Your personal brand – sell yourself! What makes you different from other candidates? What is your USP? (e.g do you have any blogs? Any work published?)</p></li>
				<li><p>List Educational achievements/qualifications and year</p></li>
				<li><p>Previous employment History </p></li>
				<li><p>Detail 4 or 5 key skills or abilities (communication, teamwork, leadership)</p></li>
				<li><p>Hobbies & Interests (especially GrowPro) – volunteering, photography, travel, activism, events..</p></li>
			</ul>
			<p>Explain why you’re potentially a great candidate, show your strengths.</p>
			<p class="mb-5">Promote yourself and be assertive. You want to catch their eye and stand out!</p>
			<a href="{{route('contact')}}">
				<button type="button" class="btn btn-primary">Contact Us</button>
		</div>
	</div>
</div>
@endsection
@section('scripts')

@endsection