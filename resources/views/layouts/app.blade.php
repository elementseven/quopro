<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="/img/favicon/site.webmanifest">
  <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="/img/favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <title>{{$pagetitle}}</title>

  <meta property="og:type" content="website">
  <meta property="og:title" content="{{$pagetitle}}">
  <meta property="og:url" content="{{Request::url()}}">
  <meta property="og:site_name" content="QuoPro">
  <meta property="og:locale" content="en_GB">
  <meta property="og:image" content="{{$ogimage}}">
  <meta property="og:description" content="{{$metadescription}}">
  <meta name="description" content="{{$metadescription}}">
  <link href="{{ mix('css/app.min.css') }}" rel="stylesheet"/>
  @yield('styles')
  <script type="application/ld+json">
   {
    "@context" : "https://schema.org",
    "@type" : "Organization",       
    "telephone": "+442895031307",
    "contactType": "Customer service"
  }
</script>
<script>window.dataLayer = window.dataLayer || [];</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5BGKSKR');</script>
<!-- End Google Tag Manager -->
</head>
<body class="front">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BGKSKR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  @yield('fbroot')
  <div id="main-wrapper">
    <div id="app" class="front {{$pagetype}} overflow-hidden mw-100" data-scroll-container>
      <div id="menu_btn" class="menu_btn float-left d-lg-none"><div class="nav-icon"><span></span><span></span><span></span></div></div>
      <div id="main-menu" class="menu">
        <div class="container-fluid px-5 ipad-px-3 mob-px-3">
          <div class="row">
            <div class="col-lg-2 py-3">
              <a href="/">
                @if($pagetype == 'light' || $pagetype == 'white')
                @if($page == 'Homepage')
                <img src="/img/logos/logo.svg" class="menu_logo d-none d-md-block" alt="QuoPro Logo" width="98" />
                <img src="/img/logos/logo-white.svg" class="menu_logo d-md-none" alt="QuoPro Logo" width="98" />
                @else
                <img src="/img/logos/logo.svg" class="menu_logo" alt="QuoPro Logo" width="98" />
                @endif
                @else
                <img src="/img/logos/logo-white.svg" class="menu_logo" alt="QuoPro Logo" width="98" />
                @endif
              </a>
            </div>
            @if($page != '404')
            <div class="col-lg-10 text-right d-none d-lg-block pt-3 mt-3">
              <div class="menu-links d-inline-block">
                <a href="{{route('find-employees')}}" class="menu-item cursor-pointer">Find Employees</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('why-quopro')}}" class="menu-item cursor-pointer">Why QuoPro?</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('services')}}" class="menu-item cursor-pointer">Services</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('blog')}}" class="menu-item cursor-pointer">Blog</a>
              </div>
              <div class="menu-links d-inline-block">
                <a href="{{route('contact')}}" class="menu-item cursor-pointer">Contact</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('growpro')}}" class="menu-item text-green cursor-pointer">GrowPro</a>
              </div>
              @if(Auth::check())
              <div class="menu-links d-inline-block ">
                <a href="{{route('login')}}" class="menu-item cursor-pointer ml-3"><i class="fa fa-user mr-2"></i> Dashboard</a>
              </div>
              @else
              <div class="menu-links d-inline-block ">
                <a href="{{route('login')}}" class="menu-item cursor-pointer">Login</a>
              </div>
              @endif
            </div>
            @endif
          </div>
        </div>
      </div>
      <div id="scroll-menu" class="menu d-none d-lg-block">
        <div class="container-fluid px-5 ipad-px-3 ">
          <div class="row">
            <div class="col-4 col-lg-2 pb-3 pt-2">
              <a href="/">
                <img src="/img/logos/logo.svg" class="menu_logo" alt="QuoPro Logo"/>
              </a>
            </div>
            <div class="col-8 col-lg-10 text-right pt-4 ipad-pt-3 d-none d-lg-block">
              <div class="menu-links d-inline-block">
                <a href="{{route('find-employees')}}" class="menu-item cursor-pointer">Find Employees</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('why-quopro')}}" class="menu-item cursor-pointer">Why QuoPro?</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('services')}}" class="menu-item cursor-pointer">Services</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('blog')}}" class="menu-item cursor-pointer">Blog</a>
              </div>
              <div class="menu-links d-inline-block">
                <a href="{{route('contact')}}" class="menu-item cursor-pointer">Contact</a>
              </div>
              <div class="menu-links d-inline-block ">
                <a href="{{route('growpro')}}" class="menu-item cursor-pointer text-green">GrowPro</a>
              </div>
              @if(Auth::check())
              <div class="menu-links d-inline-block ">
                <a href="{{route('login')}}" class="menu-item cursor-pointer ml-3"><i class="fa fa-user mr-2"></i> Dashboard</a>
              </div>
              @else
              <div class="menu-links d-inline-block ">
                <a href="{{route('login')}}" class="menu-item cursor-pointer">Login</a>
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>

      <div id="mobile-menu" class="mobile-menu">
        <div class="container-fluid px-3">
         <div class="row">
          <a href="/">
            <img src="/img/logos/logo.svg" class="menu_logo mt-2" width="125" alt="QuoPro Logo"/>
          </a>
          <div class="col-lg-10 pt-5 mt-5">
            <div class="menu-links d-block pt-4">
              <p class="mb-0"><a href="{{route('find-employees')}}" class="menu-item">Find Employees</a></p>
              <p class="mb-0"><a href="{{route('why-quopro')}}" class="menu-item">Why QuoPro?</a></p>
              <p class="mb-0"><a href="{{route('services')}}" class="menu-item">Services</a></p>
              <p class="mb-0"><a href="{{route('blog')}}" class="menu-item">Blog</a></p>
              <p class="mb-0"><a href="{{route('contact')}}" class="menu-item">Contact</a></p>
              <p class="mb-0"><a href="{{route('growpro')}}" class="menu-item text-green">GrowPro</a></p>
              @if(Auth::check())
              <p class="mb-0"><a href="{{route('login')}}" class="menu-item">Dashboard</a></p>
              @else
              <p class="mb-0"><a href="{{route('login')}}" class="menu-item">Login</a></p>
              @endif
              <p class="mb-0 text-large mt-3">
                <a href="https://www.facebook.com/QuoPro-Recruitment-101761438740068"><i class="fa fa-facebook"></i></a>
                <a href="https://twitter.com/QuoproR" ><i class="fa fa-twitter ml-3"></i></a>
                <a href="https://www.linkedin.com/company/quoprorecruitment/about/" ><i class="fa fa-linkedin ml-3"></i></a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    @yield('header')
    <main id="content" style="z-index: 2;" class="overflow-hidden">
      <div id="menu-trigger"></div>
      @yield('content')
    </main>
    @if($page != 'Login')
    <site-footer></site-footer>
    @endif
    @yield('modals')
    <loader></loader>
  </div>
  <div id="menu_body_hide"></div>
  <!-- Modal -->
  <div class="modal fade" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="notificationModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="card border-0 box-shadow">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body p-5">
            <img src="/img/logos/logo.svg" class="menu_logo mb-3" alt="QuoPro Logo" width="120" />
            <p class="modal-text"></p>
            <a class="modal-btn-link" href="#">
              <button type="button" class="btn btn-primary modal-btn mb-2"></button>
            </a>
            <div class="btn-text cursor-pointer text-center d-block w-100" data-dismiss="modal">[X] Close</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@yield('prescripts')
<script src="{{ mix('/js/manifest.js') }}"></script>
<script src="{{ mix('/js/vendor.js') }}"></script>
<script src="{{ mix('js/app.js') }}"></script>
<script>
  window.addEventListener("load", function(){
   window.cookieconsent.initialise({
     "palette": {
       "popup": {
         "background": "#1D3D8D",
         "text": "#ffffff"
       },
       "button": {
         "background": "#5CCC4A",
         "text": "#ffffff",
         "border-radius": "30px"
       }
     }
   })});
 </script>
 @yield('scripts')
</body>
</html>