@php
$page = 'Homepage';
$pagetitle = 'Contact QuoPro';
$metadescription = 'Get in touch with QuoPro Recruitment using our contact form, fund our contact details including phone number, email address and social media links.';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://quoprorecruitment.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('content')
<div id="home-header" class="container-fluid full-height position-relative mob-pt-0 page-top mob-height-auto">
  <img src="/img/graphics/wave-down.svg" class="menu-bg" alt="QuoPro Recruitment menu background"/>
  <div class="tk-blob home-top-bg d-none d-md-block" style="--time: 80s; --amount: 1; --fill: #56cbb9;">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 440.7 428.7" style="--time: 40s;">
      <defs>
        <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
          <stop offset="0%" style="stop-color:#8DC3FF;stop-opacity:1" />
          <stop offset="55%" style="stop-color:#005ABB;stop-opacity:1" />
          <stop offset="100%" style="stop-color:#3131b8;stop-opacity:1" />
        </linearGradient>
      </defs>
      <path fill="url(#grad1)" d="M410.6 78.8c36 52.5 36.1 126 19.2 194C412.9 340.7 379 403 330 421.9c-49 19-113.1-5.3-178.6-34C85.8 359.2 18.7 326.1 3.5 276.4-11.7 226.7 25 160.3 71.7 105.3 118.3 50.3 174.8 6.8 239 .7c64.1-6 135.7 25.5 171.6 78.1z"></path>
    </svg>
  </div>
  <img src="/img/graphics/mob-top.svg" class="mob-home-top d-md-none"/>
  <img src="/img/graphics/top-circle.svg" class="home-top-circle" alt="QuoPro Recruitment Northern Ireland graphic"/>
{{-- 	<div id="map" class="full-height"></div>
 --}}  <div class="row">
  	<div class="container">
  		<div class="row">
		    <div class="col-lg-12 mb-5 pr-5 mob-mt-5 mob-px-3">
		    	<div class="d-g-table w-100 full-height mob-height-auto">
            <div class="d-lg-table-cell align-middle w-100 full-height mob-height-auto">
            	<div class="card bg-white border-0 shadow px-5 mob-px-4 pt-5 mob-pb-5 mt-5">
					    	<h1 class="mb-2 smaller-title">Get in touch</h1>
					      <p class=" mb-4">Please get in touch if you have any questions at all.</p>
					      <contact-page-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></contact-page-form>
					    </div>
				    </div>
				  </div>
		    </div>
		  </div>
		</div>
  </div>
</div>
<div class="container ">
	<div class="row mb-5 text-center text-lg-left">
    <div class="col-lg-4 mob-mb-4">
      <h3 class="mb-3"><b>Contact us</b></h3>
      <p class="mb-1"><a href="tel:00447518156827"><i class="fa fa-phone mr-2"></i> +44 (0) 75 1815 6827</a></p>
      <p class="mb-4"><a href="mailto:hello@quoprorecruitment.com"><i class="fa fa-envelope mr-2"></i> hello@quoprorecruitment.com</a></p>
    </div>
    <div class="col-lg-4 mob-mb-4">
      <h3 class="mb-3"><b>Find us</b></h3>
      <p class="">Titanic Suites<br/>55-59 Adelaide St<br/>Belfast, BT2 8FE</p>
    </div>
    <div class="col-lg-4 mob-mb-4">
      <h3 class="mb-3"><b>Socials</b></h3>
      <p class="mb-0 text-large mt-3">
        <a href="https://www.facebook.com/QuoPro-Recruitment-101761438740068"><i class="fa fa-facebook"></i></a>
        <a href="https://twitter.com/QuoproR" ><i class="fa fa-twitter ml-3"></i></a>
        <a href="https://www.linkedin.com/company/quoprorecruitment/about/" ><i class="fa fa-linkedin ml-3"></i></a>
      </p>
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection
@section('modals')
@endsection