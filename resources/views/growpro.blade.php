@php
$page = 'GrowPro';
$pagetitle = 'GrowPro - QuoPro Recruitment';
$metadescription = 'GrowPro is our connective tool between education and employment. We want to target the single biggest commodity and resource we have available to us: The next generation.';
$pagetype = 'white';
$pagename = 'home';
$ogimage = 'https://quoprorecruitment.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="growpro-header" class="container-fluid position-relative mob-pt-0 pt-5 page-top">
  <img src="/img/graphics/top-circle.svg" class="home-top-circle" alt="QuoPro Recruitment Northern Ireland graphic"/>
  <div class="row position-relative z-2 pt-5">
    <div class="container mt-5 mob-pt-5">
      <div class="row">
        <div class="col-lg-8 mob-px-5 text-center text-lg-left">
          <img src="/img/logos/growpro.svg" alt="QuoPro GrowPro Recruitment Logo Northern Ireland" height="80" class="d-none d-lg-inline-block" />
          <img src="/img/logos/growpro.svg" alt="QuoPro GrowPro Recruitment Logo Northern Ireland" class="w-100 mob-px-5 d-lg-none" />
          <p class="mt-4">Along with daily life, studying and socialising... remember those days? Job searching can become a forgotten task. And that’s exactly what we want; you to forget about it and us to do it for you! </p>
        </div>
        <div class="col-lg-6 py-5 text-center text-lg-left">
          <div class="line"></div>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mb-5 pb-5">
  <div class="row pb-5">
    <div class="col-lg-6 d-lg-none mb-4">
      <img src="/img/graphics/guiding-employers.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100"/>
    </div>
    <div class="col-lg-6 mt-5 mob-mt-0 text-center text-lg-left mob-px-4">
      <h1 class="mb-3 mimic-h3">What is GrowPro?</h1>
      <p>GrowPro is our connective tool between education and employment. We want to target the single biggest commodity and resource we have available to us: The next generation.</p>
      <p class="mb-4">Our sole aim is to improve an individual’s employment prospects after leaving school or higher/further education.</p>
      <a href="{{route('sign-up')}}">
        <button class="btn btn-green btn-icon" type="button">Sign up <i class="fa fa-arrow-circle-right"></i></button>
      </a>
    </div>
    <div class="col-lg-6 d-none d-lg-block pl-5 mt-5 pt-5">
      <img src="/img/graphics/guiding-employers.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100"/>
    </div>
  </div>
  <div class="row">
    <div class="col-12 py-5 mob-py-0">
      <hr class="my-5"/>
    </div>
  </div>
  <div class="row py-5">
    <div class="col-lg-6 pr-5 mob-px-3 pt-5 mob-pt-0 mob-mb-4">
      <img src="/img/graphics/top-1.svg" alt="QuoPro Recruitment graphic Northern Ireland" class="w-100"/>
    </div>
    <div class="col-lg-6 text-center text-lg-left mob-px-4">
      <h3 class="mb-3">Our Ethos</h3>
      <p>Regardless of educational completion, every individual has something substantial to offer. This is not an agency... this is a concrete employment platform where employers can access the emerging talent our system promotes.</p>
      <p class="mb-4">Obtaining employment is a journey, not a result. So don’t just look for jobs.. let jobs look for you.. </p>
      <a href="{{route('sign-up')}}">
        <button class="btn btn-green btn-icon" type="button">Sign up <i class="fa fa-arrow-circle-right"></i></button>
      </a>
    </div>
  </div>
</div>
@endsection
@section('scripts')

@endsection