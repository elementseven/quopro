@php
$page = 'Services';
$pagetitle = 'Services - QuoPro Recruitment';
$metadescription = 'We provide a range of services for both employers and potential employees. If there is something in particular you are loooking for, please get in touch.';
$pagetype = 'white';
$pagename = 'home';
$ogimage = 'https://quoprorecruitment.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="services-header" class="container-fluid position-relative mob-pt-0 pt-5 page-top">
  <img src="/img/graphics/top-circle.svg" class="home-top-circle" alt="QuoPro Recruitment Northern Ireland graphic"/>
  <div class="row position-relative z-2 pt-5">
    <div class="container mt-5 mob-pt-5">
      <div class="row">
        <div class="col-lg-8 mob-px-5 text-center text-lg-left">
          <h1 class="page-top ipad-mb-3">Services</h1>
          <p>We provide a range of services for both employers and potential employees. If there is something in particular you are loooking for, please <a href="{{route('contact')}}"><b>get in touch.</b></a></p>
        </div>
        <div class="col-lg-6 py-5 text-center text-lg-left">
          <div class="line"></div>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container ">
  <div class="row pb-5">
    <div class="col-lg-6 d-lg-none mb-4">
      <img src="/img/graphics/personal-platforming.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100"/>
    </div>
    <div class="col-lg-6 mt-5 mob-mt-0 text-center text-lg-left mob-px-4">
      <h3>Personal Platforming</h3>
      <p>Our connective search is what sets us apart. Traditional recruitment with an engaging twist.</p>
      <p>An ever-changing market and fluctuating economy needs an evolved and improved system. A cost-effective and dynamic platform to cater for all your needs.</p>
      <p>Gain unmatched access to QuoPro professionals or tomorrows potential in GrowPro. Just search for the best and we do the rest. </p>
      <a href="{{route('sign-up')}}">
        <button class="btn btn-primary btn-icon" type="button">Sign up <i class="fa fa-arrow-circle-right"></i></button>
      </a>
    </div>
    <div class="col-lg-6 d-none d-lg-block pl-5 mt-5 pt-5">
      <img src="/img/graphics/personal-platforming.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100"/>
    </div>
  </div>
  <div class="row">
    <div class="col-12 py-5 mob-py-0">
      <hr class="my-5"/>
    </div>
  </div>
  <div class="row py-5">
    <div class="col-lg-6 pr-5 mob-px-3 pt-5 mob-pt-0 mob-mb-4">
      <img src="/img/graphics/trustedbespokestaffing.svg" alt="QuoPro Recruitment graphic Northern Ireland" class="w-100"/>
    </div>
    <div class="col-lg-6 text-center text-lg-left mob-px-4">
      <h3>Trusted Bespoke Staffing</h3>
      <p>The ‘most suitable’ or ‘best’ candidates don’t always apply for the ‘most suitable’ jobs. Either they can’t find the jobs, recruiters can’t find them, lack of awareness or simply just bad timing - Which is where we come in. </p>
      <p>QuoPro platform local professionals for local business. We want employers and candidates alike to have the greatest access to each other and ensure future success in whichever environment possible. </p>
      <p class="mb-4">Is your search limited? We have the answer. Contact us directly, 24/7 for an in-depth understanding of your business demands. </p>
      <a href="{{route('sign-up')}}">
        <button class="btn btn-primary btn-icon" type="button">Sign up <i class="fa fa-arrow-circle-right"></i></button>
      </a>
    </div>
  </div>
</div>
<div class="container position-relative">
  <img src="/img/graphics/circle.svg" class="home-middle-circle" alt="QuoPro Recruitment graphic"  data-scroll-speed="3"/>
  <div class="row">
    <div class="col-12 py-5 mob-py-0">
      <hr class="my-5"/>
    </div>
  </div>
  <div class="row py-5">
    <div class="col-lg-6 d-lg-none mb-4">
      <img src="/img/graphics/hiringadvice.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100"/>
    </div>
    <div class="col-lg-6 text-center text-lg-left mob-px-4">
      <h3>Hiring Advice</h3>
      <p>Recruitment planning is a necessary tool to find the candidate you need. Financial implications, personnel resources and ‘cultural fit’ can all be a by-product of poor recruitment.</p>
      <p>Once we have a full understanding of the situation, we apply our knowledge of the market to prescribe a relevant solution. </p>
      <p>We work to provide the RIGHT candidate, not a quick one. In business, time = money. Bad decisions? = more money “a poor hire at mid-manager level with a salary of £42,000 can cost a business more than £132,000” – REC</p>
      <p class="mb-4">Do not let it be you. We spend our time and energy helping you streamline so you don’t have to.</p>
      <a href="{{route('sign-up')}}">
        <button class="btn btn-primary btn-icon" type="button">Sign up <i class="fa fa-arrow-circle-right"></i></button>
      </a>
    </div>
    <div class="col-lg-6 d-none d-lg-block pl-5 pt-5 mt-5">
      <img src="/img/graphics/hiringadvice.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100"/>
    </div>
  </div>
  <div class="row">
    <div class="col-12 py-5 mob-py-0">
      <hr class="my-5"/>
    </div>
  </div>
  <div class="row pt-5">
    <div class="col-lg-6 pr-5 mob-px-3 pt-5 mt-5 mob-mt-0 mob-pt-0 mob-mb-4">
      <img src="/img/graphics/fixedpermrecruitment.svg" alt="QuoPro Recruitment Northern Ireland, Guiding employers to the right employee" class="w-100"/>
    </div>
    <div class="col-lg-6 text-center text-lg-left mob-px-4">
      <h3>Fixed/Permanent Recruitment </h3>
      <p>Our company policy evolves around sustainability and reliability, for both client and candidate. Trust is what runs a business. Consistency is what transforms average into excellence. </p>
      <p>A high staff turnover can often lead to instability in various aspects of business… decreased moral, imbalanced workload, poor relationships. On top of improving company culture, permanent placements allow for better understanding of staff, offer long-term strategic success and boost productivity.</p>
      <p class="mb-4">We want it to be right first time, every time.</p>
      <a href="{{route('sign-up')}}">
        <button class="btn btn-primary btn-icon" type="button">Sign up <i class="fa fa-arrow-circle-right"></i></button>
      </a>
    </div>
  </div>
</div>
<div class="container py-5 position-relative">
  <img src="/img/graphics/circle.svg" class="home-middle-circle" alt="QuoPro Recruitment graphic" data-scroll-speed="3" />
  <div class="row">
    <div class="col-12 py-5 mob-py-0">
      <hr class="my-5"/>
    </div>
  </div>
  <div class="row py-5 text-center justify-content-center">
    <div class="col-md-4 px-5 mob-px-3 mob-mb-5 ipadp-mb-5 ipadp-px-3">
      <img src="/img/graphics/clock.svg" alt="Save time with more control of the process" height="100"/>
      <p class="mt-3 mob-px-5"><b>Save time with more control of the recruitment process</b></p>
    </div>
    <div class="col-md-4 px-5 mob-px-3 mob-mb-5 ipadp-mb-5 ipadp-px-3">
      <img src="/img/graphics/money.svg" alt="Save time with more control of the process" height="100"/>
      <p class="mt-3 mob-px-5"><b>More cost effective than traditional recruitment</b></p>
    </div>
    <div class="col-md-4 px-5 mob-px-3 ipadp-px-3">
      <img src="/img/graphics/check.svg" alt="Save time with more control of the process" height="100"/>
      <p class="mt-3 mob-px-5"><b>More control allows for higher success rates </b></p>
    </div>
  </div>
</div>
@endsection
@section('scripts')

@endsection