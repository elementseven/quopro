
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');
window.Vue = require('vue');
window.LazyLoad = require('vanilla-lazyload');
window.moment = require('moment');
require('./plugins/modernizr-custom.js');
require('./a-general.js');
require("babel-polyfill");
import VueLazyload from 'vue-lazyload';
import { VueEditor } from "vue2-editor";
import VueGoogleCharts from 'vue-google-charts';
Vue.use(VueGoogleCharts);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('loader', require('./components/Loader.vue').default);
Vue.component('pagination', require('laravel-vue-pagination'));

Vue.component('website-visitors', require('./components/admin/reuse/WebsiteVisitors.vue').default);
Vue.component('user-types', require('./components/admin/reuse/UserTypes.vue').default);
Vue.component('top-referrers', require('./components/admin/reuse/TopReferrers.vue').default);
Vue.component('popular-pages', require('./components/admin/reuse/PopularPages.vue').default);

Vue.component('index-news', require('./components/admin/news/Index.vue').default);
Vue.component('create-news', require('./components/admin/news/Create.vue').default);
Vue.component('edit-news', require('./components/admin/news/Edit.vue').default);
Vue.component('remove-news', require('./components/admin/news/Remove.vue').default);
Vue.component('show-news', require('./components/admin/news/Show.vue').default);

Vue.component('index-packages', require('./components/admin/packages/Index.vue').default);
Vue.component('create-package', require('./components/admin/packages/Create.vue').default);
Vue.component('edit-package', require('./components/admin/packages/Edit.vue').default);
Vue.component('remove-package', require('./components/admin/packages/Remove.vue').default);

Vue.component('index-jobs', require('./components/admin/jobs/Index.vue').default);
Vue.component('create-job', require('./components/admin/jobs/Create.vue').default);
Vue.component('edit-job', require('./components/admin/jobs/Edit.vue').default);
Vue.component('remove-job', require('./components/admin/jobs/Remove.vue').default);

Vue.component('index-tours', require('./components/admin/tours/Index.vue').default);
Vue.component('create-tour', require('./components/admin/tours/Create.vue').default);
Vue.component('edit-tour', require('./components/admin/tours/Edit.vue').default);
Vue.component('remove-tour', require('./components/admin/tours/Remove.vue').default);

Vue.component('index-categories', require('./components/admin/categories/Index.vue').default);
Vue.component('create-category', require('./components/admin/categories/Create.vue').default);
Vue.component('remove-category', require('./components/admin/categories/Remove.vue').default);
Vue.component('show-category', require('./components/admin/categories/Show.vue').default);

Vue.component('index-customers', require('./components/admin/customers/Index.vue').default);
Vue.component('create-customer', require('./components/admin/customers/Create.vue').default);
Vue.component('edit-customer', require('./components/admin/customers/Edit.vue').default);
Vue.component('edit-password', require('./components/admin/customers/Password.vue').default);
Vue.component('remove-customer', require('./components/admin/customers/Remove.vue').default);
Vue.component('show-customer', require('./components/admin/customers/Show.vue').default);

Vue.component('index-interested', require('./components/admin/interested/Index.vue').default);
// Vue.component('create-customer', require('./components/admin/customers/Create.vue').default);
// Vue.component('edit-customer', require('./components/admin/customers/Edit.vue').default);
// Vue.component('edit-password', require('./components/admin/customers/Password.vue').default);
Vue.component('remove-interested', require('./components/admin/interested/Remove.vue').default);
// Vue.component('show-customer', require('./components/admin/customers/Show.vue').default);

Vue.component('index-postcodes', require('./components/admin/postcodes/Index.vue').default);
Vue.component('create-postcode', require('./components/admin/postcodes/Create.vue').default);
Vue.component('remove-postcode', require('./components/admin/postcodes/Remove.vue').default);
Vue.component('show-postcode', require('./components/admin/postcodes/Show.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});
