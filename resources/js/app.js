/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');
window.LazyLoad = require('vanilla-lazyload');
import Vue from 'vue';
window.moment = require('moment');
import vueDebounce from 'vue-debounce';
import VuePhoneNumberInput from 'vue-phone-number-input';
import 'vue-phone-number-input/dist/vue-phone-number-input.css';
import { Datetime } from 'vue-datetime';
import 'vue-datetime/dist/vue-datetime.css';


require('./plugins/modernizr-custom.js');
require('./plugins/cookieConsent.js');
require('waypoints/lib/jquery.waypoints.min.js');
require('./a-general.js');
require("babel-polyfill");
Vue.use(vueDebounce, {
  listenTo: ['input', 'keyup']
});
Vue.use(VuePhoneNumberInput);
Vue.use(Datetime);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))
Vue.component('vue-phone-number-input', VuePhoneNumberInput);
Vue.component('loader', () => import(/* webpackChunkName: "Loader" */ './components/Loader.vue'));
Vue.component('pagination', () => import(/* webpackChunkName: "LaravelVuePagination" */ 'laravel-vue-pagination'));
Vue.component('mailing-list', () => import(/* webpackChunkName: "MailingList" */ './components/MailingList.vue'));

Vue.component('blog-index', () => import(/* webpackChunkName: "BlogIndex" */ './components/blog/Index.vue'));
Vue.component('blog-show', () => import(/* webpackChunkName: "BlogShow" */ './components/blog/Show.vue'));

Vue.component('employees-search', () => import(/* webpackChunkName: "EmployeesSearch" */ './components/Employees/Search.vue'));
Vue.component('employees-show', () => import(/* webpackChunkName: "EmployeesShow" */ './components/Employees/Show.vue'));
Vue.component('employee-card', () => import(/* webpackChunkName: "EmployeesCard" */ './components/Employees/Card.vue'));
Vue.component('employee-profile', () => import(/* webpackChunkName: "EmployeesProfile" */ './components/Employees/Profile.vue'));
Vue.component('employee-summary', () => import(/* webpackChunkName: "EmployeesSummary" */ './components/Employees/Summary.vue'));
Vue.component('employee-toggle-favourite', () => import(/* webpackChunkName: "EmployeesFavouriteToggle" */ './components/Employees/FavouriteToggle.vue'));
Vue.component('employee-enquire', () => import(/* webpackChunkName: "EmployeesEnquire" */ './components/Employees/Enquire.vue'));
Vue.component('employee-similar', () => import(/* webpackChunkName: "EmployeesSimilar" */ './components/Employees/Similar.vue'));


Vue.component('contact-page-form', () => import(/* webpackChunkName: "ContactPageForm" */ './components/ContactPageForm.vue'));

Vue.component('signup-index', () => import(/* webpackChunkName: "SignupIndex" */ './components/signup/Index.vue'));

Vue.component('countries', () => import(/* webpackChunkName: "Countries" */ './components/Countries.vue'));

Vue.component('site-footer', () => import(/* webpackChunkName: "Footer" */ './components/Footer.vue'));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});