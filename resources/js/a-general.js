require('./plugins/modernizr-custom.js');
var windowWidth = $(window).width();
var windowHeight = $(window).height();



// Listen for menu button click
function menuBtn(){
	$('#menu-trigger').each(function(index, element){
		var inview = new Waypoint({
			element: element,
			handler: function (direction) {
				if(direction === "down"){
					$('#scroll-menu').addClass('on');
				}else{
					$('#scroll-menu').removeClass('on');
				}
			},offset: '0%'
		});
	});
}

function heightMatch(){
	$('.height-match').each(function(){
		var h = $(this).find('.height-match-1').height();
		$(this).find('.height-match-2').height(h + "px");
	});
}

function fadeEffects(){
	$('.scroll-fade').each(function(index, element){
		var effect = $(this).attr("data-fade");
		var inview = new Waypoint({
			element: element,
			handler: function (direction) {
				if (direction === 'down') {
					$(this.element).animate({ opacity: 1 }, 300)
					$(element).addClass("animated " + effect);
				}
			},offset: '95%'
		});
	});
}

// Function to resize elements to full height
function fullHeight(){
	// Get the current height and width of the window
	windowWidth = $(window).width();
	windowHeight = $(window).height();
	$(".full-Height").each(function(){
		$(this).height(windowHeight + "px");
	});
}

// Function to resize elements to square
function squares(){
	$(".square").each(function(){
		$(this).height($(this).width() + "px");
	});
}

$(document).ready(function(){
	$('#close-loader-btn').click(function(){
		$('#loader').removeClass('on');
		$('#loader-message').html('');
		$('#close-loader-btn').removeClass('d-block').addClass('d-none');
		$('#loader-roller').removeClass('d-none').addClass('d-block');
		$('#loader-success').removeClass('d-block').addClass('d-none');
	});
	$('#menu_btn').click(function(){
		$("#menu_btn .nav-icon").toggleClass('nav_open');
		$("#mobile-menu").toggleClass("on");
		$("#menu").toggleClass("on");
		$("#menu_body_hide").toggleClass("on");
		$("#content").toggleClass("opacity");
		$(".menu").each(function(){
			$(this).toggleClass("opacity");
		});
	});
	$('#close-loader-btn').click(function(){
		document.getElementById("loader").classList.remove("on");
	});
	var lazyLoadInstance = new LazyLoad({
	    elements_selector: ".lazy"
	});
	$(".scroll-btn").click(function(){
		$('html,body').animate({
		   scrollTop: $(".scrollTo").offset().top
		});
	});
	fullHeight();
	fadeEffects();
	menuBtn();
	squares();
	heightMatch();
	$.fn.moveIt = function(){
	   var $window = $(window);
	   var instances = [];
	$(this).each(function(){
	   instances.push(new moveItItem($(this)));
	});
	window.onscroll = function(){
	   var scrollTop = $window.scrollTop();
	  instances.forEach(function(inst){
	     inst.update(scrollTop);
	     });
	   }
	}

	var moveItItem = function(el){
	   this.el = $(el);
	   this.speed = parseInt(this.el.attr('data-scroll-speed'));
	};

	moveItItem.prototype.update = function(scrollTop){
	   this.el.css('transform', 'translateY(' + -(scrollTop / this.speed) + 'px)');
	};

	// Initialization
	$(function(){
	   $('[data-scroll-speed]').moveIt();
	});


});

$(window).on('resize', function(){
	// Resize full height elements when the window size changes
	fullHeight();
	fadeEffects();
	menuBtn();
	squares();
});