/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('jquery');

import Vuetify from 'vuetify'; 
import "babel-polyfill";
import VueMeta from 'vue-meta';
import VueRouter from 'vue-router';
import App from './employees/views/App';

window.moment = require('moment');
import Vue from 'vue';
Vue.use(Vuetify);
Vue.use(VueRouter);
Vue.use(VueMeta, {
  // optional pluginOptions
  refreshOnceOnNavigation: true
});

Vue.component('laravel-pagination', require('laravel-vue-pagination'));

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
          path: '/employees/dashboard',
          name: 'Dashboard',
          component:
              () => import(/* webpackChunkName: "EmployeeDashboard" */ './employees/views/Dashboard.vue'),
        },
        {
          path: '/employees/account',
          name: 'Account',
          component:
              () => import(/* webpackChunkName: "EmployeeAccount" */ './employees/views/Account.vue'),
        }
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
    vuetify: new Vuetify({
      theme: {
        themes: {
          light: {
            primary: '#0269DD',
            secondary: '#3131b8',
            accent: '#8DC3FF',
            error: '#b71c1c',
          },
        },
      },
    }),
});