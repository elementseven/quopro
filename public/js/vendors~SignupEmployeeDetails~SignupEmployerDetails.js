(function () {
'use strict';

var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var check = function (it) {
  return it && it.Math == Math && it;
};

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global_1 =
  // eslint-disable-next-line no-undef
  check(typeof globalThis == 'object' && globalThis) ||
  check(typeof window == 'object' && window) ||
  check(typeof self == 'object' && self) ||
  check(typeof commonjsGlobal == 'object' && commonjsGlobal) ||
  // eslint-disable-next-line no-new-func
  (function () { return this; })() || Function('return this')();

var fails = function (exec) {
  try {
    return !!exec();
  } catch (error) {
    return true;
  }
};

// Detect IE8's incomplete defineProperty implementation
var descriptors = !fails(function () {
  return Object.defineProperty({}, 1, { get: function () { return 7; } })[1] != 7;
});

var nativePropertyIsEnumerable = {}.propertyIsEnumerable;
var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;

// Nashorn ~ JDK8 bug
var NASHORN_BUG = getOwnPropertyDescriptor && !nativePropertyIsEnumerable.call({ 1: 2 }, 1);

// `Object.prototype.propertyIsEnumerable` method implementation
// https://tc39.es/ecma262/#sec-object.prototype.propertyisenumerable
var f = NASHORN_BUG ? function propertyIsEnumerable(V) {
  var descriptor = getOwnPropertyDescriptor(this, V);
  return !!descriptor && descriptor.enumerable;
} : nativePropertyIsEnumerable;

var objectPropertyIsEnumerable = {
	f: f
};

var createPropertyDescriptor = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};

var toString = {}.toString;

var classofRaw = function (it) {
  return toString.call(it).slice(8, -1);
};

var split = ''.split;

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var indexedObject = fails(function () {
  // throws an error in rhino, see https://github.com/mozilla/rhino/issues/346
  // eslint-disable-next-line no-prototype-builtins
  return !Object('z').propertyIsEnumerable(0);
}) ? function (it) {
  return classofRaw(it) == 'String' ? split.call(it, '') : Object(it);
} : Object;

// `RequireObjectCoercible` abstract operation
// https://tc39.es/ecma262/#sec-requireobjectcoercible
var requireObjectCoercible = function (it) {
  if (it == undefined) throw TypeError("Can't call method on " + it);
  return it;
};

// toObject with fallback for non-array-like ES3 strings



var toIndexedObject = function (it) {
  return indexedObject(requireObjectCoercible(it));
};

var isObject = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};

// `ToPrimitive` abstract operation
// https://tc39.es/ecma262/#sec-toprimitive
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
var toPrimitive = function (input, PREFERRED_STRING) {
  if (!isObject(input)) return input;
  var fn, val;
  if (PREFERRED_STRING && typeof (fn = input.toString) == 'function' && !isObject(val = fn.call(input))) return val;
  if (typeof (fn = input.valueOf) == 'function' && !isObject(val = fn.call(input))) return val;
  if (!PREFERRED_STRING && typeof (fn = input.toString) == 'function' && !isObject(val = fn.call(input))) return val;
  throw TypeError("Can't convert object to primitive value");
};

var hasOwnProperty = {}.hasOwnProperty;

var has = function (it, key) {
  return hasOwnProperty.call(it, key);
};

var document$1 = global_1.document;
// typeof document.createElement is 'object' in old IE
var EXISTS = isObject(document$1) && isObject(document$1.createElement);

var documentCreateElement = function (it) {
  return EXISTS ? document$1.createElement(it) : {};
};

// Thank's IE8 for his funny defineProperty
var ie8DomDefine = !descriptors && !fails(function () {
  return Object.defineProperty(documentCreateElement('div'), 'a', {
    get: function () { return 7; }
  }).a != 7;
});

var nativeGetOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;

// `Object.getOwnPropertyDescriptor` method
// https://tc39.es/ecma262/#sec-object.getownpropertydescriptor
var f$1 = descriptors ? nativeGetOwnPropertyDescriptor : function getOwnPropertyDescriptor(O, P) {
  O = toIndexedObject(O);
  P = toPrimitive(P, true);
  if (ie8DomDefine) try {
    return nativeGetOwnPropertyDescriptor(O, P);
  } catch (error) { /* empty */ }
  if (has(O, P)) return createPropertyDescriptor(!objectPropertyIsEnumerable.f.call(O, P), O[P]);
};

var objectGetOwnPropertyDescriptor = {
	f: f$1
};

var anObject = function (it) {
  if (!isObject(it)) {
    throw TypeError(String(it) + ' is not an object');
  } return it;
};

var nativeDefineProperty = Object.defineProperty;

// `Object.defineProperty` method
// https://tc39.es/ecma262/#sec-object.defineproperty
var f$2 = descriptors ? nativeDefineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (ie8DomDefine) try {
    return nativeDefineProperty(O, P, Attributes);
  } catch (error) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};

var objectDefineProperty = {
	f: f$2
};

var createNonEnumerableProperty = descriptors ? function (object, key, value) {
  return objectDefineProperty.f(object, key, createPropertyDescriptor(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};

var setGlobal = function (key, value) {
  try {
    createNonEnumerableProperty(global_1, key, value);
  } catch (error) {
    global_1[key] = value;
  } return value;
};

var SHARED = '__core-js_shared__';
var store = global_1[SHARED] || setGlobal(SHARED, {});

var sharedStore = store;

var functionToString = Function.toString;

// this helper broken in `3.4.1-3.4.4`, so we can't use `shared` helper
if (typeof sharedStore.inspectSource != 'function') {
  sharedStore.inspectSource = function (it) {
    return functionToString.call(it);
  };
}

var inspectSource = sharedStore.inspectSource;

var WeakMap = global_1.WeakMap;

var nativeWeakMap = typeof WeakMap === 'function' && /native code/.test(inspectSource(WeakMap));

var shared = createCommonjsModule(function (module) {
(module.exports = function (key, value) {
  return sharedStore[key] || (sharedStore[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: '3.8.2',
  mode:  'global',
  copyright: '© 2021 Denis Pushkarev (zloirock.ru)'
});
});

var id = 0;
var postfix = Math.random();

var uid = function (key) {
  return 'Symbol(' + String(key === undefined ? '' : key) + ')_' + (++id + postfix).toString(36);
};

var keys = shared('keys');

var sharedKey = function (key) {
  return keys[key] || (keys[key] = uid(key));
};

var hiddenKeys = {};

var WeakMap$1 = global_1.WeakMap;
var set, get, has$1;

var enforce = function (it) {
  return has$1(it) ? get(it) : set(it, {});
};

var getterFor = function (TYPE) {
  return function (it) {
    var state;
    if (!isObject(it) || (state = get(it)).type !== TYPE) {
      throw TypeError('Incompatible receiver, ' + TYPE + ' required');
    } return state;
  };
};

if (nativeWeakMap) {
  var store$1 = sharedStore.state || (sharedStore.state = new WeakMap$1());
  var wmget = store$1.get;
  var wmhas = store$1.has;
  var wmset = store$1.set;
  set = function (it, metadata) {
    metadata.facade = it;
    wmset.call(store$1, it, metadata);
    return metadata;
  };
  get = function (it) {
    return wmget.call(store$1, it) || {};
  };
  has$1 = function (it) {
    return wmhas.call(store$1, it);
  };
} else {
  var STATE = sharedKey('state');
  hiddenKeys[STATE] = true;
  set = function (it, metadata) {
    metadata.facade = it;
    createNonEnumerableProperty(it, STATE, metadata);
    return metadata;
  };
  get = function (it) {
    return has(it, STATE) ? it[STATE] : {};
  };
  has$1 = function (it) {
    return has(it, STATE);
  };
}

var internalState = {
  set: set,
  get: get,
  has: has$1,
  enforce: enforce,
  getterFor: getterFor
};

var redefine = createCommonjsModule(function (module) {
var getInternalState = internalState.get;
var enforceInternalState = internalState.enforce;
var TEMPLATE = String(String).split('String');

(module.exports = function (O, key, value, options) {
  var unsafe = options ? !!options.unsafe : false;
  var simple = options ? !!options.enumerable : false;
  var noTargetGet = options ? !!options.noTargetGet : false;
  var state;
  if (typeof value == 'function') {
    if (typeof key == 'string' && !has(value, 'name')) {
      createNonEnumerableProperty(value, 'name', key);
    }
    state = enforceInternalState(value);
    if (!state.source) {
      state.source = TEMPLATE.join(typeof key == 'string' ? key : '');
    }
  }
  if (O === global_1) {
    if (simple) O[key] = value;
    else setGlobal(key, value);
    return;
  } else if (!unsafe) {
    delete O[key];
  } else if (!noTargetGet && O[key]) {
    simple = true;
  }
  if (simple) O[key] = value;
  else createNonEnumerableProperty(O, key, value);
// add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
})(Function.prototype, 'toString', function toString() {
  return typeof this == 'function' && getInternalState(this).source || inspectSource(this);
});
});

var path = global_1;

var aFunction = function (variable) {
  return typeof variable == 'function' ? variable : undefined;
};

var getBuiltIn = function (namespace, method) {
  return arguments.length < 2 ? aFunction(path[namespace]) || aFunction(global_1[namespace])
    : path[namespace] && path[namespace][method] || global_1[namespace] && global_1[namespace][method];
};

var ceil = Math.ceil;
var floor = Math.floor;

// `ToInteger` abstract operation
// https://tc39.es/ecma262/#sec-tointeger
var toInteger = function (argument) {
  return isNaN(argument = +argument) ? 0 : (argument > 0 ? floor : ceil)(argument);
};

var min = Math.min;

// `ToLength` abstract operation
// https://tc39.es/ecma262/#sec-tolength
var toLength = function (argument) {
  return argument > 0 ? min(toInteger(argument), 0x1FFFFFFFFFFFFF) : 0; // 2 ** 53 - 1 == 9007199254740991
};

var max = Math.max;
var min$1 = Math.min;

// Helper for a popular repeating case of the spec:
// Let integer be ? ToInteger(index).
// If integer < 0, let result be max((length + integer), 0); else let result be min(integer, length).
var toAbsoluteIndex = function (index, length) {
  var integer = toInteger(index);
  return integer < 0 ? max(integer + length, 0) : min$1(integer, length);
};

// `Array.prototype.{ indexOf, includes }` methods implementation
var createMethod = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIndexedObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) {
      if ((IS_INCLUDES || index in O) && O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};

var arrayIncludes = {
  // `Array.prototype.includes` method
  // https://tc39.es/ecma262/#sec-array.prototype.includes
  includes: createMethod(true),
  // `Array.prototype.indexOf` method
  // https://tc39.es/ecma262/#sec-array.prototype.indexof
  indexOf: createMethod(false)
};

var indexOf = arrayIncludes.indexOf;


var objectKeysInternal = function (object, names) {
  var O = toIndexedObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) !has(hiddenKeys, key) && has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~indexOf(result, key) || result.push(key);
  }
  return result;
};

// IE8- don't enum bug keys
var enumBugKeys = [
  'constructor',
  'hasOwnProperty',
  'isPrototypeOf',
  'propertyIsEnumerable',
  'toLocaleString',
  'toString',
  'valueOf'
];

var hiddenKeys$1 = enumBugKeys.concat('length', 'prototype');

// `Object.getOwnPropertyNames` method
// https://tc39.es/ecma262/#sec-object.getownpropertynames
var f$3 = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return objectKeysInternal(O, hiddenKeys$1);
};

var objectGetOwnPropertyNames = {
	f: f$3
};

var f$4 = Object.getOwnPropertySymbols;

var objectGetOwnPropertySymbols = {
	f: f$4
};

// all object keys, includes non-enumerable and symbols
var ownKeys = getBuiltIn('Reflect', 'ownKeys') || function ownKeys(it) {
  var keys = objectGetOwnPropertyNames.f(anObject(it));
  var getOwnPropertySymbols = objectGetOwnPropertySymbols.f;
  return getOwnPropertySymbols ? keys.concat(getOwnPropertySymbols(it)) : keys;
};

var copyConstructorProperties = function (target, source) {
  var keys = ownKeys(source);
  var defineProperty = objectDefineProperty.f;
  var getOwnPropertyDescriptor = objectGetOwnPropertyDescriptor.f;
  for (var i = 0; i < keys.length; i++) {
    var key = keys[i];
    if (!has(target, key)) defineProperty(target, key, getOwnPropertyDescriptor(source, key));
  }
};

var replacement = /#|\.prototype\./;

var isForced = function (feature, detection) {
  var value = data[normalize(feature)];
  return value == POLYFILL ? true
    : value == NATIVE ? false
    : typeof detection == 'function' ? fails(detection)
    : !!detection;
};

var normalize = isForced.normalize = function (string) {
  return String(string).replace(replacement, '.').toLowerCase();
};

var data = isForced.data = {};
var NATIVE = isForced.NATIVE = 'N';
var POLYFILL = isForced.POLYFILL = 'P';

var isForced_1 = isForced;

var getOwnPropertyDescriptor$1 = objectGetOwnPropertyDescriptor.f;






/*
  options.target      - name of the target object
  options.global      - target is the global object
  options.stat        - export as static methods of target
  options.proto       - export as prototype methods of target
  options.real        - real prototype method for the `pure` version
  options.forced      - export even if the native feature is available
  options.bind        - bind methods to the target, required for the `pure` version
  options.wrap        - wrap constructors to preventing global pollution, required for the `pure` version
  options.unsafe      - use the simple assignment of property instead of delete + defineProperty
  options.sham        - add a flag to not completely full polyfills
  options.enumerable  - export as enumerable property
  options.noTargetGet - prevent calling a getter on target
*/
var _export = function (options, source) {
  var TARGET = options.target;
  var GLOBAL = options.global;
  var STATIC = options.stat;
  var FORCED, target, key, targetProperty, sourceProperty, descriptor;
  if (GLOBAL) {
    target = global_1;
  } else if (STATIC) {
    target = global_1[TARGET] || setGlobal(TARGET, {});
  } else {
    target = (global_1[TARGET] || {}).prototype;
  }
  if (target) for (key in source) {
    sourceProperty = source[key];
    if (options.noTargetGet) {
      descriptor = getOwnPropertyDescriptor$1(target, key);
      targetProperty = descriptor && descriptor.value;
    } else targetProperty = target[key];
    FORCED = isForced_1(GLOBAL ? key : TARGET + (STATIC ? '.' : '#') + key, options.forced);
    // contained in target
    if (!FORCED && targetProperty !== undefined) {
      if (typeof sourceProperty === typeof targetProperty) continue;
      copyConstructorProperties(sourceProperty, targetProperty);
    }
    // add a flag to not completely full polyfills
    if (options.sham || (targetProperty && targetProperty.sham)) {
      createNonEnumerableProperty(sourceProperty, 'sham', true);
    }
    // extend global
    redefine(target, key, sourceProperty, options);
  }
};

var nativeSymbol = !!Object.getOwnPropertySymbols && !fails(function () {
  // Chrome 38 Symbol has incorrect toString conversion
  // eslint-disable-next-line no-undef
  return !String(Symbol());
});

var useSymbolAsUid = nativeSymbol
  // eslint-disable-next-line no-undef
  && !Symbol.sham
  // eslint-disable-next-line no-undef
  && typeof Symbol.iterator == 'symbol';

// `IsArray` abstract operation
// https://tc39.es/ecma262/#sec-isarray
var isArray = Array.isArray || function isArray(arg) {
  return classofRaw(arg) == 'Array';
};

// `ToObject` abstract operation
// https://tc39.es/ecma262/#sec-toobject
var toObject = function (argument) {
  return Object(requireObjectCoercible(argument));
};

// `Object.keys` method
// https://tc39.es/ecma262/#sec-object.keys
var objectKeys = Object.keys || function keys(O) {
  return objectKeysInternal(O, enumBugKeys);
};

// `Object.defineProperties` method
// https://tc39.es/ecma262/#sec-object.defineproperties
var objectDefineProperties = descriptors ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = objectKeys(Properties);
  var length = keys.length;
  var index = 0;
  var key;
  while (length > index) objectDefineProperty.f(O, key = keys[index++], Properties[key]);
  return O;
};

var html = getBuiltIn('document', 'documentElement');

var GT = '>';
var LT = '<';
var PROTOTYPE = 'prototype';
var SCRIPT = 'script';
var IE_PROTO = sharedKey('IE_PROTO');

var EmptyConstructor = function () { /* empty */ };

var scriptTag = function (content) {
  return LT + SCRIPT + GT + content + LT + '/' + SCRIPT + GT;
};

// Create object with fake `null` prototype: use ActiveX Object with cleared prototype
var NullProtoObjectViaActiveX = function (activeXDocument) {
  activeXDocument.write(scriptTag(''));
  activeXDocument.close();
  var temp = activeXDocument.parentWindow.Object;
  activeXDocument = null; // avoid memory leak
  return temp;
};

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var NullProtoObjectViaIFrame = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = documentCreateElement('iframe');
  var JS = 'java' + SCRIPT + ':';
  var iframeDocument;
  iframe.style.display = 'none';
  html.appendChild(iframe);
  // https://github.com/zloirock/core-js/issues/475
  iframe.src = String(JS);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(scriptTag('document.F=Object'));
  iframeDocument.close();
  return iframeDocument.F;
};

// Check for document.domain and active x support
// No need to use active x approach when document.domain is not set
// see https://github.com/es-shims/es5-shim/issues/150
// variation of https://github.com/kitcambridge/es5-shim/commit/4f738ac066346
// avoid IE GC bug
var activeXDocument;
var NullProtoObject = function () {
  try {
    /* global ActiveXObject */
    activeXDocument = document.domain && new ActiveXObject('htmlfile');
  } catch (error) { /* ignore */ }
  NullProtoObject = activeXDocument ? NullProtoObjectViaActiveX(activeXDocument) : NullProtoObjectViaIFrame();
  var length = enumBugKeys.length;
  while (length--) delete NullProtoObject[PROTOTYPE][enumBugKeys[length]];
  return NullProtoObject();
};

hiddenKeys[IE_PROTO] = true;

// `Object.create` method
// https://tc39.es/ecma262/#sec-object.create
var objectCreate = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    EmptyConstructor[PROTOTYPE] = anObject(O);
    result = new EmptyConstructor();
    EmptyConstructor[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = NullProtoObject();
  return Properties === undefined ? result : objectDefineProperties(result, Properties);
};

var nativeGetOwnPropertyNames = objectGetOwnPropertyNames.f;

var toString$1 = {}.toString;

var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
  ? Object.getOwnPropertyNames(window) : [];

var getWindowNames = function (it) {
  try {
    return nativeGetOwnPropertyNames(it);
  } catch (error) {
    return windowNames.slice();
  }
};

// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
var f$5 = function getOwnPropertyNames(it) {
  return windowNames && toString$1.call(it) == '[object Window]'
    ? getWindowNames(it)
    : nativeGetOwnPropertyNames(toIndexedObject(it));
};

var objectGetOwnPropertyNamesExternal = {
	f: f$5
};

var WellKnownSymbolsStore = shared('wks');
var Symbol$1 = global_1.Symbol;
var createWellKnownSymbol = useSymbolAsUid ? Symbol$1 : Symbol$1 && Symbol$1.withoutSetter || uid;

var wellKnownSymbol = function (name) {
  if (!has(WellKnownSymbolsStore, name)) {
    if (nativeSymbol && has(Symbol$1, name)) WellKnownSymbolsStore[name] = Symbol$1[name];
    else WellKnownSymbolsStore[name] = createWellKnownSymbol('Symbol.' + name);
  } return WellKnownSymbolsStore[name];
};

var f$6 = wellKnownSymbol;

var wellKnownSymbolWrapped = {
	f: f$6
};

var defineProperty = objectDefineProperty.f;

var defineWellKnownSymbol = function (NAME) {
  var Symbol = path.Symbol || (path.Symbol = {});
  if (!has(Symbol, NAME)) defineProperty(Symbol, NAME, {
    value: wellKnownSymbolWrapped.f(NAME)
  });
};

var defineProperty$1 = objectDefineProperty.f;



var TO_STRING_TAG = wellKnownSymbol('toStringTag');

var setToStringTag = function (it, TAG, STATIC) {
  if (it && !has(it = STATIC ? it : it.prototype, TO_STRING_TAG)) {
    defineProperty$1(it, TO_STRING_TAG, { configurable: true, value: TAG });
  }
};

var aFunction$1 = function (it) {
  if (typeof it != 'function') {
    throw TypeError(String(it) + ' is not a function');
  } return it;
};

// optional / simple context binding
var functionBindContext = function (fn, that, length) {
  aFunction$1(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 0: return function () {
      return fn.call(that);
    };
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};

var SPECIES = wellKnownSymbol('species');

// `ArraySpeciesCreate` abstract operation
// https://tc39.es/ecma262/#sec-arrayspeciescreate
var arraySpeciesCreate = function (originalArray, length) {
  var C;
  if (isArray(originalArray)) {
    C = originalArray.constructor;
    // cross-realm fallback
    if (typeof C == 'function' && (C === Array || isArray(C.prototype))) C = undefined;
    else if (isObject(C)) {
      C = C[SPECIES];
      if (C === null) C = undefined;
    }
  } return new (C === undefined ? Array : C)(length === 0 ? 0 : length);
};

var push = [].push;

// `Array.prototype.{ forEach, map, filter, some, every, find, findIndex, filterOut }` methods implementation
var createMethod$1 = function (TYPE) {
  var IS_MAP = TYPE == 1;
  var IS_FILTER = TYPE == 2;
  var IS_SOME = TYPE == 3;
  var IS_EVERY = TYPE == 4;
  var IS_FIND_INDEX = TYPE == 6;
  var IS_FILTER_OUT = TYPE == 7;
  var NO_HOLES = TYPE == 5 || IS_FIND_INDEX;
  return function ($this, callbackfn, that, specificCreate) {
    var O = toObject($this);
    var self = indexedObject(O);
    var boundFunction = functionBindContext(callbackfn, that, 3);
    var length = toLength(self.length);
    var index = 0;
    var create = specificCreate || arraySpeciesCreate;
    var target = IS_MAP ? create($this, length) : IS_FILTER || IS_FILTER_OUT ? create($this, 0) : undefined;
    var value, result;
    for (;length > index; index++) if (NO_HOLES || index in self) {
      value = self[index];
      result = boundFunction(value, index, O);
      if (TYPE) {
        if (IS_MAP) target[index] = result; // map
        else if (result) switch (TYPE) {
          case 3: return true;              // some
          case 5: return value;             // find
          case 6: return index;             // findIndex
          case 2: push.call(target, value); // filter
        } else switch (TYPE) {
          case 4: return false;             // every
          case 7: push.call(target, value); // filterOut
        }
      }
    }
    return IS_FIND_INDEX ? -1 : IS_SOME || IS_EVERY ? IS_EVERY : target;
  };
};

var arrayIteration = {
  // `Array.prototype.forEach` method
  // https://tc39.es/ecma262/#sec-array.prototype.foreach
  forEach: createMethod$1(0),
  // `Array.prototype.map` method
  // https://tc39.es/ecma262/#sec-array.prototype.map
  map: createMethod$1(1),
  // `Array.prototype.filter` method
  // https://tc39.es/ecma262/#sec-array.prototype.filter
  filter: createMethod$1(2),
  // `Array.prototype.some` method
  // https://tc39.es/ecma262/#sec-array.prototype.some
  some: createMethod$1(3),
  // `Array.prototype.every` method
  // https://tc39.es/ecma262/#sec-array.prototype.every
  every: createMethod$1(4),
  // `Array.prototype.find` method
  // https://tc39.es/ecma262/#sec-array.prototype.find
  find: createMethod$1(5),
  // `Array.prototype.findIndex` method
  // https://tc39.es/ecma262/#sec-array.prototype.findIndex
  findIndex: createMethod$1(6),
  // `Array.prototype.filterOut` method
  // https://github.com/tc39/proposal-array-filtering
  filterOut: createMethod$1(7)
};

var $forEach = arrayIteration.forEach;

var HIDDEN = sharedKey('hidden');
var SYMBOL = 'Symbol';
var PROTOTYPE$1 = 'prototype';
var TO_PRIMITIVE = wellKnownSymbol('toPrimitive');
var setInternalState = internalState.set;
var getInternalState = internalState.getterFor(SYMBOL);
var ObjectPrototype = Object[PROTOTYPE$1];
var $Symbol = global_1.Symbol;
var $stringify = getBuiltIn('JSON', 'stringify');
var nativeGetOwnPropertyDescriptor$1 = objectGetOwnPropertyDescriptor.f;
var nativeDefineProperty$1 = objectDefineProperty.f;
var nativeGetOwnPropertyNames$1 = objectGetOwnPropertyNamesExternal.f;
var nativePropertyIsEnumerable$1 = objectPropertyIsEnumerable.f;
var AllSymbols = shared('symbols');
var ObjectPrototypeSymbols = shared('op-symbols');
var StringToSymbolRegistry = shared('string-to-symbol-registry');
var SymbolToStringRegistry = shared('symbol-to-string-registry');
var WellKnownSymbolsStore$1 = shared('wks');
var QObject = global_1.QObject;
// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
var USE_SETTER = !QObject || !QObject[PROTOTYPE$1] || !QObject[PROTOTYPE$1].findChild;

// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
var setSymbolDescriptor = descriptors && fails(function () {
  return objectCreate(nativeDefineProperty$1({}, 'a', {
    get: function () { return nativeDefineProperty$1(this, 'a', { value: 7 }).a; }
  })).a != 7;
}) ? function (O, P, Attributes) {
  var ObjectPrototypeDescriptor = nativeGetOwnPropertyDescriptor$1(ObjectPrototype, P);
  if (ObjectPrototypeDescriptor) delete ObjectPrototype[P];
  nativeDefineProperty$1(O, P, Attributes);
  if (ObjectPrototypeDescriptor && O !== ObjectPrototype) {
    nativeDefineProperty$1(ObjectPrototype, P, ObjectPrototypeDescriptor);
  }
} : nativeDefineProperty$1;

var wrap = function (tag, description) {
  var symbol = AllSymbols[tag] = objectCreate($Symbol[PROTOTYPE$1]);
  setInternalState(symbol, {
    type: SYMBOL,
    tag: tag,
    description: description
  });
  if (!descriptors) symbol.description = description;
  return symbol;
};

var isSymbol = useSymbolAsUid ? function (it) {
  return typeof it == 'symbol';
} : function (it) {
  return Object(it) instanceof $Symbol;
};

var $defineProperty = function defineProperty(O, P, Attributes) {
  if (O === ObjectPrototype) $defineProperty(ObjectPrototypeSymbols, P, Attributes);
  anObject(O);
  var key = toPrimitive(P, true);
  anObject(Attributes);
  if (has(AllSymbols, key)) {
    if (!Attributes.enumerable) {
      if (!has(O, HIDDEN)) nativeDefineProperty$1(O, HIDDEN, createPropertyDescriptor(1, {}));
      O[HIDDEN][key] = true;
    } else {
      if (has(O, HIDDEN) && O[HIDDEN][key]) O[HIDDEN][key] = false;
      Attributes = objectCreate(Attributes, { enumerable: createPropertyDescriptor(0, false) });
    } return setSymbolDescriptor(O, key, Attributes);
  } return nativeDefineProperty$1(O, key, Attributes);
};

var $defineProperties = function defineProperties(O, Properties) {
  anObject(O);
  var properties = toIndexedObject(Properties);
  var keys = objectKeys(properties).concat($getOwnPropertySymbols(properties));
  $forEach(keys, function (key) {
    if (!descriptors || $propertyIsEnumerable.call(properties, key)) $defineProperty(O, key, properties[key]);
  });
  return O;
};

var $create = function create(O, Properties) {
  return Properties === undefined ? objectCreate(O) : $defineProperties(objectCreate(O), Properties);
};

var $propertyIsEnumerable = function propertyIsEnumerable(V) {
  var P = toPrimitive(V, true);
  var enumerable = nativePropertyIsEnumerable$1.call(this, P);
  if (this === ObjectPrototype && has(AllSymbols, P) && !has(ObjectPrototypeSymbols, P)) return false;
  return enumerable || !has(this, P) || !has(AllSymbols, P) || has(this, HIDDEN) && this[HIDDEN][P] ? enumerable : true;
};

var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(O, P) {
  var it = toIndexedObject(O);
  var key = toPrimitive(P, true);
  if (it === ObjectPrototype && has(AllSymbols, key) && !has(ObjectPrototypeSymbols, key)) return;
  var descriptor = nativeGetOwnPropertyDescriptor$1(it, key);
  if (descriptor && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) {
    descriptor.enumerable = true;
  }
  return descriptor;
};

var $getOwnPropertyNames = function getOwnPropertyNames(O) {
  var names = nativeGetOwnPropertyNames$1(toIndexedObject(O));
  var result = [];
  $forEach(names, function (key) {
    if (!has(AllSymbols, key) && !has(hiddenKeys, key)) result.push(key);
  });
  return result;
};

var $getOwnPropertySymbols = function getOwnPropertySymbols(O) {
  var IS_OBJECT_PROTOTYPE = O === ObjectPrototype;
  var names = nativeGetOwnPropertyNames$1(IS_OBJECT_PROTOTYPE ? ObjectPrototypeSymbols : toIndexedObject(O));
  var result = [];
  $forEach(names, function (key) {
    if (has(AllSymbols, key) && (!IS_OBJECT_PROTOTYPE || has(ObjectPrototype, key))) {
      result.push(AllSymbols[key]);
    }
  });
  return result;
};

// `Symbol` constructor
// https://tc39.es/ecma262/#sec-symbol-constructor
if (!nativeSymbol) {
  $Symbol = function Symbol() {
    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor');
    var description = !arguments.length || arguments[0] === undefined ? undefined : String(arguments[0]);
    var tag = uid(description);
    var setter = function (value) {
      if (this === ObjectPrototype) setter.call(ObjectPrototypeSymbols, value);
      if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
      setSymbolDescriptor(this, tag, createPropertyDescriptor(1, value));
    };
    if (descriptors && USE_SETTER) setSymbolDescriptor(ObjectPrototype, tag, { configurable: true, set: setter });
    return wrap(tag, description);
  };

  redefine($Symbol[PROTOTYPE$1], 'toString', function toString() {
    return getInternalState(this).tag;
  });

  redefine($Symbol, 'withoutSetter', function (description) {
    return wrap(uid(description), description);
  });

  objectPropertyIsEnumerable.f = $propertyIsEnumerable;
  objectDefineProperty.f = $defineProperty;
  objectGetOwnPropertyDescriptor.f = $getOwnPropertyDescriptor;
  objectGetOwnPropertyNames.f = objectGetOwnPropertyNamesExternal.f = $getOwnPropertyNames;
  objectGetOwnPropertySymbols.f = $getOwnPropertySymbols;

  wellKnownSymbolWrapped.f = function (name) {
    return wrap(wellKnownSymbol(name), name);
  };

  if (descriptors) {
    // https://github.com/tc39/proposal-Symbol-description
    nativeDefineProperty$1($Symbol[PROTOTYPE$1], 'description', {
      configurable: true,
      get: function description() {
        return getInternalState(this).description;
      }
    });
    {
      redefine(ObjectPrototype, 'propertyIsEnumerable', $propertyIsEnumerable, { unsafe: true });
    }
  }
}

_export({ global: true, wrap: true, forced: !nativeSymbol, sham: !nativeSymbol }, {
  Symbol: $Symbol
});

$forEach(objectKeys(WellKnownSymbolsStore$1), function (name) {
  defineWellKnownSymbol(name);
});

_export({ target: SYMBOL, stat: true, forced: !nativeSymbol }, {
  // `Symbol.for` method
  // https://tc39.es/ecma262/#sec-symbol.for
  'for': function (key) {
    var string = String(key);
    if (has(StringToSymbolRegistry, string)) return StringToSymbolRegistry[string];
    var symbol = $Symbol(string);
    StringToSymbolRegistry[string] = symbol;
    SymbolToStringRegistry[symbol] = string;
    return symbol;
  },
  // `Symbol.keyFor` method
  // https://tc39.es/ecma262/#sec-symbol.keyfor
  keyFor: function keyFor(sym) {
    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol');
    if (has(SymbolToStringRegistry, sym)) return SymbolToStringRegistry[sym];
  },
  useSetter: function () { USE_SETTER = true; },
  useSimple: function () { USE_SETTER = false; }
});

_export({ target: 'Object', stat: true, forced: !nativeSymbol, sham: !descriptors }, {
  // `Object.create` method
  // https://tc39.es/ecma262/#sec-object.create
  create: $create,
  // `Object.defineProperty` method
  // https://tc39.es/ecma262/#sec-object.defineproperty
  defineProperty: $defineProperty,
  // `Object.defineProperties` method
  // https://tc39.es/ecma262/#sec-object.defineproperties
  defineProperties: $defineProperties,
  // `Object.getOwnPropertyDescriptor` method
  // https://tc39.es/ecma262/#sec-object.getownpropertydescriptors
  getOwnPropertyDescriptor: $getOwnPropertyDescriptor
});

_export({ target: 'Object', stat: true, forced: !nativeSymbol }, {
  // `Object.getOwnPropertyNames` method
  // https://tc39.es/ecma262/#sec-object.getownpropertynames
  getOwnPropertyNames: $getOwnPropertyNames,
  // `Object.getOwnPropertySymbols` method
  // https://tc39.es/ecma262/#sec-object.getownpropertysymbols
  getOwnPropertySymbols: $getOwnPropertySymbols
});

// Chrome 38 and 39 `Object.getOwnPropertySymbols` fails on primitives
// https://bugs.chromium.org/p/v8/issues/detail?id=3443
_export({ target: 'Object', stat: true, forced: fails(function () { objectGetOwnPropertySymbols.f(1); }) }, {
  getOwnPropertySymbols: function getOwnPropertySymbols(it) {
    return objectGetOwnPropertySymbols.f(toObject(it));
  }
});

// `JSON.stringify` method behavior with symbols
// https://tc39.es/ecma262/#sec-json.stringify
if ($stringify) {
  var FORCED_JSON_STRINGIFY = !nativeSymbol || fails(function () {
    var symbol = $Symbol();
    // MS Edge converts symbol values to JSON as {}
    return $stringify([symbol]) != '[null]'
      // WebKit converts symbol values to JSON as null
      || $stringify({ a: symbol }) != '{}'
      // V8 throws on boxed symbols
      || $stringify(Object(symbol)) != '{}';
  });

  _export({ target: 'JSON', stat: true, forced: FORCED_JSON_STRINGIFY }, {
    // eslint-disable-next-line no-unused-vars
    stringify: function stringify(it, replacer, space) {
      var args = [it];
      var index = 1;
      var $replacer;
      while (arguments.length > index) args.push(arguments[index++]);
      $replacer = replacer;
      if (!isObject(replacer) && it === undefined || isSymbol(it)) return; // IE8 returns string on undefined
      if (!isArray(replacer)) replacer = function (key, value) {
        if (typeof $replacer == 'function') value = $replacer.call(this, key, value);
        if (!isSymbol(value)) return value;
      };
      args[1] = replacer;
      return $stringify.apply(null, args);
    }
  });
}

// `Symbol.prototype[@@toPrimitive]` method
// https://tc39.es/ecma262/#sec-symbol.prototype-@@toprimitive
if (!$Symbol[PROTOTYPE$1][TO_PRIMITIVE]) {
  createNonEnumerableProperty($Symbol[PROTOTYPE$1], TO_PRIMITIVE, $Symbol[PROTOTYPE$1].valueOf);
}
// `Symbol.prototype[@@toStringTag]` property
// https://tc39.es/ecma262/#sec-symbol.prototype-@@tostringtag
setToStringTag($Symbol, SYMBOL);

hiddenKeys[HIDDEN] = true;

var defineProperty$2 = objectDefineProperty.f;


var NativeSymbol = global_1.Symbol;

if (descriptors && typeof NativeSymbol == 'function' && (!('description' in NativeSymbol.prototype) ||
  // Safari 12 bug
  NativeSymbol().description !== undefined
)) {
  var EmptyStringDescriptionStore = {};
  // wrap Symbol constructor for correct work with undefined description
  var SymbolWrapper = function Symbol() {
    var description = arguments.length < 1 || arguments[0] === undefined ? undefined : String(arguments[0]);
    var result = this instanceof SymbolWrapper
      ? new NativeSymbol(description)
      // in Edge 13, String(Symbol(undefined)) === 'Symbol(undefined)'
      : description === undefined ? NativeSymbol() : NativeSymbol(description);
    if (description === '') EmptyStringDescriptionStore[result] = true;
    return result;
  };
  copyConstructorProperties(SymbolWrapper, NativeSymbol);
  var symbolPrototype = SymbolWrapper.prototype = NativeSymbol.prototype;
  symbolPrototype.constructor = SymbolWrapper;

  var symbolToString = symbolPrototype.toString;
  var native = String(NativeSymbol('test')) == 'Symbol(test)';
  var regexp = /^Symbol\((.*)\)[^)]+$/;
  defineProperty$2(symbolPrototype, 'description', {
    configurable: true,
    get: function description() {
      var symbol = isObject(this) ? this.valueOf() : this;
      var string = symbolToString.call(symbol);
      if (has(EmptyStringDescriptionStore, symbol)) return '';
      var desc = native ? string.slice(7, -1) : string.replace(regexp, '$1');
      return desc === '' ? undefined : desc;
    }
  });

  _export({ global: true, forced: true }, {
    Symbol: SymbolWrapper
  });
}

// `Symbol.iterator` well-known symbol
// https://tc39.es/ecma262/#sec-symbol.iterator
defineWellKnownSymbol('iterator');

// `Symbol.species` well-known symbol
// https://tc39.es/ecma262/#sec-symbol.species
defineWellKnownSymbol('species');

var createProperty = function (object, key, value) {
  var propertyKey = toPrimitive(key);
  if (propertyKey in object) objectDefineProperty.f(object, propertyKey, createPropertyDescriptor(0, value));
  else object[propertyKey] = value;
};

var engineUserAgent = getBuiltIn('navigator', 'userAgent') || '';

var process = global_1.process;
var versions = process && process.versions;
var v8 = versions && versions.v8;
var match, version;

if (v8) {
  match = v8.split('.');
  version = match[0] + match[1];
} else if (engineUserAgent) {
  match = engineUserAgent.match(/Edge\/(\d+)/);
  if (!match || match[1] >= 74) {
    match = engineUserAgent.match(/Chrome\/(\d+)/);
    if (match) version = match[1];
  }
}

var engineV8Version = version && +version;

var SPECIES$1 = wellKnownSymbol('species');

var arrayMethodHasSpeciesSupport = function (METHOD_NAME) {
  // We can't use this feature detection in V8 since it causes
  // deoptimization and serious performance degradation
  // https://github.com/zloirock/core-js/issues/677
  return engineV8Version >= 51 || !fails(function () {
    var array = [];
    var constructor = array.constructor = {};
    constructor[SPECIES$1] = function () {
      return { foo: 1 };
    };
    return array[METHOD_NAME](Boolean).foo !== 1;
  });
};

var IS_CONCAT_SPREADABLE = wellKnownSymbol('isConcatSpreadable');
var MAX_SAFE_INTEGER = 0x1FFFFFFFFFFFFF;
var MAXIMUM_ALLOWED_INDEX_EXCEEDED = 'Maximum allowed index exceeded';

// We can't use this feature detection in V8 since it causes
// deoptimization and serious performance degradation
// https://github.com/zloirock/core-js/issues/679
var IS_CONCAT_SPREADABLE_SUPPORT = engineV8Version >= 51 || !fails(function () {
  var array = [];
  array[IS_CONCAT_SPREADABLE] = false;
  return array.concat()[0] !== array;
});

var SPECIES_SUPPORT = arrayMethodHasSpeciesSupport('concat');

var isConcatSpreadable = function (O) {
  if (!isObject(O)) return false;
  var spreadable = O[IS_CONCAT_SPREADABLE];
  return spreadable !== undefined ? !!spreadable : isArray(O);
};

var FORCED = !IS_CONCAT_SPREADABLE_SUPPORT || !SPECIES_SUPPORT;

// `Array.prototype.concat` method
// https://tc39.es/ecma262/#sec-array.prototype.concat
// with adding support of @@isConcatSpreadable and @@species
_export({ target: 'Array', proto: true, forced: FORCED }, {
  concat: function concat(arg) { // eslint-disable-line no-unused-vars
    var O = toObject(this);
    var A = arraySpeciesCreate(O, 0);
    var n = 0;
    var i, k, length, len, E;
    for (i = -1, length = arguments.length; i < length; i++) {
      E = i === -1 ? O : arguments[i];
      if (isConcatSpreadable(E)) {
        len = toLength(E.length);
        if (n + len > MAX_SAFE_INTEGER) throw TypeError(MAXIMUM_ALLOWED_INDEX_EXCEEDED);
        for (k = 0; k < len; k++, n++) if (k in E) createProperty(A, n, E[k]);
      } else {
        if (n >= MAX_SAFE_INTEGER) throw TypeError(MAXIMUM_ALLOWED_INDEX_EXCEEDED);
        createProperty(A, n++, E);
      }
    }
    A.length = n;
    return A;
  }
});

var arrayMethodIsStrict = function (METHOD_NAME, argument) {
  var method = [][METHOD_NAME];
  return !!method && fails(function () {
    // eslint-disable-next-line no-useless-call,no-throw-literal
    method.call(null, argument || function () { throw 1; }, 1);
  });
};

var defineProperty$3 = Object.defineProperty;
var cache = {};

var thrower = function (it) { throw it; };

var arrayMethodUsesToLength = function (METHOD_NAME, options) {
  if (has(cache, METHOD_NAME)) return cache[METHOD_NAME];
  if (!options) options = {};
  var method = [][METHOD_NAME];
  var ACCESSORS = has(options, 'ACCESSORS') ? options.ACCESSORS : false;
  var argument0 = has(options, 0) ? options[0] : thrower;
  var argument1 = has(options, 1) ? options[1] : undefined;

  return cache[METHOD_NAME] = !!method && !fails(function () {
    if (ACCESSORS && !descriptors) return true;
    var O = { length: -1 };

    if (ACCESSORS) defineProperty$3(O, 1, { enumerable: true, get: thrower });
    else O[1] = 1;

    method.call(O, argument0, argument1);
  });
};

var $every = arrayIteration.every;



var STRICT_METHOD = arrayMethodIsStrict('every');
var USES_TO_LENGTH = arrayMethodUsesToLength('every');

// `Array.prototype.every` method
// https://tc39.es/ecma262/#sec-array.prototype.every
_export({ target: 'Array', proto: true, forced: !STRICT_METHOD || !USES_TO_LENGTH }, {
  every: function every(callbackfn /* , thisArg */) {
    return $every(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
  }
});

// `Array.prototype.fill` method implementation
// https://tc39.es/ecma262/#sec-array.prototype.fill
var arrayFill = function fill(value /* , start = 0, end = @length */) {
  var O = toObject(this);
  var length = toLength(O.length);
  var argumentsLength = arguments.length;
  var index = toAbsoluteIndex(argumentsLength > 1 ? arguments[1] : undefined, length);
  var end = argumentsLength > 2 ? arguments[2] : undefined;
  var endPos = end === undefined ? length : toAbsoluteIndex(end, length);
  while (endPos > index) O[index++] = value;
  return O;
};

var UNSCOPABLES = wellKnownSymbol('unscopables');
var ArrayPrototype = Array.prototype;

// Array.prototype[@@unscopables]
// https://tc39.es/ecma262/#sec-array.prototype-@@unscopables
if (ArrayPrototype[UNSCOPABLES] == undefined) {
  objectDefineProperty.f(ArrayPrototype, UNSCOPABLES, {
    configurable: true,
    value: objectCreate(null)
  });
}

// add a key to Array.prototype[@@unscopables]
var addToUnscopables = function (key) {
  ArrayPrototype[UNSCOPABLES][key] = true;
};

// `Array.prototype.fill` method
// https://tc39.es/ecma262/#sec-array.prototype.fill
_export({ target: 'Array', proto: true }, {
  fill: arrayFill
});

// https://tc39.es/ecma262/#sec-array.prototype-@@unscopables
addToUnscopables('fill');

var $filter = arrayIteration.filter;



var HAS_SPECIES_SUPPORT = arrayMethodHasSpeciesSupport('filter');
// Edge 14- issue
var USES_TO_LENGTH$1 = arrayMethodUsesToLength('filter');

// `Array.prototype.filter` method
// https://tc39.es/ecma262/#sec-array.prototype.filter
// with adding support of @@species
_export({ target: 'Array', proto: true, forced: !HAS_SPECIES_SUPPORT || !USES_TO_LENGTH$1 }, {
  filter: function filter(callbackfn /* , thisArg */) {
    return $filter(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
  }
});

var $find = arrayIteration.find;



var FIND = 'find';
var SKIPS_HOLES = true;

var USES_TO_LENGTH$2 = arrayMethodUsesToLength(FIND);

// Shouldn't skip holes
if (FIND in []) Array(1)[FIND](function () { SKIPS_HOLES = false; });

// `Array.prototype.find` method
// https://tc39.es/ecma262/#sec-array.prototype.find
_export({ target: 'Array', proto: true, forced: SKIPS_HOLES || !USES_TO_LENGTH$2 }, {
  find: function find(callbackfn /* , that = undefined */) {
    return $find(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
  }
});

// https://tc39.es/ecma262/#sec-array.prototype-@@unscopables
addToUnscopables(FIND);

var $forEach$1 = arrayIteration.forEach;



var STRICT_METHOD$1 = arrayMethodIsStrict('forEach');
var USES_TO_LENGTH$3 = arrayMethodUsesToLength('forEach');

// `Array.prototype.forEach` method implementation
// https://tc39.es/ecma262/#sec-array.prototype.foreach
var arrayForEach = (!STRICT_METHOD$1 || !USES_TO_LENGTH$3) ? function forEach(callbackfn /* , thisArg */) {
  return $forEach$1(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
} : [].forEach;

// `Array.prototype.forEach` method
// https://tc39.es/ecma262/#sec-array.prototype.foreach
_export({ target: 'Array', proto: true, forced: [].forEach != arrayForEach }, {
  forEach: arrayForEach
});

var iteratorClose = function (iterator) {
  var returnMethod = iterator['return'];
  if (returnMethod !== undefined) {
    return anObject(returnMethod.call(iterator)).value;
  }
};

// call something on iterator step with safe closing on error
var callWithSafeIterationClosing = function (iterator, fn, value, ENTRIES) {
  try {
    return ENTRIES ? fn(anObject(value)[0], value[1]) : fn(value);
  // 7.4.6 IteratorClose(iterator, completion)
  } catch (error) {
    iteratorClose(iterator);
    throw error;
  }
};

var iterators = {};

var ITERATOR = wellKnownSymbol('iterator');
var ArrayPrototype$1 = Array.prototype;

// check on default Array iterator
var isArrayIteratorMethod = function (it) {
  return it !== undefined && (iterators.Array === it || ArrayPrototype$1[ITERATOR] === it);
};

var TO_STRING_TAG$1 = wellKnownSymbol('toStringTag');
var test = {};

test[TO_STRING_TAG$1] = 'z';

var toStringTagSupport = String(test) === '[object z]';

var TO_STRING_TAG$2 = wellKnownSymbol('toStringTag');
// ES3 wrong here
var CORRECT_ARGUMENTS = classofRaw(function () { return arguments; }()) == 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function (it, key) {
  try {
    return it[key];
  } catch (error) { /* empty */ }
};

// getting tag from ES6+ `Object.prototype.toString`
var classof = toStringTagSupport ? classofRaw : function (it) {
  var O, tag, result;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (tag = tryGet(O = Object(it), TO_STRING_TAG$2)) == 'string' ? tag
    // builtinTag case
    : CORRECT_ARGUMENTS ? classofRaw(O)
    // ES3 arguments fallback
    : (result = classofRaw(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : result;
};

var ITERATOR$1 = wellKnownSymbol('iterator');

var getIteratorMethod = function (it) {
  if (it != undefined) return it[ITERATOR$1]
    || it['@@iterator']
    || iterators[classof(it)];
};

// `Array.from` method implementation
// https://tc39.es/ecma262/#sec-array.from
var arrayFrom = function from(arrayLike /* , mapfn = undefined, thisArg = undefined */) {
  var O = toObject(arrayLike);
  var C = typeof this == 'function' ? this : Array;
  var argumentsLength = arguments.length;
  var mapfn = argumentsLength > 1 ? arguments[1] : undefined;
  var mapping = mapfn !== undefined;
  var iteratorMethod = getIteratorMethod(O);
  var index = 0;
  var length, result, step, iterator, next, value;
  if (mapping) mapfn = functionBindContext(mapfn, argumentsLength > 2 ? arguments[2] : undefined, 2);
  // if the target is not iterable or it's an array with the default iterator - use a simple case
  if (iteratorMethod != undefined && !(C == Array && isArrayIteratorMethod(iteratorMethod))) {
    iterator = iteratorMethod.call(O);
    next = iterator.next;
    result = new C();
    for (;!(step = next.call(iterator)).done; index++) {
      value = mapping ? callWithSafeIterationClosing(iterator, mapfn, [step.value, index], true) : step.value;
      createProperty(result, index, value);
    }
  } else {
    length = toLength(O.length);
    result = new C(length);
    for (;length > index; index++) {
      value = mapping ? mapfn(O[index], index) : O[index];
      createProperty(result, index, value);
    }
  }
  result.length = index;
  return result;
};

var ITERATOR$2 = wellKnownSymbol('iterator');
var SAFE_CLOSING = false;

try {
  var called = 0;
  var iteratorWithReturn = {
    next: function () {
      return { done: !!called++ };
    },
    'return': function () {
      SAFE_CLOSING = true;
    }
  };
  iteratorWithReturn[ITERATOR$2] = function () {
    return this;
  };
  // eslint-disable-next-line no-throw-literal
  Array.from(iteratorWithReturn, function () { throw 2; });
} catch (error) { /* empty */ }

var checkCorrectnessOfIteration = function (exec, SKIP_CLOSING) {
  if (!SKIP_CLOSING && !SAFE_CLOSING) return false;
  var ITERATION_SUPPORT = false;
  try {
    var object = {};
    object[ITERATOR$2] = function () {
      return {
        next: function () {
          return { done: ITERATION_SUPPORT = true };
        }
      };
    };
    exec(object);
  } catch (error) { /* empty */ }
  return ITERATION_SUPPORT;
};

var INCORRECT_ITERATION = !checkCorrectnessOfIteration(function (iterable) {
  Array.from(iterable);
});

// `Array.from` method
// https://tc39.es/ecma262/#sec-array.from
_export({ target: 'Array', stat: true, forced: INCORRECT_ITERATION }, {
  from: arrayFrom
});

var $includes = arrayIncludes.includes;



var USES_TO_LENGTH$4 = arrayMethodUsesToLength('indexOf', { ACCESSORS: true, 1: 0 });

// `Array.prototype.includes` method
// https://tc39.es/ecma262/#sec-array.prototype.includes
_export({ target: 'Array', proto: true, forced: !USES_TO_LENGTH$4 }, {
  includes: function includes(el /* , fromIndex = 0 */) {
    return $includes(this, el, arguments.length > 1 ? arguments[1] : undefined);
  }
});

// https://tc39.es/ecma262/#sec-array.prototype-@@unscopables
addToUnscopables('includes');

var $indexOf = arrayIncludes.indexOf;



var nativeIndexOf = [].indexOf;

var NEGATIVE_ZERO = !!nativeIndexOf && 1 / [1].indexOf(1, -0) < 0;
var STRICT_METHOD$2 = arrayMethodIsStrict('indexOf');
var USES_TO_LENGTH$5 = arrayMethodUsesToLength('indexOf', { ACCESSORS: true, 1: 0 });

// `Array.prototype.indexOf` method
// https://tc39.es/ecma262/#sec-array.prototype.indexof
_export({ target: 'Array', proto: true, forced: NEGATIVE_ZERO || !STRICT_METHOD$2 || !USES_TO_LENGTH$5 }, {
  indexOf: function indexOf(searchElement /* , fromIndex = 0 */) {
    return NEGATIVE_ZERO
      // convert -0 to +0
      ? nativeIndexOf.apply(this, arguments) || 0
      : $indexOf(this, searchElement, arguments.length > 1 ? arguments[1] : undefined);
  }
});

var correctPrototypeGetter = !fails(function () {
  function F() { /* empty */ }
  F.prototype.constructor = null;
  return Object.getPrototypeOf(new F()) !== F.prototype;
});

var IE_PROTO$1 = sharedKey('IE_PROTO');
var ObjectPrototype$1 = Object.prototype;

// `Object.getPrototypeOf` method
// https://tc39.es/ecma262/#sec-object.getprototypeof
var objectGetPrototypeOf = correctPrototypeGetter ? Object.getPrototypeOf : function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO$1)) return O[IE_PROTO$1];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectPrototype$1 : null;
};

var ITERATOR$3 = wellKnownSymbol('iterator');
var BUGGY_SAFARI_ITERATORS = false;

var returnThis = function () { return this; };

// `%IteratorPrototype%` object
// https://tc39.es/ecma262/#sec-%iteratorprototype%-object
var IteratorPrototype, PrototypeOfArrayIteratorPrototype, arrayIterator;

if ([].keys) {
  arrayIterator = [].keys();
  // Safari 8 has buggy iterators w/o `next`
  if (!('next' in arrayIterator)) BUGGY_SAFARI_ITERATORS = true;
  else {
    PrototypeOfArrayIteratorPrototype = objectGetPrototypeOf(objectGetPrototypeOf(arrayIterator));
    if (PrototypeOfArrayIteratorPrototype !== Object.prototype) IteratorPrototype = PrototypeOfArrayIteratorPrototype;
  }
}

if (IteratorPrototype == undefined) IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
if ( !has(IteratorPrototype, ITERATOR$3)) {
  createNonEnumerableProperty(IteratorPrototype, ITERATOR$3, returnThis);
}

var iteratorsCore = {
  IteratorPrototype: IteratorPrototype,
  BUGGY_SAFARI_ITERATORS: BUGGY_SAFARI_ITERATORS
};

var IteratorPrototype$1 = iteratorsCore.IteratorPrototype;





var returnThis$1 = function () { return this; };

var createIteratorConstructor = function (IteratorConstructor, NAME, next) {
  var TO_STRING_TAG = NAME + ' Iterator';
  IteratorConstructor.prototype = objectCreate(IteratorPrototype$1, { next: createPropertyDescriptor(1, next) });
  setToStringTag(IteratorConstructor, TO_STRING_TAG, false);
  iterators[TO_STRING_TAG] = returnThis$1;
  return IteratorConstructor;
};

var aPossiblePrototype = function (it) {
  if (!isObject(it) && it !== null) {
    throw TypeError("Can't set " + String(it) + ' as a prototype');
  } return it;
};

// `Object.setPrototypeOf` method
// https://tc39.es/ecma262/#sec-object.setprototypeof
// Works with __proto__ only. Old v8 can't work with null proto objects.
/* eslint-disable no-proto */
var objectSetPrototypeOf = Object.setPrototypeOf || ('__proto__' in {} ? function () {
  var CORRECT_SETTER = false;
  var test = {};
  var setter;
  try {
    setter = Object.getOwnPropertyDescriptor(Object.prototype, '__proto__').set;
    setter.call(test, []);
    CORRECT_SETTER = test instanceof Array;
  } catch (error) { /* empty */ }
  return function setPrototypeOf(O, proto) {
    anObject(O);
    aPossiblePrototype(proto);
    if (CORRECT_SETTER) setter.call(O, proto);
    else O.__proto__ = proto;
    return O;
  };
}() : undefined);

var IteratorPrototype$2 = iteratorsCore.IteratorPrototype;
var BUGGY_SAFARI_ITERATORS$1 = iteratorsCore.BUGGY_SAFARI_ITERATORS;
var ITERATOR$4 = wellKnownSymbol('iterator');
var KEYS = 'keys';
var VALUES = 'values';
var ENTRIES = 'entries';

var returnThis$2 = function () { return this; };

var defineIterator = function (Iterable, NAME, IteratorConstructor, next, DEFAULT, IS_SET, FORCED) {
  createIteratorConstructor(IteratorConstructor, NAME, next);

  var getIterationMethod = function (KIND) {
    if (KIND === DEFAULT && defaultIterator) return defaultIterator;
    if (!BUGGY_SAFARI_ITERATORS$1 && KIND in IterablePrototype) return IterablePrototype[KIND];
    switch (KIND) {
      case KEYS: return function keys() { return new IteratorConstructor(this, KIND); };
      case VALUES: return function values() { return new IteratorConstructor(this, KIND); };
      case ENTRIES: return function entries() { return new IteratorConstructor(this, KIND); };
    } return function () { return new IteratorConstructor(this); };
  };

  var TO_STRING_TAG = NAME + ' Iterator';
  var INCORRECT_VALUES_NAME = false;
  var IterablePrototype = Iterable.prototype;
  var nativeIterator = IterablePrototype[ITERATOR$4]
    || IterablePrototype['@@iterator']
    || DEFAULT && IterablePrototype[DEFAULT];
  var defaultIterator = !BUGGY_SAFARI_ITERATORS$1 && nativeIterator || getIterationMethod(DEFAULT);
  var anyNativeIterator = NAME == 'Array' ? IterablePrototype.entries || nativeIterator : nativeIterator;
  var CurrentIteratorPrototype, methods, KEY;

  // fix native
  if (anyNativeIterator) {
    CurrentIteratorPrototype = objectGetPrototypeOf(anyNativeIterator.call(new Iterable()));
    if (IteratorPrototype$2 !== Object.prototype && CurrentIteratorPrototype.next) {
      if ( objectGetPrototypeOf(CurrentIteratorPrototype) !== IteratorPrototype$2) {
        if (objectSetPrototypeOf) {
          objectSetPrototypeOf(CurrentIteratorPrototype, IteratorPrototype$2);
        } else if (typeof CurrentIteratorPrototype[ITERATOR$4] != 'function') {
          createNonEnumerableProperty(CurrentIteratorPrototype, ITERATOR$4, returnThis$2);
        }
      }
      // Set @@toStringTag to native iterators
      setToStringTag(CurrentIteratorPrototype, TO_STRING_TAG, true);
    }
  }

  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEFAULT == VALUES && nativeIterator && nativeIterator.name !== VALUES) {
    INCORRECT_VALUES_NAME = true;
    defaultIterator = function values() { return nativeIterator.call(this); };
  }

  // define iterator
  if ( IterablePrototype[ITERATOR$4] !== defaultIterator) {
    createNonEnumerableProperty(IterablePrototype, ITERATOR$4, defaultIterator);
  }
  iterators[NAME] = defaultIterator;

  // export additional methods
  if (DEFAULT) {
    methods = {
      values: getIterationMethod(VALUES),
      keys: IS_SET ? defaultIterator : getIterationMethod(KEYS),
      entries: getIterationMethod(ENTRIES)
    };
    if (FORCED) for (KEY in methods) {
      if (BUGGY_SAFARI_ITERATORS$1 || INCORRECT_VALUES_NAME || !(KEY in IterablePrototype)) {
        redefine(IterablePrototype, KEY, methods[KEY]);
      }
    } else _export({ target: NAME, proto: true, forced: BUGGY_SAFARI_ITERATORS$1 || INCORRECT_VALUES_NAME }, methods);
  }

  return methods;
};

var ARRAY_ITERATOR = 'Array Iterator';
var setInternalState$1 = internalState.set;
var getInternalState$1 = internalState.getterFor(ARRAY_ITERATOR);

// `Array.prototype.entries` method
// https://tc39.es/ecma262/#sec-array.prototype.entries
// `Array.prototype.keys` method
// https://tc39.es/ecma262/#sec-array.prototype.keys
// `Array.prototype.values` method
// https://tc39.es/ecma262/#sec-array.prototype.values
// `Array.prototype[@@iterator]` method
// https://tc39.es/ecma262/#sec-array.prototype-@@iterator
// `CreateArrayIterator` internal method
// https://tc39.es/ecma262/#sec-createarrayiterator
var es_array_iterator = defineIterator(Array, 'Array', function (iterated, kind) {
  setInternalState$1(this, {
    type: ARRAY_ITERATOR,
    target: toIndexedObject(iterated), // target
    index: 0,                          // next index
    kind: kind                         // kind
  });
// `%ArrayIteratorPrototype%.next` method
// https://tc39.es/ecma262/#sec-%arrayiteratorprototype%.next
}, function () {
  var state = getInternalState$1(this);
  var target = state.target;
  var kind = state.kind;
  var index = state.index++;
  if (!target || index >= target.length) {
    state.target = undefined;
    return { value: undefined, done: true };
  }
  if (kind == 'keys') return { value: index, done: false };
  if (kind == 'values') return { value: target[index], done: false };
  return { value: [index, target[index]], done: false };
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values%
// https://tc39.es/ecma262/#sec-createunmappedargumentsobject
// https://tc39.es/ecma262/#sec-createmappedargumentsobject
iterators.Arguments = iterators.Array;

// https://tc39.es/ecma262/#sec-array.prototype-@@unscopables
addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');

var nativeJoin = [].join;

var ES3_STRINGS = indexedObject != Object;
var STRICT_METHOD$3 = arrayMethodIsStrict('join', ',');

// `Array.prototype.join` method
// https://tc39.es/ecma262/#sec-array.prototype.join
_export({ target: 'Array', proto: true, forced: ES3_STRINGS || !STRICT_METHOD$3 }, {
  join: function join(separator) {
    return nativeJoin.call(toIndexedObject(this), separator === undefined ? ',' : separator);
  }
});

var min$2 = Math.min;
var nativeLastIndexOf = [].lastIndexOf;
var NEGATIVE_ZERO$1 = !!nativeLastIndexOf && 1 / [1].lastIndexOf(1, -0) < 0;
var STRICT_METHOD$4 = arrayMethodIsStrict('lastIndexOf');
// For preventing possible almost infinite loop in non-standard implementations, test the forward version of the method
var USES_TO_LENGTH$6 = arrayMethodUsesToLength('indexOf', { ACCESSORS: true, 1: 0 });
var FORCED$1 = NEGATIVE_ZERO$1 || !STRICT_METHOD$4 || !USES_TO_LENGTH$6;

// `Array.prototype.lastIndexOf` method implementation
// https://tc39.es/ecma262/#sec-array.prototype.lastindexof
var arrayLastIndexOf = FORCED$1 ? function lastIndexOf(searchElement /* , fromIndex = @[*-1] */) {
  // convert -0 to +0
  if (NEGATIVE_ZERO$1) return nativeLastIndexOf.apply(this, arguments) || 0;
  var O = toIndexedObject(this);
  var length = toLength(O.length);
  var index = length - 1;
  if (arguments.length > 1) index = min$2(index, toInteger(arguments[1]));
  if (index < 0) index = length + index;
  for (;index >= 0; index--) if (index in O && O[index] === searchElement) return index || 0;
  return -1;
} : nativeLastIndexOf;

// `Array.prototype.lastIndexOf` method
// https://tc39.es/ecma262/#sec-array.prototype.lastindexof
_export({ target: 'Array', proto: true, forced: arrayLastIndexOf !== [].lastIndexOf }, {
  lastIndexOf: arrayLastIndexOf
});

var $map = arrayIteration.map;



var HAS_SPECIES_SUPPORT$1 = arrayMethodHasSpeciesSupport('map');
// FF49- issue
var USES_TO_LENGTH$7 = arrayMethodUsesToLength('map');

// `Array.prototype.map` method
// https://tc39.es/ecma262/#sec-array.prototype.map
// with adding support of @@species
_export({ target: 'Array', proto: true, forced: !HAS_SPECIES_SUPPORT$1 || !USES_TO_LENGTH$7 }, {
  map: function map(callbackfn /* , thisArg */) {
    return $map(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
  }
});

// `Array.prototype.{ reduce, reduceRight }` methods implementation
var createMethod$2 = function (IS_RIGHT) {
  return function (that, callbackfn, argumentsLength, memo) {
    aFunction$1(callbackfn);
    var O = toObject(that);
    var self = indexedObject(O);
    var length = toLength(O.length);
    var index = IS_RIGHT ? length - 1 : 0;
    var i = IS_RIGHT ? -1 : 1;
    if (argumentsLength < 2) while (true) {
      if (index in self) {
        memo = self[index];
        index += i;
        break;
      }
      index += i;
      if (IS_RIGHT ? index < 0 : length <= index) {
        throw TypeError('Reduce of empty array with no initial value');
      }
    }
    for (;IS_RIGHT ? index >= 0 : length > index; index += i) if (index in self) {
      memo = callbackfn(memo, self[index], index, O);
    }
    return memo;
  };
};

var arrayReduce = {
  // `Array.prototype.reduce` method
  // https://tc39.es/ecma262/#sec-array.prototype.reduce
  left: createMethod$2(false),
  // `Array.prototype.reduceRight` method
  // https://tc39.es/ecma262/#sec-array.prototype.reduceright
  right: createMethod$2(true)
};

var engineIsNode = classofRaw(global_1.process) == 'process';

var $reduce = arrayReduce.left;





var STRICT_METHOD$5 = arrayMethodIsStrict('reduce');
var USES_TO_LENGTH$8 = arrayMethodUsesToLength('reduce', { 1: 0 });
// Chrome 80-82 has a critical bug
// https://bugs.chromium.org/p/chromium/issues/detail?id=1049982
var CHROME_BUG = !engineIsNode && engineV8Version > 79 && engineV8Version < 83;

// `Array.prototype.reduce` method
// https://tc39.es/ecma262/#sec-array.prototype.reduce
_export({ target: 'Array', proto: true, forced: !STRICT_METHOD$5 || !USES_TO_LENGTH$8 || CHROME_BUG }, {
  reduce: function reduce(callbackfn /* , initialValue */) {
    return $reduce(this, callbackfn, arguments.length, arguments.length > 1 ? arguments[1] : undefined);
  }
});

var HAS_SPECIES_SUPPORT$2 = arrayMethodHasSpeciesSupport('slice');
var USES_TO_LENGTH$9 = arrayMethodUsesToLength('slice', { ACCESSORS: true, 0: 0, 1: 2 });

var SPECIES$2 = wellKnownSymbol('species');
var nativeSlice = [].slice;
var max$1 = Math.max;

// `Array.prototype.slice` method
// https://tc39.es/ecma262/#sec-array.prototype.slice
// fallback for not array-like ES3 strings and DOM objects
_export({ target: 'Array', proto: true, forced: !HAS_SPECIES_SUPPORT$2 || !USES_TO_LENGTH$9 }, {
  slice: function slice(start, end) {
    var O = toIndexedObject(this);
    var length = toLength(O.length);
    var k = toAbsoluteIndex(start, length);
    var fin = toAbsoluteIndex(end === undefined ? length : end, length);
    // inline `ArraySpeciesCreate` for usage native `Array#slice` where it's possible
    var Constructor, result, n;
    if (isArray(O)) {
      Constructor = O.constructor;
      // cross-realm fallback
      if (typeof Constructor == 'function' && (Constructor === Array || isArray(Constructor.prototype))) {
        Constructor = undefined;
      } else if (isObject(Constructor)) {
        Constructor = Constructor[SPECIES$2];
        if (Constructor === null) Constructor = undefined;
      }
      if (Constructor === Array || Constructor === undefined) {
        return nativeSlice.call(O, k, fin);
      }
    }
    result = new (Constructor === undefined ? Array : Constructor)(max$1(fin - k, 0));
    for (n = 0; k < fin; k++, n++) if (k in O) createProperty(result, n, O[k]);
    result.length = n;
    return result;
  }
});

var $some = arrayIteration.some;



var STRICT_METHOD$6 = arrayMethodIsStrict('some');
var USES_TO_LENGTH$a = arrayMethodUsesToLength('some');

// `Array.prototype.some` method
// https://tc39.es/ecma262/#sec-array.prototype.some
_export({ target: 'Array', proto: true, forced: !STRICT_METHOD$6 || !USES_TO_LENGTH$a }, {
  some: function some(callbackfn /* , thisArg */) {
    return $some(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
  }
});

var test$1 = [];
var nativeSort = test$1.sort;

// IE8-
var FAILS_ON_UNDEFINED = fails(function () {
  test$1.sort(undefined);
});
// V8 bug
var FAILS_ON_NULL = fails(function () {
  test$1.sort(null);
});
// Old WebKit
var STRICT_METHOD$7 = arrayMethodIsStrict('sort');

var FORCED$2 = FAILS_ON_UNDEFINED || !FAILS_ON_NULL || !STRICT_METHOD$7;

// `Array.prototype.sort` method
// https://tc39.es/ecma262/#sec-array.prototype.sort
_export({ target: 'Array', proto: true, forced: FORCED$2 }, {
  sort: function sort(comparefn) {
    return comparefn === undefined
      ? nativeSort.call(toObject(this))
      : nativeSort.call(toObject(this), aFunction$1(comparefn));
  }
});

var SPECIES$3 = wellKnownSymbol('species');

var setSpecies = function (CONSTRUCTOR_NAME) {
  var Constructor = getBuiltIn(CONSTRUCTOR_NAME);
  var defineProperty = objectDefineProperty.f;

  if (descriptors && Constructor && !Constructor[SPECIES$3]) {
    defineProperty(Constructor, SPECIES$3, {
      configurable: true,
      get: function () { return this; }
    });
  }
};

// `Array[@@species]` getter
// https://tc39.es/ecma262/#sec-get-array-@@species
setSpecies('Array');

var HAS_SPECIES_SUPPORT$3 = arrayMethodHasSpeciesSupport('splice');
var USES_TO_LENGTH$b = arrayMethodUsesToLength('splice', { ACCESSORS: true, 0: 0, 1: 2 });

var max$2 = Math.max;
var min$3 = Math.min;
var MAX_SAFE_INTEGER$1 = 0x1FFFFFFFFFFFFF;
var MAXIMUM_ALLOWED_LENGTH_EXCEEDED = 'Maximum allowed length exceeded';

// `Array.prototype.splice` method
// https://tc39.es/ecma262/#sec-array.prototype.splice
// with adding support of @@species
_export({ target: 'Array', proto: true, forced: !HAS_SPECIES_SUPPORT$3 || !USES_TO_LENGTH$b }, {
  splice: function splice(start, deleteCount /* , ...items */) {
    var O = toObject(this);
    var len = toLength(O.length);
    var actualStart = toAbsoluteIndex(start, len);
    var argumentsLength = arguments.length;
    var insertCount, actualDeleteCount, A, k, from, to;
    if (argumentsLength === 0) {
      insertCount = actualDeleteCount = 0;
    } else if (argumentsLength === 1) {
      insertCount = 0;
      actualDeleteCount = len - actualStart;
    } else {
      insertCount = argumentsLength - 2;
      actualDeleteCount = min$3(max$2(toInteger(deleteCount), 0), len - actualStart);
    }
    if (len + insertCount - actualDeleteCount > MAX_SAFE_INTEGER$1) {
      throw TypeError(MAXIMUM_ALLOWED_LENGTH_EXCEEDED);
    }
    A = arraySpeciesCreate(O, actualDeleteCount);
    for (k = 0; k < actualDeleteCount; k++) {
      from = actualStart + k;
      if (from in O) createProperty(A, k, O[from]);
    }
    A.length = actualDeleteCount;
    if (insertCount < actualDeleteCount) {
      for (k = actualStart; k < len - actualDeleteCount; k++) {
        from = k + actualDeleteCount;
        to = k + insertCount;
        if (from in O) O[to] = O[from];
        else delete O[to];
      }
      for (k = len; k > len - actualDeleteCount + insertCount; k--) delete O[k - 1];
    } else if (insertCount > actualDeleteCount) {
      for (k = len - actualDeleteCount; k > actualStart; k--) {
        from = k + actualDeleteCount - 1;
        to = k + insertCount - 1;
        if (from in O) O[to] = O[from];
        else delete O[to];
      }
    }
    for (k = 0; k < insertCount; k++) {
      O[k + actualStart] = arguments[k + 2];
    }
    O.length = len - actualDeleteCount + insertCount;
    return A;
  }
});

var arrayBufferNative = typeof ArrayBuffer !== 'undefined' && typeof DataView !== 'undefined';

var redefineAll = function (target, src, options) {
  for (var key in src) redefine(target, key, src[key], options);
  return target;
};

var anInstance = function (it, Constructor, name) {
  if (!(it instanceof Constructor)) {
    throw TypeError('Incorrect ' + (name ? name + ' ' : '') + 'invocation');
  } return it;
};

// `ToIndex` abstract operation
// https://tc39.es/ecma262/#sec-toindex
var toIndex = function (it) {
  if (it === undefined) return 0;
  var number = toInteger(it);
  var length = toLength(number);
  if (number !== length) throw RangeError('Wrong length or index');
  return length;
};

// IEEE754 conversions based on https://github.com/feross/ieee754
// eslint-disable-next-line no-shadow-restricted-names
var Infinity$1 = 1 / 0;
var abs = Math.abs;
var pow = Math.pow;
var floor$1 = Math.floor;
var log = Math.log;
var LN2 = Math.LN2;

var pack = function (number, mantissaLength, bytes) {
  var buffer = new Array(bytes);
  var exponentLength = bytes * 8 - mantissaLength - 1;
  var eMax = (1 << exponentLength) - 1;
  var eBias = eMax >> 1;
  var rt = mantissaLength === 23 ? pow(2, -24) - pow(2, -77) : 0;
  var sign = number < 0 || number === 0 && 1 / number < 0 ? 1 : 0;
  var index = 0;
  var exponent, mantissa, c;
  number = abs(number);
  // eslint-disable-next-line no-self-compare
  if (number != number || number === Infinity$1) {
    // eslint-disable-next-line no-self-compare
    mantissa = number != number ? 1 : 0;
    exponent = eMax;
  } else {
    exponent = floor$1(log(number) / LN2);
    if (number * (c = pow(2, -exponent)) < 1) {
      exponent--;
      c *= 2;
    }
    if (exponent + eBias >= 1) {
      number += rt / c;
    } else {
      number += rt * pow(2, 1 - eBias);
    }
    if (number * c >= 2) {
      exponent++;
      c /= 2;
    }
    if (exponent + eBias >= eMax) {
      mantissa = 0;
      exponent = eMax;
    } else if (exponent + eBias >= 1) {
      mantissa = (number * c - 1) * pow(2, mantissaLength);
      exponent = exponent + eBias;
    } else {
      mantissa = number * pow(2, eBias - 1) * pow(2, mantissaLength);
      exponent = 0;
    }
  }
  for (; mantissaLength >= 8; buffer[index++] = mantissa & 255, mantissa /= 256, mantissaLength -= 8);
  exponent = exponent << mantissaLength | mantissa;
  exponentLength += mantissaLength;
  for (; exponentLength > 0; buffer[index++] = exponent & 255, exponent /= 256, exponentLength -= 8);
  buffer[--index] |= sign * 128;
  return buffer;
};

var unpack = function (buffer, mantissaLength) {
  var bytes = buffer.length;
  var exponentLength = bytes * 8 - mantissaLength - 1;
  var eMax = (1 << exponentLength) - 1;
  var eBias = eMax >> 1;
  var nBits = exponentLength - 7;
  var index = bytes - 1;
  var sign = buffer[index--];
  var exponent = sign & 127;
  var mantissa;
  sign >>= 7;
  for (; nBits > 0; exponent = exponent * 256 + buffer[index], index--, nBits -= 8);
  mantissa = exponent & (1 << -nBits) - 1;
  exponent >>= -nBits;
  nBits += mantissaLength;
  for (; nBits > 0; mantissa = mantissa * 256 + buffer[index], index--, nBits -= 8);
  if (exponent === 0) {
    exponent = 1 - eBias;
  } else if (exponent === eMax) {
    return mantissa ? NaN : sign ? -Infinity$1 : Infinity$1;
  } else {
    mantissa = mantissa + pow(2, mantissaLength);
    exponent = exponent - eBias;
  } return (sign ? -1 : 1) * mantissa * pow(2, exponent - mantissaLength);
};

var ieee754 = {
  pack: pack,
  unpack: unpack
};

var getOwnPropertyNames = objectGetOwnPropertyNames.f;
var defineProperty$4 = objectDefineProperty.f;




var getInternalState$2 = internalState.get;
var setInternalState$2 = internalState.set;
var ARRAY_BUFFER = 'ArrayBuffer';
var DATA_VIEW = 'DataView';
var PROTOTYPE$2 = 'prototype';
var WRONG_LENGTH = 'Wrong length';
var WRONG_INDEX = 'Wrong index';
var NativeArrayBuffer = global_1[ARRAY_BUFFER];
var $ArrayBuffer = NativeArrayBuffer;
var $DataView = global_1[DATA_VIEW];
var $DataViewPrototype = $DataView && $DataView[PROTOTYPE$2];
var ObjectPrototype$2 = Object.prototype;
var RangeError$1 = global_1.RangeError;

var packIEEE754 = ieee754.pack;
var unpackIEEE754 = ieee754.unpack;

var packInt8 = function (number) {
  return [number & 0xFF];
};

var packInt16 = function (number) {
  return [number & 0xFF, number >> 8 & 0xFF];
};

var packInt32 = function (number) {
  return [number & 0xFF, number >> 8 & 0xFF, number >> 16 & 0xFF, number >> 24 & 0xFF];
};

var unpackInt32 = function (buffer) {
  return buffer[3] << 24 | buffer[2] << 16 | buffer[1] << 8 | buffer[0];
};

var packFloat32 = function (number) {
  return packIEEE754(number, 23, 4);
};

var packFloat64 = function (number) {
  return packIEEE754(number, 52, 8);
};

var addGetter = function (Constructor, key) {
  defineProperty$4(Constructor[PROTOTYPE$2], key, { get: function () { return getInternalState$2(this)[key]; } });
};

var get$1 = function (view, count, index, isLittleEndian) {
  var intIndex = toIndex(index);
  var store = getInternalState$2(view);
  if (intIndex + count > store.byteLength) throw RangeError$1(WRONG_INDEX);
  var bytes = getInternalState$2(store.buffer).bytes;
  var start = intIndex + store.byteOffset;
  var pack = bytes.slice(start, start + count);
  return isLittleEndian ? pack : pack.reverse();
};

var set$1 = function (view, count, index, conversion, value, isLittleEndian) {
  var intIndex = toIndex(index);
  var store = getInternalState$2(view);
  if (intIndex + count > store.byteLength) throw RangeError$1(WRONG_INDEX);
  var bytes = getInternalState$2(store.buffer).bytes;
  var start = intIndex + store.byteOffset;
  var pack = conversion(+value);
  for (var i = 0; i < count; i++) bytes[start + i] = pack[isLittleEndian ? i : count - i - 1];
};

if (!arrayBufferNative) {
  $ArrayBuffer = function ArrayBuffer(length) {
    anInstance(this, $ArrayBuffer, ARRAY_BUFFER);
    var byteLength = toIndex(length);
    setInternalState$2(this, {
      bytes: arrayFill.call(new Array(byteLength), 0),
      byteLength: byteLength
    });
    if (!descriptors) this.byteLength = byteLength;
  };

  $DataView = function DataView(buffer, byteOffset, byteLength) {
    anInstance(this, $DataView, DATA_VIEW);
    anInstance(buffer, $ArrayBuffer, DATA_VIEW);
    var bufferLength = getInternalState$2(buffer).byteLength;
    var offset = toInteger(byteOffset);
    if (offset < 0 || offset > bufferLength) throw RangeError$1('Wrong offset');
    byteLength = byteLength === undefined ? bufferLength - offset : toLength(byteLength);
    if (offset + byteLength > bufferLength) throw RangeError$1(WRONG_LENGTH);
    setInternalState$2(this, {
      buffer: buffer,
      byteLength: byteLength,
      byteOffset: offset
    });
    if (!descriptors) {
      this.buffer = buffer;
      this.byteLength = byteLength;
      this.byteOffset = offset;
    }
  };

  if (descriptors) {
    addGetter($ArrayBuffer, 'byteLength');
    addGetter($DataView, 'buffer');
    addGetter($DataView, 'byteLength');
    addGetter($DataView, 'byteOffset');
  }

  redefineAll($DataView[PROTOTYPE$2], {
    getInt8: function getInt8(byteOffset) {
      return get$1(this, 1, byteOffset)[0] << 24 >> 24;
    },
    getUint8: function getUint8(byteOffset) {
      return get$1(this, 1, byteOffset)[0];
    },
    getInt16: function getInt16(byteOffset /* , littleEndian */) {
      var bytes = get$1(this, 2, byteOffset, arguments.length > 1 ? arguments[1] : undefined);
      return (bytes[1] << 8 | bytes[0]) << 16 >> 16;
    },
    getUint16: function getUint16(byteOffset /* , littleEndian */) {
      var bytes = get$1(this, 2, byteOffset, arguments.length > 1 ? arguments[1] : undefined);
      return bytes[1] << 8 | bytes[0];
    },
    getInt32: function getInt32(byteOffset /* , littleEndian */) {
      return unpackInt32(get$1(this, 4, byteOffset, arguments.length > 1 ? arguments[1] : undefined));
    },
    getUint32: function getUint32(byteOffset /* , littleEndian */) {
      return unpackInt32(get$1(this, 4, byteOffset, arguments.length > 1 ? arguments[1] : undefined)) >>> 0;
    },
    getFloat32: function getFloat32(byteOffset /* , littleEndian */) {
      return unpackIEEE754(get$1(this, 4, byteOffset, arguments.length > 1 ? arguments[1] : undefined), 23);
    },
    getFloat64: function getFloat64(byteOffset /* , littleEndian */) {
      return unpackIEEE754(get$1(this, 8, byteOffset, arguments.length > 1 ? arguments[1] : undefined), 52);
    },
    setInt8: function setInt8(byteOffset, value) {
      set$1(this, 1, byteOffset, packInt8, value);
    },
    setUint8: function setUint8(byteOffset, value) {
      set$1(this, 1, byteOffset, packInt8, value);
    },
    setInt16: function setInt16(byteOffset, value /* , littleEndian */) {
      set$1(this, 2, byteOffset, packInt16, value, arguments.length > 2 ? arguments[2] : undefined);
    },
    setUint16: function setUint16(byteOffset, value /* , littleEndian */) {
      set$1(this, 2, byteOffset, packInt16, value, arguments.length > 2 ? arguments[2] : undefined);
    },
    setInt32: function setInt32(byteOffset, value /* , littleEndian */) {
      set$1(this, 4, byteOffset, packInt32, value, arguments.length > 2 ? arguments[2] : undefined);
    },
    setUint32: function setUint32(byteOffset, value /* , littleEndian */) {
      set$1(this, 4, byteOffset, packInt32, value, arguments.length > 2 ? arguments[2] : undefined);
    },
    setFloat32: function setFloat32(byteOffset, value /* , littleEndian */) {
      set$1(this, 4, byteOffset, packFloat32, value, arguments.length > 2 ? arguments[2] : undefined);
    },
    setFloat64: function setFloat64(byteOffset, value /* , littleEndian */) {
      set$1(this, 8, byteOffset, packFloat64, value, arguments.length > 2 ? arguments[2] : undefined);
    }
  });
} else {
  if (!fails(function () {
    NativeArrayBuffer(1);
  }) || !fails(function () {
    new NativeArrayBuffer(-1); // eslint-disable-line no-new
  }) || fails(function () {
    new NativeArrayBuffer(); // eslint-disable-line no-new
    new NativeArrayBuffer(1.5); // eslint-disable-line no-new
    new NativeArrayBuffer(NaN); // eslint-disable-line no-new
    return NativeArrayBuffer.name != ARRAY_BUFFER;
  })) {
    $ArrayBuffer = function ArrayBuffer(length) {
      anInstance(this, $ArrayBuffer);
      return new NativeArrayBuffer(toIndex(length));
    };
    var ArrayBufferPrototype = $ArrayBuffer[PROTOTYPE$2] = NativeArrayBuffer[PROTOTYPE$2];
    for (var keys$1 = getOwnPropertyNames(NativeArrayBuffer), j = 0, key; keys$1.length > j;) {
      if (!((key = keys$1[j++]) in $ArrayBuffer)) {
        createNonEnumerableProperty($ArrayBuffer, key, NativeArrayBuffer[key]);
      }
    }
    ArrayBufferPrototype.constructor = $ArrayBuffer;
  }

  // WebKit bug - the same parent prototype for typed arrays and data view
  if (objectSetPrototypeOf && objectGetPrototypeOf($DataViewPrototype) !== ObjectPrototype$2) {
    objectSetPrototypeOf($DataViewPrototype, ObjectPrototype$2);
  }

  // iOS Safari 7.x bug
  var testView = new $DataView(new $ArrayBuffer(2));
  var nativeSetInt8 = $DataViewPrototype.setInt8;
  testView.setInt8(0, 2147483648);
  testView.setInt8(1, 2147483649);
  if (testView.getInt8(0) || !testView.getInt8(1)) redefineAll($DataViewPrototype, {
    setInt8: function setInt8(byteOffset, value) {
      nativeSetInt8.call(this, byteOffset, value << 24 >> 24);
    },
    setUint8: function setUint8(byteOffset, value) {
      nativeSetInt8.call(this, byteOffset, value << 24 >> 24);
    }
  }, { unsafe: true });
}

setToStringTag($ArrayBuffer, ARRAY_BUFFER);
setToStringTag($DataView, DATA_VIEW);

var arrayBuffer = {
  ArrayBuffer: $ArrayBuffer,
  DataView: $DataView
};

var ARRAY_BUFFER$1 = 'ArrayBuffer';
var ArrayBuffer$1 = arrayBuffer[ARRAY_BUFFER$1];
var NativeArrayBuffer$1 = global_1[ARRAY_BUFFER$1];

// `ArrayBuffer` constructor
// https://tc39.es/ecma262/#sec-arraybuffer-constructor
_export({ global: true, forced: NativeArrayBuffer$1 !== ArrayBuffer$1 }, {
  ArrayBuffer: ArrayBuffer$1
});

setSpecies(ARRAY_BUFFER$1);

var defineProperty$5 = objectDefineProperty.f;





var Int8Array$1 = global_1.Int8Array;
var Int8ArrayPrototype = Int8Array$1 && Int8Array$1.prototype;
var Uint8ClampedArray = global_1.Uint8ClampedArray;
var Uint8ClampedArrayPrototype = Uint8ClampedArray && Uint8ClampedArray.prototype;
var TypedArray = Int8Array$1 && objectGetPrototypeOf(Int8Array$1);
var TypedArrayPrototype = Int8ArrayPrototype && objectGetPrototypeOf(Int8ArrayPrototype);
var ObjectPrototype$3 = Object.prototype;
var isPrototypeOf = ObjectPrototype$3.isPrototypeOf;

var TO_STRING_TAG$3 = wellKnownSymbol('toStringTag');
var TYPED_ARRAY_TAG = uid('TYPED_ARRAY_TAG');
// Fixing native typed arrays in Opera Presto crashes the browser, see #595
var NATIVE_ARRAY_BUFFER_VIEWS = arrayBufferNative && !!objectSetPrototypeOf && classof(global_1.opera) !== 'Opera';
var TYPED_ARRAY_TAG_REQIRED = false;
var NAME;

var TypedArrayConstructorsList = {
  Int8Array: 1,
  Uint8Array: 1,
  Uint8ClampedArray: 1,
  Int16Array: 2,
  Uint16Array: 2,
  Int32Array: 4,
  Uint32Array: 4,
  Float32Array: 4,
  Float64Array: 8
};

var BigIntArrayConstructorsList = {
  BigInt64Array: 8,
  BigUint64Array: 8
};

var isView = function isView(it) {
  if (!isObject(it)) return false;
  var klass = classof(it);
  return klass === 'DataView'
    || has(TypedArrayConstructorsList, klass)
    || has(BigIntArrayConstructorsList, klass);
};

var isTypedArray = function (it) {
  if (!isObject(it)) return false;
  var klass = classof(it);
  return has(TypedArrayConstructorsList, klass)
    || has(BigIntArrayConstructorsList, klass);
};

var aTypedArray = function (it) {
  if (isTypedArray(it)) return it;
  throw TypeError('Target is not a typed array');
};

var aTypedArrayConstructor = function (C) {
  if (objectSetPrototypeOf) {
    if (isPrototypeOf.call(TypedArray, C)) return C;
  } else for (var ARRAY in TypedArrayConstructorsList) if (has(TypedArrayConstructorsList, NAME)) {
    var TypedArrayConstructor = global_1[ARRAY];
    if (TypedArrayConstructor && (C === TypedArrayConstructor || isPrototypeOf.call(TypedArrayConstructor, C))) {
      return C;
    }
  } throw TypeError('Target is not a typed array constructor');
};

var exportTypedArrayMethod = function (KEY, property, forced) {
  if (!descriptors) return;
  if (forced) for (var ARRAY in TypedArrayConstructorsList) {
    var TypedArrayConstructor = global_1[ARRAY];
    if (TypedArrayConstructor && has(TypedArrayConstructor.prototype, KEY)) {
      delete TypedArrayConstructor.prototype[KEY];
    }
  }
  if (!TypedArrayPrototype[KEY] || forced) {
    redefine(TypedArrayPrototype, KEY, forced ? property
      : NATIVE_ARRAY_BUFFER_VIEWS && Int8ArrayPrototype[KEY] || property);
  }
};

var exportTypedArrayStaticMethod = function (KEY, property, forced) {
  var ARRAY, TypedArrayConstructor;
  if (!descriptors) return;
  if (objectSetPrototypeOf) {
    if (forced) for (ARRAY in TypedArrayConstructorsList) {
      TypedArrayConstructor = global_1[ARRAY];
      if (TypedArrayConstructor && has(TypedArrayConstructor, KEY)) {
        delete TypedArrayConstructor[KEY];
      }
    }
    if (!TypedArray[KEY] || forced) {
      // V8 ~ Chrome 49-50 `%TypedArray%` methods are non-writable non-configurable
      try {
        return redefine(TypedArray, KEY, forced ? property : NATIVE_ARRAY_BUFFER_VIEWS && Int8Array$1[KEY] || property);
      } catch (error) { /* empty */ }
    } else return;
  }
  for (ARRAY in TypedArrayConstructorsList) {
    TypedArrayConstructor = global_1[ARRAY];
    if (TypedArrayConstructor && (!TypedArrayConstructor[KEY] || forced)) {
      redefine(TypedArrayConstructor, KEY, property);
    }
  }
};

for (NAME in TypedArrayConstructorsList) {
  if (!global_1[NAME]) NATIVE_ARRAY_BUFFER_VIEWS = false;
}

// WebKit bug - typed arrays constructors prototype is Object.prototype
if (!NATIVE_ARRAY_BUFFER_VIEWS || typeof TypedArray != 'function' || TypedArray === Function.prototype) {
  // eslint-disable-next-line no-shadow
  TypedArray = function TypedArray() {
    throw TypeError('Incorrect invocation');
  };
  if (NATIVE_ARRAY_BUFFER_VIEWS) for (NAME in TypedArrayConstructorsList) {
    if (global_1[NAME]) objectSetPrototypeOf(global_1[NAME], TypedArray);
  }
}

if (!NATIVE_ARRAY_BUFFER_VIEWS || !TypedArrayPrototype || TypedArrayPrototype === ObjectPrototype$3) {
  TypedArrayPrototype = TypedArray.prototype;
  if (NATIVE_ARRAY_BUFFER_VIEWS) for (NAME in TypedArrayConstructorsList) {
    if (global_1[NAME]) objectSetPrototypeOf(global_1[NAME].prototype, TypedArrayPrototype);
  }
}

// WebKit bug - one more object in Uint8ClampedArray prototype chain
if (NATIVE_ARRAY_BUFFER_VIEWS && objectGetPrototypeOf(Uint8ClampedArrayPrototype) !== TypedArrayPrototype) {
  objectSetPrototypeOf(Uint8ClampedArrayPrototype, TypedArrayPrototype);
}

if (descriptors && !has(TypedArrayPrototype, TO_STRING_TAG$3)) {
  TYPED_ARRAY_TAG_REQIRED = true;
  defineProperty$5(TypedArrayPrototype, TO_STRING_TAG$3, { get: function () {
    return isObject(this) ? this[TYPED_ARRAY_TAG] : undefined;
  } });
  for (NAME in TypedArrayConstructorsList) if (global_1[NAME]) {
    createNonEnumerableProperty(global_1[NAME], TYPED_ARRAY_TAG, NAME);
  }
}

var arrayBufferViewCore = {
  NATIVE_ARRAY_BUFFER_VIEWS: NATIVE_ARRAY_BUFFER_VIEWS,
  TYPED_ARRAY_TAG: TYPED_ARRAY_TAG_REQIRED && TYPED_ARRAY_TAG,
  aTypedArray: aTypedArray,
  aTypedArrayConstructor: aTypedArrayConstructor,
  exportTypedArrayMethod: exportTypedArrayMethod,
  exportTypedArrayStaticMethod: exportTypedArrayStaticMethod,
  isView: isView,
  isTypedArray: isTypedArray,
  TypedArray: TypedArray,
  TypedArrayPrototype: TypedArrayPrototype
};

var NATIVE_ARRAY_BUFFER_VIEWS$1 = arrayBufferViewCore.NATIVE_ARRAY_BUFFER_VIEWS;

// `ArrayBuffer.isView` method
// https://tc39.es/ecma262/#sec-arraybuffer.isview
_export({ target: 'ArrayBuffer', stat: true, forced: !NATIVE_ARRAY_BUFFER_VIEWS$1 }, {
  isView: arrayBufferViewCore.isView
});

var SPECIES$4 = wellKnownSymbol('species');

// `SpeciesConstructor` abstract operation
// https://tc39.es/ecma262/#sec-speciesconstructor
var speciesConstructor = function (O, defaultConstructor) {
  var C = anObject(O).constructor;
  var S;
  return C === undefined || (S = anObject(C)[SPECIES$4]) == undefined ? defaultConstructor : aFunction$1(S);
};

var ArrayBuffer$2 = arrayBuffer.ArrayBuffer;
var DataView$1 = arrayBuffer.DataView;
var nativeArrayBufferSlice = ArrayBuffer$2.prototype.slice;

var INCORRECT_SLICE = fails(function () {
  return !new ArrayBuffer$2(2).slice(1, undefined).byteLength;
});

// `ArrayBuffer.prototype.slice` method
// https://tc39.es/ecma262/#sec-arraybuffer.prototype.slice
_export({ target: 'ArrayBuffer', proto: true, unsafe: true, forced: INCORRECT_SLICE }, {
  slice: function slice(start, end) {
    if (nativeArrayBufferSlice !== undefined && end === undefined) {
      return nativeArrayBufferSlice.call(anObject(this), start); // FF fix
    }
    var length = anObject(this).byteLength;
    var first = toAbsoluteIndex(start, length);
    var fin = toAbsoluteIndex(end === undefined ? length : end, length);
    var result = new (speciesConstructor(this, ArrayBuffer$2))(toLength(fin - first));
    var viewSource = new DataView$1(this);
    var viewTarget = new DataView$1(result);
    var index = 0;
    while (first < fin) {
      viewTarget.setUint8(index++, viewSource.getUint8(first++));
    } return result;
  }
});

var defineProperty$6 = objectDefineProperty.f;

var FunctionPrototype = Function.prototype;
var FunctionPrototypeToString = FunctionPrototype.toString;
var nameRE = /^\s*function ([^ (]*)/;
var NAME$1 = 'name';

// Function instances `.name` property
// https://tc39.es/ecma262/#sec-function-instances-name
if (descriptors && !(NAME$1 in FunctionPrototype)) {
  defineProperty$6(FunctionPrototype, NAME$1, {
    configurable: true,
    get: function () {
      try {
        return FunctionPrototypeToString.call(this).match(nameRE)[1];
      } catch (error) {
        return '';
      }
    }
  });
}

var freezing = !fails(function () {
  return Object.isExtensible(Object.preventExtensions({}));
});

var internalMetadata = createCommonjsModule(function (module) {
var defineProperty = objectDefineProperty.f;



var METADATA = uid('meta');
var id = 0;

var isExtensible = Object.isExtensible || function () {
  return true;
};

var setMetadata = function (it) {
  defineProperty(it, METADATA, { value: {
    objectID: 'O' + ++id, // object ID
    weakData: {}          // weak collections IDs
  } });
};

var fastKey = function (it, create) {
  // return a primitive with prefix
  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
  if (!has(it, METADATA)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return 'F';
    // not necessary to add metadata
    if (!create) return 'E';
    // add missing metadata
    setMetadata(it);
  // return object ID
  } return it[METADATA].objectID;
};

var getWeakData = function (it, create) {
  if (!has(it, METADATA)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return true;
    // not necessary to add metadata
    if (!create) return false;
    // add missing metadata
    setMetadata(it);
  // return the store of weak collections IDs
  } return it[METADATA].weakData;
};

// add metadata on freeze-family methods calling
var onFreeze = function (it) {
  if (freezing && meta.REQUIRED && isExtensible(it) && !has(it, METADATA)) setMetadata(it);
  return it;
};

var meta = module.exports = {
  REQUIRED: false,
  fastKey: fastKey,
  getWeakData: getWeakData,
  onFreeze: onFreeze
};

hiddenKeys[METADATA] = true;
});
var internalMetadata_1 = internalMetadata.REQUIRED;
var internalMetadata_2 = internalMetadata.fastKey;
var internalMetadata_3 = internalMetadata.getWeakData;
var internalMetadata_4 = internalMetadata.onFreeze;

var Result = function (stopped, result) {
  this.stopped = stopped;
  this.result = result;
};

var iterate = function (iterable, unboundFunction, options) {
  var that = options && options.that;
  var AS_ENTRIES = !!(options && options.AS_ENTRIES);
  var IS_ITERATOR = !!(options && options.IS_ITERATOR);
  var INTERRUPTED = !!(options && options.INTERRUPTED);
  var fn = functionBindContext(unboundFunction, that, 1 + AS_ENTRIES + INTERRUPTED);
  var iterator, iterFn, index, length, result, next, step;

  var stop = function (condition) {
    if (iterator) iteratorClose(iterator);
    return new Result(true, condition);
  };

  var callFn = function (value) {
    if (AS_ENTRIES) {
      anObject(value);
      return INTERRUPTED ? fn(value[0], value[1], stop) : fn(value[0], value[1]);
    } return INTERRUPTED ? fn(value, stop) : fn(value);
  };

  if (IS_ITERATOR) {
    iterator = iterable;
  } else {
    iterFn = getIteratorMethod(iterable);
    if (typeof iterFn != 'function') throw TypeError('Target is not iterable');
    // optimisation for array iterators
    if (isArrayIteratorMethod(iterFn)) {
      for (index = 0, length = toLength(iterable.length); length > index; index++) {
        result = callFn(iterable[index]);
        if (result && result instanceof Result) return result;
      } return new Result(false);
    }
    iterator = iterFn.call(iterable);
  }

  next = iterator.next;
  while (!(step = next.call(iterator)).done) {
    try {
      result = callFn(step.value);
    } catch (error) {
      iteratorClose(iterator);
      throw error;
    }
    if (typeof result == 'object' && result && result instanceof Result) return result;
  } return new Result(false);
};

// makes subclassing work correct for wrapped built-ins
var inheritIfRequired = function ($this, dummy, Wrapper) {
  var NewTarget, NewTargetPrototype;
  if (
    // it can work only with native `setPrototypeOf`
    objectSetPrototypeOf &&
    // we haven't completely correct pre-ES6 way for getting `new.target`, so use this
    typeof (NewTarget = dummy.constructor) == 'function' &&
    NewTarget !== Wrapper &&
    isObject(NewTargetPrototype = NewTarget.prototype) &&
    NewTargetPrototype !== Wrapper.prototype
  ) objectSetPrototypeOf($this, NewTargetPrototype);
  return $this;
};

var collection = function (CONSTRUCTOR_NAME, wrapper, common) {
  var IS_MAP = CONSTRUCTOR_NAME.indexOf('Map') !== -1;
  var IS_WEAK = CONSTRUCTOR_NAME.indexOf('Weak') !== -1;
  var ADDER = IS_MAP ? 'set' : 'add';
  var NativeConstructor = global_1[CONSTRUCTOR_NAME];
  var NativePrototype = NativeConstructor && NativeConstructor.prototype;
  var Constructor = NativeConstructor;
  var exported = {};

  var fixMethod = function (KEY) {
    var nativeMethod = NativePrototype[KEY];
    redefine(NativePrototype, KEY,
      KEY == 'add' ? function add(value) {
        nativeMethod.call(this, value === 0 ? 0 : value);
        return this;
      } : KEY == 'delete' ? function (key) {
        return IS_WEAK && !isObject(key) ? false : nativeMethod.call(this, key === 0 ? 0 : key);
      } : KEY == 'get' ? function get(key) {
        return IS_WEAK && !isObject(key) ? undefined : nativeMethod.call(this, key === 0 ? 0 : key);
      } : KEY == 'has' ? function has(key) {
        return IS_WEAK && !isObject(key) ? false : nativeMethod.call(this, key === 0 ? 0 : key);
      } : function set(key, value) {
        nativeMethod.call(this, key === 0 ? 0 : key, value);
        return this;
      }
    );
  };

  // eslint-disable-next-line max-len
  if (isForced_1(CONSTRUCTOR_NAME, typeof NativeConstructor != 'function' || !(IS_WEAK || NativePrototype.forEach && !fails(function () {
    new NativeConstructor().entries().next();
  })))) {
    // create collection constructor
    Constructor = common.getConstructor(wrapper, CONSTRUCTOR_NAME, IS_MAP, ADDER);
    internalMetadata.REQUIRED = true;
  } else if (isForced_1(CONSTRUCTOR_NAME, true)) {
    var instance = new Constructor();
    // early implementations not supports chaining
    var HASNT_CHAINING = instance[ADDER](IS_WEAK ? {} : -0, 1) != instance;
    // V8 ~ Chromium 40- weak-collections throws on primitives, but should return false
    var THROWS_ON_PRIMITIVES = fails(function () { instance.has(1); });
    // most early implementations doesn't supports iterables, most modern - not close it correctly
    // eslint-disable-next-line no-new
    var ACCEPT_ITERABLES = checkCorrectnessOfIteration(function (iterable) { new NativeConstructor(iterable); });
    // for early implementations -0 and +0 not the same
    var BUGGY_ZERO = !IS_WEAK && fails(function () {
      // V8 ~ Chromium 42- fails only with 5+ elements
      var $instance = new NativeConstructor();
      var index = 5;
      while (index--) $instance[ADDER](index, index);
      return !$instance.has(-0);
    });

    if (!ACCEPT_ITERABLES) {
      Constructor = wrapper(function (dummy, iterable) {
        anInstance(dummy, Constructor, CONSTRUCTOR_NAME);
        var that = inheritIfRequired(new NativeConstructor(), dummy, Constructor);
        if (iterable != undefined) iterate(iterable, that[ADDER], { that: that, AS_ENTRIES: IS_MAP });
        return that;
      });
      Constructor.prototype = NativePrototype;
      NativePrototype.constructor = Constructor;
    }

    if (THROWS_ON_PRIMITIVES || BUGGY_ZERO) {
      fixMethod('delete');
      fixMethod('has');
      IS_MAP && fixMethod('get');
    }

    if (BUGGY_ZERO || HASNT_CHAINING) fixMethod(ADDER);

    // weak collections should not contains .clear method
    if (IS_WEAK && NativePrototype.clear) delete NativePrototype.clear;
  }

  exported[CONSTRUCTOR_NAME] = Constructor;
  _export({ global: true, forced: Constructor != NativeConstructor }, exported);

  setToStringTag(Constructor, CONSTRUCTOR_NAME);

  if (!IS_WEAK) common.setStrong(Constructor, CONSTRUCTOR_NAME, IS_MAP);

  return Constructor;
};

var defineProperty$7 = objectDefineProperty.f;








var fastKey = internalMetadata.fastKey;


var setInternalState$3 = internalState.set;
var internalStateGetterFor = internalState.getterFor;

var collectionStrong = {
  getConstructor: function (wrapper, CONSTRUCTOR_NAME, IS_MAP, ADDER) {
    var C = wrapper(function (that, iterable) {
      anInstance(that, C, CONSTRUCTOR_NAME);
      setInternalState$3(that, {
        type: CONSTRUCTOR_NAME,
        index: objectCreate(null),
        first: undefined,
        last: undefined,
        size: 0
      });
      if (!descriptors) that.size = 0;
      if (iterable != undefined) iterate(iterable, that[ADDER], { that: that, AS_ENTRIES: IS_MAP });
    });

    var getInternalState = internalStateGetterFor(CONSTRUCTOR_NAME);

    var define = function (that, key, value) {
      var state = getInternalState(that);
      var entry = getEntry(that, key);
      var previous, index;
      // change existing entry
      if (entry) {
        entry.value = value;
      // create new entry
      } else {
        state.last = entry = {
          index: index = fastKey(key, true),
          key: key,
          value: value,
          previous: previous = state.last,
          next: undefined,
          removed: false
        };
        if (!state.first) state.first = entry;
        if (previous) previous.next = entry;
        if (descriptors) state.size++;
        else that.size++;
        // add to index
        if (index !== 'F') state.index[index] = entry;
      } return that;
    };

    var getEntry = function (that, key) {
      var state = getInternalState(that);
      // fast case
      var index = fastKey(key);
      var entry;
      if (index !== 'F') return state.index[index];
      // frozen object case
      for (entry = state.first; entry; entry = entry.next) {
        if (entry.key == key) return entry;
      }
    };

    redefineAll(C.prototype, {
      // 23.1.3.1 Map.prototype.clear()
      // 23.2.3.2 Set.prototype.clear()
      clear: function clear() {
        var that = this;
        var state = getInternalState(that);
        var data = state.index;
        var entry = state.first;
        while (entry) {
          entry.removed = true;
          if (entry.previous) entry.previous = entry.previous.next = undefined;
          delete data[entry.index];
          entry = entry.next;
        }
        state.first = state.last = undefined;
        if (descriptors) state.size = 0;
        else that.size = 0;
      },
      // 23.1.3.3 Map.prototype.delete(key)
      // 23.2.3.4 Set.prototype.delete(value)
      'delete': function (key) {
        var that = this;
        var state = getInternalState(that);
        var entry = getEntry(that, key);
        if (entry) {
          var next = entry.next;
          var prev = entry.previous;
          delete state.index[entry.index];
          entry.removed = true;
          if (prev) prev.next = next;
          if (next) next.previous = prev;
          if (state.first == entry) state.first = next;
          if (state.last == entry) state.last = prev;
          if (descriptors) state.size--;
          else that.size--;
        } return !!entry;
      },
      // 23.2.3.6 Set.prototype.forEach(callbackfn, thisArg = undefined)
      // 23.1.3.5 Map.prototype.forEach(callbackfn, thisArg = undefined)
      forEach: function forEach(callbackfn /* , that = undefined */) {
        var state = getInternalState(this);
        var boundFunction = functionBindContext(callbackfn, arguments.length > 1 ? arguments[1] : undefined, 3);
        var entry;
        while (entry = entry ? entry.next : state.first) {
          boundFunction(entry.value, entry.key, this);
          // revert to the last existing entry
          while (entry && entry.removed) entry = entry.previous;
        }
      },
      // 23.1.3.7 Map.prototype.has(key)
      // 23.2.3.7 Set.prototype.has(value)
      has: function has(key) {
        return !!getEntry(this, key);
      }
    });

    redefineAll(C.prototype, IS_MAP ? {
      // 23.1.3.6 Map.prototype.get(key)
      get: function get(key) {
        var entry = getEntry(this, key);
        return entry && entry.value;
      },
      // 23.1.3.9 Map.prototype.set(key, value)
      set: function set(key, value) {
        return define(this, key === 0 ? 0 : key, value);
      }
    } : {
      // 23.2.3.1 Set.prototype.add(value)
      add: function add(value) {
        return define(this, value = value === 0 ? 0 : value, value);
      }
    });
    if (descriptors) defineProperty$7(C.prototype, 'size', {
      get: function () {
        return getInternalState(this).size;
      }
    });
    return C;
  },
  setStrong: function (C, CONSTRUCTOR_NAME, IS_MAP) {
    var ITERATOR_NAME = CONSTRUCTOR_NAME + ' Iterator';
    var getInternalCollectionState = internalStateGetterFor(CONSTRUCTOR_NAME);
    var getInternalIteratorState = internalStateGetterFor(ITERATOR_NAME);
    // add .keys, .values, .entries, [@@iterator]
    // 23.1.3.4, 23.1.3.8, 23.1.3.11, 23.1.3.12, 23.2.3.5, 23.2.3.8, 23.2.3.10, 23.2.3.11
    defineIterator(C, CONSTRUCTOR_NAME, function (iterated, kind) {
      setInternalState$3(this, {
        type: ITERATOR_NAME,
        target: iterated,
        state: getInternalCollectionState(iterated),
        kind: kind,
        last: undefined
      });
    }, function () {
      var state = getInternalIteratorState(this);
      var kind = state.kind;
      var entry = state.last;
      // revert to the last existing entry
      while (entry && entry.removed) entry = entry.previous;
      // get next entry
      if (!state.target || !(state.last = entry = entry ? entry.next : state.state.first)) {
        // or finish the iteration
        state.target = undefined;
        return { value: undefined, done: true };
      }
      // return step by kind
      if (kind == 'keys') return { value: entry.key, done: false };
      if (kind == 'values') return { value: entry.value, done: false };
      return { value: [entry.key, entry.value], done: false };
    }, IS_MAP ? 'entries' : 'values', !IS_MAP, true);

    // add [@@species], 23.1.2.2, 23.2.2.2
    setSpecies(CONSTRUCTOR_NAME);
  }
};

// `Map` constructor
// https://tc39.es/ecma262/#sec-map-objects
var es_map = collection('Map', function (init) {
  return function Map() { return init(this, arguments.length ? arguments[0] : undefined); };
}, collectionStrong);

// a string of all valid unicode whitespaces
// eslint-disable-next-line max-len
var whitespaces = '\u0009\u000A\u000B\u000C\u000D\u0020\u00A0\u1680\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028\u2029\uFEFF';

var whitespace = '[' + whitespaces + ']';
var ltrim = RegExp('^' + whitespace + whitespace + '*');
var rtrim = RegExp(whitespace + whitespace + '*$');

// `String.prototype.{ trim, trimStart, trimEnd, trimLeft, trimRight }` methods implementation
var createMethod$3 = function (TYPE) {
  return function ($this) {
    var string = String(requireObjectCoercible($this));
    if (TYPE & 1) string = string.replace(ltrim, '');
    if (TYPE & 2) string = string.replace(rtrim, '');
    return string;
  };
};

var stringTrim = {
  // `String.prototype.{ trimLeft, trimStart }` methods
  // https://tc39.es/ecma262/#sec-string.prototype.trimstart
  start: createMethod$3(1),
  // `String.prototype.{ trimRight, trimEnd }` methods
  // https://tc39.es/ecma262/#sec-string.prototype.trimend
  end: createMethod$3(2),
  // `String.prototype.trim` method
  // https://tc39.es/ecma262/#sec-string.prototype.trim
  trim: createMethod$3(3)
};

var getOwnPropertyNames$1 = objectGetOwnPropertyNames.f;
var getOwnPropertyDescriptor$2 = objectGetOwnPropertyDescriptor.f;
var defineProperty$8 = objectDefineProperty.f;
var trim = stringTrim.trim;

var NUMBER = 'Number';
var NativeNumber = global_1[NUMBER];
var NumberPrototype = NativeNumber.prototype;

// Opera ~12 has broken Object#toString
var BROKEN_CLASSOF = classofRaw(objectCreate(NumberPrototype)) == NUMBER;

// `ToNumber` abstract operation
// https://tc39.es/ecma262/#sec-tonumber
var toNumber = function (argument) {
  var it = toPrimitive(argument, false);
  var first, third, radix, maxCode, digits, length, index, code;
  if (typeof it == 'string' && it.length > 2) {
    it = trim(it);
    first = it.charCodeAt(0);
    if (first === 43 || first === 45) {
      third = it.charCodeAt(2);
      if (third === 88 || third === 120) return NaN; // Number('+0x1') should be NaN, old V8 fix
    } else if (first === 48) {
      switch (it.charCodeAt(1)) {
        case 66: case 98: radix = 2; maxCode = 49; break; // fast equal of /^0b[01]+$/i
        case 79: case 111: radix = 8; maxCode = 55; break; // fast equal of /^0o[0-7]+$/i
        default: return +it;
      }
      digits = it.slice(2);
      length = digits.length;
      for (index = 0; index < length; index++) {
        code = digits.charCodeAt(index);
        // parseInt parses a string to a first unavailable symbol
        // but ToNumber should return NaN if a string contains unavailable symbols
        if (code < 48 || code > maxCode) return NaN;
      } return parseInt(digits, radix);
    }
  } return +it;
};

// `Number` constructor
// https://tc39.es/ecma262/#sec-number-constructor
if (isForced_1(NUMBER, !NativeNumber(' 0o1') || !NativeNumber('0b1') || NativeNumber('+0x1'))) {
  var NumberWrapper = function Number(value) {
    var it = arguments.length < 1 ? 0 : value;
    var dummy = this;
    return dummy instanceof NumberWrapper
      // check on 1..constructor(foo) case
      && (BROKEN_CLASSOF ? fails(function () { NumberPrototype.valueOf.call(dummy); }) : classofRaw(dummy) != NUMBER)
        ? inheritIfRequired(new NativeNumber(toNumber(it)), dummy, NumberWrapper) : toNumber(it);
  };
  for (var keys$2 = descriptors ? getOwnPropertyNames$1(NativeNumber) : (
    // ES3:
    'MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,' +
    // ES2015 (in case, if modules with ES2015 Number statics required before):
    'EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,' +
    'MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger,' +
    // ESNext
    'fromString,range'
  ).split(','), j$1 = 0, key$1; keys$2.length > j$1; j$1++) {
    if (has(NativeNumber, key$1 = keys$2[j$1]) && !has(NumberWrapper, key$1)) {
      defineProperty$8(NumberWrapper, key$1, getOwnPropertyDescriptor$2(NativeNumber, key$1));
    }
  }
  NumberWrapper.prototype = NumberPrototype;
  NumberPrototype.constructor = NumberWrapper;
  redefine(global_1, NUMBER, NumberWrapper);
}

var nativeAssign = Object.assign;
var defineProperty$9 = Object.defineProperty;

// `Object.assign` method
// https://tc39.es/ecma262/#sec-object.assign
var objectAssign = !nativeAssign || fails(function () {
  // should have correct order of operations (Edge bug)
  if (descriptors && nativeAssign({ b: 1 }, nativeAssign(defineProperty$9({}, 'a', {
    enumerable: true,
    get: function () {
      defineProperty$9(this, 'b', {
        value: 3,
        enumerable: false
      });
    }
  }), { b: 2 })).b !== 1) return true;
  // should work with symbols and should have deterministic property order (V8 bug)
  var A = {};
  var B = {};
  // eslint-disable-next-line no-undef
  var symbol = Symbol();
  var alphabet = 'abcdefghijklmnopqrst';
  A[symbol] = 7;
  alphabet.split('').forEach(function (chr) { B[chr] = chr; });
  return nativeAssign({}, A)[symbol] != 7 || objectKeys(nativeAssign({}, B)).join('') != alphabet;
}) ? function assign(target, source) { // eslint-disable-line no-unused-vars
  var T = toObject(target);
  var argumentsLength = arguments.length;
  var index = 1;
  var getOwnPropertySymbols = objectGetOwnPropertySymbols.f;
  var propertyIsEnumerable = objectPropertyIsEnumerable.f;
  while (argumentsLength > index) {
    var S = indexedObject(arguments[index++]);
    var keys = getOwnPropertySymbols ? objectKeys(S).concat(getOwnPropertySymbols(S)) : objectKeys(S);
    var length = keys.length;
    var j = 0;
    var key;
    while (length > j) {
      key = keys[j++];
      if (!descriptors || propertyIsEnumerable.call(S, key)) T[key] = S[key];
    }
  } return T;
} : nativeAssign;

// `Object.assign` method
// https://tc39.es/ecma262/#sec-object.assign
_export({ target: 'Object', stat: true, forced: Object.assign !== objectAssign }, {
  assign: objectAssign
});

var nativeGetOwnPropertyDescriptor$2 = objectGetOwnPropertyDescriptor.f;


var FAILS_ON_PRIMITIVES = fails(function () { nativeGetOwnPropertyDescriptor$2(1); });
var FORCED$3 = !descriptors || FAILS_ON_PRIMITIVES;

// `Object.getOwnPropertyDescriptor` method
// https://tc39.es/ecma262/#sec-object.getownpropertydescriptor
_export({ target: 'Object', stat: true, forced: FORCED$3, sham: !descriptors }, {
  getOwnPropertyDescriptor: function getOwnPropertyDescriptor(it, key) {
    return nativeGetOwnPropertyDescriptor$2(toIndexedObject(it), key);
  }
});

var nativeGetOwnPropertyNames$2 = objectGetOwnPropertyNamesExternal.f;

var FAILS_ON_PRIMITIVES$1 = fails(function () { return !Object.getOwnPropertyNames(1); });

// `Object.getOwnPropertyNames` method
// https://tc39.es/ecma262/#sec-object.getownpropertynames
_export({ target: 'Object', stat: true, forced: FAILS_ON_PRIMITIVES$1 }, {
  getOwnPropertyNames: nativeGetOwnPropertyNames$2
});

var FAILS_ON_PRIMITIVES$2 = fails(function () { objectGetPrototypeOf(1); });

// `Object.getPrototypeOf` method
// https://tc39.es/ecma262/#sec-object.getprototypeof
_export({ target: 'Object', stat: true, forced: FAILS_ON_PRIMITIVES$2, sham: !correctPrototypeGetter }, {
  getPrototypeOf: function getPrototypeOf(it) {
    return objectGetPrototypeOf(toObject(it));
  }
});

var FAILS_ON_PRIMITIVES$3 = fails(function () { objectKeys(1); });

// `Object.keys` method
// https://tc39.es/ecma262/#sec-object.keys
_export({ target: 'Object', stat: true, forced: FAILS_ON_PRIMITIVES$3 }, {
  keys: function keys(it) {
    return objectKeys(toObject(it));
  }
});

// `Object.setPrototypeOf` method
// https://tc39.es/ecma262/#sec-object.setprototypeof
_export({ target: 'Object', stat: true }, {
  setPrototypeOf: objectSetPrototypeOf
});

// `Object.prototype.toString` method implementation
// https://tc39.es/ecma262/#sec-object.prototype.tostring
var objectToString = toStringTagSupport ? {}.toString : function toString() {
  return '[object ' + classof(this) + ']';
};

// `Object.prototype.toString` method
// https://tc39.es/ecma262/#sec-object.prototype.tostring
if (!toStringTagSupport) {
  redefine(Object.prototype, 'toString', objectToString, { unsafe: true });
}

var nativePromiseConstructor = global_1.Promise;

var engineIsIos = /(iphone|ipod|ipad).*applewebkit/i.test(engineUserAgent);

var location = global_1.location;
var set$2 = global_1.setImmediate;
var clear = global_1.clearImmediate;
var process$1 = global_1.process;
var MessageChannel = global_1.MessageChannel;
var Dispatch = global_1.Dispatch;
var counter = 0;
var queue = {};
var ONREADYSTATECHANGE = 'onreadystatechange';
var defer, channel, port;

var run = function (id) {
  // eslint-disable-next-line no-prototype-builtins
  if (queue.hasOwnProperty(id)) {
    var fn = queue[id];
    delete queue[id];
    fn();
  }
};

var runner = function (id) {
  return function () {
    run(id);
  };
};

var listener = function (event) {
  run(event.data);
};

var post = function (id) {
  // old engines have not location.origin
  global_1.postMessage(id + '', location.protocol + '//' + location.host);
};

// Node.js 0.9+ & IE10+ has setImmediate, otherwise:
if (!set$2 || !clear) {
  set$2 = function setImmediate(fn) {
    var args = [];
    var i = 1;
    while (arguments.length > i) args.push(arguments[i++]);
    queue[++counter] = function () {
      // eslint-disable-next-line no-new-func
      (typeof fn == 'function' ? fn : Function(fn)).apply(undefined, args);
    };
    defer(counter);
    return counter;
  };
  clear = function clearImmediate(id) {
    delete queue[id];
  };
  // Node.js 0.8-
  if (engineIsNode) {
    defer = function (id) {
      process$1.nextTick(runner(id));
    };
  // Sphere (JS game engine) Dispatch API
  } else if (Dispatch && Dispatch.now) {
    defer = function (id) {
      Dispatch.now(runner(id));
    };
  // Browsers with MessageChannel, includes WebWorkers
  // except iOS - https://github.com/zloirock/core-js/issues/624
  } else if (MessageChannel && !engineIsIos) {
    channel = new MessageChannel();
    port = channel.port2;
    channel.port1.onmessage = listener;
    defer = functionBindContext(port.postMessage, port, 1);
  // Browsers with postMessage, skip WebWorkers
  // IE8 has postMessage, but it's sync & typeof its postMessage is 'object'
  } else if (
    global_1.addEventListener &&
    typeof postMessage == 'function' &&
    !global_1.importScripts &&
    location && location.protocol !== 'file:' &&
    !fails(post)
  ) {
    defer = post;
    global_1.addEventListener('message', listener, false);
  // IE8-
  } else if (ONREADYSTATECHANGE in documentCreateElement('script')) {
    defer = function (id) {
      html.appendChild(documentCreateElement('script'))[ONREADYSTATECHANGE] = function () {
        html.removeChild(this);
        run(id);
      };
    };
  // Rest old browsers
  } else {
    defer = function (id) {
      setTimeout(runner(id), 0);
    };
  }
}

var task = {
  set: set$2,
  clear: clear
};

var engineIsWebosWebkit = /web0s(?!.*chrome)/i.test(engineUserAgent);

var getOwnPropertyDescriptor$3 = objectGetOwnPropertyDescriptor.f;
var macrotask = task.set;




var MutationObserver$1 = global_1.MutationObserver || global_1.WebKitMutationObserver;
var document$2 = global_1.document;
var process$2 = global_1.process;
var Promise$1 = global_1.Promise;
// Node.js 11 shows ExperimentalWarning on getting `queueMicrotask`
var queueMicrotaskDescriptor = getOwnPropertyDescriptor$3(global_1, 'queueMicrotask');
var queueMicrotask = queueMicrotaskDescriptor && queueMicrotaskDescriptor.value;

var flush, head, last, notify, toggle, node, promise, then;

// modern engines have queueMicrotask method
if (!queueMicrotask) {
  flush = function () {
    var parent, fn;
    if (engineIsNode && (parent = process$2.domain)) parent.exit();
    while (head) {
      fn = head.fn;
      head = head.next;
      try {
        fn();
      } catch (error) {
        if (head) notify();
        else last = undefined;
        throw error;
      }
    } last = undefined;
    if (parent) parent.enter();
  };

  // browsers with MutationObserver, except iOS - https://github.com/zloirock/core-js/issues/339
  // also except WebOS Webkit https://github.com/zloirock/core-js/issues/898
  if (!engineIsIos && !engineIsNode && !engineIsWebosWebkit && MutationObserver$1 && document$2) {
    toggle = true;
    node = document$2.createTextNode('');
    new MutationObserver$1(flush).observe(node, { characterData: true });
    notify = function () {
      node.data = toggle = !toggle;
    };
  // environments with maybe non-completely correct, but existent Promise
  } else if (Promise$1 && Promise$1.resolve) {
    // Promise.resolve without an argument throws an error in LG WebOS 2
    promise = Promise$1.resolve(undefined);
    then = promise.then;
    notify = function () {
      then.call(promise, flush);
    };
  // Node.js without promises
  } else if (engineIsNode) {
    notify = function () {
      process$2.nextTick(flush);
    };
  // for other environments - macrotask based on:
  // - setImmediate
  // - MessageChannel
  // - window.postMessag
  // - onreadystatechange
  // - setTimeout
  } else {
    notify = function () {
      // strange IE + webpack dev server bug - use .call(global)
      macrotask.call(global_1, flush);
    };
  }
}

var microtask = queueMicrotask || function (fn) {
  var task = { fn: fn, next: undefined };
  if (last) last.next = task;
  if (!head) {
    head = task;
    notify();
  } last = task;
};

var PromiseCapability = function (C) {
  var resolve, reject;
  this.promise = new C(function ($$resolve, $$reject) {
    if (resolve !== undefined || reject !== undefined) throw TypeError('Bad Promise constructor');
    resolve = $$resolve;
    reject = $$reject;
  });
  this.resolve = aFunction$1(resolve);
  this.reject = aFunction$1(reject);
};

// 25.4.1.5 NewPromiseCapability(C)
var f$7 = function (C) {
  return new PromiseCapability(C);
};

var newPromiseCapability = {
	f: f$7
};

var promiseResolve = function (C, x) {
  anObject(C);
  if (isObject(x) && x.constructor === C) return x;
  var promiseCapability = newPromiseCapability.f(C);
  var resolve = promiseCapability.resolve;
  resolve(x);
  return promiseCapability.promise;
};

var hostReportErrors = function (a, b) {
  var console = global_1.console;
  if (console && console.error) {
    arguments.length === 1 ? console.error(a) : console.error(a, b);
  }
};

var perform = function (exec) {
  try {
    return { error: false, value: exec() };
  } catch (error) {
    return { error: true, value: error };
  }
};

var task$1 = task.set;











var SPECIES$5 = wellKnownSymbol('species');
var PROMISE = 'Promise';
var getInternalState$3 = internalState.get;
var setInternalState$4 = internalState.set;
var getInternalPromiseState = internalState.getterFor(PROMISE);
var PromiseConstructor = nativePromiseConstructor;
var TypeError$1 = global_1.TypeError;
var document$3 = global_1.document;
var process$3 = global_1.process;
var $fetch = getBuiltIn('fetch');
var newPromiseCapability$1 = newPromiseCapability.f;
var newGenericPromiseCapability = newPromiseCapability$1;
var DISPATCH_EVENT = !!(document$3 && document$3.createEvent && global_1.dispatchEvent);
var NATIVE_REJECTION_EVENT = typeof PromiseRejectionEvent == 'function';
var UNHANDLED_REJECTION = 'unhandledrejection';
var REJECTION_HANDLED = 'rejectionhandled';
var PENDING = 0;
var FULFILLED = 1;
var REJECTED = 2;
var HANDLED = 1;
var UNHANDLED = 2;
var Internal, OwnPromiseCapability, PromiseWrapper, nativeThen;

var FORCED$4 = isForced_1(PROMISE, function () {
  var GLOBAL_CORE_JS_PROMISE = inspectSource(PromiseConstructor) !== String(PromiseConstructor);
  if (!GLOBAL_CORE_JS_PROMISE) {
    // V8 6.6 (Node 10 and Chrome 66) have a bug with resolving custom thenables
    // https://bugs.chromium.org/p/chromium/issues/detail?id=830565
    // We can't detect it synchronously, so just check versions
    if (engineV8Version === 66) return true;
    // Unhandled rejections tracking support, NodeJS Promise without it fails @@species test
    if (!engineIsNode && !NATIVE_REJECTION_EVENT) return true;
  }
  // We can't use @@species feature detection in V8 since it causes
  // deoptimization and performance degradation
  // https://github.com/zloirock/core-js/issues/679
  if (engineV8Version >= 51 && /native code/.test(PromiseConstructor)) return false;
  // Detect correctness of subclassing with @@species support
  var promise = PromiseConstructor.resolve(1);
  var FakePromise = function (exec) {
    exec(function () { /* empty */ }, function () { /* empty */ });
  };
  var constructor = promise.constructor = {};
  constructor[SPECIES$5] = FakePromise;
  return !(promise.then(function () { /* empty */ }) instanceof FakePromise);
});

var INCORRECT_ITERATION$1 = FORCED$4 || !checkCorrectnessOfIteration(function (iterable) {
  PromiseConstructor.all(iterable)['catch'](function () { /* empty */ });
});

// helpers
var isThenable = function (it) {
  var then;
  return isObject(it) && typeof (then = it.then) == 'function' ? then : false;
};

var notify$1 = function (state, isReject) {
  if (state.notified) return;
  state.notified = true;
  var chain = state.reactions;
  microtask(function () {
    var value = state.value;
    var ok = state.state == FULFILLED;
    var index = 0;
    // variable length - can't use forEach
    while (chain.length > index) {
      var reaction = chain[index++];
      var handler = ok ? reaction.ok : reaction.fail;
      var resolve = reaction.resolve;
      var reject = reaction.reject;
      var domain = reaction.domain;
      var result, then, exited;
      try {
        if (handler) {
          if (!ok) {
            if (state.rejection === UNHANDLED) onHandleUnhandled(state);
            state.rejection = HANDLED;
          }
          if (handler === true) result = value;
          else {
            if (domain) domain.enter();
            result = handler(value); // can throw
            if (domain) {
              domain.exit();
              exited = true;
            }
          }
          if (result === reaction.promise) {
            reject(TypeError$1('Promise-chain cycle'));
          } else if (then = isThenable(result)) {
            then.call(result, resolve, reject);
          } else resolve(result);
        } else reject(value);
      } catch (error) {
        if (domain && !exited) domain.exit();
        reject(error);
      }
    }
    state.reactions = [];
    state.notified = false;
    if (isReject && !state.rejection) onUnhandled(state);
  });
};

var dispatchEvent = function (name, promise, reason) {
  var event, handler;
  if (DISPATCH_EVENT) {
    event = document$3.createEvent('Event');
    event.promise = promise;
    event.reason = reason;
    event.initEvent(name, false, true);
    global_1.dispatchEvent(event);
  } else event = { promise: promise, reason: reason };
  if (!NATIVE_REJECTION_EVENT && (handler = global_1['on' + name])) handler(event);
  else if (name === UNHANDLED_REJECTION) hostReportErrors('Unhandled promise rejection', reason);
};

var onUnhandled = function (state) {
  task$1.call(global_1, function () {
    var promise = state.facade;
    var value = state.value;
    var IS_UNHANDLED = isUnhandled(state);
    var result;
    if (IS_UNHANDLED) {
      result = perform(function () {
        if (engineIsNode) {
          process$3.emit('unhandledRejection', value, promise);
        } else dispatchEvent(UNHANDLED_REJECTION, promise, value);
      });
      // Browsers should not trigger `rejectionHandled` event if it was handled here, NodeJS - should
      state.rejection = engineIsNode || isUnhandled(state) ? UNHANDLED : HANDLED;
      if (result.error) throw result.value;
    }
  });
};

var isUnhandled = function (state) {
  return state.rejection !== HANDLED && !state.parent;
};

var onHandleUnhandled = function (state) {
  task$1.call(global_1, function () {
    var promise = state.facade;
    if (engineIsNode) {
      process$3.emit('rejectionHandled', promise);
    } else dispatchEvent(REJECTION_HANDLED, promise, state.value);
  });
};

var bind = function (fn, state, unwrap) {
  return function (value) {
    fn(state, value, unwrap);
  };
};

var internalReject = function (state, value, unwrap) {
  if (state.done) return;
  state.done = true;
  if (unwrap) state = unwrap;
  state.value = value;
  state.state = REJECTED;
  notify$1(state, true);
};

var internalResolve = function (state, value, unwrap) {
  if (state.done) return;
  state.done = true;
  if (unwrap) state = unwrap;
  try {
    if (state.facade === value) throw TypeError$1("Promise can't be resolved itself");
    var then = isThenable(value);
    if (then) {
      microtask(function () {
        var wrapper = { done: false };
        try {
          then.call(value,
            bind(internalResolve, wrapper, state),
            bind(internalReject, wrapper, state)
          );
        } catch (error) {
          internalReject(wrapper, error, state);
        }
      });
    } else {
      state.value = value;
      state.state = FULFILLED;
      notify$1(state, false);
    }
  } catch (error) {
    internalReject({ done: false }, error, state);
  }
};

// constructor polyfill
if (FORCED$4) {
  // 25.4.3.1 Promise(executor)
  PromiseConstructor = function Promise(executor) {
    anInstance(this, PromiseConstructor, PROMISE);
    aFunction$1(executor);
    Internal.call(this);
    var state = getInternalState$3(this);
    try {
      executor(bind(internalResolve, state), bind(internalReject, state));
    } catch (error) {
      internalReject(state, error);
    }
  };
  // eslint-disable-next-line no-unused-vars
  Internal = function Promise(executor) {
    setInternalState$4(this, {
      type: PROMISE,
      done: false,
      notified: false,
      parent: false,
      reactions: [],
      rejection: false,
      state: PENDING,
      value: undefined
    });
  };
  Internal.prototype = redefineAll(PromiseConstructor.prototype, {
    // `Promise.prototype.then` method
    // https://tc39.es/ecma262/#sec-promise.prototype.then
    then: function then(onFulfilled, onRejected) {
      var state = getInternalPromiseState(this);
      var reaction = newPromiseCapability$1(speciesConstructor(this, PromiseConstructor));
      reaction.ok = typeof onFulfilled == 'function' ? onFulfilled : true;
      reaction.fail = typeof onRejected == 'function' && onRejected;
      reaction.domain = engineIsNode ? process$3.domain : undefined;
      state.parent = true;
      state.reactions.push(reaction);
      if (state.state != PENDING) notify$1(state, false);
      return reaction.promise;
    },
    // `Promise.prototype.catch` method
    // https://tc39.es/ecma262/#sec-promise.prototype.catch
    'catch': function (onRejected) {
      return this.then(undefined, onRejected);
    }
  });
  OwnPromiseCapability = function () {
    var promise = new Internal();
    var state = getInternalState$3(promise);
    this.promise = promise;
    this.resolve = bind(internalResolve, state);
    this.reject = bind(internalReject, state);
  };
  newPromiseCapability.f = newPromiseCapability$1 = function (C) {
    return C === PromiseConstructor || C === PromiseWrapper
      ? new OwnPromiseCapability(C)
      : newGenericPromiseCapability(C);
  };

  if ( typeof nativePromiseConstructor == 'function') {
    nativeThen = nativePromiseConstructor.prototype.then;

    // wrap native Promise#then for native async functions
    redefine(nativePromiseConstructor.prototype, 'then', function then(onFulfilled, onRejected) {
      var that = this;
      return new PromiseConstructor(function (resolve, reject) {
        nativeThen.call(that, resolve, reject);
      }).then(onFulfilled, onRejected);
    // https://github.com/zloirock/core-js/issues/640
    }, { unsafe: true });

    // wrap fetch result
    if (typeof $fetch == 'function') _export({ global: true, enumerable: true, forced: true }, {
      // eslint-disable-next-line no-unused-vars
      fetch: function fetch(input /* , init */) {
        return promiseResolve(PromiseConstructor, $fetch.apply(global_1, arguments));
      }
    });
  }
}

_export({ global: true, wrap: true, forced: FORCED$4 }, {
  Promise: PromiseConstructor
});

setToStringTag(PromiseConstructor, PROMISE, false);
setSpecies(PROMISE);

PromiseWrapper = getBuiltIn(PROMISE);

// statics
_export({ target: PROMISE, stat: true, forced: FORCED$4 }, {
  // `Promise.reject` method
  // https://tc39.es/ecma262/#sec-promise.reject
  reject: function reject(r) {
    var capability = newPromiseCapability$1(this);
    capability.reject.call(undefined, r);
    return capability.promise;
  }
});

_export({ target: PROMISE, stat: true, forced:  FORCED$4 }, {
  // `Promise.resolve` method
  // https://tc39.es/ecma262/#sec-promise.resolve
  resolve: function resolve(x) {
    return promiseResolve( this, x);
  }
});

_export({ target: PROMISE, stat: true, forced: INCORRECT_ITERATION$1 }, {
  // `Promise.all` method
  // https://tc39.es/ecma262/#sec-promise.all
  all: function all(iterable) {
    var C = this;
    var capability = newPromiseCapability$1(C);
    var resolve = capability.resolve;
    var reject = capability.reject;
    var result = perform(function () {
      var $promiseResolve = aFunction$1(C.resolve);
      var values = [];
      var counter = 0;
      var remaining = 1;
      iterate(iterable, function (promise) {
        var index = counter++;
        var alreadyCalled = false;
        values.push(undefined);
        remaining++;
        $promiseResolve.call(C, promise).then(function (value) {
          if (alreadyCalled) return;
          alreadyCalled = true;
          values[index] = value;
          --remaining || resolve(values);
        }, reject);
      });
      --remaining || resolve(values);
    });
    if (result.error) reject(result.value);
    return capability.promise;
  },
  // `Promise.race` method
  // https://tc39.es/ecma262/#sec-promise.race
  race: function race(iterable) {
    var C = this;
    var capability = newPromiseCapability$1(C);
    var reject = capability.reject;
    var result = perform(function () {
      var $promiseResolve = aFunction$1(C.resolve);
      iterate(iterable, function (promise) {
        $promiseResolve.call(C, promise).then(capability.resolve, reject);
      });
    });
    if (result.error) reject(result.value);
    return capability.promise;
  }
});

var MATCH = wellKnownSymbol('match');

// `IsRegExp` abstract operation
// https://tc39.es/ecma262/#sec-isregexp
var isRegexp = function (it) {
  var isRegExp;
  return isObject(it) && ((isRegExp = it[MATCH]) !== undefined ? !!isRegExp : classofRaw(it) == 'RegExp');
};

// `RegExp.prototype.flags` getter implementation
// https://tc39.es/ecma262/#sec-get-regexp.prototype.flags
var regexpFlags = function () {
  var that = anObject(this);
  var result = '';
  if (that.global) result += 'g';
  if (that.ignoreCase) result += 'i';
  if (that.multiline) result += 'm';
  if (that.dotAll) result += 's';
  if (that.unicode) result += 'u';
  if (that.sticky) result += 'y';
  return result;
};

// babel-minify transpiles RegExp('a', 'y') -> /a/y and it causes SyntaxError,
// so we use an intermediate function.
function RE(s, f) {
  return RegExp(s, f);
}

var UNSUPPORTED_Y = fails(function () {
  // babel-minify transpiles RegExp('a', 'y') -> /a/y and it causes SyntaxError
  var re = RE('a', 'y');
  re.lastIndex = 2;
  return re.exec('abcd') != null;
});

var BROKEN_CARET = fails(function () {
  // https://bugzilla.mozilla.org/show_bug.cgi?id=773687
  var re = RE('^r', 'gy');
  re.lastIndex = 2;
  return re.exec('str') != null;
});

var regexpStickyHelpers = {
	UNSUPPORTED_Y: UNSUPPORTED_Y,
	BROKEN_CARET: BROKEN_CARET
};

var defineProperty$a = objectDefineProperty.f;
var getOwnPropertyNames$2 = objectGetOwnPropertyNames.f;





var setInternalState$5 = internalState.set;



var MATCH$1 = wellKnownSymbol('match');
var NativeRegExp = global_1.RegExp;
var RegExpPrototype = NativeRegExp.prototype;
var re1 = /a/g;
var re2 = /a/g;

// "new" should create a new object, old webkit bug
var CORRECT_NEW = new NativeRegExp(re1) !== re1;

var UNSUPPORTED_Y$1 = regexpStickyHelpers.UNSUPPORTED_Y;

var FORCED$5 = descriptors && isForced_1('RegExp', (!CORRECT_NEW || UNSUPPORTED_Y$1 || fails(function () {
  re2[MATCH$1] = false;
  // RegExp constructor can alter flags and IsRegExp works correct with @@match
  return NativeRegExp(re1) != re1 || NativeRegExp(re2) == re2 || NativeRegExp(re1, 'i') != '/a/i';
})));

// `RegExp` constructor
// https://tc39.es/ecma262/#sec-regexp-constructor
if (FORCED$5) {
  var RegExpWrapper = function RegExp(pattern, flags) {
    var thisIsRegExp = this instanceof RegExpWrapper;
    var patternIsRegExp = isRegexp(pattern);
    var flagsAreUndefined = flags === undefined;
    var sticky;

    if (!thisIsRegExp && patternIsRegExp && pattern.constructor === RegExpWrapper && flagsAreUndefined) {
      return pattern;
    }

    if (CORRECT_NEW) {
      if (patternIsRegExp && !flagsAreUndefined) pattern = pattern.source;
    } else if (pattern instanceof RegExpWrapper) {
      if (flagsAreUndefined) flags = regexpFlags.call(pattern);
      pattern = pattern.source;
    }

    if (UNSUPPORTED_Y$1) {
      sticky = !!flags && flags.indexOf('y') > -1;
      if (sticky) flags = flags.replace(/y/g, '');
    }

    var result = inheritIfRequired(
      CORRECT_NEW ? new NativeRegExp(pattern, flags) : NativeRegExp(pattern, flags),
      thisIsRegExp ? this : RegExpPrototype,
      RegExpWrapper
    );

    if (UNSUPPORTED_Y$1 && sticky) setInternalState$5(result, { sticky: sticky });

    return result;
  };
  var proxy = function (key) {
    key in RegExpWrapper || defineProperty$a(RegExpWrapper, key, {
      configurable: true,
      get: function () { return NativeRegExp[key]; },
      set: function (it) { NativeRegExp[key] = it; }
    });
  };
  var keys$3 = getOwnPropertyNames$2(NativeRegExp);
  var index = 0;
  while (keys$3.length > index) proxy(keys$3[index++]);
  RegExpPrototype.constructor = RegExpWrapper;
  RegExpWrapper.prototype = RegExpPrototype;
  redefine(global_1, 'RegExp', RegExpWrapper);
}

// https://tc39.es/ecma262/#sec-get-regexp-@@species
setSpecies('RegExp');

var nativeExec = RegExp.prototype.exec;
// This always refers to the native implementation, because the
// String#replace polyfill uses ./fix-regexp-well-known-symbol-logic.js,
// which loads this file before patching the method.
var nativeReplace = String.prototype.replace;

var patchedExec = nativeExec;

var UPDATES_LAST_INDEX_WRONG = (function () {
  var re1 = /a/;
  var re2 = /b*/g;
  nativeExec.call(re1, 'a');
  nativeExec.call(re2, 'a');
  return re1.lastIndex !== 0 || re2.lastIndex !== 0;
})();

var UNSUPPORTED_Y$2 = regexpStickyHelpers.UNSUPPORTED_Y || regexpStickyHelpers.BROKEN_CARET;

// nonparticipating capturing group, copied from es5-shim's String#split patch.
var NPCG_INCLUDED = /()??/.exec('')[1] !== undefined;

var PATCH = UPDATES_LAST_INDEX_WRONG || NPCG_INCLUDED || UNSUPPORTED_Y$2;

if (PATCH) {
  patchedExec = function exec(str) {
    var re = this;
    var lastIndex, reCopy, match, i;
    var sticky = UNSUPPORTED_Y$2 && re.sticky;
    var flags = regexpFlags.call(re);
    var source = re.source;
    var charsAdded = 0;
    var strCopy = str;

    if (sticky) {
      flags = flags.replace('y', '');
      if (flags.indexOf('g') === -1) {
        flags += 'g';
      }

      strCopy = String(str).slice(re.lastIndex);
      // Support anchored sticky behavior.
      if (re.lastIndex > 0 && (!re.multiline || re.multiline && str[re.lastIndex - 1] !== '\n')) {
        source = '(?: ' + source + ')';
        strCopy = ' ' + strCopy;
        charsAdded++;
      }
      // ^(? + rx + ) is needed, in combination with some str slicing, to
      // simulate the 'y' flag.
      reCopy = new RegExp('^(?:' + source + ')', flags);
    }

    if (NPCG_INCLUDED) {
      reCopy = new RegExp('^' + source + '$(?!\\s)', flags);
    }
    if (UPDATES_LAST_INDEX_WRONG) lastIndex = re.lastIndex;

    match = nativeExec.call(sticky ? reCopy : re, strCopy);

    if (sticky) {
      if (match) {
        match.input = match.input.slice(charsAdded);
        match[0] = match[0].slice(charsAdded);
        match.index = re.lastIndex;
        re.lastIndex += match[0].length;
      } else re.lastIndex = 0;
    } else if (UPDATES_LAST_INDEX_WRONG && match) {
      re.lastIndex = re.global ? match.index + match[0].length : lastIndex;
    }
    if (NPCG_INCLUDED && match && match.length > 1) {
      // Fix browsers whose `exec` methods don't consistently return `undefined`
      // for NPCG, like IE8. NOTE: This doesn' work for /(.?)?/
      nativeReplace.call(match[0], reCopy, function () {
        for (i = 1; i < arguments.length - 2; i++) {
          if (arguments[i] === undefined) match[i] = undefined;
        }
      });
    }

    return match;
  };
}

var regexpExec = patchedExec;

// `RegExp.prototype.exec` method
// https://tc39.es/ecma262/#sec-regexp.prototype.exec
_export({ target: 'RegExp', proto: true, forced: /./.exec !== regexpExec }, {
  exec: regexpExec
});

var TO_STRING = 'toString';
var RegExpPrototype$1 = RegExp.prototype;
var nativeToString = RegExpPrototype$1[TO_STRING];

var NOT_GENERIC = fails(function () { return nativeToString.call({ source: 'a', flags: 'b' }) != '/a/b'; });
// FF44- RegExp#toString has a wrong name
var INCORRECT_NAME = nativeToString.name != TO_STRING;

// `RegExp.prototype.toString` method
// https://tc39.es/ecma262/#sec-regexp.prototype.tostring
if (NOT_GENERIC || INCORRECT_NAME) {
  redefine(RegExp.prototype, TO_STRING, function toString() {
    var R = anObject(this);
    var p = String(R.source);
    var rf = R.flags;
    var f = String(rf === undefined && R instanceof RegExp && !('flags' in RegExpPrototype$1) ? regexpFlags.call(R) : rf);
    return '/' + p + '/' + f;
  }, { unsafe: true });
}

// `Set` constructor
// https://tc39.es/ecma262/#sec-set-objects
var es_set = collection('Set', function (init) {
  return function Set() { return init(this, arguments.length ? arguments[0] : undefined); };
}, collectionStrong);

var notARegexp = function (it) {
  if (isRegexp(it)) {
    throw TypeError("The method doesn't accept regular expressions");
  } return it;
};

var MATCH$2 = wellKnownSymbol('match');

var correctIsRegexpLogic = function (METHOD_NAME) {
  var regexp = /./;
  try {
    '/./'[METHOD_NAME](regexp);
  } catch (error1) {
    try {
      regexp[MATCH$2] = false;
      return '/./'[METHOD_NAME](regexp);
    } catch (error2) { /* empty */ }
  } return false;
};

var getOwnPropertyDescriptor$4 = objectGetOwnPropertyDescriptor.f;






var nativeEndsWith = ''.endsWith;
var min$4 = Math.min;

var CORRECT_IS_REGEXP_LOGIC = correctIsRegexpLogic('endsWith');
// https://github.com/zloirock/core-js/pull/702
var MDN_POLYFILL_BUG =  !CORRECT_IS_REGEXP_LOGIC && !!function () {
  var descriptor = getOwnPropertyDescriptor$4(String.prototype, 'endsWith');
  return descriptor && !descriptor.writable;
}();

// `String.prototype.endsWith` method
// https://tc39.es/ecma262/#sec-string.prototype.endswith
_export({ target: 'String', proto: true, forced: !MDN_POLYFILL_BUG && !CORRECT_IS_REGEXP_LOGIC }, {
  endsWith: function endsWith(searchString /* , endPosition = @length */) {
    var that = String(requireObjectCoercible(this));
    notARegexp(searchString);
    var endPosition = arguments.length > 1 ? arguments[1] : undefined;
    var len = toLength(that.length);
    var end = endPosition === undefined ? len : min$4(toLength(endPosition), len);
    var search = String(searchString);
    return nativeEndsWith
      ? nativeEndsWith.call(that, search, end)
      : that.slice(end - search.length, end) === search;
  }
});

// `String.prototype.includes` method
// https://tc39.es/ecma262/#sec-string.prototype.includes
_export({ target: 'String', proto: true, forced: !correctIsRegexpLogic('includes') }, {
  includes: function includes(searchString /* , position = 0 */) {
    return !!~String(requireObjectCoercible(this))
      .indexOf(notARegexp(searchString), arguments.length > 1 ? arguments[1] : undefined);
  }
});

// `String.prototype.{ codePointAt, at }` methods implementation
var createMethod$4 = function (CONVERT_TO_STRING) {
  return function ($this, pos) {
    var S = String(requireObjectCoercible($this));
    var position = toInteger(pos);
    var size = S.length;
    var first, second;
    if (position < 0 || position >= size) return CONVERT_TO_STRING ? '' : undefined;
    first = S.charCodeAt(position);
    return first < 0xD800 || first > 0xDBFF || position + 1 === size
      || (second = S.charCodeAt(position + 1)) < 0xDC00 || second > 0xDFFF
        ? CONVERT_TO_STRING ? S.charAt(position) : first
        : CONVERT_TO_STRING ? S.slice(position, position + 2) : (first - 0xD800 << 10) + (second - 0xDC00) + 0x10000;
  };
};

var stringMultibyte = {
  // `String.prototype.codePointAt` method
  // https://tc39.es/ecma262/#sec-string.prototype.codepointat
  codeAt: createMethod$4(false),
  // `String.prototype.at` method
  // https://github.com/mathiasbynens/String.prototype.at
  charAt: createMethod$4(true)
};

var charAt = stringMultibyte.charAt;



var STRING_ITERATOR = 'String Iterator';
var setInternalState$6 = internalState.set;
var getInternalState$4 = internalState.getterFor(STRING_ITERATOR);

// `String.prototype[@@iterator]` method
// https://tc39.es/ecma262/#sec-string.prototype-@@iterator
defineIterator(String, 'String', function (iterated) {
  setInternalState$6(this, {
    type: STRING_ITERATOR,
    string: String(iterated),
    index: 0
  });
// `%StringIteratorPrototype%.next` method
// https://tc39.es/ecma262/#sec-%stringiteratorprototype%.next
}, function next() {
  var state = getInternalState$4(this);
  var string = state.string;
  var index = state.index;
  var point;
  if (index >= string.length) return { value: undefined, done: true };
  point = charAt(string, index);
  state.index += point.length;
  return { value: point, done: false };
});

// TODO: Remove from `core-js@4` since it's moved to entry points







var SPECIES$6 = wellKnownSymbol('species');

var REPLACE_SUPPORTS_NAMED_GROUPS = !fails(function () {
  // #replace needs built-in support for named groups.
  // #match works fine because it just return the exec results, even if it has
  // a "grops" property.
  var re = /./;
  re.exec = function () {
    var result = [];
    result.groups = { a: '7' };
    return result;
  };
  return ''.replace(re, '$<a>') !== '7';
});

// IE <= 11 replaces $0 with the whole match, as if it was $&
// https://stackoverflow.com/questions/6024666/getting-ie-to-replace-a-regex-with-the-literal-string-0
var REPLACE_KEEPS_$0 = (function () {
  return 'a'.replace(/./, '$0') === '$0';
})();

var REPLACE = wellKnownSymbol('replace');
// Safari <= 13.0.3(?) substitutes nth capture where n>m with an empty string
var REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE = (function () {
  if (/./[REPLACE]) {
    return /./[REPLACE]('a', '$0') === '';
  }
  return false;
})();

// Chrome 51 has a buggy "split" implementation when RegExp#exec !== nativeExec
// Weex JS has frozen built-in prototypes, so use try / catch wrapper
var SPLIT_WORKS_WITH_OVERWRITTEN_EXEC = !fails(function () {
  var re = /(?:)/;
  var originalExec = re.exec;
  re.exec = function () { return originalExec.apply(this, arguments); };
  var result = 'ab'.split(re);
  return result.length !== 2 || result[0] !== 'a' || result[1] !== 'b';
});

var fixRegexpWellKnownSymbolLogic = function (KEY, length, exec, sham) {
  var SYMBOL = wellKnownSymbol(KEY);

  var DELEGATES_TO_SYMBOL = !fails(function () {
    // String methods call symbol-named RegEp methods
    var O = {};
    O[SYMBOL] = function () { return 7; };
    return ''[KEY](O) != 7;
  });

  var DELEGATES_TO_EXEC = DELEGATES_TO_SYMBOL && !fails(function () {
    // Symbol-named RegExp methods call .exec
    var execCalled = false;
    var re = /a/;

    if (KEY === 'split') {
      // We can't use real regex here since it causes deoptimization
      // and serious performance degradation in V8
      // https://github.com/zloirock/core-js/issues/306
      re = {};
      // RegExp[@@split] doesn't call the regex's exec method, but first creates
      // a new one. We need to return the patched regex when creating the new one.
      re.constructor = {};
      re.constructor[SPECIES$6] = function () { return re; };
      re.flags = '';
      re[SYMBOL] = /./[SYMBOL];
    }

    re.exec = function () { execCalled = true; return null; };

    re[SYMBOL]('');
    return !execCalled;
  });

  if (
    !DELEGATES_TO_SYMBOL ||
    !DELEGATES_TO_EXEC ||
    (KEY === 'replace' && !(
      REPLACE_SUPPORTS_NAMED_GROUPS &&
      REPLACE_KEEPS_$0 &&
      !REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE
    )) ||
    (KEY === 'split' && !SPLIT_WORKS_WITH_OVERWRITTEN_EXEC)
  ) {
    var nativeRegExpMethod = /./[SYMBOL];
    var methods = exec(SYMBOL, ''[KEY], function (nativeMethod, regexp, str, arg2, forceStringMethod) {
      if (regexp.exec === regexpExec) {
        if (DELEGATES_TO_SYMBOL && !forceStringMethod) {
          // The native String method already delegates to @@method (this
          // polyfilled function), leasing to infinite recursion.
          // We avoid it by directly calling the native @@method method.
          return { done: true, value: nativeRegExpMethod.call(regexp, str, arg2) };
        }
        return { done: true, value: nativeMethod.call(str, regexp, arg2) };
      }
      return { done: false };
    }, {
      REPLACE_KEEPS_$0: REPLACE_KEEPS_$0,
      REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE: REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE
    });
    var stringMethod = methods[0];
    var regexMethod = methods[1];

    redefine(String.prototype, KEY, stringMethod);
    redefine(RegExp.prototype, SYMBOL, length == 2
      // 21.2.5.8 RegExp.prototype[@@replace](string, replaceValue)
      // 21.2.5.11 RegExp.prototype[@@split](string, limit)
      ? function (string, arg) { return regexMethod.call(string, this, arg); }
      // 21.2.5.6 RegExp.prototype[@@match](string)
      // 21.2.5.9 RegExp.prototype[@@search](string)
      : function (string) { return regexMethod.call(string, this); }
    );
  }

  if (sham) createNonEnumerableProperty(RegExp.prototype[SYMBOL], 'sham', true);
};

var charAt$1 = stringMultibyte.charAt;

// `AdvanceStringIndex` abstract operation
// https://tc39.es/ecma262/#sec-advancestringindex
var advanceStringIndex = function (S, index, unicode) {
  return index + (unicode ? charAt$1(S, index).length : 1);
};

// `RegExpExec` abstract operation
// https://tc39.es/ecma262/#sec-regexpexec
var regexpExecAbstract = function (R, S) {
  var exec = R.exec;
  if (typeof exec === 'function') {
    var result = exec.call(R, S);
    if (typeof result !== 'object') {
      throw TypeError('RegExp exec method returned something other than an Object or null');
    }
    return result;
  }

  if (classofRaw(R) !== 'RegExp') {
    throw TypeError('RegExp#exec called on incompatible receiver');
  }

  return regexpExec.call(R, S);
};

// @@match logic
fixRegexpWellKnownSymbolLogic('match', 1, function (MATCH, nativeMatch, maybeCallNative) {
  return [
    // `String.prototype.match` method
    // https://tc39.es/ecma262/#sec-string.prototype.match
    function match(regexp) {
      var O = requireObjectCoercible(this);
      var matcher = regexp == undefined ? undefined : regexp[MATCH];
      return matcher !== undefined ? matcher.call(regexp, O) : new RegExp(regexp)[MATCH](String(O));
    },
    // `RegExp.prototype[@@match]` method
    // https://tc39.es/ecma262/#sec-regexp.prototype-@@match
    function (regexp) {
      var res = maybeCallNative(nativeMatch, regexp, this);
      if (res.done) return res.value;

      var rx = anObject(regexp);
      var S = String(this);

      if (!rx.global) return regexpExecAbstract(rx, S);

      var fullUnicode = rx.unicode;
      rx.lastIndex = 0;
      var A = [];
      var n = 0;
      var result;
      while ((result = regexpExecAbstract(rx, S)) !== null) {
        var matchStr = String(result[0]);
        A[n] = matchStr;
        if (matchStr === '') rx.lastIndex = advanceStringIndex(S, toLength(rx.lastIndex), fullUnicode);
        n++;
      }
      return n === 0 ? null : A;
    }
  ];
});

var floor$2 = Math.floor;
var replace = ''.replace;
var SUBSTITUTION_SYMBOLS = /\$([$&'`]|\d\d?|<[^>]*>)/g;
var SUBSTITUTION_SYMBOLS_NO_NAMED = /\$([$&'`]|\d\d?)/g;

// https://tc39.es/ecma262/#sec-getsubstitution
var getSubstitution = function (matched, str, position, captures, namedCaptures, replacement) {
  var tailPos = position + matched.length;
  var m = captures.length;
  var symbols = SUBSTITUTION_SYMBOLS_NO_NAMED;
  if (namedCaptures !== undefined) {
    namedCaptures = toObject(namedCaptures);
    symbols = SUBSTITUTION_SYMBOLS;
  }
  return replace.call(replacement, symbols, function (match, ch) {
    var capture;
    switch (ch.charAt(0)) {
      case '$': return '$';
      case '&': return matched;
      case '`': return str.slice(0, position);
      case "'": return str.slice(tailPos);
      case '<':
        capture = namedCaptures[ch.slice(1, -1)];
        break;
      default: // \d\d?
        var n = +ch;
        if (n === 0) return match;
        if (n > m) {
          var f = floor$2(n / 10);
          if (f === 0) return match;
          if (f <= m) return captures[f - 1] === undefined ? ch.charAt(1) : captures[f - 1] + ch.charAt(1);
          return match;
        }
        capture = captures[n - 1];
    }
    return capture === undefined ? '' : capture;
  });
};

var max$3 = Math.max;
var min$5 = Math.min;

var maybeToString = function (it) {
  return it === undefined ? it : String(it);
};

// @@replace logic
fixRegexpWellKnownSymbolLogic('replace', 2, function (REPLACE, nativeReplace, maybeCallNative, reason) {
  var REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE = reason.REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE;
  var REPLACE_KEEPS_$0 = reason.REPLACE_KEEPS_$0;
  var UNSAFE_SUBSTITUTE = REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE ? '$' : '$0';

  return [
    // `String.prototype.replace` method
    // https://tc39.es/ecma262/#sec-string.prototype.replace
    function replace(searchValue, replaceValue) {
      var O = requireObjectCoercible(this);
      var replacer = searchValue == undefined ? undefined : searchValue[REPLACE];
      return replacer !== undefined
        ? replacer.call(searchValue, O, replaceValue)
        : nativeReplace.call(String(O), searchValue, replaceValue);
    },
    // `RegExp.prototype[@@replace]` method
    // https://tc39.es/ecma262/#sec-regexp.prototype-@@replace
    function (regexp, replaceValue) {
      if (
        (!REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE && REPLACE_KEEPS_$0) ||
        (typeof replaceValue === 'string' && replaceValue.indexOf(UNSAFE_SUBSTITUTE) === -1)
      ) {
        var res = maybeCallNative(nativeReplace, regexp, this, replaceValue);
        if (res.done) return res.value;
      }

      var rx = anObject(regexp);
      var S = String(this);

      var functionalReplace = typeof replaceValue === 'function';
      if (!functionalReplace) replaceValue = String(replaceValue);

      var global = rx.global;
      if (global) {
        var fullUnicode = rx.unicode;
        rx.lastIndex = 0;
      }
      var results = [];
      while (true) {
        var result = regexpExecAbstract(rx, S);
        if (result === null) break;

        results.push(result);
        if (!global) break;

        var matchStr = String(result[0]);
        if (matchStr === '') rx.lastIndex = advanceStringIndex(S, toLength(rx.lastIndex), fullUnicode);
      }

      var accumulatedResult = '';
      var nextSourcePosition = 0;
      for (var i = 0; i < results.length; i++) {
        result = results[i];

        var matched = String(result[0]);
        var position = max$3(min$5(toInteger(result.index), S.length), 0);
        var captures = [];
        // NOTE: This is equivalent to
        //   captures = result.slice(1).map(maybeToString)
        // but for some reason `nativeSlice.call(result, 1, result.length)` (called in
        // the slice polyfill when slicing native arrays) "doesn't work" in safari 9 and
        // causes a crash (https://pastebin.com/N21QzeQA) when trying to debug it.
        for (var j = 1; j < result.length; j++) captures.push(maybeToString(result[j]));
        var namedCaptures = result.groups;
        if (functionalReplace) {
          var replacerArgs = [matched].concat(captures, position, S);
          if (namedCaptures !== undefined) replacerArgs.push(namedCaptures);
          var replacement = String(replaceValue.apply(undefined, replacerArgs));
        } else {
          replacement = getSubstitution(matched, S, position, captures, namedCaptures, replaceValue);
        }
        if (position >= nextSourcePosition) {
          accumulatedResult += S.slice(nextSourcePosition, position) + replacement;
          nextSourcePosition = position + matched.length;
        }
      }
      return accumulatedResult + S.slice(nextSourcePosition);
    }
  ];
});

// `SameValue` abstract operation
// https://tc39.es/ecma262/#sec-samevalue
var sameValue = Object.is || function is(x, y) {
  // eslint-disable-next-line no-self-compare
  return x === y ? x !== 0 || 1 / x === 1 / y : x != x && y != y;
};

// @@search logic
fixRegexpWellKnownSymbolLogic('search', 1, function (SEARCH, nativeSearch, maybeCallNative) {
  return [
    // `String.prototype.search` method
    // https://tc39.es/ecma262/#sec-string.prototype.search
    function search(regexp) {
      var O = requireObjectCoercible(this);
      var searcher = regexp == undefined ? undefined : regexp[SEARCH];
      return searcher !== undefined ? searcher.call(regexp, O) : new RegExp(regexp)[SEARCH](String(O));
    },
    // `RegExp.prototype[@@search]` method
    // https://tc39.es/ecma262/#sec-regexp.prototype-@@search
    function (regexp) {
      var res = maybeCallNative(nativeSearch, regexp, this);
      if (res.done) return res.value;

      var rx = anObject(regexp);
      var S = String(this);

      var previousLastIndex = rx.lastIndex;
      if (!sameValue(previousLastIndex, 0)) rx.lastIndex = 0;
      var result = regexpExecAbstract(rx, S);
      if (!sameValue(rx.lastIndex, previousLastIndex)) rx.lastIndex = previousLastIndex;
      return result === null ? -1 : result.index;
    }
  ];
});

var arrayPush = [].push;
var min$6 = Math.min;
var MAX_UINT32 = 0xFFFFFFFF;

// babel-minify transpiles RegExp('x', 'y') -> /x/y and it causes SyntaxError
var SUPPORTS_Y = !fails(function () { return !RegExp(MAX_UINT32, 'y'); });

// @@split logic
fixRegexpWellKnownSymbolLogic('split', 2, function (SPLIT, nativeSplit, maybeCallNative) {
  var internalSplit;
  if (
    'abbc'.split(/(b)*/)[1] == 'c' ||
    'test'.split(/(?:)/, -1).length != 4 ||
    'ab'.split(/(?:ab)*/).length != 2 ||
    '.'.split(/(.?)(.?)/).length != 4 ||
    '.'.split(/()()/).length > 1 ||
    ''.split(/.?/).length
  ) {
    // based on es5-shim implementation, need to rework it
    internalSplit = function (separator, limit) {
      var string = String(requireObjectCoercible(this));
      var lim = limit === undefined ? MAX_UINT32 : limit >>> 0;
      if (lim === 0) return [];
      if (separator === undefined) return [string];
      // If `separator` is not a regex, use native split
      if (!isRegexp(separator)) {
        return nativeSplit.call(string, separator, lim);
      }
      var output = [];
      var flags = (separator.ignoreCase ? 'i' : '') +
                  (separator.multiline ? 'm' : '') +
                  (separator.unicode ? 'u' : '') +
                  (separator.sticky ? 'y' : '');
      var lastLastIndex = 0;
      // Make `global` and avoid `lastIndex` issues by working with a copy
      var separatorCopy = new RegExp(separator.source, flags + 'g');
      var match, lastIndex, lastLength;
      while (match = regexpExec.call(separatorCopy, string)) {
        lastIndex = separatorCopy.lastIndex;
        if (lastIndex > lastLastIndex) {
          output.push(string.slice(lastLastIndex, match.index));
          if (match.length > 1 && match.index < string.length) arrayPush.apply(output, match.slice(1));
          lastLength = match[0].length;
          lastLastIndex = lastIndex;
          if (output.length >= lim) break;
        }
        if (separatorCopy.lastIndex === match.index) separatorCopy.lastIndex++; // Avoid an infinite loop
      }
      if (lastLastIndex === string.length) {
        if (lastLength || !separatorCopy.test('')) output.push('');
      } else output.push(string.slice(lastLastIndex));
      return output.length > lim ? output.slice(0, lim) : output;
    };
  // Chakra, V8
  } else if ('0'.split(undefined, 0).length) {
    internalSplit = function (separator, limit) {
      return separator === undefined && limit === 0 ? [] : nativeSplit.call(this, separator, limit);
    };
  } else internalSplit = nativeSplit;

  return [
    // `String.prototype.split` method
    // https://tc39.es/ecma262/#sec-string.prototype.split
    function split(separator, limit) {
      var O = requireObjectCoercible(this);
      var splitter = separator == undefined ? undefined : separator[SPLIT];
      return splitter !== undefined
        ? splitter.call(separator, O, limit)
        : internalSplit.call(String(O), separator, limit);
    },
    // `RegExp.prototype[@@split]` method
    // https://tc39.es/ecma262/#sec-regexp.prototype-@@split
    //
    // NOTE: This cannot be properly polyfilled in engines that don't support
    // the 'y' flag.
    function (regexp, limit) {
      var res = maybeCallNative(internalSplit, regexp, this, limit, internalSplit !== nativeSplit);
      if (res.done) return res.value;

      var rx = anObject(regexp);
      var S = String(this);
      var C = speciesConstructor(rx, RegExp);

      var unicodeMatching = rx.unicode;
      var flags = (rx.ignoreCase ? 'i' : '') +
                  (rx.multiline ? 'm' : '') +
                  (rx.unicode ? 'u' : '') +
                  (SUPPORTS_Y ? 'y' : 'g');

      // ^(? + rx + ) is needed, in combination with some S slicing, to
      // simulate the 'y' flag.
      var splitter = new C(SUPPORTS_Y ? rx : '^(?:' + rx.source + ')', flags);
      var lim = limit === undefined ? MAX_UINT32 : limit >>> 0;
      if (lim === 0) return [];
      if (S.length === 0) return regexpExecAbstract(splitter, S) === null ? [S] : [];
      var p = 0;
      var q = 0;
      var A = [];
      while (q < S.length) {
        splitter.lastIndex = SUPPORTS_Y ? q : 0;
        var z = regexpExecAbstract(splitter, SUPPORTS_Y ? S : S.slice(q));
        var e;
        if (
          z === null ||
          (e = min$6(toLength(splitter.lastIndex + (SUPPORTS_Y ? 0 : q)), S.length)) === p
        ) {
          q = advanceStringIndex(S, q, unicodeMatching);
        } else {
          A.push(S.slice(p, q));
          if (A.length === lim) return A;
          for (var i = 1; i <= z.length - 1; i++) {
            A.push(z[i]);
            if (A.length === lim) return A;
          }
          q = p = e;
        }
      }
      A.push(S.slice(p));
      return A;
    }
  ];
}, !SUPPORTS_Y);

var getOwnPropertyDescriptor$5 = objectGetOwnPropertyDescriptor.f;






var nativeStartsWith = ''.startsWith;
var min$7 = Math.min;

var CORRECT_IS_REGEXP_LOGIC$1 = correctIsRegexpLogic('startsWith');
// https://github.com/zloirock/core-js/pull/702
var MDN_POLYFILL_BUG$1 =  !CORRECT_IS_REGEXP_LOGIC$1 && !!function () {
  var descriptor = getOwnPropertyDescriptor$5(String.prototype, 'startsWith');
  return descriptor && !descriptor.writable;
}();

// `String.prototype.startsWith` method
// https://tc39.es/ecma262/#sec-string.prototype.startswith
_export({ target: 'String', proto: true, forced: !MDN_POLYFILL_BUG$1 && !CORRECT_IS_REGEXP_LOGIC$1 }, {
  startsWith: function startsWith(searchString /* , position = 0 */) {
    var that = String(requireObjectCoercible(this));
    notARegexp(searchString);
    var index = toLength(min$7(arguments.length > 1 ? arguments[1] : undefined, that.length));
    var search = String(searchString);
    return nativeStartsWith
      ? nativeStartsWith.call(that, search, index)
      : that.slice(index, index + search.length) === search;
  }
});

var non = '\u200B\u0085\u180E';

// check that a method works with the correct list
// of whitespaces and has a correct name
var stringTrimForced = function (METHOD_NAME) {
  return fails(function () {
    return !!whitespaces[METHOD_NAME]() || non[METHOD_NAME]() != non || whitespaces[METHOD_NAME].name !== METHOD_NAME;
  });
};

var $trim = stringTrim.trim;


// `String.prototype.trim` method
// https://tc39.es/ecma262/#sec-string.prototype.trim
_export({ target: 'String', proto: true, forced: stringTrimForced('trim') }, {
  trim: function trim() {
    return $trim(this);
  }
});

var quot = /"/g;

// B.2.3.2.1 CreateHTML(string, tag, attribute, value)
// https://tc39.es/ecma262/#sec-createhtml
var createHtml = function (string, tag, attribute, value) {
  var S = String(requireObjectCoercible(string));
  var p1 = '<' + tag;
  if (attribute !== '') p1 += ' ' + attribute + '="' + String(value).replace(quot, '&quot;') + '"';
  return p1 + '>' + S + '</' + tag + '>';
};

// check the existence of a method, lowercase
// of a tag and escaping quotes in arguments
var stringHtmlForced = function (METHOD_NAME) {
  return fails(function () {
    var test = ''[METHOD_NAME]('"');
    return test !== test.toLowerCase() || test.split('"').length > 3;
  });
};

// `String.prototype.link` method
// https://tc39.es/ecma262/#sec-string.prototype.link
_export({ target: 'String', proto: true, forced: stringHtmlForced('link') }, {
  link: function link(url) {
    return createHtml(this, 'a', 'href', url);
  }
});

/* eslint-disable no-new */



var NATIVE_ARRAY_BUFFER_VIEWS$2 = arrayBufferViewCore.NATIVE_ARRAY_BUFFER_VIEWS;

var ArrayBuffer$3 = global_1.ArrayBuffer;
var Int8Array$2 = global_1.Int8Array;

var typedArrayConstructorsRequireWrappers = !NATIVE_ARRAY_BUFFER_VIEWS$2 || !fails(function () {
  Int8Array$2(1);
}) || !fails(function () {
  new Int8Array$2(-1);
}) || !checkCorrectnessOfIteration(function (iterable) {
  new Int8Array$2();
  new Int8Array$2(null);
  new Int8Array$2(1.5);
  new Int8Array$2(iterable);
}, true) || fails(function () {
  // Safari (11+) bug - a reason why even Safari 13 should load a typed array polyfill
  return new Int8Array$2(new ArrayBuffer$3(2), 1, undefined).length !== 1;
});

var toPositiveInteger = function (it) {
  var result = toInteger(it);
  if (result < 0) throw RangeError("The argument can't be less than 0");
  return result;
};

var toOffset = function (it, BYTES) {
  var offset = toPositiveInteger(it);
  if (offset % BYTES) throw RangeError('Wrong offset');
  return offset;
};

var aTypedArrayConstructor$1 = arrayBufferViewCore.aTypedArrayConstructor;

var typedArrayFrom = function from(source /* , mapfn, thisArg */) {
  var O = toObject(source);
  var argumentsLength = arguments.length;
  var mapfn = argumentsLength > 1 ? arguments[1] : undefined;
  var mapping = mapfn !== undefined;
  var iteratorMethod = getIteratorMethod(O);
  var i, length, result, step, iterator, next;
  if (iteratorMethod != undefined && !isArrayIteratorMethod(iteratorMethod)) {
    iterator = iteratorMethod.call(O);
    next = iterator.next;
    O = [];
    while (!(step = next.call(iterator)).done) {
      O.push(step.value);
    }
  }
  if (mapping && argumentsLength > 2) {
    mapfn = functionBindContext(mapfn, arguments[2], 2);
  }
  length = toLength(O.length);
  result = new (aTypedArrayConstructor$1(this))(length);
  for (i = 0; length > i; i++) {
    result[i] = mapping ? mapfn(O[i], i) : O[i];
  }
  return result;
};

var typedArrayConstructor = createCommonjsModule(function (module) {


















var getOwnPropertyNames = objectGetOwnPropertyNames.f;

var forEach = arrayIteration.forEach;






var getInternalState = internalState.get;
var setInternalState = internalState.set;
var nativeDefineProperty = objectDefineProperty.f;
var nativeGetOwnPropertyDescriptor = objectGetOwnPropertyDescriptor.f;
var round = Math.round;
var RangeError = global_1.RangeError;
var ArrayBuffer = arrayBuffer.ArrayBuffer;
var DataView = arrayBuffer.DataView;
var NATIVE_ARRAY_BUFFER_VIEWS = arrayBufferViewCore.NATIVE_ARRAY_BUFFER_VIEWS;
var TYPED_ARRAY_TAG = arrayBufferViewCore.TYPED_ARRAY_TAG;
var TypedArray = arrayBufferViewCore.TypedArray;
var TypedArrayPrototype = arrayBufferViewCore.TypedArrayPrototype;
var aTypedArrayConstructor = arrayBufferViewCore.aTypedArrayConstructor;
var isTypedArray = arrayBufferViewCore.isTypedArray;
var BYTES_PER_ELEMENT = 'BYTES_PER_ELEMENT';
var WRONG_LENGTH = 'Wrong length';

var fromList = function (C, list) {
  var index = 0;
  var length = list.length;
  var result = new (aTypedArrayConstructor(C))(length);
  while (length > index) result[index] = list[index++];
  return result;
};

var addGetter = function (it, key) {
  nativeDefineProperty(it, key, { get: function () {
    return getInternalState(this)[key];
  } });
};

var isArrayBuffer = function (it) {
  var klass;
  return it instanceof ArrayBuffer || (klass = classof(it)) == 'ArrayBuffer' || klass == 'SharedArrayBuffer';
};

var isTypedArrayIndex = function (target, key) {
  return isTypedArray(target)
    && typeof key != 'symbol'
    && key in target
    && String(+key) == String(key);
};

var wrappedGetOwnPropertyDescriptor = function getOwnPropertyDescriptor(target, key) {
  return isTypedArrayIndex(target, key = toPrimitive(key, true))
    ? createPropertyDescriptor(2, target[key])
    : nativeGetOwnPropertyDescriptor(target, key);
};

var wrappedDefineProperty = function defineProperty(target, key, descriptor) {
  if (isTypedArrayIndex(target, key = toPrimitive(key, true))
    && isObject(descriptor)
    && has(descriptor, 'value')
    && !has(descriptor, 'get')
    && !has(descriptor, 'set')
    // TODO: add validation descriptor w/o calling accessors
    && !descriptor.configurable
    && (!has(descriptor, 'writable') || descriptor.writable)
    && (!has(descriptor, 'enumerable') || descriptor.enumerable)
  ) {
    target[key] = descriptor.value;
    return target;
  } return nativeDefineProperty(target, key, descriptor);
};

if (descriptors) {
  if (!NATIVE_ARRAY_BUFFER_VIEWS) {
    objectGetOwnPropertyDescriptor.f = wrappedGetOwnPropertyDescriptor;
    objectDefineProperty.f = wrappedDefineProperty;
    addGetter(TypedArrayPrototype, 'buffer');
    addGetter(TypedArrayPrototype, 'byteOffset');
    addGetter(TypedArrayPrototype, 'byteLength');
    addGetter(TypedArrayPrototype, 'length');
  }

  _export({ target: 'Object', stat: true, forced: !NATIVE_ARRAY_BUFFER_VIEWS }, {
    getOwnPropertyDescriptor: wrappedGetOwnPropertyDescriptor,
    defineProperty: wrappedDefineProperty
  });

  module.exports = function (TYPE, wrapper, CLAMPED) {
    var BYTES = TYPE.match(/\d+$/)[0] / 8;
    var CONSTRUCTOR_NAME = TYPE + (CLAMPED ? 'Clamped' : '') + 'Array';
    var GETTER = 'get' + TYPE;
    var SETTER = 'set' + TYPE;
    var NativeTypedArrayConstructor = global_1[CONSTRUCTOR_NAME];
    var TypedArrayConstructor = NativeTypedArrayConstructor;
    var TypedArrayConstructorPrototype = TypedArrayConstructor && TypedArrayConstructor.prototype;
    var exported = {};

    var getter = function (that, index) {
      var data = getInternalState(that);
      return data.view[GETTER](index * BYTES + data.byteOffset, true);
    };

    var setter = function (that, index, value) {
      var data = getInternalState(that);
      if (CLAMPED) value = (value = round(value)) < 0 ? 0 : value > 0xFF ? 0xFF : value & 0xFF;
      data.view[SETTER](index * BYTES + data.byteOffset, value, true);
    };

    var addElement = function (that, index) {
      nativeDefineProperty(that, index, {
        get: function () {
          return getter(this, index);
        },
        set: function (value) {
          return setter(this, index, value);
        },
        enumerable: true
      });
    };

    if (!NATIVE_ARRAY_BUFFER_VIEWS) {
      TypedArrayConstructor = wrapper(function (that, data, offset, $length) {
        anInstance(that, TypedArrayConstructor, CONSTRUCTOR_NAME);
        var index = 0;
        var byteOffset = 0;
        var buffer, byteLength, length;
        if (!isObject(data)) {
          length = toIndex(data);
          byteLength = length * BYTES;
          buffer = new ArrayBuffer(byteLength);
        } else if (isArrayBuffer(data)) {
          buffer = data;
          byteOffset = toOffset(offset, BYTES);
          var $len = data.byteLength;
          if ($length === undefined) {
            if ($len % BYTES) throw RangeError(WRONG_LENGTH);
            byteLength = $len - byteOffset;
            if (byteLength < 0) throw RangeError(WRONG_LENGTH);
          } else {
            byteLength = toLength($length) * BYTES;
            if (byteLength + byteOffset > $len) throw RangeError(WRONG_LENGTH);
          }
          length = byteLength / BYTES;
        } else if (isTypedArray(data)) {
          return fromList(TypedArrayConstructor, data);
        } else {
          return typedArrayFrom.call(TypedArrayConstructor, data);
        }
        setInternalState(that, {
          buffer: buffer,
          byteOffset: byteOffset,
          byteLength: byteLength,
          length: length,
          view: new DataView(buffer)
        });
        while (index < length) addElement(that, index++);
      });

      if (objectSetPrototypeOf) objectSetPrototypeOf(TypedArrayConstructor, TypedArray);
      TypedArrayConstructorPrototype = TypedArrayConstructor.prototype = objectCreate(TypedArrayPrototype);
    } else if (typedArrayConstructorsRequireWrappers) {
      TypedArrayConstructor = wrapper(function (dummy, data, typedArrayOffset, $length) {
        anInstance(dummy, TypedArrayConstructor, CONSTRUCTOR_NAME);
        return inheritIfRequired(function () {
          if (!isObject(data)) return new NativeTypedArrayConstructor(toIndex(data));
          if (isArrayBuffer(data)) return $length !== undefined
            ? new NativeTypedArrayConstructor(data, toOffset(typedArrayOffset, BYTES), $length)
            : typedArrayOffset !== undefined
              ? new NativeTypedArrayConstructor(data, toOffset(typedArrayOffset, BYTES))
              : new NativeTypedArrayConstructor(data);
          if (isTypedArray(data)) return fromList(TypedArrayConstructor, data);
          return typedArrayFrom.call(TypedArrayConstructor, data);
        }(), dummy, TypedArrayConstructor);
      });

      if (objectSetPrototypeOf) objectSetPrototypeOf(TypedArrayConstructor, TypedArray);
      forEach(getOwnPropertyNames(NativeTypedArrayConstructor), function (key) {
        if (!(key in TypedArrayConstructor)) {
          createNonEnumerableProperty(TypedArrayConstructor, key, NativeTypedArrayConstructor[key]);
        }
      });
      TypedArrayConstructor.prototype = TypedArrayConstructorPrototype;
    }

    if (TypedArrayConstructorPrototype.constructor !== TypedArrayConstructor) {
      createNonEnumerableProperty(TypedArrayConstructorPrototype, 'constructor', TypedArrayConstructor);
    }

    if (TYPED_ARRAY_TAG) {
      createNonEnumerableProperty(TypedArrayConstructorPrototype, TYPED_ARRAY_TAG, CONSTRUCTOR_NAME);
    }

    exported[CONSTRUCTOR_NAME] = TypedArrayConstructor;

    _export({
      global: true, forced: TypedArrayConstructor != NativeTypedArrayConstructor, sham: !NATIVE_ARRAY_BUFFER_VIEWS
    }, exported);

    if (!(BYTES_PER_ELEMENT in TypedArrayConstructor)) {
      createNonEnumerableProperty(TypedArrayConstructor, BYTES_PER_ELEMENT, BYTES);
    }

    if (!(BYTES_PER_ELEMENT in TypedArrayConstructorPrototype)) {
      createNonEnumerableProperty(TypedArrayConstructorPrototype, BYTES_PER_ELEMENT, BYTES);
    }

    setSpecies(CONSTRUCTOR_NAME);
  };
} else module.exports = function () { /* empty */ };
});

// `Uint8Array` constructor
// https://tc39.es/ecma262/#sec-typedarray-objects
typedArrayConstructor('Uint8', function (init) {
  return function Uint8Array(data, byteOffset, length) {
    return init(this, data, byteOffset, length);
  };
});

var min$8 = Math.min;

// `Array.prototype.copyWithin` method implementation
// https://tc39.es/ecma262/#sec-array.prototype.copywithin
var arrayCopyWithin = [].copyWithin || function copyWithin(target /* = 0 */, start /* = 0, end = @length */) {
  var O = toObject(this);
  var len = toLength(O.length);
  var to = toAbsoluteIndex(target, len);
  var from = toAbsoluteIndex(start, len);
  var end = arguments.length > 2 ? arguments[2] : undefined;
  var count = min$8((end === undefined ? len : toAbsoluteIndex(end, len)) - from, len - to);
  var inc = 1;
  if (from < to && to < from + count) {
    inc = -1;
    from += count - 1;
    to += count - 1;
  }
  while (count-- > 0) {
    if (from in O) O[to] = O[from];
    else delete O[to];
    to += inc;
    from += inc;
  } return O;
};

var aTypedArray$1 = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$1 = arrayBufferViewCore.exportTypedArrayMethod;

// `%TypedArray%.prototype.copyWithin` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.copywithin
exportTypedArrayMethod$1('copyWithin', function copyWithin(target, start /* , end */) {
  return arrayCopyWithin.call(aTypedArray$1(this), target, start, arguments.length > 2 ? arguments[2] : undefined);
});

var $every$1 = arrayIteration.every;

var aTypedArray$2 = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$2 = arrayBufferViewCore.exportTypedArrayMethod;

// `%TypedArray%.prototype.every` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.every
exportTypedArrayMethod$2('every', function every(callbackfn /* , thisArg */) {
  return $every$1(aTypedArray$2(this), callbackfn, arguments.length > 1 ? arguments[1] : undefined);
});

var aTypedArray$3 = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$3 = arrayBufferViewCore.exportTypedArrayMethod;

// `%TypedArray%.prototype.fill` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.fill
// eslint-disable-next-line no-unused-vars
exportTypedArrayMethod$3('fill', function fill(value /* , start, end */) {
  return arrayFill.apply(aTypedArray$3(this), arguments);
});

var $filter$1 = arrayIteration.filter;


var aTypedArray$4 = arrayBufferViewCore.aTypedArray;
var aTypedArrayConstructor$2 = arrayBufferViewCore.aTypedArrayConstructor;
var exportTypedArrayMethod$4 = arrayBufferViewCore.exportTypedArrayMethod;

// `%TypedArray%.prototype.filter` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.filter
exportTypedArrayMethod$4('filter', function filter(callbackfn /* , thisArg */) {
  var list = $filter$1(aTypedArray$4(this), callbackfn, arguments.length > 1 ? arguments[1] : undefined);
  var C = speciesConstructor(this, this.constructor);
  var index = 0;
  var length = list.length;
  var result = new (aTypedArrayConstructor$2(C))(length);
  while (length > index) result[index] = list[index++];
  return result;
});

var $find$1 = arrayIteration.find;

var aTypedArray$5 = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$5 = arrayBufferViewCore.exportTypedArrayMethod;

// `%TypedArray%.prototype.find` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.find
exportTypedArrayMethod$5('find', function find(predicate /* , thisArg */) {
  return $find$1(aTypedArray$5(this), predicate, arguments.length > 1 ? arguments[1] : undefined);
});

var $findIndex = arrayIteration.findIndex;

var aTypedArray$6 = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$6 = arrayBufferViewCore.exportTypedArrayMethod;

// `%TypedArray%.prototype.findIndex` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.findindex
exportTypedArrayMethod$6('findIndex', function findIndex(predicate /* , thisArg */) {
  return $findIndex(aTypedArray$6(this), predicate, arguments.length > 1 ? arguments[1] : undefined);
});

var $forEach$2 = arrayIteration.forEach;

var aTypedArray$7 = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$7 = arrayBufferViewCore.exportTypedArrayMethod;

// `%TypedArray%.prototype.forEach` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.foreach
exportTypedArrayMethod$7('forEach', function forEach(callbackfn /* , thisArg */) {
  $forEach$2(aTypedArray$7(this), callbackfn, arguments.length > 1 ? arguments[1] : undefined);
});

var $includes$1 = arrayIncludes.includes;

var aTypedArray$8 = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$8 = arrayBufferViewCore.exportTypedArrayMethod;

// `%TypedArray%.prototype.includes` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.includes
exportTypedArrayMethod$8('includes', function includes(searchElement /* , fromIndex */) {
  return $includes$1(aTypedArray$8(this), searchElement, arguments.length > 1 ? arguments[1] : undefined);
});

var $indexOf$1 = arrayIncludes.indexOf;

var aTypedArray$9 = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$9 = arrayBufferViewCore.exportTypedArrayMethod;

// `%TypedArray%.prototype.indexOf` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.indexof
exportTypedArrayMethod$9('indexOf', function indexOf(searchElement /* , fromIndex */) {
  return $indexOf$1(aTypedArray$9(this), searchElement, arguments.length > 1 ? arguments[1] : undefined);
});

var ITERATOR$5 = wellKnownSymbol('iterator');
var Uint8Array$1 = global_1.Uint8Array;
var arrayValues = es_array_iterator.values;
var arrayKeys = es_array_iterator.keys;
var arrayEntries = es_array_iterator.entries;
var aTypedArray$a = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$a = arrayBufferViewCore.exportTypedArrayMethod;
var nativeTypedArrayIterator = Uint8Array$1 && Uint8Array$1.prototype[ITERATOR$5];

var CORRECT_ITER_NAME = !!nativeTypedArrayIterator
  && (nativeTypedArrayIterator.name == 'values' || nativeTypedArrayIterator.name == undefined);

var typedArrayValues = function values() {
  return arrayValues.call(aTypedArray$a(this));
};

// `%TypedArray%.prototype.entries` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.entries
exportTypedArrayMethod$a('entries', function entries() {
  return arrayEntries.call(aTypedArray$a(this));
});
// `%TypedArray%.prototype.keys` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.keys
exportTypedArrayMethod$a('keys', function keys() {
  return arrayKeys.call(aTypedArray$a(this));
});
// `%TypedArray%.prototype.values` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.values
exportTypedArrayMethod$a('values', typedArrayValues, !CORRECT_ITER_NAME);
// `%TypedArray%.prototype[@@iterator]` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype-@@iterator
exportTypedArrayMethod$a(ITERATOR$5, typedArrayValues, !CORRECT_ITER_NAME);

var aTypedArray$b = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$b = arrayBufferViewCore.exportTypedArrayMethod;
var $join = [].join;

// `%TypedArray%.prototype.join` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.join
// eslint-disable-next-line no-unused-vars
exportTypedArrayMethod$b('join', function join(separator) {
  return $join.apply(aTypedArray$b(this), arguments);
});

var aTypedArray$c = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$c = arrayBufferViewCore.exportTypedArrayMethod;

// `%TypedArray%.prototype.lastIndexOf` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.lastindexof
// eslint-disable-next-line no-unused-vars
exportTypedArrayMethod$c('lastIndexOf', function lastIndexOf(searchElement /* , fromIndex */) {
  return arrayLastIndexOf.apply(aTypedArray$c(this), arguments);
});

var $map$1 = arrayIteration.map;


var aTypedArray$d = arrayBufferViewCore.aTypedArray;
var aTypedArrayConstructor$3 = arrayBufferViewCore.aTypedArrayConstructor;
var exportTypedArrayMethod$d = arrayBufferViewCore.exportTypedArrayMethod;

// `%TypedArray%.prototype.map` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.map
exportTypedArrayMethod$d('map', function map(mapfn /* , thisArg */) {
  return $map$1(aTypedArray$d(this), mapfn, arguments.length > 1 ? arguments[1] : undefined, function (O, length) {
    return new (aTypedArrayConstructor$3(speciesConstructor(O, O.constructor)))(length);
  });
});

var $reduce$1 = arrayReduce.left;

var aTypedArray$e = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$e = arrayBufferViewCore.exportTypedArrayMethod;

// `%TypedArray%.prototype.reduce` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.reduce
exportTypedArrayMethod$e('reduce', function reduce(callbackfn /* , initialValue */) {
  return $reduce$1(aTypedArray$e(this), callbackfn, arguments.length, arguments.length > 1 ? arguments[1] : undefined);
});

var $reduceRight = arrayReduce.right;

var aTypedArray$f = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$f = arrayBufferViewCore.exportTypedArrayMethod;

// `%TypedArray%.prototype.reduceRicht` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.reduceright
exportTypedArrayMethod$f('reduceRight', function reduceRight(callbackfn /* , initialValue */) {
  return $reduceRight(aTypedArray$f(this), callbackfn, arguments.length, arguments.length > 1 ? arguments[1] : undefined);
});

var aTypedArray$g = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$g = arrayBufferViewCore.exportTypedArrayMethod;
var floor$3 = Math.floor;

// `%TypedArray%.prototype.reverse` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.reverse
exportTypedArrayMethod$g('reverse', function reverse() {
  var that = this;
  var length = aTypedArray$g(that).length;
  var middle = floor$3(length / 2);
  var index = 0;
  var value;
  while (index < middle) {
    value = that[index];
    that[index++] = that[--length];
    that[length] = value;
  } return that;
});

var aTypedArray$h = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$h = arrayBufferViewCore.exportTypedArrayMethod;

var FORCED$6 = fails(function () {
  // eslint-disable-next-line no-undef
  new Int8Array(1).set({});
});

// `%TypedArray%.prototype.set` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.set
exportTypedArrayMethod$h('set', function set(arrayLike /* , offset */) {
  aTypedArray$h(this);
  var offset = toOffset(arguments.length > 1 ? arguments[1] : undefined, 1);
  var length = this.length;
  var src = toObject(arrayLike);
  var len = toLength(src.length);
  var index = 0;
  if (len + offset > length) throw RangeError('Wrong length');
  while (index < len) this[offset + index] = src[index++];
}, FORCED$6);

var aTypedArray$i = arrayBufferViewCore.aTypedArray;
var aTypedArrayConstructor$4 = arrayBufferViewCore.aTypedArrayConstructor;
var exportTypedArrayMethod$i = arrayBufferViewCore.exportTypedArrayMethod;
var $slice = [].slice;

var FORCED$7 = fails(function () {
  // eslint-disable-next-line no-undef
  new Int8Array(1).slice();
});

// `%TypedArray%.prototype.slice` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.slice
exportTypedArrayMethod$i('slice', function slice(start, end) {
  var list = $slice.call(aTypedArray$i(this), start, end);
  var C = speciesConstructor(this, this.constructor);
  var index = 0;
  var length = list.length;
  var result = new (aTypedArrayConstructor$4(C))(length);
  while (length > index) result[index] = list[index++];
  return result;
}, FORCED$7);

var $some$1 = arrayIteration.some;

var aTypedArray$j = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$j = arrayBufferViewCore.exportTypedArrayMethod;

// `%TypedArray%.prototype.some` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.some
exportTypedArrayMethod$j('some', function some(callbackfn /* , thisArg */) {
  return $some$1(aTypedArray$j(this), callbackfn, arguments.length > 1 ? arguments[1] : undefined);
});

var aTypedArray$k = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$k = arrayBufferViewCore.exportTypedArrayMethod;
var $sort = [].sort;

// `%TypedArray%.prototype.sort` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.sort
exportTypedArrayMethod$k('sort', function sort(comparefn) {
  return $sort.call(aTypedArray$k(this), comparefn);
});

var aTypedArray$l = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$l = arrayBufferViewCore.exportTypedArrayMethod;

// `%TypedArray%.prototype.subarray` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.subarray
exportTypedArrayMethod$l('subarray', function subarray(begin, end) {
  var O = aTypedArray$l(this);
  var length = O.length;
  var beginIndex = toAbsoluteIndex(begin, length);
  return new (speciesConstructor(O, O.constructor))(
    O.buffer,
    O.byteOffset + beginIndex * O.BYTES_PER_ELEMENT,
    toLength((end === undefined ? length : toAbsoluteIndex(end, length)) - beginIndex)
  );
});

var Int8Array$3 = global_1.Int8Array;
var aTypedArray$m = arrayBufferViewCore.aTypedArray;
var exportTypedArrayMethod$m = arrayBufferViewCore.exportTypedArrayMethod;
var $toLocaleString = [].toLocaleString;
var $slice$1 = [].slice;

// iOS Safari 6.x fails here
var TO_LOCALE_STRING_BUG = !!Int8Array$3 && fails(function () {
  $toLocaleString.call(new Int8Array$3(1));
});

var FORCED$8 = fails(function () {
  return [1, 2].toLocaleString() != new Int8Array$3([1, 2]).toLocaleString();
}) || !fails(function () {
  Int8Array$3.prototype.toLocaleString.call([1, 2]);
});

// `%TypedArray%.prototype.toLocaleString` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.tolocalestring
exportTypedArrayMethod$m('toLocaleString', function toLocaleString() {
  return $toLocaleString.apply(TO_LOCALE_STRING_BUG ? $slice$1.call(aTypedArray$m(this)) : aTypedArray$m(this), arguments);
}, FORCED$8);

var exportTypedArrayMethod$n = arrayBufferViewCore.exportTypedArrayMethod;



var Uint8Array$2 = global_1.Uint8Array;
var Uint8ArrayPrototype = Uint8Array$2 && Uint8Array$2.prototype || {};
var arrayToString = [].toString;
var arrayJoin = [].join;

if (fails(function () { arrayToString.call({}); })) {
  arrayToString = function toString() {
    return arrayJoin.call(this);
  };
}

var IS_NOT_ARRAY_METHOD = Uint8ArrayPrototype.toString != arrayToString;

// `%TypedArray%.prototype.toString` method
// https://tc39.es/ecma262/#sec-%typedarray%.prototype.tostring
exportTypedArrayMethod$n('toString', arrayToString, IS_NOT_ARRAY_METHOD);

// iterable DOM collections
// flag - `iterable` interface - 'entries', 'keys', 'values', 'forEach' methods
var domIterables = {
  CSSRuleList: 0,
  CSSStyleDeclaration: 0,
  CSSValueList: 0,
  ClientRectList: 0,
  DOMRectList: 0,
  DOMStringList: 0,
  DOMTokenList: 1,
  DataTransferItemList: 0,
  FileList: 0,
  HTMLAllCollection: 0,
  HTMLCollection: 0,
  HTMLFormElement: 0,
  HTMLSelectElement: 0,
  MediaList: 0,
  MimeTypeArray: 0,
  NamedNodeMap: 0,
  NodeList: 1,
  PaintRequestList: 0,
  Plugin: 0,
  PluginArray: 0,
  SVGLengthList: 0,
  SVGNumberList: 0,
  SVGPathSegList: 0,
  SVGPointList: 0,
  SVGStringList: 0,
  SVGTransformList: 0,
  SourceBufferList: 0,
  StyleSheetList: 0,
  TextTrackCueList: 0,
  TextTrackList: 0,
  TouchList: 0
};

for (var COLLECTION_NAME in domIterables) {
  var Collection = global_1[COLLECTION_NAME];
  var CollectionPrototype = Collection && Collection.prototype;
  // some Chrome versions have non-configurable methods on DOMTokenList
  if (CollectionPrototype && CollectionPrototype.forEach !== arrayForEach) try {
    createNonEnumerableProperty(CollectionPrototype, 'forEach', arrayForEach);
  } catch (error) {
    CollectionPrototype.forEach = arrayForEach;
  }
}

var ITERATOR$6 = wellKnownSymbol('iterator');
var TO_STRING_TAG$4 = wellKnownSymbol('toStringTag');
var ArrayValues = es_array_iterator.values;

for (var COLLECTION_NAME$1 in domIterables) {
  var Collection$1 = global_1[COLLECTION_NAME$1];
  var CollectionPrototype$1 = Collection$1 && Collection$1.prototype;
  if (CollectionPrototype$1) {
    // some Chrome versions have non-configurable methods on DOMTokenList
    if (CollectionPrototype$1[ITERATOR$6] !== ArrayValues) try {
      createNonEnumerableProperty(CollectionPrototype$1, ITERATOR$6, ArrayValues);
    } catch (error) {
      CollectionPrototype$1[ITERATOR$6] = ArrayValues;
    }
    if (!CollectionPrototype$1[TO_STRING_TAG$4]) {
      createNonEnumerableProperty(CollectionPrototype$1, TO_STRING_TAG$4, COLLECTION_NAME$1);
    }
    if (domIterables[COLLECTION_NAME$1]) for (var METHOD_NAME in es_array_iterator) {
      // some Chrome versions have non-configurable methods on DOMTokenList
      if (CollectionPrototype$1[METHOD_NAME] !== es_array_iterator[METHOD_NAME]) try {
        createNonEnumerableProperty(CollectionPrototype$1, METHOD_NAME, es_array_iterator[METHOD_NAME]);
      } catch (error) {
        CollectionPrototype$1[METHOD_NAME] = es_array_iterator[METHOD_NAME];
      }
    }
  }
}

// `URL.prototype.toJSON` method
// https://url.spec.whatwg.org/#dom-url-tojson
_export({ target: 'URL', proto: true, enumerable: true }, {
  toJSON: function toJSON() {
    return URL.prototype.toString.call(this);
  }
});

(window["webpackJsonp"]=window["webpackJsonp"]||[]).push([["vendors~SignupEmployeeDetails~SignupEmployerDetails"],{

/***/"./node_modules/base64-js/index.js":
/*!*****************************************!*\
  !*** ./node_modules/base64-js/index.js ***!
  \*****************************************/
/*! no static exports found */
/***/function node_modulesBase64JsIndexJs(module,exports,__webpack_require__){


exports.byteLength=byteLength;
exports.toByteArray=toByteArray;
exports.fromByteArray=fromByteArray;

var lookup=[];
var revLookup=[];
var Arr=typeof Uint8Array!=='undefined'?Uint8Array:Array;

var code='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
for(var i=0,len=code.length;i<len;++i){
lookup[i]=code[i];
revLookup[code.charCodeAt(i)]=i;
}

// Support decoding URL-safe base64 strings, as Node.js does.
// See: https://en.wikipedia.org/wiki/Base64#URL_applications
revLookup['-'.charCodeAt(0)]=62;
revLookup['_'.charCodeAt(0)]=63;

function getLens(b64){
var len=b64.length;

if(len%4>0){
throw new Error('Invalid string. Length must be a multiple of 4');
}

// Trim off extra bytes after placeholder bytes are found
// See: https://github.com/beatgammit/base64-js/issues/42
var validLen=b64.indexOf('=');
if(validLen===-1)validLen=len;

var placeHoldersLen=validLen===len?
0:
4-validLen%4;

return [validLen,placeHoldersLen];
}

// base64 is 4/3 + up to two characters of the original data
function byteLength(b64){
var lens=getLens(b64);
var validLen=lens[0];
var placeHoldersLen=lens[1];
return (validLen+placeHoldersLen)*3/4-placeHoldersLen;
}

function _byteLength(b64,validLen,placeHoldersLen){
return (validLen+placeHoldersLen)*3/4-placeHoldersLen;
}

function toByteArray(b64){
var tmp;
var lens=getLens(b64);
var validLen=lens[0];
var placeHoldersLen=lens[1];

var arr=new Arr(_byteLength(b64,validLen,placeHoldersLen));

var curByte=0;

// if there are placeholders, only get up to the last complete 4 chars
var len=placeHoldersLen>0?
validLen-4:
validLen;

var i;
for(i=0;i<len;i+=4){
tmp=
revLookup[b64.charCodeAt(i)]<<18|
revLookup[b64.charCodeAt(i+1)]<<12|
revLookup[b64.charCodeAt(i+2)]<<6|
revLookup[b64.charCodeAt(i+3)];
arr[curByte++]=tmp>>16&0xFF;
arr[curByte++]=tmp>>8&0xFF;
arr[curByte++]=tmp&0xFF;
}

if(placeHoldersLen===2){
tmp=
revLookup[b64.charCodeAt(i)]<<2|
revLookup[b64.charCodeAt(i+1)]>>4;
arr[curByte++]=tmp&0xFF;
}

if(placeHoldersLen===1){
tmp=
revLookup[b64.charCodeAt(i)]<<10|
revLookup[b64.charCodeAt(i+1)]<<4|
revLookup[b64.charCodeAt(i+2)]>>2;
arr[curByte++]=tmp>>8&0xFF;
arr[curByte++]=tmp&0xFF;
}

return arr;
}

function tripletToBase64(num){
return lookup[num>>18&0x3F]+
lookup[num>>12&0x3F]+
lookup[num>>6&0x3F]+
lookup[num&0x3F];
}

function encodeChunk(uint8,start,end){
var tmp;
var output=[];
for(var i=start;i<end;i+=3){
tmp=
(uint8[i]<<16&0xFF0000)+(
uint8[i+1]<<8&0xFF00)+(
uint8[i+2]&0xFF);
output.push(tripletToBase64(tmp));
}
return output.join('');
}

function fromByteArray(uint8){
var tmp;
var len=uint8.length;
var extraBytes=len%3;// if we have 1 byte left, pad 2 bytes
var parts=[];
var maxChunkLength=16383;// must be multiple of 3

// go through the array every three bytes, we'll deal with trailing stuff later
for(var i=0,len2=len-extraBytes;i<len2;i+=maxChunkLength){
parts.push(encodeChunk(uint8,i,i+maxChunkLength>len2?len2:i+maxChunkLength));
}

// pad the end with zeros, but make sure to not forget the extra bytes
if(extraBytes===1){
tmp=uint8[len-1];
parts.push(
lookup[tmp>>2]+
lookup[tmp<<4&0x3F]+
'==');

}else if(extraBytes===2){
tmp=(uint8[len-2]<<8)+uint8[len-1];
parts.push(
lookup[tmp>>10]+
lookup[tmp>>4&0x3F]+
lookup[tmp<<2&0x3F]+
'=');

}

return parts.join('');
}


/***/},

/***/"./node_modules/buffer/index.js":
/*!**************************************!*\
  !*** ./node_modules/buffer/index.js ***!
  \**************************************/
/*! no static exports found */
/***/function node_modulesBufferIndexJs(module,exports,__webpack_require__){
/* WEBPACK VAR INJECTION */(function(global){/*!
 * The buffer module from node.js, for the browser.
 *
 * @author   Feross Aboukhadijeh <http://feross.org>
 * @license  MIT
 */
/* eslint-disable no-proto */



var base64=__webpack_require__(/*! base64-js */"./node_modules/base64-js/index.js");
var ieee754=__webpack_require__(/*! ieee754 */"./node_modules/ieee754/index.js");
var isArray=__webpack_require__(/*! isarray */"./node_modules/isarray/index.js");

exports.Buffer=Buffer;
exports.SlowBuffer=SlowBuffer;
exports.INSPECT_MAX_BYTES=50;

/**
 * If `Buffer.TYPED_ARRAY_SUPPORT`:
 *   === true    Use Uint8Array implementation (fastest)
 *   === false   Use Object implementation (most compatible, even IE6)
 *
 * Browsers that support typed arrays are IE 10+, Firefox 4+, Chrome 7+, Safari 5.1+,
 * Opera 11.6+, iOS 4.2+.
 *
 * Due to various browser bugs, sometimes the Object implementation will be used even
 * when the browser supports typed arrays.
 *
 * Note:
 *
 *   - Firefox 4-29 lacks support for adding new properties to `Uint8Array` instances,
 *     See: https://bugzilla.mozilla.org/show_bug.cgi?id=695438.
 *
 *   - Chrome 9-10 is missing the `TypedArray.prototype.subarray` function.
 *
 *   - IE10 has a broken `TypedArray.prototype.subarray` function which returns arrays of
 *     incorrect length in some situations.

 * We detect these buggy browsers and set `Buffer.TYPED_ARRAY_SUPPORT` to `false` so they
 * get the Object implementation, which is slower but behaves correctly.
 */
Buffer.TYPED_ARRAY_SUPPORT=global.TYPED_ARRAY_SUPPORT!==undefined?
global.TYPED_ARRAY_SUPPORT:
typedArraySupport();

/*
 * Export kMaxLength after typed array support is determined.
 */
exports.kMaxLength=kMaxLength();

function typedArraySupport(){
try{
var arr=new Uint8Array(1);
arr.__proto__={__proto__:Uint8Array.prototype,foo:function foo(){return 42;}};
return arr.foo()===42&&// typed array instances can be augmented
typeof arr.subarray==='function'&&// chrome 9-10 lack `subarray`
arr.subarray(1,1).byteLength===0;// ie10 has broken `subarray`
}catch(e){
return false;
}
}

function kMaxLength(){
return Buffer.TYPED_ARRAY_SUPPORT?
0x7fffffff:
0x3fffffff;
}

function createBuffer(that,length){
if(kMaxLength()<length){
throw new RangeError('Invalid typed array length');
}
if(Buffer.TYPED_ARRAY_SUPPORT){
// Return an augmented `Uint8Array` instance, for best performance
that=new Uint8Array(length);
that.__proto__=Buffer.prototype;
}else {
// Fallback: Return an object instance of the Buffer class
if(that===null){
that=new Buffer(length);
}
that.length=length;
}

return that;
}

/**
 * The Buffer constructor returns instances of `Uint8Array` that have their
 * prototype changed to `Buffer.prototype`. Furthermore, `Buffer` is a subclass of
 * `Uint8Array`, so the returned instances will have all the node `Buffer` methods
 * and the `Uint8Array` methods. Square bracket notation works as expected -- it
 * returns a single octet.
 *
 * The `Uint8Array` prototype remains unmodified.
 */

function Buffer(arg,encodingOrOffset,length){
if(!Buffer.TYPED_ARRAY_SUPPORT&&!(this instanceof Buffer)){
return new Buffer(arg,encodingOrOffset,length);
}

// Common case.
if(typeof arg==='number'){
if(typeof encodingOrOffset==='string'){
throw new Error(
'If encoding is specified then the first argument must be a string');

}
return allocUnsafe(this,arg);
}
return from(this,arg,encodingOrOffset,length);
}

Buffer.poolSize=8192;// not used by this implementation

// TODO: Legacy, not needed anymore. Remove in next major version.
Buffer._augment=function(arr){
arr.__proto__=Buffer.prototype;
return arr;
};

function from(that,value,encodingOrOffset,length){
if(typeof value==='number'){
throw new TypeError('"value" argument must not be a number');
}

if(typeof ArrayBuffer!=='undefined'&&value instanceof ArrayBuffer){
return fromArrayBuffer(that,value,encodingOrOffset,length);
}

if(typeof value==='string'){
return fromString(that,value,encodingOrOffset);
}

return fromObject(that,value);
}

/**
 * Functionally equivalent to Buffer(arg, encoding) but throws a TypeError
 * if value is a number.
 * Buffer.from(str[, encoding])
 * Buffer.from(array)
 * Buffer.from(buffer)
 * Buffer.from(arrayBuffer[, byteOffset[, length]])
 **/
Buffer.from=function(value,encodingOrOffset,length){
return from(null,value,encodingOrOffset,length);
};

if(Buffer.TYPED_ARRAY_SUPPORT){
Buffer.prototype.__proto__=Uint8Array.prototype;
Buffer.__proto__=Uint8Array;
if(typeof Symbol!=='undefined'&&Symbol.species&&
Buffer[Symbol.species]===Buffer){
// Fix subarray() in ES2016. See: https://github.com/feross/buffer/pull/97
Object.defineProperty(Buffer,Symbol.species,{
value:null,
configurable:true});

}
}

function assertSize(size){
if(typeof size!=='number'){
throw new TypeError('"size" argument must be a number');
}else if(size<0){
throw new RangeError('"size" argument must not be negative');
}
}

function alloc(that,size,fill,encoding){
assertSize(size);
if(size<=0){
return createBuffer(that,size);
}
if(fill!==undefined){
// Only pay attention to encoding if it's a string. This
// prevents accidentally sending in a number that would
// be interpretted as a start offset.
return typeof encoding==='string'?
createBuffer(that,size).fill(fill,encoding):
createBuffer(that,size).fill(fill);
}
return createBuffer(that,size);
}

/**
 * Creates a new filled Buffer instance.
 * alloc(size[, fill[, encoding]])
 **/
Buffer.alloc=function(size,fill,encoding){
return alloc(null,size,fill,encoding);
};

function allocUnsafe(that,size){
assertSize(size);
that=createBuffer(that,size<0?0:checked(size)|0);
if(!Buffer.TYPED_ARRAY_SUPPORT){
for(var i=0;i<size;++i){
that[i]=0;
}
}
return that;
}

/**
 * Equivalent to Buffer(num), by default creates a non-zero-filled Buffer instance.
 * */
Buffer.allocUnsafe=function(size){
return allocUnsafe(null,size);
};
/**
 * Equivalent to SlowBuffer(num), by default creates a non-zero-filled Buffer instance.
 */
Buffer.allocUnsafeSlow=function(size){
return allocUnsafe(null,size);
};

function fromString(that,string,encoding){
if(typeof encoding!=='string'||encoding===''){
encoding='utf8';
}

if(!Buffer.isEncoding(encoding)){
throw new TypeError('"encoding" must be a valid string encoding');
}

var length=byteLength(string,encoding)|0;
that=createBuffer(that,length);

var actual=that.write(string,encoding);

if(actual!==length){
// Writing a hex string, for example, that contains invalid characters will
// cause everything after the first invalid character to be ignored. (e.g.
// 'abxxcd' will be treated as 'ab')
that=that.slice(0,actual);
}

return that;
}

function fromArrayLike(that,array){
var length=array.length<0?0:checked(array.length)|0;
that=createBuffer(that,length);
for(var i=0;i<length;i+=1){
that[i]=array[i]&255;
}
return that;
}

function fromArrayBuffer(that,array,byteOffset,length){
array.byteLength;// this throws if `array` is not a valid ArrayBuffer

if(byteOffset<0||array.byteLength<byteOffset){
throw new RangeError('\'offset\' is out of bounds');
}

if(array.byteLength<byteOffset+(length||0)){
throw new RangeError('\'length\' is out of bounds');
}

if(byteOffset===undefined&&length===undefined){
array=new Uint8Array(array);
}else if(length===undefined){
array=new Uint8Array(array,byteOffset);
}else {
array=new Uint8Array(array,byteOffset,length);
}

if(Buffer.TYPED_ARRAY_SUPPORT){
// Return an augmented `Uint8Array` instance, for best performance
that=array;
that.__proto__=Buffer.prototype;
}else {
// Fallback: Return an object instance of the Buffer class
that=fromArrayLike(that,array);
}
return that;
}

function fromObject(that,obj){
if(Buffer.isBuffer(obj)){
var len=checked(obj.length)|0;
that=createBuffer(that,len);

if(that.length===0){
return that;
}

obj.copy(that,0,0,len);
return that;
}

if(obj){
if(typeof ArrayBuffer!=='undefined'&&
obj.buffer instanceof ArrayBuffer||'length'in obj){
if(typeof obj.length!=='number'||isnan(obj.length)){
return createBuffer(that,0);
}
return fromArrayLike(that,obj);
}

if(obj.type==='Buffer'&&isArray(obj.data)){
return fromArrayLike(that,obj.data);
}
}

throw new TypeError('First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.');
}

function checked(length){
// Note: cannot use `length < kMaxLength()` here because that fails when
// length is NaN (which is otherwise coerced to zero.)
if(length>=kMaxLength()){
throw new RangeError('Attempt to allocate Buffer larger than maximum '+
'size: 0x'+kMaxLength().toString(16)+' bytes');
}
return length|0;
}

function SlowBuffer(length){
if(+length!=length){// eslint-disable-line eqeqeq
length=0;
}
return Buffer.alloc(+length);
}

Buffer.isBuffer=function isBuffer(b){
return !!(b!=null&&b._isBuffer);
};

Buffer.compare=function compare(a,b){
if(!Buffer.isBuffer(a)||!Buffer.isBuffer(b)){
throw new TypeError('Arguments must be Buffers');
}

if(a===b)return 0;

var x=a.length;
var y=b.length;

for(var i=0,len=Math.min(x,y);i<len;++i){
if(a[i]!==b[i]){
x=a[i];
y=b[i];
break;
}
}

if(x<y)return -1;
if(y<x)return 1;
return 0;
};

Buffer.isEncoding=function isEncoding(encoding){
switch(String(encoding).toLowerCase()){
case'hex':
case'utf8':
case'utf-8':
case'ascii':
case'latin1':
case'binary':
case'base64':
case'ucs2':
case'ucs-2':
case'utf16le':
case'utf-16le':
return true;
default:
return false;}

};

Buffer.concat=function concat(list,length){
if(!isArray(list)){
throw new TypeError('"list" argument must be an Array of Buffers');
}

if(list.length===0){
return Buffer.alloc(0);
}

var i;
if(length===undefined){
length=0;
for(i=0;i<list.length;++i){
length+=list[i].length;
}
}

var buffer=Buffer.allocUnsafe(length);
var pos=0;
for(i=0;i<list.length;++i){
var buf=list[i];
if(!Buffer.isBuffer(buf)){
throw new TypeError('"list" argument must be an Array of Buffers');
}
buf.copy(buffer,pos);
pos+=buf.length;
}
return buffer;
};

function byteLength(string,encoding){
if(Buffer.isBuffer(string)){
return string.length;
}
if(typeof ArrayBuffer!=='undefined'&&typeof ArrayBuffer.isView==='function'&&(
ArrayBuffer.isView(string)||string instanceof ArrayBuffer)){
return string.byteLength;
}
if(typeof string!=='string'){
string=''+string;
}

var len=string.length;
if(len===0)return 0;

// Use a for loop to avoid recursion
var loweredCase=false;
for(;;){
switch(encoding){
case'ascii':
case'latin1':
case'binary':
return len;
case'utf8':
case'utf-8':
case undefined:
return utf8ToBytes(string).length;
case'ucs2':
case'ucs-2':
case'utf16le':
case'utf-16le':
return len*2;
case'hex':
return len>>>1;
case'base64':
return base64ToBytes(string).length;
default:
if(loweredCase)return utf8ToBytes(string).length;// assume utf8
encoding=(''+encoding).toLowerCase();
loweredCase=true;}

}
}
Buffer.byteLength=byteLength;

function slowToString(encoding,start,end){
var loweredCase=false;

// No need to verify that "this.length <= MAX_UINT32" since it's a read-only
// property of a typed array.

// This behaves neither like String nor Uint8Array in that we set start/end
// to their upper/lower bounds if the value passed is out of range.
// undefined is handled specially as per ECMA-262 6th Edition,
// Section 13.3.3.7 Runtime Semantics: KeyedBindingInitialization.
if(start===undefined||start<0){
start=0;
}
// Return early if start > this.length. Done here to prevent potential uint32
// coercion fail below.
if(start>this.length){
return '';
}

if(end===undefined||end>this.length){
end=this.length;
}

if(end<=0){
return '';
}

// Force coersion to uint32. This will also coerce falsey/NaN values to 0.
end>>>=0;
start>>>=0;

if(end<=start){
return '';
}

if(!encoding)encoding='utf8';

while(true){
switch(encoding){
case'hex':
return hexSlice(this,start,end);

case'utf8':
case'utf-8':
return utf8Slice(this,start,end);

case'ascii':
return asciiSlice(this,start,end);

case'latin1':
case'binary':
return latin1Slice(this,start,end);

case'base64':
return base64Slice(this,start,end);

case'ucs2':
case'ucs-2':
case'utf16le':
case'utf-16le':
return utf16leSlice(this,start,end);

default:
if(loweredCase)throw new TypeError('Unknown encoding: '+encoding);
encoding=(encoding+'').toLowerCase();
loweredCase=true;}

}
}

// The property is used by `Buffer.isBuffer` and `is-buffer` (in Safari 5-7) to detect
// Buffer instances.
Buffer.prototype._isBuffer=true;

function swap(b,n,m){
var i=b[n];
b[n]=b[m];
b[m]=i;
}

Buffer.prototype.swap16=function swap16(){
var len=this.length;
if(len%2!==0){
throw new RangeError('Buffer size must be a multiple of 16-bits');
}
for(var i=0;i<len;i+=2){
swap(this,i,i+1);
}
return this;
};

Buffer.prototype.swap32=function swap32(){
var len=this.length;
if(len%4!==0){
throw new RangeError('Buffer size must be a multiple of 32-bits');
}
for(var i=0;i<len;i+=4){
swap(this,i,i+3);
swap(this,i+1,i+2);
}
return this;
};

Buffer.prototype.swap64=function swap64(){
var len=this.length;
if(len%8!==0){
throw new RangeError('Buffer size must be a multiple of 64-bits');
}
for(var i=0;i<len;i+=8){
swap(this,i,i+7);
swap(this,i+1,i+6);
swap(this,i+2,i+5);
swap(this,i+3,i+4);
}
return this;
};

Buffer.prototype.toString=function toString(){
var length=this.length|0;
if(length===0)return '';
if(arguments.length===0)return utf8Slice(this,0,length);
return slowToString.apply(this,arguments);
};

Buffer.prototype.equals=function equals(b){
if(!Buffer.isBuffer(b))throw new TypeError('Argument must be a Buffer');
if(this===b)return true;
return Buffer.compare(this,b)===0;
};

Buffer.prototype.inspect=function inspect(){
var str='';
var max=exports.INSPECT_MAX_BYTES;
if(this.length>0){
str=this.toString('hex',0,max).match(/.{2}/g).join(' ');
if(this.length>max)str+=' ... ';
}
return '<Buffer '+str+'>';
};

Buffer.prototype.compare=function compare(target,start,end,thisStart,thisEnd){
if(!Buffer.isBuffer(target)){
throw new TypeError('Argument must be a Buffer');
}

if(start===undefined){
start=0;
}
if(end===undefined){
end=target?target.length:0;
}
if(thisStart===undefined){
thisStart=0;
}
if(thisEnd===undefined){
thisEnd=this.length;
}

if(start<0||end>target.length||thisStart<0||thisEnd>this.length){
throw new RangeError('out of range index');
}

if(thisStart>=thisEnd&&start>=end){
return 0;
}
if(thisStart>=thisEnd){
return -1;
}
if(start>=end){
return 1;
}

start>>>=0;
end>>>=0;
thisStart>>>=0;
thisEnd>>>=0;

if(this===target)return 0;

var x=thisEnd-thisStart;
var y=end-start;
var len=Math.min(x,y);

var thisCopy=this.slice(thisStart,thisEnd);
var targetCopy=target.slice(start,end);

for(var i=0;i<len;++i){
if(thisCopy[i]!==targetCopy[i]){
x=thisCopy[i];
y=targetCopy[i];
break;
}
}

if(x<y)return -1;
if(y<x)return 1;
return 0;
};

// Finds either the first index of `val` in `buffer` at offset >= `byteOffset`,
// OR the last index of `val` in `buffer` at offset <= `byteOffset`.
//
// Arguments:
// - buffer - a Buffer to search
// - val - a string, Buffer, or number
// - byteOffset - an index into `buffer`; will be clamped to an int32
// - encoding - an optional encoding, relevant is val is a string
// - dir - true for indexOf, false for lastIndexOf
function bidirectionalIndexOf(buffer,val,byteOffset,encoding,dir){
// Empty buffer means no match
if(buffer.length===0)return -1;

// Normalize byteOffset
if(typeof byteOffset==='string'){
encoding=byteOffset;
byteOffset=0;
}else if(byteOffset>0x7fffffff){
byteOffset=0x7fffffff;
}else if(byteOffset<-0x80000000){
byteOffset=-0x80000000;
}
byteOffset=+byteOffset;// Coerce to Number.
if(isNaN(byteOffset)){
// byteOffset: it it's undefined, null, NaN, "foo", etc, search whole buffer
byteOffset=dir?0:buffer.length-1;
}

// Normalize byteOffset: negative offsets start from the end of the buffer
if(byteOffset<0)byteOffset=buffer.length+byteOffset;
if(byteOffset>=buffer.length){
if(dir)return -1;else
byteOffset=buffer.length-1;
}else if(byteOffset<0){
if(dir)byteOffset=0;else
return -1;
}

// Normalize val
if(typeof val==='string'){
val=Buffer.from(val,encoding);
}

// Finally, search either indexOf (if dir is true) or lastIndexOf
if(Buffer.isBuffer(val)){
// Special case: looking for empty string/buffer always fails
if(val.length===0){
return -1;
}
return arrayIndexOf(buffer,val,byteOffset,encoding,dir);
}else if(typeof val==='number'){
val=val&0xFF;// Search for a byte value [0-255]
if(Buffer.TYPED_ARRAY_SUPPORT&&
typeof Uint8Array.prototype.indexOf==='function'){
if(dir){
return Uint8Array.prototype.indexOf.call(buffer,val,byteOffset);
}else {
return Uint8Array.prototype.lastIndexOf.call(buffer,val,byteOffset);
}
}
return arrayIndexOf(buffer,[val],byteOffset,encoding,dir);
}

throw new TypeError('val must be string, number or Buffer');
}

function arrayIndexOf(arr,val,byteOffset,encoding,dir){
var indexSize=1;
var arrLength=arr.length;
var valLength=val.length;

if(encoding!==undefined){
encoding=String(encoding).toLowerCase();
if(encoding==='ucs2'||encoding==='ucs-2'||
encoding==='utf16le'||encoding==='utf-16le'){
if(arr.length<2||val.length<2){
return -1;
}
indexSize=2;
arrLength/=2;
valLength/=2;
byteOffset/=2;
}
}

function read(buf,i){
if(indexSize===1){
return buf[i];
}else {
return buf.readUInt16BE(i*indexSize);
}
}

var i;
if(dir){
var foundIndex=-1;
for(i=byteOffset;i<arrLength;i++){
if(read(arr,i)===read(val,foundIndex===-1?0:i-foundIndex)){
if(foundIndex===-1)foundIndex=i;
if(i-foundIndex+1===valLength)return foundIndex*indexSize;
}else {
if(foundIndex!==-1)i-=i-foundIndex;
foundIndex=-1;
}
}
}else {
if(byteOffset+valLength>arrLength)byteOffset=arrLength-valLength;
for(i=byteOffset;i>=0;i--){
var found=true;
for(var j=0;j<valLength;j++){
if(read(arr,i+j)!==read(val,j)){
found=false;
break;
}
}
if(found)return i;
}
}

return -1;
}

Buffer.prototype.includes=function includes(val,byteOffset,encoding){
return this.indexOf(val,byteOffset,encoding)!==-1;
};

Buffer.prototype.indexOf=function indexOf(val,byteOffset,encoding){
return bidirectionalIndexOf(this,val,byteOffset,encoding,true);
};

Buffer.prototype.lastIndexOf=function lastIndexOf(val,byteOffset,encoding){
return bidirectionalIndexOf(this,val,byteOffset,encoding,false);
};

function hexWrite(buf,string,offset,length){
offset=Number(offset)||0;
var remaining=buf.length-offset;
if(!length){
length=remaining;
}else {
length=Number(length);
if(length>remaining){
length=remaining;
}
}

// must be an even number of digits
var strLen=string.length;
if(strLen%2!==0)throw new TypeError('Invalid hex string');

if(length>strLen/2){
length=strLen/2;
}
for(var i=0;i<length;++i){
var parsed=parseInt(string.substr(i*2,2),16);
if(isNaN(parsed))return i;
buf[offset+i]=parsed;
}
return i;
}

function utf8Write(buf,string,offset,length){
return blitBuffer(utf8ToBytes(string,buf.length-offset),buf,offset,length);
}

function asciiWrite(buf,string,offset,length){
return blitBuffer(asciiToBytes(string),buf,offset,length);
}

function latin1Write(buf,string,offset,length){
return asciiWrite(buf,string,offset,length);
}

function base64Write(buf,string,offset,length){
return blitBuffer(base64ToBytes(string),buf,offset,length);
}

function ucs2Write(buf,string,offset,length){
return blitBuffer(utf16leToBytes(string,buf.length-offset),buf,offset,length);
}

Buffer.prototype.write=function write(string,offset,length,encoding){
// Buffer#write(string)
if(offset===undefined){
encoding='utf8';
length=this.length;
offset=0;
// Buffer#write(string, encoding)
}else if(length===undefined&&typeof offset==='string'){
encoding=offset;
length=this.length;
offset=0;
// Buffer#write(string, offset[, length][, encoding])
}else if(isFinite(offset)){
offset=offset|0;
if(isFinite(length)){
length=length|0;
if(encoding===undefined)encoding='utf8';
}else {
encoding=length;
length=undefined;
}
// legacy write(string, encoding, offset, length) - remove in v0.13
}else {
throw new Error(
'Buffer.write(string, encoding, offset[, length]) is no longer supported');

}

var remaining=this.length-offset;
if(length===undefined||length>remaining)length=remaining;

if(string.length>0&&(length<0||offset<0)||offset>this.length){
throw new RangeError('Attempt to write outside buffer bounds');
}

if(!encoding)encoding='utf8';

var loweredCase=false;
for(;;){
switch(encoding){
case'hex':
return hexWrite(this,string,offset,length);

case'utf8':
case'utf-8':
return utf8Write(this,string,offset,length);

case'ascii':
return asciiWrite(this,string,offset,length);

case'latin1':
case'binary':
return latin1Write(this,string,offset,length);

case'base64':
// Warning: maxLength not taken into account in base64Write
return base64Write(this,string,offset,length);

case'ucs2':
case'ucs-2':
case'utf16le':
case'utf-16le':
return ucs2Write(this,string,offset,length);

default:
if(loweredCase)throw new TypeError('Unknown encoding: '+encoding);
encoding=(''+encoding).toLowerCase();
loweredCase=true;}

}
};

Buffer.prototype.toJSON=function toJSON(){
return {
type:'Buffer',
data:Array.prototype.slice.call(this._arr||this,0)};

};

function base64Slice(buf,start,end){
if(start===0&&end===buf.length){
return base64.fromByteArray(buf);
}else {
return base64.fromByteArray(buf.slice(start,end));
}
}

function utf8Slice(buf,start,end){
end=Math.min(buf.length,end);
var res=[];

var i=start;
while(i<end){
var firstByte=buf[i];
var codePoint=null;
var bytesPerSequence=firstByte>0xEF?4:
firstByte>0xDF?3:
firstByte>0xBF?2:
1;

if(i+bytesPerSequence<=end){
var secondByte,thirdByte,fourthByte,tempCodePoint;

switch(bytesPerSequence){
case 1:
if(firstByte<0x80){
codePoint=firstByte;
}
break;
case 2:
secondByte=buf[i+1];
if((secondByte&0xC0)===0x80){
tempCodePoint=(firstByte&0x1F)<<0x6|secondByte&0x3F;
if(tempCodePoint>0x7F){
codePoint=tempCodePoint;
}
}
break;
case 3:
secondByte=buf[i+1];
thirdByte=buf[i+2];
if((secondByte&0xC0)===0x80&&(thirdByte&0xC0)===0x80){
tempCodePoint=(firstByte&0xF)<<0xC|(secondByte&0x3F)<<0x6|thirdByte&0x3F;
if(tempCodePoint>0x7FF&&(tempCodePoint<0xD800||tempCodePoint>0xDFFF)){
codePoint=tempCodePoint;
}
}
break;
case 4:
secondByte=buf[i+1];
thirdByte=buf[i+2];
fourthByte=buf[i+3];
if((secondByte&0xC0)===0x80&&(thirdByte&0xC0)===0x80&&(fourthByte&0xC0)===0x80){
tempCodePoint=(firstByte&0xF)<<0x12|(secondByte&0x3F)<<0xC|(thirdByte&0x3F)<<0x6|fourthByte&0x3F;
if(tempCodePoint>0xFFFF&&tempCodePoint<0x110000){
codePoint=tempCodePoint;
}
}}

}

if(codePoint===null){
// we did not generate a valid codePoint so insert a
// replacement char (U+FFFD) and advance only 1 byte
codePoint=0xFFFD;
bytesPerSequence=1;
}else if(codePoint>0xFFFF){
// encode to utf16 (surrogate pair dance)
codePoint-=0x10000;
res.push(codePoint>>>10&0x3FF|0xD800);
codePoint=0xDC00|codePoint&0x3FF;
}

res.push(codePoint);
i+=bytesPerSequence;
}

return decodeCodePointsArray(res);
}

// Based on http://stackoverflow.com/a/22747272/680742, the browser with
// the lowest limit is Chrome, with 0x10000 args.
// We go 1 magnitude less, for safety
var MAX_ARGUMENTS_LENGTH=0x1000;

function decodeCodePointsArray(codePoints){
var len=codePoints.length;
if(len<=MAX_ARGUMENTS_LENGTH){
return String.fromCharCode.apply(String,codePoints);// avoid extra slice()
}

// Decode in chunks to avoid "call stack size exceeded".
var res='';
var i=0;
while(i<len){
res+=String.fromCharCode.apply(
String,
codePoints.slice(i,i+=MAX_ARGUMENTS_LENGTH));

}
return res;
}

function asciiSlice(buf,start,end){
var ret='';
end=Math.min(buf.length,end);

for(var i=start;i<end;++i){
ret+=String.fromCharCode(buf[i]&0x7F);
}
return ret;
}

function latin1Slice(buf,start,end){
var ret='';
end=Math.min(buf.length,end);

for(var i=start;i<end;++i){
ret+=String.fromCharCode(buf[i]);
}
return ret;
}

function hexSlice(buf,start,end){
var len=buf.length;

if(!start||start<0)start=0;
if(!end||end<0||end>len)end=len;

var out='';
for(var i=start;i<end;++i){
out+=toHex(buf[i]);
}
return out;
}

function utf16leSlice(buf,start,end){
var bytes=buf.slice(start,end);
var res='';
for(var i=0;i<bytes.length;i+=2){
res+=String.fromCharCode(bytes[i]+bytes[i+1]*256);
}
return res;
}

Buffer.prototype.slice=function slice(start,end){
var len=this.length;
start=~~start;
end=end===undefined?len:~~end;

if(start<0){
start+=len;
if(start<0)start=0;
}else if(start>len){
start=len;
}

if(end<0){
end+=len;
if(end<0)end=0;
}else if(end>len){
end=len;
}

if(end<start)end=start;

var newBuf;
if(Buffer.TYPED_ARRAY_SUPPORT){
newBuf=this.subarray(start,end);
newBuf.__proto__=Buffer.prototype;
}else {
var sliceLen=end-start;
newBuf=new Buffer(sliceLen,undefined);
for(var i=0;i<sliceLen;++i){
newBuf[i]=this[i+start];
}
}

return newBuf;
};

/*
 * Need to make sure that buffer isn't trying to write out of bounds.
 */
function checkOffset(offset,ext,length){
if(offset%1!==0||offset<0)throw new RangeError('offset is not uint');
if(offset+ext>length)throw new RangeError('Trying to access beyond buffer length');
}

Buffer.prototype.readUIntLE=function readUIntLE(offset,byteLength,noAssert){
offset=offset|0;
byteLength=byteLength|0;
if(!noAssert)checkOffset(offset,byteLength,this.length);

var val=this[offset];
var mul=1;
var i=0;
while(++i<byteLength&&(mul*=0x100)){
val+=this[offset+i]*mul;
}

return val;
};

Buffer.prototype.readUIntBE=function readUIntBE(offset,byteLength,noAssert){
offset=offset|0;
byteLength=byteLength|0;
if(!noAssert){
checkOffset(offset,byteLength,this.length);
}

var val=this[offset+--byteLength];
var mul=1;
while(byteLength>0&&(mul*=0x100)){
val+=this[offset+--byteLength]*mul;
}

return val;
};

Buffer.prototype.readUInt8=function readUInt8(offset,noAssert){
if(!noAssert)checkOffset(offset,1,this.length);
return this[offset];
};

Buffer.prototype.readUInt16LE=function readUInt16LE(offset,noAssert){
if(!noAssert)checkOffset(offset,2,this.length);
return this[offset]|this[offset+1]<<8;
};

Buffer.prototype.readUInt16BE=function readUInt16BE(offset,noAssert){
if(!noAssert)checkOffset(offset,2,this.length);
return this[offset]<<8|this[offset+1];
};

Buffer.prototype.readUInt32LE=function readUInt32LE(offset,noAssert){
if(!noAssert)checkOffset(offset,4,this.length);

return (this[offset]|
this[offset+1]<<8|
this[offset+2]<<16)+
this[offset+3]*0x1000000;
};

Buffer.prototype.readUInt32BE=function readUInt32BE(offset,noAssert){
if(!noAssert)checkOffset(offset,4,this.length);

return this[offset]*0x1000000+(
this[offset+1]<<16|
this[offset+2]<<8|
this[offset+3]);
};

Buffer.prototype.readIntLE=function readIntLE(offset,byteLength,noAssert){
offset=offset|0;
byteLength=byteLength|0;
if(!noAssert)checkOffset(offset,byteLength,this.length);

var val=this[offset];
var mul=1;
var i=0;
while(++i<byteLength&&(mul*=0x100)){
val+=this[offset+i]*mul;
}
mul*=0x80;

if(val>=mul)val-=Math.pow(2,8*byteLength);

return val;
};

Buffer.prototype.readIntBE=function readIntBE(offset,byteLength,noAssert){
offset=offset|0;
byteLength=byteLength|0;
if(!noAssert)checkOffset(offset,byteLength,this.length);

var i=byteLength;
var mul=1;
var val=this[offset+--i];
while(i>0&&(mul*=0x100)){
val+=this[offset+--i]*mul;
}
mul*=0x80;

if(val>=mul)val-=Math.pow(2,8*byteLength);

return val;
};

Buffer.prototype.readInt8=function readInt8(offset,noAssert){
if(!noAssert)checkOffset(offset,1,this.length);
if(!(this[offset]&0x80))return this[offset];
return (0xff-this[offset]+1)*-1;
};

Buffer.prototype.readInt16LE=function readInt16LE(offset,noAssert){
if(!noAssert)checkOffset(offset,2,this.length);
var val=this[offset]|this[offset+1]<<8;
return val&0x8000?val|0xFFFF0000:val;
};

Buffer.prototype.readInt16BE=function readInt16BE(offset,noAssert){
if(!noAssert)checkOffset(offset,2,this.length);
var val=this[offset+1]|this[offset]<<8;
return val&0x8000?val|0xFFFF0000:val;
};

Buffer.prototype.readInt32LE=function readInt32LE(offset,noAssert){
if(!noAssert)checkOffset(offset,4,this.length);

return this[offset]|
this[offset+1]<<8|
this[offset+2]<<16|
this[offset+3]<<24;
};

Buffer.prototype.readInt32BE=function readInt32BE(offset,noAssert){
if(!noAssert)checkOffset(offset,4,this.length);

return this[offset]<<24|
this[offset+1]<<16|
this[offset+2]<<8|
this[offset+3];
};

Buffer.prototype.readFloatLE=function readFloatLE(offset,noAssert){
if(!noAssert)checkOffset(offset,4,this.length);
return ieee754.read(this,offset,true,23,4);
};

Buffer.prototype.readFloatBE=function readFloatBE(offset,noAssert){
if(!noAssert)checkOffset(offset,4,this.length);
return ieee754.read(this,offset,false,23,4);
};

Buffer.prototype.readDoubleLE=function readDoubleLE(offset,noAssert){
if(!noAssert)checkOffset(offset,8,this.length);
return ieee754.read(this,offset,true,52,8);
};

Buffer.prototype.readDoubleBE=function readDoubleBE(offset,noAssert){
if(!noAssert)checkOffset(offset,8,this.length);
return ieee754.read(this,offset,false,52,8);
};

function checkInt(buf,value,offset,ext,max,min){
if(!Buffer.isBuffer(buf))throw new TypeError('"buffer" argument must be a Buffer instance');
if(value>max||value<min)throw new RangeError('"value" argument is out of bounds');
if(offset+ext>buf.length)throw new RangeError('Index out of range');
}

Buffer.prototype.writeUIntLE=function writeUIntLE(value,offset,byteLength,noAssert){
value=+value;
offset=offset|0;
byteLength=byteLength|0;
if(!noAssert){
var maxBytes=Math.pow(2,8*byteLength)-1;
checkInt(this,value,offset,byteLength,maxBytes,0);
}

var mul=1;
var i=0;
this[offset]=value&0xFF;
while(++i<byteLength&&(mul*=0x100)){
this[offset+i]=value/mul&0xFF;
}

return offset+byteLength;
};

Buffer.prototype.writeUIntBE=function writeUIntBE(value,offset,byteLength,noAssert){
value=+value;
offset=offset|0;
byteLength=byteLength|0;
if(!noAssert){
var maxBytes=Math.pow(2,8*byteLength)-1;
checkInt(this,value,offset,byteLength,maxBytes,0);
}

var i=byteLength-1;
var mul=1;
this[offset+i]=value&0xFF;
while(--i>=0&&(mul*=0x100)){
this[offset+i]=value/mul&0xFF;
}

return offset+byteLength;
};

Buffer.prototype.writeUInt8=function writeUInt8(value,offset,noAssert){
value=+value;
offset=offset|0;
if(!noAssert)checkInt(this,value,offset,1,0xff,0);
if(!Buffer.TYPED_ARRAY_SUPPORT)value=Math.floor(value);
this[offset]=value&0xff;
return offset+1;
};

function objectWriteUInt16(buf,value,offset,littleEndian){
if(value<0)value=0xffff+value+1;
for(var i=0,j=Math.min(buf.length-offset,2);i<j;++i){
buf[offset+i]=(value&0xff<<8*(littleEndian?i:1-i))>>>
(littleEndian?i:1-i)*8;
}
}

Buffer.prototype.writeUInt16LE=function writeUInt16LE(value,offset,noAssert){
value=+value;
offset=offset|0;
if(!noAssert)checkInt(this,value,offset,2,0xffff,0);
if(Buffer.TYPED_ARRAY_SUPPORT){
this[offset]=value&0xff;
this[offset+1]=value>>>8;
}else {
objectWriteUInt16(this,value,offset,true);
}
return offset+2;
};

Buffer.prototype.writeUInt16BE=function writeUInt16BE(value,offset,noAssert){
value=+value;
offset=offset|0;
if(!noAssert)checkInt(this,value,offset,2,0xffff,0);
if(Buffer.TYPED_ARRAY_SUPPORT){
this[offset]=value>>>8;
this[offset+1]=value&0xff;
}else {
objectWriteUInt16(this,value,offset,false);
}
return offset+2;
};

function objectWriteUInt32(buf,value,offset,littleEndian){
if(value<0)value=0xffffffff+value+1;
for(var i=0,j=Math.min(buf.length-offset,4);i<j;++i){
buf[offset+i]=value>>>(littleEndian?i:3-i)*8&0xff;
}
}

Buffer.prototype.writeUInt32LE=function writeUInt32LE(value,offset,noAssert){
value=+value;
offset=offset|0;
if(!noAssert)checkInt(this,value,offset,4,0xffffffff,0);
if(Buffer.TYPED_ARRAY_SUPPORT){
this[offset+3]=value>>>24;
this[offset+2]=value>>>16;
this[offset+1]=value>>>8;
this[offset]=value&0xff;
}else {
objectWriteUInt32(this,value,offset,true);
}
return offset+4;
};

Buffer.prototype.writeUInt32BE=function writeUInt32BE(value,offset,noAssert){
value=+value;
offset=offset|0;
if(!noAssert)checkInt(this,value,offset,4,0xffffffff,0);
if(Buffer.TYPED_ARRAY_SUPPORT){
this[offset]=value>>>24;
this[offset+1]=value>>>16;
this[offset+2]=value>>>8;
this[offset+3]=value&0xff;
}else {
objectWriteUInt32(this,value,offset,false);
}
return offset+4;
};

Buffer.prototype.writeIntLE=function writeIntLE(value,offset,byteLength,noAssert){
value=+value;
offset=offset|0;
if(!noAssert){
var limit=Math.pow(2,8*byteLength-1);

checkInt(this,value,offset,byteLength,limit-1,-limit);
}

var i=0;
var mul=1;
var sub=0;
this[offset]=value&0xFF;
while(++i<byteLength&&(mul*=0x100)){
if(value<0&&sub===0&&this[offset+i-1]!==0){
sub=1;
}
this[offset+i]=(value/mul>>0)-sub&0xFF;
}

return offset+byteLength;
};

Buffer.prototype.writeIntBE=function writeIntBE(value,offset,byteLength,noAssert){
value=+value;
offset=offset|0;
if(!noAssert){
var limit=Math.pow(2,8*byteLength-1);

checkInt(this,value,offset,byteLength,limit-1,-limit);
}

var i=byteLength-1;
var mul=1;
var sub=0;
this[offset+i]=value&0xFF;
while(--i>=0&&(mul*=0x100)){
if(value<0&&sub===0&&this[offset+i+1]!==0){
sub=1;
}
this[offset+i]=(value/mul>>0)-sub&0xFF;
}

return offset+byteLength;
};

Buffer.prototype.writeInt8=function writeInt8(value,offset,noAssert){
value=+value;
offset=offset|0;
if(!noAssert)checkInt(this,value,offset,1,0x7f,-0x80);
if(!Buffer.TYPED_ARRAY_SUPPORT)value=Math.floor(value);
if(value<0)value=0xff+value+1;
this[offset]=value&0xff;
return offset+1;
};

Buffer.prototype.writeInt16LE=function writeInt16LE(value,offset,noAssert){
value=+value;
offset=offset|0;
if(!noAssert)checkInt(this,value,offset,2,0x7fff,-0x8000);
if(Buffer.TYPED_ARRAY_SUPPORT){
this[offset]=value&0xff;
this[offset+1]=value>>>8;
}else {
objectWriteUInt16(this,value,offset,true);
}
return offset+2;
};

Buffer.prototype.writeInt16BE=function writeInt16BE(value,offset,noAssert){
value=+value;
offset=offset|0;
if(!noAssert)checkInt(this,value,offset,2,0x7fff,-0x8000);
if(Buffer.TYPED_ARRAY_SUPPORT){
this[offset]=value>>>8;
this[offset+1]=value&0xff;
}else {
objectWriteUInt16(this,value,offset,false);
}
return offset+2;
};

Buffer.prototype.writeInt32LE=function writeInt32LE(value,offset,noAssert){
value=+value;
offset=offset|0;
if(!noAssert)checkInt(this,value,offset,4,0x7fffffff,-0x80000000);
if(Buffer.TYPED_ARRAY_SUPPORT){
this[offset]=value&0xff;
this[offset+1]=value>>>8;
this[offset+2]=value>>>16;
this[offset+3]=value>>>24;
}else {
objectWriteUInt32(this,value,offset,true);
}
return offset+4;
};

Buffer.prototype.writeInt32BE=function writeInt32BE(value,offset,noAssert){
value=+value;
offset=offset|0;
if(!noAssert)checkInt(this,value,offset,4,0x7fffffff,-0x80000000);
if(value<0)value=0xffffffff+value+1;
if(Buffer.TYPED_ARRAY_SUPPORT){
this[offset]=value>>>24;
this[offset+1]=value>>>16;
this[offset+2]=value>>>8;
this[offset+3]=value&0xff;
}else {
objectWriteUInt32(this,value,offset,false);
}
return offset+4;
};

function checkIEEE754(buf,value,offset,ext,max,min){
if(offset+ext>buf.length)throw new RangeError('Index out of range');
if(offset<0)throw new RangeError('Index out of range');
}

function writeFloat(buf,value,offset,littleEndian,noAssert){
if(!noAssert){
checkIEEE754(buf,value,offset,4);
}
ieee754.write(buf,value,offset,littleEndian,23,4);
return offset+4;
}

Buffer.prototype.writeFloatLE=function writeFloatLE(value,offset,noAssert){
return writeFloat(this,value,offset,true,noAssert);
};

Buffer.prototype.writeFloatBE=function writeFloatBE(value,offset,noAssert){
return writeFloat(this,value,offset,false,noAssert);
};

function writeDouble(buf,value,offset,littleEndian,noAssert){
if(!noAssert){
checkIEEE754(buf,value,offset,8);
}
ieee754.write(buf,value,offset,littleEndian,52,8);
return offset+8;
}

Buffer.prototype.writeDoubleLE=function writeDoubleLE(value,offset,noAssert){
return writeDouble(this,value,offset,true,noAssert);
};

Buffer.prototype.writeDoubleBE=function writeDoubleBE(value,offset,noAssert){
return writeDouble(this,value,offset,false,noAssert);
};

// copy(targetBuffer, targetStart=0, sourceStart=0, sourceEnd=buffer.length)
Buffer.prototype.copy=function copy(target,targetStart,start,end){
if(!start)start=0;
if(!end&&end!==0)end=this.length;
if(targetStart>=target.length)targetStart=target.length;
if(!targetStart)targetStart=0;
if(end>0&&end<start)end=start;

// Copy 0 bytes; we're done
if(end===start)return 0;
if(target.length===0||this.length===0)return 0;

// Fatal error conditions
if(targetStart<0){
throw new RangeError('targetStart out of bounds');
}
if(start<0||start>=this.length)throw new RangeError('sourceStart out of bounds');
if(end<0)throw new RangeError('sourceEnd out of bounds');

// Are we oob?
if(end>this.length)end=this.length;
if(target.length-targetStart<end-start){
end=target.length-targetStart+start;
}

var len=end-start;
var i;

if(this===target&&start<targetStart&&targetStart<end){
// descending copy from end
for(i=len-1;i>=0;--i){
target[i+targetStart]=this[i+start];
}
}else if(len<1000||!Buffer.TYPED_ARRAY_SUPPORT){
// ascending copy from start
for(i=0;i<len;++i){
target[i+targetStart]=this[i+start];
}
}else {
Uint8Array.prototype.set.call(
target,
this.subarray(start,start+len),
targetStart);

}

return len;
};

// Usage:
//    buffer.fill(number[, offset[, end]])
//    buffer.fill(buffer[, offset[, end]])
//    buffer.fill(string[, offset[, end]][, encoding])
Buffer.prototype.fill=function fill(val,start,end,encoding){
// Handle string cases:
if(typeof val==='string'){
if(typeof start==='string'){
encoding=start;
start=0;
end=this.length;
}else if(typeof end==='string'){
encoding=end;
end=this.length;
}
if(val.length===1){
var code=val.charCodeAt(0);
if(code<256){
val=code;
}
}
if(encoding!==undefined&&typeof encoding!=='string'){
throw new TypeError('encoding must be a string');
}
if(typeof encoding==='string'&&!Buffer.isEncoding(encoding)){
throw new TypeError('Unknown encoding: '+encoding);
}
}else if(typeof val==='number'){
val=val&255;
}

// Invalid ranges are not set to a default, so can range check early.
if(start<0||this.length<start||this.length<end){
throw new RangeError('Out of range index');
}

if(end<=start){
return this;
}

start=start>>>0;
end=end===undefined?this.length:end>>>0;

if(!val)val=0;

var i;
if(typeof val==='number'){
for(i=start;i<end;++i){
this[i]=val;
}
}else {
var bytes=Buffer.isBuffer(val)?
val:
utf8ToBytes(new Buffer(val,encoding).toString());
var len=bytes.length;
for(i=0;i<end-start;++i){
this[i+start]=bytes[i%len];
}
}

return this;
};

// HELPER FUNCTIONS
// ================

var INVALID_BASE64_RE=/[^+\/0-9A-Za-z-_]/g;

function base64clean(str){
// Node strips out invalid characters like \n and \t from the string, base64-js does not
str=stringtrim(str).replace(INVALID_BASE64_RE,'');
// Node converts strings with length < 2 to ''
if(str.length<2)return '';
// Node allows for non-padded base64 strings (missing trailing ===), base64-js does not
while(str.length%4!==0){
str=str+'=';
}
return str;
}

function stringtrim(str){
if(str.trim)return str.trim();
return str.replace(/^\s+|\s+$/g,'');
}

function toHex(n){
if(n<16)return '0'+n.toString(16);
return n.toString(16);
}

function utf8ToBytes(string,units){
units=units||Infinity;
var codePoint;
var length=string.length;
var leadSurrogate=null;
var bytes=[];

for(var i=0;i<length;++i){
codePoint=string.charCodeAt(i);

// is surrogate component
if(codePoint>0xD7FF&&codePoint<0xE000){
// last char was a lead
if(!leadSurrogate){
// no lead yet
if(codePoint>0xDBFF){
// unexpected trail
if((units-=3)>-1)bytes.push(0xEF,0xBF,0xBD);
continue;
}else if(i+1===length){
// unpaired lead
if((units-=3)>-1)bytes.push(0xEF,0xBF,0xBD);
continue;
}

// valid lead
leadSurrogate=codePoint;

continue;
}

// 2 leads in a row
if(codePoint<0xDC00){
if((units-=3)>-1)bytes.push(0xEF,0xBF,0xBD);
leadSurrogate=codePoint;
continue;
}

// valid surrogate pair
codePoint=(leadSurrogate-0xD800<<10|codePoint-0xDC00)+0x10000;
}else if(leadSurrogate){
// valid bmp char, but last char was a lead
if((units-=3)>-1)bytes.push(0xEF,0xBF,0xBD);
}

leadSurrogate=null;

// encode utf8
if(codePoint<0x80){
if((units-=1)<0)break;
bytes.push(codePoint);
}else if(codePoint<0x800){
if((units-=2)<0)break;
bytes.push(
codePoint>>0x6|0xC0,
codePoint&0x3F|0x80);

}else if(codePoint<0x10000){
if((units-=3)<0)break;
bytes.push(
codePoint>>0xC|0xE0,
codePoint>>0x6&0x3F|0x80,
codePoint&0x3F|0x80);

}else if(codePoint<0x110000){
if((units-=4)<0)break;
bytes.push(
codePoint>>0x12|0xF0,
codePoint>>0xC&0x3F|0x80,
codePoint>>0x6&0x3F|0x80,
codePoint&0x3F|0x80);

}else {
throw new Error('Invalid code point');
}
}

return bytes;
}

function asciiToBytes(str){
var byteArray=[];
for(var i=0;i<str.length;++i){
// Node's code seems to be doing this and not & 0x7F..
byteArray.push(str.charCodeAt(i)&0xFF);
}
return byteArray;
}

function utf16leToBytes(str,units){
var c,hi,lo;
var byteArray=[];
for(var i=0;i<str.length;++i){
if((units-=2)<0)break;

c=str.charCodeAt(i);
hi=c>>8;
lo=c%256;
byteArray.push(lo);
byteArray.push(hi);
}

return byteArray;
}

function base64ToBytes(str){
return base64.toByteArray(base64clean(str));
}

function blitBuffer(src,dst,offset,length){
for(var i=0;i<length;++i){
if(i+offset>=dst.length||i>=src.length)break;
dst[i+offset]=src[i];
}
return i;
}

function isnan(val){
return val!==val;// eslint-disable-line no-self-compare
}

/* WEBPACK VAR INJECTION */}).call(this,__webpack_require__(/*! ./../webpack/buildin/global.js */"./node_modules/webpack/buildin/global.js"));

/***/},

/***/"./node_modules/ieee754/index.js":
/*!***************************************!*\
  !*** ./node_modules/ieee754/index.js ***!
  \***************************************/
/*! no static exports found */
/***/function node_modulesIeee754IndexJs(module,exports){

/*! ieee754. BSD-3-Clause License. Feross Aboukhadijeh <https://feross.org/opensource> */
exports.read=function(buffer,offset,isLE,mLen,nBytes){
var e,m;
var eLen=nBytes*8-mLen-1;
var eMax=(1<<eLen)-1;
var eBias=eMax>>1;
var nBits=-7;
var i=isLE?nBytes-1:0;
var d=isLE?-1:1;
var s=buffer[offset+i];

i+=d;

e=s&(1<<-nBits)-1;
s>>=-nBits;
nBits+=eLen;
for(;nBits>0;e=e*256+buffer[offset+i],i+=d,nBits-=8){}

m=e&(1<<-nBits)-1;
e>>=-nBits;
nBits+=mLen;
for(;nBits>0;m=m*256+buffer[offset+i],i+=d,nBits-=8){}

if(e===0){
e=1-eBias;
}else if(e===eMax){
return m?NaN:(s?-1:1)*Infinity;
}else {
m=m+Math.pow(2,mLen);
e=e-eBias;
}
return (s?-1:1)*m*Math.pow(2,e-mLen);
};

exports.write=function(buffer,value,offset,isLE,mLen,nBytes){
var e,m,c;
var eLen=nBytes*8-mLen-1;
var eMax=(1<<eLen)-1;
var eBias=eMax>>1;
var rt=mLen===23?Math.pow(2,-24)-Math.pow(2,-77):0;
var i=isLE?0:nBytes-1;
var d=isLE?1:-1;
var s=value<0||value===0&&1/value<0?1:0;

value=Math.abs(value);

if(isNaN(value)||value===Infinity){
m=isNaN(value)?1:0;
e=eMax;
}else {
e=Math.floor(Math.log(value)/Math.LN2);
if(value*(c=Math.pow(2,-e))<1){
e--;
c*=2;
}
if(e+eBias>=1){
value+=rt/c;
}else {
value+=rt*Math.pow(2,1-eBias);
}
if(value*c>=2){
e++;
c/=2;
}

if(e+eBias>=eMax){
m=0;
e=eMax;
}else if(e+eBias>=1){
m=(value*c-1)*Math.pow(2,mLen);
e=e+eBias;
}else {
m=value*Math.pow(2,eBias-1)*Math.pow(2,mLen);
e=0;
}
}

for(;mLen>=8;buffer[offset+i]=m&0xff,i+=d,m/=256,mLen-=8){}

e=e<<mLen|m;
eLen+=mLen;
for(;eLen>0;buffer[offset+i]=e&0xff,i+=d,e/=256,eLen-=8){}

buffer[offset+i-d]|=s*128;
};


/***/},

/***/"./node_modules/isarray/index.js":
/*!***************************************!*\
  !*** ./node_modules/isarray/index.js ***!
  \***************************************/
/*! no static exports found */
/***/function node_modulesIsarrayIndexJs(module,exports){

var toString={}.toString;

module.exports=Array.isArray||function(arr){
return toString.call(arr)=='[object Array]';
};


/***/},

/***/"./node_modules/quill/dist/quill.js":
/*!******************************************!*\
  !*** ./node_modules/quill/dist/quill.js ***!
  \******************************************/
/*! no static exports found */
/***/function node_modulesQuillDistQuillJs(module,exports,__webpack_require__){

/* WEBPACK VAR INJECTION */(function(Buffer){/*!
 * Quill Editor v1.3.7
 * https://quilljs.com/
 * Copyright (c) 2014, Jason Chen
 * Copyright (c) 2013, salesforce.com
 */
(function webpackUniversalModuleDefinition(root,factory){
module.exports=factory();
})(typeof self!=='undefined'?self:this,function(){
return(/******/function(modules){// webpackBootstrap
/******/ // The module cache
/******/var installedModules={};
/******/
/******/ // The require function
/******/function __webpack_require__(moduleId){
/******/
/******/ // Check if module is in cache
/******/if(installedModules[moduleId]){
/******/return installedModules[moduleId].exports;
/******/}
/******/ // Create a new module (and put it into the cache)
/******/var module=installedModules[moduleId]={
/******/i:moduleId,
/******/l:false,
/******/exports:{}
/******/};
/******/
/******/ // Execute the module function
/******/modules[moduleId].call(module.exports,module,module.exports,__webpack_require__);
/******/
/******/ // Flag the module as loaded
/******/module.l=true;
/******/
/******/ // Return the exports of the module
/******/return module.exports;
/******/}
/******/
/******/
/******/ // expose the modules object (__webpack_modules__)
/******/__webpack_require__.m=modules;
/******/
/******/ // expose the module cache
/******/__webpack_require__.c=installedModules;
/******/
/******/ // define getter function for harmony exports
/******/__webpack_require__.d=function(exports,name,getter){
/******/if(!__webpack_require__.o(exports,name)){
/******/Object.defineProperty(exports,name,{
/******/configurable:false,
/******/enumerable:true,
/******/get:getter
/******/});
/******/}
/******/};
/******/
/******/ // getDefaultExport function for compatibility with non-harmony modules
/******/__webpack_require__.n=function(module){
/******/var getter=module&&module.__esModule?
/******/function getDefault(){return module['default'];}:
/******/function getModuleExports(){return module;};
/******/__webpack_require__.d(getter,'a',getter);
/******/return getter;
/******/};
/******/
/******/ // Object.prototype.hasOwnProperty.call
/******/__webpack_require__.o=function(object,property){return Object.prototype.hasOwnProperty.call(object,property);};
/******/
/******/ // __webpack_public_path__
/******/__webpack_require__.p="";
/******/
/******/ // Load entry module and return exports
/******/return __webpack_require__(__webpack_require__.s=109);
/******/}(
/************************************************************************/
/******/[
/* 0 */
/***/function(module,exports,__webpack_require__){

Object.defineProperty(exports,"__esModule",{value:true});
var container_1=__webpack_require__(17);
var format_1=__webpack_require__(18);
var leaf_1=__webpack_require__(19);
var scroll_1=__webpack_require__(45);
var inline_1=__webpack_require__(46);
var block_1=__webpack_require__(47);
var embed_1=__webpack_require__(48);
var text_1=__webpack_require__(49);
var attributor_1=__webpack_require__(12);
var class_1=__webpack_require__(32);
var style_1=__webpack_require__(33);
var store_1=__webpack_require__(31);
var Registry=__webpack_require__(1);
var Parchment={
Scope:Registry.Scope,
create:Registry.create,
find:Registry.find,
query:Registry.query,
register:Registry.register,
Container:container_1.default,
Format:format_1.default,
Leaf:leaf_1.default,
Embed:embed_1.default,
Scroll:scroll_1.default,
Block:block_1.default,
Inline:inline_1.default,
Text:text_1.default,
Attributor:{
Attribute:attributor_1.default,
Class:class_1.default,
Style:style_1.default,
Store:store_1.default}};


exports.default=Parchment;


/***/},
/* 1 */
/***/function(module,exports,__webpack_require__){

var __extends=this&&this.__extends||function(){
var extendStatics=Object.setPrototypeOf||
{__proto__:[]}instanceof Array&&function(d,b){d.__proto__=b;}||
function(d,b){for(var p in b){if(b.hasOwnProperty(p))d[p]=b[p];}};
return function(d,b){
extendStatics(d,b);
function __(){this.constructor=d;}
d.prototype=b===null?Object.create(b):(__.prototype=b.prototype,new __());
};
}();
Object.defineProperty(exports,"__esModule",{value:true});
var ParchmentError=/** @class */function(_super){
__extends(ParchmentError,_super);
function ParchmentError(message){
var _this=this;
message='[Parchment] '+message;
_this=_super.call(this,message)||this;
_this.message=message;
_this.name=_this.constructor.name;
return _this;
}
return ParchmentError;
}(Error);
exports.ParchmentError=ParchmentError;
var attributes={};
var classes={};
var tags={};
var types={};
exports.DATA_KEY='__blot';
var Scope;
(function(Scope){
Scope[Scope["TYPE"]=3]="TYPE";
Scope[Scope["LEVEL"]=12]="LEVEL";
Scope[Scope["ATTRIBUTE"]=13]="ATTRIBUTE";
Scope[Scope["BLOT"]=14]="BLOT";
Scope[Scope["INLINE"]=7]="INLINE";
Scope[Scope["BLOCK"]=11]="BLOCK";
Scope[Scope["BLOCK_BLOT"]=10]="BLOCK_BLOT";
Scope[Scope["INLINE_BLOT"]=6]="INLINE_BLOT";
Scope[Scope["BLOCK_ATTRIBUTE"]=9]="BLOCK_ATTRIBUTE";
Scope[Scope["INLINE_ATTRIBUTE"]=5]="INLINE_ATTRIBUTE";
Scope[Scope["ANY"]=15]="ANY";
})(Scope=exports.Scope||(exports.Scope={}));
function create(input,value){
var match=query(input);
if(match==null){
throw new ParchmentError("Unable to create "+input+" blot");
}
var BlotClass=match;
var node=
// @ts-ignore
input instanceof Node||input['nodeType']===Node.TEXT_NODE?input:BlotClass.create(value);
return new BlotClass(node,value);
}
exports.create=create;
function find(node,bubble){
if(bubble===void 0){bubble=false;}
if(node==null)
return null;
// @ts-ignore
if(node[exports.DATA_KEY]!=null)
return node[exports.DATA_KEY].blot;
if(bubble)
return find(node.parentNode,bubble);
return null;
}
exports.find=find;
function query(query,scope){
if(scope===void 0){scope=Scope.ANY;}
var match;
if(typeof query==='string'){
match=types[query]||attributes[query];
// @ts-ignore
}else
if(query instanceof Text||query['nodeType']===Node.TEXT_NODE){
match=types['text'];
}else
if(typeof query==='number'){
if(query&Scope.LEVEL&Scope.BLOCK){
match=types['block'];
}else
if(query&Scope.LEVEL&Scope.INLINE){
match=types['inline'];
}
}else
if(query instanceof HTMLElement){
var names=(query.getAttribute('class')||'').split(/\s+/);
for(var i in names){
match=classes[names[i]];
if(match)
break;
}
match=match||tags[query.tagName];
}
if(match==null)
return null;
// @ts-ignore
if(scope&Scope.LEVEL&match.scope&&scope&Scope.TYPE&match.scope)
return match;
return null;
}
exports.query=query;
function register(){
var Definitions=[];
for(var _i=0;_i<arguments.length;_i++){
Definitions[_i]=arguments[_i];
}
if(Definitions.length>1){
return Definitions.map(function(d){
return register(d);
});
}
var Definition=Definitions[0];
if(typeof Definition.blotName!=='string'&&typeof Definition.attrName!=='string'){
throw new ParchmentError('Invalid definition');
}else
if(Definition.blotName==='abstract'){
throw new ParchmentError('Cannot register abstract class');
}
types[Definition.blotName||Definition.attrName]=Definition;
if(typeof Definition.keyName==='string'){
attributes[Definition.keyName]=Definition;
}else
{
if(Definition.className!=null){
classes[Definition.className]=Definition;
}
if(Definition.tagName!=null){
if(Array.isArray(Definition.tagName)){
Definition.tagName=Definition.tagName.map(function(tagName){
return tagName.toUpperCase();
});
}else
{
Definition.tagName=Definition.tagName.toUpperCase();
}
var tagNames=Array.isArray(Definition.tagName)?Definition.tagName:[Definition.tagName];
tagNames.forEach(function(tag){
if(tags[tag]==null||Definition.className==null){
tags[tag]=Definition;
}
});
}
}
return Definition;
}
exports.register=register;


/***/},
/* 2 */
/***/function(module,exports,__webpack_require__){

var diff=__webpack_require__(51);
var equal=__webpack_require__(11);
var extend=__webpack_require__(3);
var op=__webpack_require__(20);


var NULL_CHARACTER=String.fromCharCode(0);// Placeholder char for embed in diff()


var Delta=function Delta(ops){
// Assume we are given a well formed ops
if(Array.isArray(ops)){
this.ops=ops;
}else if(ops!=null&&Array.isArray(ops.ops)){
this.ops=ops.ops;
}else {
this.ops=[];
}
};


Delta.prototype.insert=function(text,attributes){
var newOp={};
if(text.length===0)return this;
newOp.insert=text;
if(attributes!=null&&typeof attributes==='object'&&Object.keys(attributes).length>0){
newOp.attributes=attributes;
}
return this.push(newOp);
};

Delta.prototype['delete']=function(length){
if(length<=0)return this;
return this.push({'delete':length});
};

Delta.prototype.retain=function(length,attributes){
if(length<=0)return this;
var newOp={retain:length};
if(attributes!=null&&typeof attributes==='object'&&Object.keys(attributes).length>0){
newOp.attributes=attributes;
}
return this.push(newOp);
};

Delta.prototype.push=function(newOp){
var index=this.ops.length;
var lastOp=this.ops[index-1];
newOp=extend(true,{},newOp);
if(typeof lastOp==='object'){
if(typeof newOp['delete']==='number'&&typeof lastOp['delete']==='number'){
this.ops[index-1]={'delete':lastOp['delete']+newOp['delete']};
return this;
}
// Since it does not matter if we insert before or after deleting at the same index,
// always prefer to insert first
if(typeof lastOp['delete']==='number'&&newOp.insert!=null){
index-=1;
lastOp=this.ops[index-1];
if(typeof lastOp!=='object'){
this.ops.unshift(newOp);
return this;
}
}
if(equal(newOp.attributes,lastOp.attributes)){
if(typeof newOp.insert==='string'&&typeof lastOp.insert==='string'){
this.ops[index-1]={insert:lastOp.insert+newOp.insert};
if(typeof newOp.attributes==='object')this.ops[index-1].attributes=newOp.attributes;
return this;
}else if(typeof newOp.retain==='number'&&typeof lastOp.retain==='number'){
this.ops[index-1]={retain:lastOp.retain+newOp.retain};
if(typeof newOp.attributes==='object')this.ops[index-1].attributes=newOp.attributes;
return this;
}
}
}
if(index===this.ops.length){
this.ops.push(newOp);
}else {
this.ops.splice(index,0,newOp);
}
return this;
};

Delta.prototype.chop=function(){
var lastOp=this.ops[this.ops.length-1];
if(lastOp&&lastOp.retain&&!lastOp.attributes){
this.ops.pop();
}
return this;
};

Delta.prototype.filter=function(predicate){
return this.ops.filter(predicate);
};

Delta.prototype.forEach=function(predicate){
this.ops.forEach(predicate);
};

Delta.prototype.map=function(predicate){
return this.ops.map(predicate);
};

Delta.prototype.partition=function(predicate){
var passed=[],failed=[];
this.forEach(function(op){
var target=predicate(op)?passed:failed;
target.push(op);
});
return [passed,failed];
};

Delta.prototype.reduce=function(predicate,initial){
return this.ops.reduce(predicate,initial);
};

Delta.prototype.changeLength=function(){
return this.reduce(function(length,elem){
if(elem.insert){
return length+op.length(elem);
}else if(elem.delete){
return length-elem.delete;
}
return length;
},0);
};

Delta.prototype.length=function(){
return this.reduce(function(length,elem){
return length+op.length(elem);
},0);
};

Delta.prototype.slice=function(start,end){
start=start||0;
if(typeof end!=='number')end=Infinity;
var ops=[];
var iter=op.iterator(this.ops);
var index=0;
while(index<end&&iter.hasNext()){
var nextOp;
if(index<start){
nextOp=iter.next(start-index);
}else {
nextOp=iter.next(end-index);
ops.push(nextOp);
}
index+=op.length(nextOp);
}
return new Delta(ops);
};


Delta.prototype.compose=function(other){
var thisIter=op.iterator(this.ops);
var otherIter=op.iterator(other.ops);
var ops=[];
var firstOther=otherIter.peek();
if(firstOther!=null&&typeof firstOther.retain==='number'&&firstOther.attributes==null){
var firstLeft=firstOther.retain;
while(thisIter.peekType()==='insert'&&thisIter.peekLength()<=firstLeft){
firstLeft-=thisIter.peekLength();
ops.push(thisIter.next());
}
if(firstOther.retain-firstLeft>0){
otherIter.next(firstOther.retain-firstLeft);
}
}
var delta=new Delta(ops);
while(thisIter.hasNext()||otherIter.hasNext()){
if(otherIter.peekType()==='insert'){
delta.push(otherIter.next());
}else if(thisIter.peekType()==='delete'){
delta.push(thisIter.next());
}else {
var length=Math.min(thisIter.peekLength(),otherIter.peekLength());
var thisOp=thisIter.next(length);
var otherOp=otherIter.next(length);
if(typeof otherOp.retain==='number'){
var newOp={};
if(typeof thisOp.retain==='number'){
newOp.retain=length;
}else {
newOp.insert=thisOp.insert;
}
// Preserve null when composing with a retain, otherwise remove it for inserts
var attributes=op.attributes.compose(thisOp.attributes,otherOp.attributes,typeof thisOp.retain==='number');
if(attributes)newOp.attributes=attributes;
delta.push(newOp);

// Optimization if rest of other is just retain
if(!otherIter.hasNext()&&equal(delta.ops[delta.ops.length-1],newOp)){
var rest=new Delta(thisIter.rest());
return delta.concat(rest).chop();
}

// Other op should be delete, we could be an insert or retain
// Insert + delete cancels out
}else if(typeof otherOp['delete']==='number'&&typeof thisOp.retain==='number'){
delta.push(otherOp);
}
}
}
return delta.chop();
};

Delta.prototype.concat=function(other){
var delta=new Delta(this.ops.slice());
if(other.ops.length>0){
delta.push(other.ops[0]);
delta.ops=delta.ops.concat(other.ops.slice(1));
}
return delta;
};

Delta.prototype.diff=function(other,index){
if(this.ops===other.ops){
return new Delta();
}
var strings=[this,other].map(function(delta){
return delta.map(function(op){
if(op.insert!=null){
return typeof op.insert==='string'?op.insert:NULL_CHARACTER;
}
var prep=delta===other?'on':'with';
throw new Error('diff() called '+prep+' non-document');
}).join('');
});
var delta=new Delta();
var diffResult=diff(strings[0],strings[1],index);
var thisIter=op.iterator(this.ops);
var otherIter=op.iterator(other.ops);
diffResult.forEach(function(component){
var length=component[1].length;
while(length>0){
var opLength=0;
switch(component[0]){
case diff.INSERT:
opLength=Math.min(otherIter.peekLength(),length);
delta.push(otherIter.next(opLength));
break;
case diff.DELETE:
opLength=Math.min(length,thisIter.peekLength());
thisIter.next(opLength);
delta['delete'](opLength);
break;
case diff.EQUAL:
opLength=Math.min(thisIter.peekLength(),otherIter.peekLength(),length);
var thisOp=thisIter.next(opLength);
var otherOp=otherIter.next(opLength);
if(equal(thisOp.insert,otherOp.insert)){
delta.retain(opLength,op.attributes.diff(thisOp.attributes,otherOp.attributes));
}else {
delta.push(otherOp)['delete'](opLength);
}
break;}

length-=opLength;
}
});
return delta.chop();
};

Delta.prototype.eachLine=function(predicate,newline){
newline=newline||'\n';
var iter=op.iterator(this.ops);
var line=new Delta();
var i=0;
while(iter.hasNext()){
if(iter.peekType()!=='insert')return;
var thisOp=iter.peek();
var start=op.length(thisOp)-iter.peekLength();
var index=typeof thisOp.insert==='string'?
thisOp.insert.indexOf(newline,start)-start:-1;
if(index<0){
line.push(iter.next());
}else if(index>0){
line.push(iter.next(index));
}else {
if(predicate(line,iter.next(1).attributes||{},i)===false){
return;
}
i+=1;
line=new Delta();
}
}
if(line.length()>0){
predicate(line,{},i);
}
};

Delta.prototype.transform=function(other,priority){
priority=!!priority;
if(typeof other==='number'){
return this.transformPosition(other,priority);
}
var thisIter=op.iterator(this.ops);
var otherIter=op.iterator(other.ops);
var delta=new Delta();
while(thisIter.hasNext()||otherIter.hasNext()){
if(thisIter.peekType()==='insert'&&(priority||otherIter.peekType()!=='insert')){
delta.retain(op.length(thisIter.next()));
}else if(otherIter.peekType()==='insert'){
delta.push(otherIter.next());
}else {
var length=Math.min(thisIter.peekLength(),otherIter.peekLength());
var thisOp=thisIter.next(length);
var otherOp=otherIter.next(length);
if(thisOp['delete']){
// Our delete either makes their delete redundant or removes their retain
continue;
}else if(otherOp['delete']){
delta.push(otherOp);
}else {
// We retain either their retain or insert
delta.retain(length,op.attributes.transform(thisOp.attributes,otherOp.attributes,priority));
}
}
}
return delta.chop();
};

Delta.prototype.transformPosition=function(index,priority){
priority=!!priority;
var thisIter=op.iterator(this.ops);
var offset=0;
while(thisIter.hasNext()&&offset<=index){
var length=thisIter.peekLength();
var nextType=thisIter.peekType();
thisIter.next();
if(nextType==='delete'){
index-=Math.min(length,index-offset);
continue;
}else if(nextType==='insert'&&(offset<index||!priority)){
index+=length;
}
offset+=length;
}
return index;
};


module.exports=Delta;


/***/},
/* 3 */
/***/function(module,exports){

var hasOwn=Object.prototype.hasOwnProperty;
var toStr=Object.prototype.toString;
var defineProperty=Object.defineProperty;
var gOPD=Object.getOwnPropertyDescriptor;

var isArray=function isArray(arr){
if(typeof Array.isArray==='function'){
return Array.isArray(arr);
}

return toStr.call(arr)==='[object Array]';
};

var isPlainObject=function isPlainObject(obj){
if(!obj||toStr.call(obj)!=='[object Object]'){
return false;
}

var hasOwnConstructor=hasOwn.call(obj,'constructor');
var hasIsPrototypeOf=obj.constructor&&obj.constructor.prototype&&hasOwn.call(obj.constructor.prototype,'isPrototypeOf');
// Not own constructor property must be Object
if(obj.constructor&&!hasOwnConstructor&&!hasIsPrototypeOf){
return false;
}

// Own properties are enumerated firstly, so to speed up,
// if last one is own, then all properties are own.
var key;
for(key in obj){/**/}

return typeof key==='undefined'||hasOwn.call(obj,key);
};

// If name is '__proto__', and Object.defineProperty is available, define __proto__ as an own property on target
var setProperty=function setProperty(target,options){
if(defineProperty&&options.name==='__proto__'){
defineProperty(target,options.name,{
enumerable:true,
configurable:true,
value:options.newValue,
writable:true});

}else {
target[options.name]=options.newValue;
}
};

// Return undefined instead of __proto__ if '__proto__' is not an own property
var getProperty=function getProperty(obj,name){
if(name==='__proto__'){
if(!hasOwn.call(obj,name)){
return void 0;
}else if(gOPD){
// In early versions of node, obj['__proto__'] is buggy when obj has
// __proto__ as an own property. Object.getOwnPropertyDescriptor() works.
return gOPD(obj,name).value;
}
}

return obj[name];
};

module.exports=function extend(){
var options,name,src,copy,copyIsArray,clone;
var target=arguments[0];
var i=1;
var length=arguments.length;
var deep=false;

// Handle a deep copy situation
if(typeof target==='boolean'){
deep=target;
target=arguments[1]||{};
// skip the boolean and the target
i=2;
}
if(target==null||typeof target!=='object'&&typeof target!=='function'){
target={};
}

for(;i<length;++i){
options=arguments[i];
// Only deal with non-null/undefined values
if(options!=null){
// Extend the base object
for(name in options){
src=getProperty(target,name);
copy=getProperty(options,name);

// Prevent never-ending loop
if(target!==copy){
// Recurse if we're merging plain objects or arrays
if(deep&&copy&&(isPlainObject(copy)||(copyIsArray=isArray(copy)))){
if(copyIsArray){
copyIsArray=false;
clone=src&&isArray(src)?src:[];
}else {
clone=src&&isPlainObject(src)?src:{};
}

// Never move original objects, clone them
setProperty(target,{name:name,newValue:extend(deep,clone,copy)});

// Don't bring in undefined values
}else if(typeof copy!=='undefined'){
setProperty(target,{name:name,newValue:copy});
}
}
}
}
}

// Return the modified object
return target;
};


/***/},
/* 4 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.default=exports.BlockEmbed=exports.bubbleFormats=undefined;

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _extend=__webpack_require__(3);

var _extend2=_interopRequireDefault(_extend);

var _quillDelta=__webpack_require__(2);

var _quillDelta2=_interopRequireDefault(_quillDelta);

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

var _break=__webpack_require__(16);

var _break2=_interopRequireDefault(_break);

var _inline=__webpack_require__(6);

var _inline2=_interopRequireDefault(_inline);

var _text=__webpack_require__(7);

var _text2=_interopRequireDefault(_text);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var NEWLINE_LENGTH=1;

var BlockEmbed=function(_Parchment$Embed){
_inherits(BlockEmbed,_Parchment$Embed);

function BlockEmbed(){
_classCallCheck(this,BlockEmbed);

return _possibleConstructorReturn(this,(BlockEmbed.__proto__||Object.getPrototypeOf(BlockEmbed)).apply(this,arguments));
}

_createClass(BlockEmbed,[{
key:'attach',
value:function attach(){
_get(BlockEmbed.prototype.__proto__||Object.getPrototypeOf(BlockEmbed.prototype),'attach',this).call(this);
this.attributes=new _parchment2.default.Attributor.Store(this.domNode);
}},
{
key:'delta',
value:function delta(){
return new _quillDelta2.default().insert(this.value(),(0, _extend2.default)(this.formats(),this.attributes.values()));
}},
{
key:'format',
value:function format(name,value){
var attribute=_parchment2.default.query(name,_parchment2.default.Scope.BLOCK_ATTRIBUTE);
if(attribute!=null){
this.attributes.attribute(attribute,value);
}
}},
{
key:'formatAt',
value:function formatAt(index,length,name,value){
this.format(name,value);
}},
{
key:'insertAt',
value:function insertAt(index,value,def){
if(typeof value==='string'&&value.endsWith('\n')){
var block=_parchment2.default.create(Block.blotName);
this.parent.insertBefore(block,index===0?this:this.next);
block.insertAt(0,value.slice(0,-1));
}else {
_get(BlockEmbed.prototype.__proto__||Object.getPrototypeOf(BlockEmbed.prototype),'insertAt',this).call(this,index,value,def);
}
}}]);


return BlockEmbed;
}(_parchment2.default.Embed);

BlockEmbed.scope=_parchment2.default.Scope.BLOCK_BLOT;
// It is important for cursor behavior BlockEmbeds use tags that are block level elements


var Block=function(_Parchment$Block){
_inherits(Block,_Parchment$Block);

function Block(domNode){
_classCallCheck(this,Block);

var _this2=_possibleConstructorReturn(this,(Block.__proto__||Object.getPrototypeOf(Block)).call(this,domNode));

_this2.cache={};
return _this2;
}

_createClass(Block,[{
key:'delta',
value:function delta(){
if(this.cache.delta==null){
this.cache.delta=this.descendants(_parchment2.default.Leaf).reduce(function(delta,leaf){
if(leaf.length()===0){
return delta;
}else {
return delta.insert(leaf.value(),bubbleFormats(leaf));
}
},new _quillDelta2.default()).insert('\n',bubbleFormats(this));
}
return this.cache.delta;
}},
{
key:'deleteAt',
value:function deleteAt(index,length){
_get(Block.prototype.__proto__||Object.getPrototypeOf(Block.prototype),'deleteAt',this).call(this,index,length);
this.cache={};
}},
{
key:'formatAt',
value:function formatAt(index,length,name,value){
if(length<=0)return;
if(_parchment2.default.query(name,_parchment2.default.Scope.BLOCK)){
if(index+length===this.length()){
this.format(name,value);
}
}else {
_get(Block.prototype.__proto__||Object.getPrototypeOf(Block.prototype),'formatAt',this).call(this,index,Math.min(length,this.length()-index-1),name,value);
}
this.cache={};
}},
{
key:'insertAt',
value:function insertAt(index,value,def){
if(def!=null)return _get(Block.prototype.__proto__||Object.getPrototypeOf(Block.prototype),'insertAt',this).call(this,index,value,def);
if(value.length===0)return;
var lines=value.split('\n');
var text=lines.shift();
if(text.length>0){
if(index<this.length()-1||this.children.tail==null){
_get(Block.prototype.__proto__||Object.getPrototypeOf(Block.prototype),'insertAt',this).call(this,Math.min(index,this.length()-1),text);
}else {
this.children.tail.insertAt(this.children.tail.length(),text);
}
this.cache={};
}
var block=this;
lines.reduce(function(index,line){
block=block.split(index,true);
block.insertAt(0,line);
return line.length;
},index+text.length);
}},
{
key:'insertBefore',
value:function insertBefore(blot,ref){
var head=this.children.head;
_get(Block.prototype.__proto__||Object.getPrototypeOf(Block.prototype),'insertBefore',this).call(this,blot,ref);
if(head instanceof _break2.default){
head.remove();
}
this.cache={};
}},
{
key:'length',
value:function length(){
if(this.cache.length==null){
this.cache.length=_get(Block.prototype.__proto__||Object.getPrototypeOf(Block.prototype),'length',this).call(this)+NEWLINE_LENGTH;
}
return this.cache.length;
}},
{
key:'moveChildren',
value:function moveChildren(target,ref){
_get(Block.prototype.__proto__||Object.getPrototypeOf(Block.prototype),'moveChildren',this).call(this,target,ref);
this.cache={};
}},
{
key:'optimize',
value:function optimize(context){
_get(Block.prototype.__proto__||Object.getPrototypeOf(Block.prototype),'optimize',this).call(this,context);
this.cache={};
}},
{
key:'path',
value:function path(index){
return _get(Block.prototype.__proto__||Object.getPrototypeOf(Block.prototype),'path',this).call(this,index,true);
}},
{
key:'removeChild',
value:function removeChild(child){
_get(Block.prototype.__proto__||Object.getPrototypeOf(Block.prototype),'removeChild',this).call(this,child);
this.cache={};
}},
{
key:'split',
value:function split(index){
var force=arguments.length>1&&arguments[1]!==undefined?arguments[1]:false;

if(force&&(index===0||index>=this.length()-NEWLINE_LENGTH)){
var clone=this.clone();
if(index===0){
this.parent.insertBefore(clone,this);
return this;
}else {
this.parent.insertBefore(clone,this.next);
return clone;
}
}else {
var next=_get(Block.prototype.__proto__||Object.getPrototypeOf(Block.prototype),'split',this).call(this,index,force);
this.cache={};
return next;
}
}}]);


return Block;
}(_parchment2.default.Block);

Block.blotName='block';
Block.tagName='P';
Block.defaultChild='break';
Block.allowedChildren=[_inline2.default,_parchment2.default.Embed,_text2.default];

function bubbleFormats(blot){
var formats=arguments.length>1&&arguments[1]!==undefined?arguments[1]:{};

if(blot==null)return formats;
if(typeof blot.formats==='function'){
formats=(0, _extend2.default)(formats,blot.formats());
}
if(blot.parent==null||blot.parent.blotName=='scroll'||blot.parent.statics.scope!==blot.statics.scope){
return formats;
}
return bubbleFormats(blot.parent,formats);
}

exports.bubbleFormats=bubbleFormats;
exports.BlockEmbed=BlockEmbed;
exports.default=Block;

/***/},
/* 5 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.default=exports.overload=exports.expandConfig=undefined;

var _typeof=typeof Symbol==="function"&&typeof Symbol.iterator==="symbol"?function(obj){return typeof obj;}:function(obj){return obj&&typeof Symbol==="function"&&obj.constructor===Symbol&&obj!==Symbol.prototype?"symbol":typeof obj;};

var _slicedToArray=function(){function sliceIterator(arr,i){var _arr=[];var _n=true;var _d=false;var _e=undefined;try{for(var _i=arr[Symbol.iterator](),_s;!(_n=(_s=_i.next()).done);_n=true){_arr.push(_s.value);if(i&&_arr.length===i)break;}}catch(err){_d=true;_e=err;}finally{try{if(!_n&&_i["return"])_i["return"]();}finally{if(_d)throw _e;}}return _arr;}return function(arr,i){if(Array.isArray(arr)){return arr;}else if(Symbol.iterator in Object(arr)){return sliceIterator(arr,i);}else {throw new TypeError("Invalid attempt to destructure non-iterable instance");}};}();

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

__webpack_require__(50);

var _quillDelta=__webpack_require__(2);

var _quillDelta2=_interopRequireDefault(_quillDelta);

var _editor=__webpack_require__(14);

var _editor2=_interopRequireDefault(_editor);

var _emitter3=__webpack_require__(8);

var _emitter4=_interopRequireDefault(_emitter3);

var _module=__webpack_require__(9);

var _module2=_interopRequireDefault(_module);

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

var _selection=__webpack_require__(15);

var _selection2=_interopRequireDefault(_selection);

var _extend=__webpack_require__(3);

var _extend2=_interopRequireDefault(_extend);

var _logger=__webpack_require__(10);

var _logger2=_interopRequireDefault(_logger);

var _theme=__webpack_require__(34);

var _theme2=_interopRequireDefault(_theme);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _defineProperty(obj,key,value){if(key in obj){Object.defineProperty(obj,key,{value:value,enumerable:true,configurable:true,writable:true});}else {obj[key]=value;}return obj;}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

var debug=(0, _logger2.default)('quill');

var Quill=function(){
_createClass(Quill,null,[{
key:'debug',
value:function debug(limit){
if(limit===true){
limit='log';
}
_logger2.default.level(limit);
}},
{
key:'find',
value:function find(node){
return node.__quill||_parchment2.default.find(node);
}},
{
key:'import',
value:function _import(name){
if(this.imports[name]==null){
debug.error('Cannot import '+name+'. Are you sure it was registered?');
}
return this.imports[name];
}},
{
key:'register',
value:function register(path,target){
var _this=this;

var overwrite=arguments.length>2&&arguments[2]!==undefined?arguments[2]:false;

if(typeof path!=='string'){
var name=path.attrName||path.blotName;
if(typeof name==='string'){
// register(Blot | Attributor, overwrite)
this.register('formats/'+name,path,target);
}else {
Object.keys(path).forEach(function(key){
_this.register(key,path[key],target);
});
}
}else {
if(this.imports[path]!=null&&!overwrite){
debug.warn('Overwriting '+path+' with',target);
}
this.imports[path]=target;
if((path.startsWith('blots/')||path.startsWith('formats/'))&&target.blotName!=='abstract'){
_parchment2.default.register(target);
}else if(path.startsWith('modules')&&typeof target.register==='function'){
target.register();
}
}
}}]);


function Quill(container){
var _this2=this;

var options=arguments.length>1&&arguments[1]!==undefined?arguments[1]:{};

_classCallCheck(this,Quill);

this.options=expandConfig(container,options);
this.container=this.options.container;
if(this.container==null){
return debug.error('Invalid Quill container',container);
}
if(this.options.debug){
Quill.debug(this.options.debug);
}
var html=this.container.innerHTML.trim();
this.container.classList.add('ql-container');
this.container.innerHTML='';
this.container.__quill=this;
this.root=this.addContainer('ql-editor');
this.root.classList.add('ql-blank');
this.root.setAttribute('data-gramm',false);
this.scrollingContainer=this.options.scrollingContainer||this.root;
this.emitter=new _emitter4.default();
this.scroll=_parchment2.default.create(this.root,{
emitter:this.emitter,
whitelist:this.options.formats});

this.editor=new _editor2.default(this.scroll);
this.selection=new _selection2.default(this.scroll,this.emitter);
this.theme=new this.options.theme(this,this.options);
this.keyboard=this.theme.addModule('keyboard');
this.clipboard=this.theme.addModule('clipboard');
this.history=this.theme.addModule('history');
this.theme.init();
this.emitter.on(_emitter4.default.events.EDITOR_CHANGE,function(type){
if(type===_emitter4.default.events.TEXT_CHANGE){
_this2.root.classList.toggle('ql-blank',_this2.editor.isBlank());
}
});
this.emitter.on(_emitter4.default.events.SCROLL_UPDATE,function(source,mutations){
var range=_this2.selection.lastRange;
var index=range&&range.length===0?range.index:undefined;
modify.call(_this2,function(){
return _this2.editor.update(null,mutations,index);
},source);
});
var contents=this.clipboard.convert('<div class=\'ql-editor\' style="white-space: normal;">'+html+'<p><br></p></div>');
this.setContents(contents);
this.history.clear();
if(this.options.placeholder){
this.root.setAttribute('data-placeholder',this.options.placeholder);
}
if(this.options.readOnly){
this.disable();
}
}

_createClass(Quill,[{
key:'addContainer',
value:function addContainer(container){
var refNode=arguments.length>1&&arguments[1]!==undefined?arguments[1]:null;

if(typeof container==='string'){
var className=container;
container=document.createElement('div');
container.classList.add(className);
}
this.container.insertBefore(container,refNode);
return container;
}},
{
key:'blur',
value:function blur(){
this.selection.setRange(null);
}},
{
key:'deleteText',
value:function deleteText(index,length,source){
var _this3=this;

var _overload=overload(index,length,source);

var _overload2=_slicedToArray(_overload,4);

index=_overload2[0];
length=_overload2[1];
source=_overload2[3];

return modify.call(this,function(){
return _this3.editor.deleteText(index,length);
},source,index,-1*length);
}},
{
key:'disable',
value:function disable(){
this.enable(false);
}},
{
key:'enable',
value:function enable(){
var enabled=arguments.length>0&&arguments[0]!==undefined?arguments[0]:true;

this.scroll.enable(enabled);
this.container.classList.toggle('ql-disabled',!enabled);
}},
{
key:'focus',
value:function focus(){
var scrollTop=this.scrollingContainer.scrollTop;
this.selection.focus();
this.scrollingContainer.scrollTop=scrollTop;
this.scrollIntoView();
}},
{
key:'format',
value:function format(name,value){
var _this4=this;

var source=arguments.length>2&&arguments[2]!==undefined?arguments[2]:_emitter4.default.sources.API;

return modify.call(this,function(){
var range=_this4.getSelection(true);
var change=new _quillDelta2.default();
if(range==null){
return change;
}else if(_parchment2.default.query(name,_parchment2.default.Scope.BLOCK)){
change=_this4.editor.formatLine(range.index,range.length,_defineProperty({},name,value));
}else if(range.length===0){
_this4.selection.format(name,value);
return change;
}else {
change=_this4.editor.formatText(range.index,range.length,_defineProperty({},name,value));
}
_this4.setSelection(range,_emitter4.default.sources.SILENT);
return change;
},source);
}},
{
key:'formatLine',
value:function formatLine(index,length,name,value,source){
var _this5=this;

var formats=void 0;

var _overload3=overload(index,length,name,value,source);

var _overload4=_slicedToArray(_overload3,4);

index=_overload4[0];
length=_overload4[1];
formats=_overload4[2];
source=_overload4[3];

return modify.call(this,function(){
return _this5.editor.formatLine(index,length,formats);
},source,index,0);
}},
{
key:'formatText',
value:function formatText(index,length,name,value,source){
var _this6=this;

var formats=void 0;

var _overload5=overload(index,length,name,value,source);

var _overload6=_slicedToArray(_overload5,4);

index=_overload6[0];
length=_overload6[1];
formats=_overload6[2];
source=_overload6[3];

return modify.call(this,function(){
return _this6.editor.formatText(index,length,formats);
},source,index,0);
}},
{
key:'getBounds',
value:function getBounds(index){
var length=arguments.length>1&&arguments[1]!==undefined?arguments[1]:0;

var bounds=void 0;
if(typeof index==='number'){
bounds=this.selection.getBounds(index,length);
}else {
bounds=this.selection.getBounds(index.index,index.length);
}
var containerBounds=this.container.getBoundingClientRect();
return {
bottom:bounds.bottom-containerBounds.top,
height:bounds.height,
left:bounds.left-containerBounds.left,
right:bounds.right-containerBounds.left,
top:bounds.top-containerBounds.top,
width:bounds.width};

}},
{
key:'getContents',
value:function getContents(){
var index=arguments.length>0&&arguments[0]!==undefined?arguments[0]:0;
var length=arguments.length>1&&arguments[1]!==undefined?arguments[1]:this.getLength()-index;

var _overload7=overload(index,length);

var _overload8=_slicedToArray(_overload7,2);

index=_overload8[0];
length=_overload8[1];

return this.editor.getContents(index,length);
}},
{
key:'getFormat',
value:function getFormat(){
var index=arguments.length>0&&arguments[0]!==undefined?arguments[0]:this.getSelection(true);
var length=arguments.length>1&&arguments[1]!==undefined?arguments[1]:0;

if(typeof index==='number'){
return this.editor.getFormat(index,length);
}else {
return this.editor.getFormat(index.index,index.length);
}
}},
{
key:'getIndex',
value:function getIndex(blot){
return blot.offset(this.scroll);
}},
{
key:'getLength',
value:function getLength(){
return this.scroll.length();
}},
{
key:'getLeaf',
value:function getLeaf(index){
return this.scroll.leaf(index);
}},
{
key:'getLine',
value:function getLine(index){
return this.scroll.line(index);
}},
{
key:'getLines',
value:function getLines(){
var index=arguments.length>0&&arguments[0]!==undefined?arguments[0]:0;
var length=arguments.length>1&&arguments[1]!==undefined?arguments[1]:Number.MAX_VALUE;

if(typeof index!=='number'){
return this.scroll.lines(index.index,index.length);
}else {
return this.scroll.lines(index,length);
}
}},
{
key:'getModule',
value:function getModule(name){
return this.theme.modules[name];
}},
{
key:'getSelection',
value:function getSelection(){
var focus=arguments.length>0&&arguments[0]!==undefined?arguments[0]:false;

if(focus)this.focus();
this.update();// Make sure we access getRange with editor in consistent state
return this.selection.getRange()[0];
}},
{
key:'getText',
value:function getText(){
var index=arguments.length>0&&arguments[0]!==undefined?arguments[0]:0;
var length=arguments.length>1&&arguments[1]!==undefined?arguments[1]:this.getLength()-index;

var _overload9=overload(index,length);

var _overload10=_slicedToArray(_overload9,2);

index=_overload10[0];
length=_overload10[1];

return this.editor.getText(index,length);
}},
{
key:'hasFocus',
value:function hasFocus(){
return this.selection.hasFocus();
}},
{
key:'insertEmbed',
value:function insertEmbed(index,embed,value){
var _this7=this;

var source=arguments.length>3&&arguments[3]!==undefined?arguments[3]:Quill.sources.API;

return modify.call(this,function(){
return _this7.editor.insertEmbed(index,embed,value);
},source,index);
}},
{
key:'insertText',
value:function insertText(index,text,name,value,source){
var _this8=this;

var formats=void 0;

var _overload11=overload(index,0,name,value,source);

var _overload12=_slicedToArray(_overload11,4);

index=_overload12[0];
formats=_overload12[2];
source=_overload12[3];

return modify.call(this,function(){
return _this8.editor.insertText(index,text,formats);
},source,index,text.length);
}},
{
key:'isEnabled',
value:function isEnabled(){
return !this.container.classList.contains('ql-disabled');
}},
{
key:'off',
value:function off(){
return this.emitter.off.apply(this.emitter,arguments);
}},
{
key:'on',
value:function on(){
return this.emitter.on.apply(this.emitter,arguments);
}},
{
key:'once',
value:function once(){
return this.emitter.once.apply(this.emitter,arguments);
}},
{
key:'pasteHTML',
value:function pasteHTML(index,html,source){
this.clipboard.dangerouslyPasteHTML(index,html,source);
}},
{
key:'removeFormat',
value:function removeFormat(index,length,source){
var _this9=this;

var _overload13=overload(index,length,source);

var _overload14=_slicedToArray(_overload13,4);

index=_overload14[0];
length=_overload14[1];
source=_overload14[3];

return modify.call(this,function(){
return _this9.editor.removeFormat(index,length);
},source,index);
}},
{
key:'scrollIntoView',
value:function scrollIntoView(){
this.selection.scrollIntoView(this.scrollingContainer);
}},
{
key:'setContents',
value:function setContents(delta){
var _this10=this;

var source=arguments.length>1&&arguments[1]!==undefined?arguments[1]:_emitter4.default.sources.API;

return modify.call(this,function(){
delta=new _quillDelta2.default(delta);
var length=_this10.getLength();
var deleted=_this10.editor.deleteText(0,length);
var applied=_this10.editor.applyDelta(delta);
var lastOp=applied.ops[applied.ops.length-1];
if(lastOp!=null&&typeof lastOp.insert==='string'&&lastOp.insert[lastOp.insert.length-1]==='\n'){
_this10.editor.deleteText(_this10.getLength()-1,1);
applied.delete(1);
}
var ret=deleted.compose(applied);
return ret;
},source);
}},
{
key:'setSelection',
value:function setSelection(index,length,source){
if(index==null){
this.selection.setRange(null,length||Quill.sources.API);
}else {
var _overload15=overload(index,length,source);

var _overload16=_slicedToArray(_overload15,4);

index=_overload16[0];
length=_overload16[1];
source=_overload16[3];

this.selection.setRange(new _selection.Range(index,length),source);
if(source!==_emitter4.default.sources.SILENT){
this.selection.scrollIntoView(this.scrollingContainer);
}
}
}},
{
key:'setText',
value:function setText(text){
var source=arguments.length>1&&arguments[1]!==undefined?arguments[1]:_emitter4.default.sources.API;

var delta=new _quillDelta2.default().insert(text);
return this.setContents(delta,source);
}},
{
key:'update',
value:function update(){
var source=arguments.length>0&&arguments[0]!==undefined?arguments[0]:_emitter4.default.sources.USER;

var change=this.scroll.update(source);// Will update selection before selection.update() does if text changes
this.selection.update(source);
return change;
}},
{
key:'updateContents',
value:function updateContents(delta){
var _this11=this;

var source=arguments.length>1&&arguments[1]!==undefined?arguments[1]:_emitter4.default.sources.API;

return modify.call(this,function(){
delta=new _quillDelta2.default(delta);
return _this11.editor.applyDelta(delta,source);
},source,true);
}}]);


return Quill;
}();

Quill.DEFAULTS={
bounds:null,
formats:null,
modules:{},
placeholder:'',
readOnly:false,
scrollingContainer:null,
strict:true,
theme:'default'};

Quill.events=_emitter4.default.events;
Quill.sources=_emitter4.default.sources;
// eslint-disable-next-line no-undef
Quill.version="1.3.7";

Quill.imports={
'delta':_quillDelta2.default,
'parchment':_parchment2.default,
'core/module':_module2.default,
'core/theme':_theme2.default};


function expandConfig(container,userConfig){
userConfig=(0, _extend2.default)(true,{
container:container,
modules:{
clipboard:true,
keyboard:true,
history:true}},

userConfig);
if(!userConfig.theme||userConfig.theme===Quill.DEFAULTS.theme){
userConfig.theme=_theme2.default;
}else {
userConfig.theme=Quill.import('themes/'+userConfig.theme);
if(userConfig.theme==null){
throw new Error('Invalid theme '+userConfig.theme+'. Did you register it?');
}
}
var themeConfig=(0, _extend2.default)(true,{},userConfig.theme.DEFAULTS);
[themeConfig,userConfig].forEach(function(config){
config.modules=config.modules||{};
Object.keys(config.modules).forEach(function(module){
if(config.modules[module]===true){
config.modules[module]={};
}
});
});
var moduleNames=Object.keys(themeConfig.modules).concat(Object.keys(userConfig.modules));
var moduleConfig=moduleNames.reduce(function(config,name){
var moduleClass=Quill.import('modules/'+name);
if(moduleClass==null){
debug.error('Cannot load '+name+' module. Are you sure you registered it?');
}else {
config[name]=moduleClass.DEFAULTS||{};
}
return config;
},{});
// Special case toolbar shorthand
if(userConfig.modules!=null&&userConfig.modules.toolbar&&userConfig.modules.toolbar.constructor!==Object){
userConfig.modules.toolbar={
container:userConfig.modules.toolbar};

}
userConfig=(0, _extend2.default)(true,{},Quill.DEFAULTS,{modules:moduleConfig},themeConfig,userConfig);
['bounds','container','scrollingContainer'].forEach(function(key){
if(typeof userConfig[key]==='string'){
userConfig[key]=document.querySelector(userConfig[key]);
}
});
userConfig.modules=Object.keys(userConfig.modules).reduce(function(config,name){
if(userConfig.modules[name]){
config[name]=userConfig.modules[name];
}
return config;
},{});
return userConfig;
}

// Handle selection preservation and TEXT_CHANGE emission
// common to modification APIs
function modify(modifier,source,index,shift){
if(this.options.strict&&!this.isEnabled()&&source===_emitter4.default.sources.USER){
return new _quillDelta2.default();
}
var range=index==null?null:this.getSelection();
var oldDelta=this.editor.delta;
var change=modifier();
if(range!=null){
if(index===true)index=range.index;
if(shift==null){
range=shiftRange(range,change,source);
}else if(shift!==0){
range=shiftRange(range,index,shift,source);
}
this.setSelection(range,_emitter4.default.sources.SILENT);
}
if(change.length()>0){
var _emitter;

var args=[_emitter4.default.events.TEXT_CHANGE,change,oldDelta,source];
(_emitter=this.emitter).emit.apply(_emitter,[_emitter4.default.events.EDITOR_CHANGE].concat(args));
if(source!==_emitter4.default.sources.SILENT){
var _emitter2;

(_emitter2=this.emitter).emit.apply(_emitter2,args);
}
}
return change;
}

function overload(index,length,name,value,source){
var formats={};
if(typeof index.index==='number'&&typeof index.length==='number'){
// Allow for throwaway end (used by insertText/insertEmbed)
if(typeof length!=='number'){
source=value,value=name,name=length,length=index.length,index=index.index;
}else {
length=index.length,index=index.index;
}
}else if(typeof length!=='number'){
source=value,value=name,name=length,length=0;
}
// Handle format being object, two format name/value strings or excluded
if((typeof name==='undefined'?'undefined':_typeof(name))==='object'){
formats=name;
source=value;
}else if(typeof name==='string'){
if(value!=null){
formats[name]=value;
}else {
source=name;
}
}
// Handle optional source
source=source||_emitter4.default.sources.API;
return [index,length,formats,source];
}

function shiftRange(range,index,length,source){
if(range==null)return null;
var start=void 0,
end=void 0;
if(index instanceof _quillDelta2.default){
var _map=[range.index,range.index+range.length].map(function(pos){
return index.transformPosition(pos,source!==_emitter4.default.sources.USER);
});

var _map2=_slicedToArray(_map,2);

start=_map2[0];
end=_map2[1];
}else {
var _map3=[range.index,range.index+range.length].map(function(pos){
if(pos<index||pos===index&&source===_emitter4.default.sources.USER)return pos;
if(length>=0){
return pos+length;
}else {
return Math.max(index,pos+length);
}
});

var _map4=_slicedToArray(_map3,2);

start=_map4[0];
end=_map4[1];
}
return new _selection.Range(start,end-start);
}

exports.expandConfig=expandConfig;
exports.overload=overload;
exports.default=Quill;

/***/},
/* 6 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _text=__webpack_require__(7);

var _text2=_interopRequireDefault(_text);

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var Inline=function(_Parchment$Inline){
_inherits(Inline,_Parchment$Inline);

function Inline(){
_classCallCheck(this,Inline);

return _possibleConstructorReturn(this,(Inline.__proto__||Object.getPrototypeOf(Inline)).apply(this,arguments));
}

_createClass(Inline,[{
key:'formatAt',
value:function formatAt(index,length,name,value){
if(Inline.compare(this.statics.blotName,name)<0&&_parchment2.default.query(name,_parchment2.default.Scope.BLOT)){
var blot=this.isolate(index,length);
if(value){
blot.wrap(name,value);
}
}else {
_get(Inline.prototype.__proto__||Object.getPrototypeOf(Inline.prototype),'formatAt',this).call(this,index,length,name,value);
}
}},
{
key:'optimize',
value:function optimize(context){
_get(Inline.prototype.__proto__||Object.getPrototypeOf(Inline.prototype),'optimize',this).call(this,context);
if(this.parent instanceof Inline&&Inline.compare(this.statics.blotName,this.parent.statics.blotName)>0){
var parent=this.parent.isolate(this.offset(),this.length());
this.moveChildren(parent);
parent.wrap(this);
}
}}],
[{
key:'compare',
value:function compare(self,other){
var selfIndex=Inline.order.indexOf(self);
var otherIndex=Inline.order.indexOf(other);
if(selfIndex>=0||otherIndex>=0){
return selfIndex-otherIndex;
}else if(self===other){
return 0;
}else if(self<other){
return -1;
}else {
return 1;
}
}}]);


return Inline;
}(_parchment2.default.Inline);

Inline.allowedChildren=[Inline,_parchment2.default.Embed,_text2.default];
// Lower index means deeper in the DOM tree, since not found (-1) is for embeds
Inline.order=['cursor','inline',// Must be lower
'underline','strike','italic','bold','script','link','code'// Must be higher
];

exports.default=Inline;

/***/},
/* 7 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var TextBlot=function(_Parchment$Text){
_inherits(TextBlot,_Parchment$Text);

function TextBlot(){
_classCallCheck(this,TextBlot);

return _possibleConstructorReturn(this,(TextBlot.__proto__||Object.getPrototypeOf(TextBlot)).apply(this,arguments));
}

return TextBlot;
}(_parchment2.default.Text);

exports.default=TextBlot;

/***/},
/* 8 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _eventemitter=__webpack_require__(54);

var _eventemitter2=_interopRequireDefault(_eventemitter);

var _logger=__webpack_require__(10);

var _logger2=_interopRequireDefault(_logger);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var debug=(0, _logger2.default)('quill:events');

var EVENTS=['selectionchange','mousedown','mouseup','click'];

EVENTS.forEach(function(eventName){
document.addEventListener(eventName,function(){
for(var _len=arguments.length,args=Array(_len),_key=0;_key<_len;_key++){
args[_key]=arguments[_key];
}

[].slice.call(document.querySelectorAll('.ql-container')).forEach(function(node){
// TODO use WeakMap
if(node.__quill&&node.__quill.emitter){
var _node$__quill$emitter;

(_node$__quill$emitter=node.__quill.emitter).handleDOM.apply(_node$__quill$emitter,args);
}
});
});
});

var Emitter=function(_EventEmitter){
_inherits(Emitter,_EventEmitter);

function Emitter(){
_classCallCheck(this,Emitter);

var _this=_possibleConstructorReturn(this,(Emitter.__proto__||Object.getPrototypeOf(Emitter)).call(this));

_this.listeners={};
_this.on('error',debug.error);
return _this;
}

_createClass(Emitter,[{
key:'emit',
value:function emit(){
debug.log.apply(debug,arguments);
_get(Emitter.prototype.__proto__||Object.getPrototypeOf(Emitter.prototype),'emit',this).apply(this,arguments);
}},
{
key:'handleDOM',
value:function handleDOM(event){
for(var _len2=arguments.length,args=Array(_len2>1?_len2-1:0),_key2=1;_key2<_len2;_key2++){
args[_key2-1]=arguments[_key2];
}

(this.listeners[event.type]||[]).forEach(function(_ref){
var node=_ref.node,
handler=_ref.handler;

if(event.target===node||node.contains(event.target)){
handler.apply(undefined,[event].concat(args));
}
});
}},
{
key:'listenDOM',
value:function listenDOM(eventName,node,handler){
if(!this.listeners[eventName]){
this.listeners[eventName]=[];
}
this.listeners[eventName].push({node:node,handler:handler});
}}]);


return Emitter;
}(_eventemitter2.default);

Emitter.events={
EDITOR_CHANGE:'editor-change',
SCROLL_BEFORE_UPDATE:'scroll-before-update',
SCROLL_OPTIMIZE:'scroll-optimize',
SCROLL_UPDATE:'scroll-update',
SELECTION_CHANGE:'selection-change',
TEXT_CHANGE:'text-change'};

Emitter.sources={
API:'api',
SILENT:'silent',
USER:'user'};


exports.default=Emitter;

/***/},
/* 9 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

var Module=function Module(quill){
var options=arguments.length>1&&arguments[1]!==undefined?arguments[1]:{};

_classCallCheck(this,Module);

this.quill=quill;
this.options=options;
};

Module.DEFAULTS={};

exports.default=Module;

/***/},
/* 10 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

var levels=['error','warn','log','info'];
var level='warn';

function debug(method){
if(levels.indexOf(method)<=levels.indexOf(level)){
var _console;

for(var _len=arguments.length,args=Array(_len>1?_len-1:0),_key=1;_key<_len;_key++){
args[_key-1]=arguments[_key];
}

(_console=console)[method].apply(_console,args);// eslint-disable-line no-console
}
}

function namespace(ns){
return levels.reduce(function(logger,method){
logger[method]=debug.bind(console,method,ns);
return logger;
},{});
}

debug.level=namespace.level=function(newLevel){
level=newLevel;
};

exports.default=namespace;

/***/},
/* 11 */
/***/function(module,exports,__webpack_require__){

var pSlice=Array.prototype.slice;
var objectKeys=__webpack_require__(52);
var isArguments=__webpack_require__(53);

var deepEqual=module.exports=function(actual,expected,opts){
if(!opts)opts={};
// 7.1. All identical values are equivalent, as determined by ===.
if(actual===expected){
return true;

}else if(actual instanceof Date&&expected instanceof Date){
return actual.getTime()===expected.getTime();

// 7.3. Other pairs that do not both pass typeof value == 'object',
// equivalence is determined by ==.
}else if(!actual||!expected||typeof actual!='object'&&typeof expected!='object'){
return opts.strict?actual===expected:actual==expected;

// 7.4. For all other Object pairs, including Array objects, equivalence is
// determined by having the same number of owned properties (as verified
// with Object.prototype.hasOwnProperty.call), the same set of keys
// (although not necessarily the same order), equivalent values for every
// corresponding key, and an identical 'prototype' property. Note: this
// accounts for both named and indexed properties on Arrays.
}else {
return objEquiv(actual,expected,opts);
}
};

function isUndefinedOrNull(value){
return value===null||value===undefined;
}

function isBuffer(x){
if(!x||typeof x!=='object'||typeof x.length!=='number')return false;
if(typeof x.copy!=='function'||typeof x.slice!=='function'){
return false;
}
if(x.length>0&&typeof x[0]!=='number')return false;
return true;
}

function objEquiv(a,b,opts){
var i,key;
if(isUndefinedOrNull(a)||isUndefinedOrNull(b))
return false;
// an identical 'prototype' property.
if(a.prototype!==b.prototype)return false;
//~~~I've managed to break Object.keys through screwy arguments passing.
//   Converting to array solves the problem.
if(isArguments(a)){
if(!isArguments(b)){
return false;
}
a=pSlice.call(a);
b=pSlice.call(b);
return deepEqual(a,b,opts);
}
if(isBuffer(a)){
if(!isBuffer(b)){
return false;
}
if(a.length!==b.length)return false;
for(i=0;i<a.length;i++){
if(a[i]!==b[i])return false;
}
return true;
}
try{
var ka=objectKeys(a),
kb=objectKeys(b);
}catch(e){//happens when one is a string literal and the other isn't
return false;
}
// having the same number of owned properties (keys incorporates
// hasOwnProperty)
if(ka.length!=kb.length)
return false;
//the same set of keys (although not necessarily the same order),
ka.sort();
kb.sort();
//~~~cheap key test
for(i=ka.length-1;i>=0;i--){
if(ka[i]!=kb[i])
return false;
}
//equivalent values for every corresponding key, and
//~~~possibly expensive deep test
for(i=ka.length-1;i>=0;i--){
key=ka[i];
if(!deepEqual(a[key],b[key],opts))return false;
}
return typeof a===typeof b;
}


/***/},
/* 12 */
/***/function(module,exports,__webpack_require__){

Object.defineProperty(exports,"__esModule",{value:true});
var Registry=__webpack_require__(1);
var Attributor=/** @class */function(){
function Attributor(attrName,keyName,options){
if(options===void 0){options={};}
this.attrName=attrName;
this.keyName=keyName;
var attributeBit=Registry.Scope.TYPE&Registry.Scope.ATTRIBUTE;
if(options.scope!=null){
// Ignore type bits, force attribute bit
this.scope=options.scope&Registry.Scope.LEVEL|attributeBit;
}else
{
this.scope=Registry.Scope.ATTRIBUTE;
}
if(options.whitelist!=null)
this.whitelist=options.whitelist;
}
Attributor.keys=function(node){
return [].map.call(node.attributes,function(item){
return item.name;
});
};
Attributor.prototype.add=function(node,value){
if(!this.canAdd(node,value))
return false;
node.setAttribute(this.keyName,value);
return true;
};
Attributor.prototype.canAdd=function(node,value){
var match=Registry.query(node,Registry.Scope.BLOT&(this.scope|Registry.Scope.TYPE));
if(match==null)
return false;
if(this.whitelist==null)
return true;
if(typeof value==='string'){
return this.whitelist.indexOf(value.replace(/["']/g,''))>-1;
}else
{
return this.whitelist.indexOf(value)>-1;
}
};
Attributor.prototype.remove=function(node){
node.removeAttribute(this.keyName);
};
Attributor.prototype.value=function(node){
var value=node.getAttribute(this.keyName);
if(this.canAdd(node,value)&&value){
return value;
}
return '';
};
return Attributor;
}();
exports.default=Attributor;


/***/},
/* 13 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.default=exports.Code=undefined;

var _slicedToArray=function(){function sliceIterator(arr,i){var _arr=[];var _n=true;var _d=false;var _e=undefined;try{for(var _i=arr[Symbol.iterator](),_s;!(_n=(_s=_i.next()).done);_n=true){_arr.push(_s.value);if(i&&_arr.length===i)break;}}catch(err){_d=true;_e=err;}finally{try{if(!_n&&_i["return"])_i["return"]();}finally{if(_d)throw _e;}}return _arr;}return function(arr,i){if(Array.isArray(arr)){return arr;}else if(Symbol.iterator in Object(arr)){return sliceIterator(arr,i);}else {throw new TypeError("Invalid attempt to destructure non-iterable instance");}};}();

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _quillDelta=__webpack_require__(2);

var _quillDelta2=_interopRequireDefault(_quillDelta);

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

var _block=__webpack_require__(4);

var _block2=_interopRequireDefault(_block);

var _inline=__webpack_require__(6);

var _inline2=_interopRequireDefault(_inline);

var _text=__webpack_require__(7);

var _text2=_interopRequireDefault(_text);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var Code=function(_Inline){
_inherits(Code,_Inline);

function Code(){
_classCallCheck(this,Code);

return _possibleConstructorReturn(this,(Code.__proto__||Object.getPrototypeOf(Code)).apply(this,arguments));
}

return Code;
}(_inline2.default);

Code.blotName='code';
Code.tagName='CODE';

var CodeBlock=function(_Block){
_inherits(CodeBlock,_Block);

function CodeBlock(){
_classCallCheck(this,CodeBlock);

return _possibleConstructorReturn(this,(CodeBlock.__proto__||Object.getPrototypeOf(CodeBlock)).apply(this,arguments));
}

_createClass(CodeBlock,[{
key:'delta',
value:function delta(){
var _this3=this;

var text=this.domNode.textContent;
if(text.endsWith('\n')){
// Should always be true
text=text.slice(0,-1);
}
return text.split('\n').reduce(function(delta,frag){
return delta.insert(frag).insert('\n',_this3.formats());
},new _quillDelta2.default());
}},
{
key:'format',
value:function format(name,value){
if(name===this.statics.blotName&&value)return;

var _descendant=this.descendant(_text2.default,this.length()-1),
_descendant2=_slicedToArray(_descendant,1),
text=_descendant2[0];

if(text!=null){
text.deleteAt(text.length()-1,1);
}
_get(CodeBlock.prototype.__proto__||Object.getPrototypeOf(CodeBlock.prototype),'format',this).call(this,name,value);
}},
{
key:'formatAt',
value:function formatAt(index,length,name,value){
if(length===0)return;
if(_parchment2.default.query(name,_parchment2.default.Scope.BLOCK)==null||name===this.statics.blotName&&value===this.statics.formats(this.domNode)){
return;
}
var nextNewline=this.newlineIndex(index);
if(nextNewline<0||nextNewline>=index+length)return;
var prevNewline=this.newlineIndex(index,true)+1;
var isolateLength=nextNewline-prevNewline+1;
var blot=this.isolate(prevNewline,isolateLength);
var next=blot.next;
blot.format(name,value);
if(next instanceof CodeBlock){
next.formatAt(0,index-prevNewline+length-isolateLength,name,value);
}
}},
{
key:'insertAt',
value:function insertAt(index,value,def){
if(def!=null)return;

var _descendant3=this.descendant(_text2.default,index),
_descendant4=_slicedToArray(_descendant3,2),
text=_descendant4[0],
offset=_descendant4[1];

text.insertAt(offset,value);
}},
{
key:'length',
value:function length(){
var length=this.domNode.textContent.length;
if(!this.domNode.textContent.endsWith('\n')){
return length+1;
}
return length;
}},
{
key:'newlineIndex',
value:function newlineIndex(searchIndex){
var reverse=arguments.length>1&&arguments[1]!==undefined?arguments[1]:false;

if(!reverse){
var offset=this.domNode.textContent.slice(searchIndex).indexOf('\n');
return offset>-1?searchIndex+offset:-1;
}else {
return this.domNode.textContent.slice(0,searchIndex).lastIndexOf('\n');
}
}},
{
key:'optimize',
value:function optimize(context){
if(!this.domNode.textContent.endsWith('\n')){
this.appendChild(_parchment2.default.create('text','\n'));
}
_get(CodeBlock.prototype.__proto__||Object.getPrototypeOf(CodeBlock.prototype),'optimize',this).call(this,context);
var next=this.next;
if(next!=null&&next.prev===this&&next.statics.blotName===this.statics.blotName&&this.statics.formats(this.domNode)===next.statics.formats(next.domNode)){
next.optimize(context);
next.moveChildren(this);
next.remove();
}
}},
{
key:'replace',
value:function replace(target){
_get(CodeBlock.prototype.__proto__||Object.getPrototypeOf(CodeBlock.prototype),'replace',this).call(this,target);
[].slice.call(this.domNode.querySelectorAll('*')).forEach(function(node){
var blot=_parchment2.default.find(node);
if(blot==null){
node.parentNode.removeChild(node);
}else if(blot instanceof _parchment2.default.Embed){
blot.remove();
}else {
blot.unwrap();
}
});
}}],
[{
key:'create',
value:function create(value){
var domNode=_get(CodeBlock.__proto__||Object.getPrototypeOf(CodeBlock),'create',this).call(this,value);
domNode.setAttribute('spellcheck',false);
return domNode;
}},
{
key:'formats',
value:function formats(){
return true;
}}]);


return CodeBlock;
}(_block2.default);

CodeBlock.blotName='code-block';
CodeBlock.tagName='PRE';
CodeBlock.TAB='  ';

exports.Code=Code;
exports.default=CodeBlock;

/***/},
/* 14 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _typeof=typeof Symbol==="function"&&typeof Symbol.iterator==="symbol"?function(obj){return typeof obj;}:function(obj){return obj&&typeof Symbol==="function"&&obj.constructor===Symbol&&obj!==Symbol.prototype?"symbol":typeof obj;};

var _slicedToArray=function(){function sliceIterator(arr,i){var _arr=[];var _n=true;var _d=false;var _e=undefined;try{for(var _i=arr[Symbol.iterator](),_s;!(_n=(_s=_i.next()).done);_n=true){_arr.push(_s.value);if(i&&_arr.length===i)break;}}catch(err){_d=true;_e=err;}finally{try{if(!_n&&_i["return"])_i["return"]();}finally{if(_d)throw _e;}}return _arr;}return function(arr,i){if(Array.isArray(arr)){return arr;}else if(Symbol.iterator in Object(arr)){return sliceIterator(arr,i);}else {throw new TypeError("Invalid attempt to destructure non-iterable instance");}};}();

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _quillDelta=__webpack_require__(2);

var _quillDelta2=_interopRequireDefault(_quillDelta);

var _op=__webpack_require__(20);

var _op2=_interopRequireDefault(_op);

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

var _code=__webpack_require__(13);

var _code2=_interopRequireDefault(_code);

var _cursor=__webpack_require__(24);

var _cursor2=_interopRequireDefault(_cursor);

var _block=__webpack_require__(4);

var _block2=_interopRequireDefault(_block);

var _break=__webpack_require__(16);

var _break2=_interopRequireDefault(_break);

var _clone=__webpack_require__(21);

var _clone2=_interopRequireDefault(_clone);

var _deepEqual=__webpack_require__(11);

var _deepEqual2=_interopRequireDefault(_deepEqual);

var _extend=__webpack_require__(3);

var _extend2=_interopRequireDefault(_extend);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _defineProperty(obj,key,value){if(key in obj){Object.defineProperty(obj,key,{value:value,enumerable:true,configurable:true,writable:true});}else {obj[key]=value;}return obj;}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

var ASCII=/^[ -~]*$/;

var Editor=function(){
function Editor(scroll){
_classCallCheck(this,Editor);

this.scroll=scroll;
this.delta=this.getDelta();
}

_createClass(Editor,[{
key:'applyDelta',
value:function applyDelta(delta){
var _this=this;

var consumeNextNewline=false;
this.scroll.update();
var scrollLength=this.scroll.length();
this.scroll.batchStart();
delta=normalizeDelta(delta);
delta.reduce(function(index,op){
var length=op.retain||op.delete||op.insert.length||1;
var attributes=op.attributes||{};
if(op.insert!=null){
if(typeof op.insert==='string'){
var text=op.insert;
if(text.endsWith('\n')&&consumeNextNewline){
consumeNextNewline=false;
text=text.slice(0,-1);
}
if(index>=scrollLength&&!text.endsWith('\n')){
consumeNextNewline=true;
}
_this.scroll.insertAt(index,text);

var _scroll$line=_this.scroll.line(index),
_scroll$line2=_slicedToArray(_scroll$line,2),
line=_scroll$line2[0],
offset=_scroll$line2[1];

var formats=(0, _extend2.default)({},(0, _block.bubbleFormats)(line));
if(line instanceof _block2.default){
var _line$descendant=line.descendant(_parchment2.default.Leaf,offset),
_line$descendant2=_slicedToArray(_line$descendant,1),
leaf=_line$descendant2[0];

formats=(0, _extend2.default)(formats,(0, _block.bubbleFormats)(leaf));
}
attributes=_op2.default.attributes.diff(formats,attributes)||{};
}else if(_typeof(op.insert)==='object'){
var key=Object.keys(op.insert)[0];// There should only be one key
if(key==null)return index;
_this.scroll.insertAt(index,key,op.insert[key]);
}
scrollLength+=length;
}
Object.keys(attributes).forEach(function(name){
_this.scroll.formatAt(index,length,name,attributes[name]);
});
return index+length;
},0);
delta.reduce(function(index,op){
if(typeof op.delete==='number'){
_this.scroll.deleteAt(index,op.delete);
return index;
}
return index+(op.retain||op.insert.length||1);
},0);
this.scroll.batchEnd();
return this.update(delta);
}},
{
key:'deleteText',
value:function deleteText(index,length){
this.scroll.deleteAt(index,length);
return this.update(new _quillDelta2.default().retain(index).delete(length));
}},
{
key:'formatLine',
value:function formatLine(index,length){
var _this2=this;

var formats=arguments.length>2&&arguments[2]!==undefined?arguments[2]:{};

this.scroll.update();
Object.keys(formats).forEach(function(format){
if(_this2.scroll.whitelist!=null&&!_this2.scroll.whitelist[format])return;
var lines=_this2.scroll.lines(index,Math.max(length,1));
var lengthRemaining=length;
lines.forEach(function(line){
var lineLength=line.length();
if(!(line instanceof _code2.default)){
line.format(format,formats[format]);
}else {
var codeIndex=index-line.offset(_this2.scroll);
var codeLength=line.newlineIndex(codeIndex+lengthRemaining)-codeIndex+1;
line.formatAt(codeIndex,codeLength,format,formats[format]);
}
lengthRemaining-=lineLength;
});
});
this.scroll.optimize();
return this.update(new _quillDelta2.default().retain(index).retain(length,(0, _clone2.default)(formats)));
}},
{
key:'formatText',
value:function formatText(index,length){
var _this3=this;

var formats=arguments.length>2&&arguments[2]!==undefined?arguments[2]:{};

Object.keys(formats).forEach(function(format){
_this3.scroll.formatAt(index,length,format,formats[format]);
});
return this.update(new _quillDelta2.default().retain(index).retain(length,(0, _clone2.default)(formats)));
}},
{
key:'getContents',
value:function getContents(index,length){
return this.delta.slice(index,index+length);
}},
{
key:'getDelta',
value:function getDelta(){
return this.scroll.lines().reduce(function(delta,line){
return delta.concat(line.delta());
},new _quillDelta2.default());
}},
{
key:'getFormat',
value:function getFormat(index){
var length=arguments.length>1&&arguments[1]!==undefined?arguments[1]:0;

var lines=[],
leaves=[];
if(length===0){
this.scroll.path(index).forEach(function(path){
var _path=_slicedToArray(path,1),
blot=_path[0];

if(blot instanceof _block2.default){
lines.push(blot);
}else if(blot instanceof _parchment2.default.Leaf){
leaves.push(blot);
}
});
}else {
lines=this.scroll.lines(index,length);
leaves=this.scroll.descendants(_parchment2.default.Leaf,index,length);
}
var formatsArr=[lines,leaves].map(function(blots){
if(blots.length===0)return {};
var formats=(0, _block.bubbleFormats)(blots.shift());
while(Object.keys(formats).length>0){
var blot=blots.shift();
if(blot==null)return formats;
formats=combineFormats((0, _block.bubbleFormats)(blot),formats);
}
return formats;
});
return _extend2.default.apply(_extend2.default,formatsArr);
}},
{
key:'getText',
value:function getText(index,length){
return this.getContents(index,length).filter(function(op){
return typeof op.insert==='string';
}).map(function(op){
return op.insert;
}).join('');
}},
{
key:'insertEmbed',
value:function insertEmbed(index,embed,value){
this.scroll.insertAt(index,embed,value);
return this.update(new _quillDelta2.default().retain(index).insert(_defineProperty({},embed,value)));
}},
{
key:'insertText',
value:function insertText(index,text){
var _this4=this;

var formats=arguments.length>2&&arguments[2]!==undefined?arguments[2]:{};

text=text.replace(/\r\n/g,'\n').replace(/\r/g,'\n');
this.scroll.insertAt(index,text);
Object.keys(formats).forEach(function(format){
_this4.scroll.formatAt(index,text.length,format,formats[format]);
});
return this.update(new _quillDelta2.default().retain(index).insert(text,(0, _clone2.default)(formats)));
}},
{
key:'isBlank',
value:function isBlank(){
if(this.scroll.children.length==0)return true;
if(this.scroll.children.length>1)return false;
var block=this.scroll.children.head;
if(block.statics.blotName!==_block2.default.blotName)return false;
if(block.children.length>1)return false;
return block.children.head instanceof _break2.default;
}},
{
key:'removeFormat',
value:function removeFormat(index,length){
var text=this.getText(index,length);

var _scroll$line3=this.scroll.line(index+length),
_scroll$line4=_slicedToArray(_scroll$line3,2),
line=_scroll$line4[0],
offset=_scroll$line4[1];

var suffixLength=0,
suffix=new _quillDelta2.default();
if(line!=null){
if(!(line instanceof _code2.default)){
suffixLength=line.length()-offset;
}else {
suffixLength=line.newlineIndex(offset)-offset+1;
}
suffix=line.delta().slice(offset,offset+suffixLength-1).insert('\n');
}
var contents=this.getContents(index,length+suffixLength);
var diff=contents.diff(new _quillDelta2.default().insert(text).concat(suffix));
var delta=new _quillDelta2.default().retain(index).concat(diff);
return this.applyDelta(delta);
}},
{
key:'update',
value:function update(change){
var mutations=arguments.length>1&&arguments[1]!==undefined?arguments[1]:[];
var cursorIndex=arguments.length>2&&arguments[2]!==undefined?arguments[2]:undefined;

var oldDelta=this.delta;
if(mutations.length===1&&mutations[0].type==='characterData'&&mutations[0].target.data.match(ASCII)&&_parchment2.default.find(mutations[0].target)){
// Optimization for character changes
var textBlot=_parchment2.default.find(mutations[0].target);
var formats=(0, _block.bubbleFormats)(textBlot);
var index=textBlot.offset(this.scroll);
var oldValue=mutations[0].oldValue.replace(_cursor2.default.CONTENTS,'');
var oldText=new _quillDelta2.default().insert(oldValue);
var newText=new _quillDelta2.default().insert(textBlot.value());
var diffDelta=new _quillDelta2.default().retain(index).concat(oldText.diff(newText,cursorIndex));
change=diffDelta.reduce(function(delta,op){
if(op.insert){
return delta.insert(op.insert,formats);
}else {
return delta.push(op);
}
},new _quillDelta2.default());
this.delta=oldDelta.compose(change);
}else {
this.delta=this.getDelta();
if(!change||!(0, _deepEqual2.default)(oldDelta.compose(change),this.delta)){
change=oldDelta.diff(this.delta,cursorIndex);
}
}
return change;
}}]);


return Editor;
}();

function combineFormats(formats,combined){
return Object.keys(combined).reduce(function(merged,name){
if(formats[name]==null)return merged;
if(combined[name]===formats[name]){
merged[name]=combined[name];
}else if(Array.isArray(combined[name])){
if(combined[name].indexOf(formats[name])<0){
merged[name]=combined[name].concat([formats[name]]);
}
}else {
merged[name]=[combined[name],formats[name]];
}
return merged;
},{});
}

function normalizeDelta(delta){
return delta.reduce(function(delta,op){
if(op.insert===1){
var attributes=(0, _clone2.default)(op.attributes);
delete attributes['image'];
return delta.insert({image:op.attributes.image},attributes);
}
if(op.attributes!=null&&(op.attributes.list===true||op.attributes.bullet===true)){
op=(0, _clone2.default)(op);
if(op.attributes.list){
op.attributes.list='ordered';
}else {
op.attributes.list='bullet';
delete op.attributes.bullet;
}
}
if(typeof op.insert==='string'){
var text=op.insert.replace(/\r\n/g,'\n').replace(/\r/g,'\n');
return delta.insert(text,op.attributes);
}
return delta.push(op);
},new _quillDelta2.default());
}

exports.default=Editor;

/***/},
/* 15 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.default=exports.Range=undefined;

var _slicedToArray=function(){function sliceIterator(arr,i){var _arr=[];var _n=true;var _d=false;var _e=undefined;try{for(var _i=arr[Symbol.iterator](),_s;!(_n=(_s=_i.next()).done);_n=true){_arr.push(_s.value);if(i&&_arr.length===i)break;}}catch(err){_d=true;_e=err;}finally{try{if(!_n&&_i["return"])_i["return"]();}finally{if(_d)throw _e;}}return _arr;}return function(arr,i){if(Array.isArray(arr)){return arr;}else if(Symbol.iterator in Object(arr)){return sliceIterator(arr,i);}else {throw new TypeError("Invalid attempt to destructure non-iterable instance");}};}();

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

var _clone=__webpack_require__(21);

var _clone2=_interopRequireDefault(_clone);

var _deepEqual=__webpack_require__(11);

var _deepEqual2=_interopRequireDefault(_deepEqual);

var _emitter3=__webpack_require__(8);

var _emitter4=_interopRequireDefault(_emitter3);

var _logger=__webpack_require__(10);

var _logger2=_interopRequireDefault(_logger);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _toConsumableArray(arr){if(Array.isArray(arr)){for(var i=0,arr2=Array(arr.length);i<arr.length;i++){arr2[i]=arr[i];}return arr2;}else {return Array.from(arr);}}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

var debug=(0, _logger2.default)('quill:selection');

var Range=function Range(index){
var length=arguments.length>1&&arguments[1]!==undefined?arguments[1]:0;

_classCallCheck(this,Range);

this.index=index;
this.length=length;
};

var Selection=function(){
function Selection(scroll,emitter){
var _this=this;

_classCallCheck(this,Selection);

this.emitter=emitter;
this.scroll=scroll;
this.composing=false;
this.mouseDown=false;
this.root=this.scroll.domNode;
this.cursor=_parchment2.default.create('cursor',this);
// savedRange is last non-null range
this.lastRange=this.savedRange=new Range(0,0);
this.handleComposition();
this.handleDragging();
this.emitter.listenDOM('selectionchange',document,function(){
if(!_this.mouseDown){
setTimeout(_this.update.bind(_this,_emitter4.default.sources.USER),1);
}
});
this.emitter.on(_emitter4.default.events.EDITOR_CHANGE,function(type,delta){
if(type===_emitter4.default.events.TEXT_CHANGE&&delta.length()>0){
_this.update(_emitter4.default.sources.SILENT);
}
});
this.emitter.on(_emitter4.default.events.SCROLL_BEFORE_UPDATE,function(){
if(!_this.hasFocus())return;
var native=_this.getNativeRange();
if(native==null)return;
if(native.start.node===_this.cursor.textNode)return;// cursor.restore() will handle
// TODO unclear if this has negative side effects
_this.emitter.once(_emitter4.default.events.SCROLL_UPDATE,function(){
try{
_this.setNativeRange(native.start.node,native.start.offset,native.end.node,native.end.offset);
}catch(ignored){}
});
});
this.emitter.on(_emitter4.default.events.SCROLL_OPTIMIZE,function(mutations,context){
if(context.range){
var _context$range=context.range,
startNode=_context$range.startNode,
startOffset=_context$range.startOffset,
endNode=_context$range.endNode,
endOffset=_context$range.endOffset;

_this.setNativeRange(startNode,startOffset,endNode,endOffset);
}
});
this.update(_emitter4.default.sources.SILENT);
}

_createClass(Selection,[{
key:'handleComposition',
value:function handleComposition(){
var _this2=this;

this.root.addEventListener('compositionstart',function(){
_this2.composing=true;
});
this.root.addEventListener('compositionend',function(){
_this2.composing=false;
if(_this2.cursor.parent){
var range=_this2.cursor.restore();
if(!range)return;
setTimeout(function(){
_this2.setNativeRange(range.startNode,range.startOffset,range.endNode,range.endOffset);
},1);
}
});
}},
{
key:'handleDragging',
value:function handleDragging(){
var _this3=this;

this.emitter.listenDOM('mousedown',document.body,function(){
_this3.mouseDown=true;
});
this.emitter.listenDOM('mouseup',document.body,function(){
_this3.mouseDown=false;
_this3.update(_emitter4.default.sources.USER);
});
}},
{
key:'focus',
value:function focus(){
if(this.hasFocus())return;
this.root.focus();
this.setRange(this.savedRange);
}},
{
key:'format',
value:function format(_format,value){
if(this.scroll.whitelist!=null&&!this.scroll.whitelist[_format])return;
this.scroll.update();
var nativeRange=this.getNativeRange();
if(nativeRange==null||!nativeRange.native.collapsed||_parchment2.default.query(_format,_parchment2.default.Scope.BLOCK))return;
if(nativeRange.start.node!==this.cursor.textNode){
var blot=_parchment2.default.find(nativeRange.start.node,false);
if(blot==null)return;
// TODO Give blot ability to not split
if(blot instanceof _parchment2.default.Leaf){
var after=blot.split(nativeRange.start.offset);
blot.parent.insertBefore(this.cursor,after);
}else {
blot.insertBefore(this.cursor,nativeRange.start.node);// Should never happen
}
this.cursor.attach();
}
this.cursor.format(_format,value);
this.scroll.optimize();
this.setNativeRange(this.cursor.textNode,this.cursor.textNode.data.length);
this.update();
}},
{
key:'getBounds',
value:function getBounds(index){
var length=arguments.length>1&&arguments[1]!==undefined?arguments[1]:0;

var scrollLength=this.scroll.length();
index=Math.min(index,scrollLength-1);
length=Math.min(index+length,scrollLength-1)-index;
var node=void 0,
_scroll$leaf=this.scroll.leaf(index),
_scroll$leaf2=_slicedToArray(_scroll$leaf,2),
leaf=_scroll$leaf2[0],
offset=_scroll$leaf2[1];
if(leaf==null)return null;

var _leaf$position=leaf.position(offset,true);

var _leaf$position2=_slicedToArray(_leaf$position,2);

node=_leaf$position2[0];
offset=_leaf$position2[1];

var range=document.createRange();
if(length>0){
range.setStart(node,offset);

var _scroll$leaf3=this.scroll.leaf(index+length);

var _scroll$leaf4=_slicedToArray(_scroll$leaf3,2);

leaf=_scroll$leaf4[0];
offset=_scroll$leaf4[1];

if(leaf==null)return null;

var _leaf$position3=leaf.position(offset,true);

var _leaf$position4=_slicedToArray(_leaf$position3,2);

node=_leaf$position4[0];
offset=_leaf$position4[1];

range.setEnd(node,offset);
return range.getBoundingClientRect();
}else {
var side='left';
var rect=void 0;
if(node instanceof Text){
if(offset<node.data.length){
range.setStart(node,offset);
range.setEnd(node,offset+1);
}else {
range.setStart(node,offset-1);
range.setEnd(node,offset);
side='right';
}
rect=range.getBoundingClientRect();
}else {
rect=leaf.domNode.getBoundingClientRect();
if(offset>0)side='right';
}
return {
bottom:rect.top+rect.height,
height:rect.height,
left:rect[side],
right:rect[side],
top:rect.top,
width:0};

}
}},
{
key:'getNativeRange',
value:function getNativeRange(){
var selection=document.getSelection();
if(selection==null||selection.rangeCount<=0)return null;
var nativeRange=selection.getRangeAt(0);
if(nativeRange==null)return null;
var range=this.normalizeNative(nativeRange);
debug.info('getNativeRange',range);
return range;
}},
{
key:'getRange',
value:function getRange(){
var normalized=this.getNativeRange();
if(normalized==null)return [null,null];
var range=this.normalizedToRange(normalized);
return [range,normalized];
}},
{
key:'hasFocus',
value:function hasFocus(){
return document.activeElement===this.root;
}},
{
key:'normalizedToRange',
value:function normalizedToRange(range){
var _this4=this;

var positions=[[range.start.node,range.start.offset]];
if(!range.native.collapsed){
positions.push([range.end.node,range.end.offset]);
}
var indexes=positions.map(function(position){
var _position=_slicedToArray(position,2),
node=_position[0],
offset=_position[1];

var blot=_parchment2.default.find(node,true);
var index=blot.offset(_this4.scroll);
if(offset===0){
return index;
}else if(blot instanceof _parchment2.default.Container){
return index+blot.length();
}else {
return index+blot.index(node,offset);
}
});
var end=Math.min(Math.max.apply(Math,_toConsumableArray(indexes)),this.scroll.length()-1);
var start=Math.min.apply(Math,[end].concat(_toConsumableArray(indexes)));
return new Range(start,end-start);
}},
{
key:'normalizeNative',
value:function normalizeNative(nativeRange){
if(!contains(this.root,nativeRange.startContainer)||!nativeRange.collapsed&&!contains(this.root,nativeRange.endContainer)){
return null;
}
var range={
start:{node:nativeRange.startContainer,offset:nativeRange.startOffset},
end:{node:nativeRange.endContainer,offset:nativeRange.endOffset},
native:nativeRange};

[range.start,range.end].forEach(function(position){
var node=position.node,
offset=position.offset;
while(!(node instanceof Text)&&node.childNodes.length>0){
if(node.childNodes.length>offset){
node=node.childNodes[offset];
offset=0;
}else if(node.childNodes.length===offset){
node=node.lastChild;
offset=node instanceof Text?node.data.length:node.childNodes.length+1;
}else {
break;
}
}
position.node=node,position.offset=offset;
});
return range;
}},
{
key:'rangeToNative',
value:function rangeToNative(range){
var _this5=this;

var indexes=range.collapsed?[range.index]:[range.index,range.index+range.length];
var args=[];
var scrollLength=this.scroll.length();
indexes.forEach(function(index,i){
index=Math.min(scrollLength-1,index);
var node=void 0,
_scroll$leaf5=_this5.scroll.leaf(index),
_scroll$leaf6=_slicedToArray(_scroll$leaf5,2),
leaf=_scroll$leaf6[0],
offset=_scroll$leaf6[1];
var _leaf$position5=leaf.position(offset,i!==0);

var _leaf$position6=_slicedToArray(_leaf$position5,2);

node=_leaf$position6[0];
offset=_leaf$position6[1];

args.push(node,offset);
});
if(args.length<2){
args=args.concat(args);
}
return args;
}},
{
key:'scrollIntoView',
value:function scrollIntoView(scrollingContainer){
var range=this.lastRange;
if(range==null)return;
var bounds=this.getBounds(range.index,range.length);
if(bounds==null)return;
var limit=this.scroll.length()-1;

var _scroll$line=this.scroll.line(Math.min(range.index,limit)),
_scroll$line2=_slicedToArray(_scroll$line,1),
first=_scroll$line2[0];

var last=first;
if(range.length>0){
var _scroll$line3=this.scroll.line(Math.min(range.index+range.length,limit));

var _scroll$line4=_slicedToArray(_scroll$line3,1);

last=_scroll$line4[0];
}
if(first==null||last==null)return;
var scrollBounds=scrollingContainer.getBoundingClientRect();
if(bounds.top<scrollBounds.top){
scrollingContainer.scrollTop-=scrollBounds.top-bounds.top;
}else if(bounds.bottom>scrollBounds.bottom){
scrollingContainer.scrollTop+=bounds.bottom-scrollBounds.bottom;
}
}},
{
key:'setNativeRange',
value:function setNativeRange(startNode,startOffset){
var endNode=arguments.length>2&&arguments[2]!==undefined?arguments[2]:startNode;
var endOffset=arguments.length>3&&arguments[3]!==undefined?arguments[3]:startOffset;
var force=arguments.length>4&&arguments[4]!==undefined?arguments[4]:false;

debug.info('setNativeRange',startNode,startOffset,endNode,endOffset);
if(startNode!=null&&(this.root.parentNode==null||startNode.parentNode==null||endNode.parentNode==null)){
return;
}
var selection=document.getSelection();
if(selection==null)return;
if(startNode!=null){
if(!this.hasFocus())this.root.focus();
var native=(this.getNativeRange()||{}).native;
if(native==null||force||startNode!==native.startContainer||startOffset!==native.startOffset||endNode!==native.endContainer||endOffset!==native.endOffset){

if(startNode.tagName=="BR"){
startOffset=[].indexOf.call(startNode.parentNode.childNodes,startNode);
startNode=startNode.parentNode;
}
if(endNode.tagName=="BR"){
endOffset=[].indexOf.call(endNode.parentNode.childNodes,endNode);
endNode=endNode.parentNode;
}
var range=document.createRange();
range.setStart(startNode,startOffset);
range.setEnd(endNode,endOffset);
selection.removeAllRanges();
selection.addRange(range);
}
}else {
selection.removeAllRanges();
this.root.blur();
document.body.focus();// root.blur() not enough on IE11+Travis+SauceLabs (but not local VMs)
}
}},
{
key:'setRange',
value:function setRange(range){
var force=arguments.length>1&&arguments[1]!==undefined?arguments[1]:false;
var source=arguments.length>2&&arguments[2]!==undefined?arguments[2]:_emitter4.default.sources.API;

if(typeof force==='string'){
source=force;
force=false;
}
debug.info('setRange',range);
if(range!=null){
var args=this.rangeToNative(range);
this.setNativeRange.apply(this,_toConsumableArray(args).concat([force]));
}else {
this.setNativeRange(null);
}
this.update(source);
}},
{
key:'update',
value:function update(){
var source=arguments.length>0&&arguments[0]!==undefined?arguments[0]:_emitter4.default.sources.USER;

var oldRange=this.lastRange;

var _getRange=this.getRange(),
_getRange2=_slicedToArray(_getRange,2),
lastRange=_getRange2[0],
nativeRange=_getRange2[1];

this.lastRange=lastRange;
if(this.lastRange!=null){
this.savedRange=this.lastRange;
}
if(!(0, _deepEqual2.default)(oldRange,this.lastRange)){
var _emitter;

if(!this.composing&&nativeRange!=null&&nativeRange.native.collapsed&&nativeRange.start.node!==this.cursor.textNode){
this.cursor.restore();
}
var args=[_emitter4.default.events.SELECTION_CHANGE,(0, _clone2.default)(this.lastRange),(0, _clone2.default)(oldRange),source];
(_emitter=this.emitter).emit.apply(_emitter,[_emitter4.default.events.EDITOR_CHANGE].concat(args));
if(source!==_emitter4.default.sources.SILENT){
var _emitter2;

(_emitter2=this.emitter).emit.apply(_emitter2,args);
}
}
}}]);


return Selection;
}();

function contains(parent,descendant){
try{
// Firefox inserts inaccessible nodes around video elements
descendant.parentNode;
}catch(e){
return false;
}
// IE11 has bug with Text nodes
// https://connect.microsoft.com/IE/feedback/details/780874/node-contains-is-incorrect
if(descendant instanceof Text){
descendant=descendant.parentNode;
}
return parent.contains(descendant);
}

exports.Range=Range;
exports.default=Selection;

/***/},
/* 16 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var Break=function(_Parchment$Embed){
_inherits(Break,_Parchment$Embed);

function Break(){
_classCallCheck(this,Break);

return _possibleConstructorReturn(this,(Break.__proto__||Object.getPrototypeOf(Break)).apply(this,arguments));
}

_createClass(Break,[{
key:'insertInto',
value:function insertInto(parent,ref){
if(parent.children.length===0){
_get(Break.prototype.__proto__||Object.getPrototypeOf(Break.prototype),'insertInto',this).call(this,parent,ref);
}else {
this.remove();
}
}},
{
key:'length',
value:function length(){
return 0;
}},
{
key:'value',
value:function value(){
return '';
}}],
[{
key:'value',
value:function value(){
return undefined;
}}]);


return Break;
}(_parchment2.default.Embed);

Break.blotName='break';
Break.tagName='BR';

exports.default=Break;

/***/},
/* 17 */
/***/function(module,exports,__webpack_require__){

var __extends=this&&this.__extends||function(){
var extendStatics=Object.setPrototypeOf||
{__proto__:[]}instanceof Array&&function(d,b){d.__proto__=b;}||
function(d,b){for(var p in b){if(b.hasOwnProperty(p))d[p]=b[p];}};
return function(d,b){
extendStatics(d,b);
function __(){this.constructor=d;}
d.prototype=b===null?Object.create(b):(__.prototype=b.prototype,new __());
};
}();
Object.defineProperty(exports,"__esModule",{value:true});
var linked_list_1=__webpack_require__(44);
var shadow_1=__webpack_require__(30);
var Registry=__webpack_require__(1);
var ContainerBlot=/** @class */function(_super){
__extends(ContainerBlot,_super);
function ContainerBlot(domNode){
var _this=_super.call(this,domNode)||this;
_this.build();
return _this;
}
ContainerBlot.prototype.appendChild=function(other){
this.insertBefore(other);
};
ContainerBlot.prototype.attach=function(){
_super.prototype.attach.call(this);
this.children.forEach(function(child){
child.attach();
});
};
ContainerBlot.prototype.build=function(){
var _this=this;
this.children=new linked_list_1.default();
// Need to be reversed for if DOM nodes already in order
[].slice.
call(this.domNode.childNodes).
reverse().
forEach(function(node){
try{
var child=makeBlot(node);
_this.insertBefore(child,_this.children.head||undefined);
}
catch(err){
if(err instanceof Registry.ParchmentError)
return;else

throw err;
}
});
};
ContainerBlot.prototype.deleteAt=function(index,length){
if(index===0&&length===this.length()){
return this.remove();
}
this.children.forEachAt(index,length,function(child,offset,length){
child.deleteAt(offset,length);
});
};
ContainerBlot.prototype.descendant=function(criteria,index){
var _a=this.children.find(index),child=_a[0],offset=_a[1];
if(criteria.blotName==null&&criteria(child)||
criteria.blotName!=null&&child instanceof criteria){
return [child,offset];
}else
if(child instanceof ContainerBlot){
return child.descendant(criteria,offset);
}else
{
return [null,-1];
}
};
ContainerBlot.prototype.descendants=function(criteria,index,length){
if(index===void 0){index=0;}
if(length===void 0){length=Number.MAX_VALUE;}
var descendants=[];
var lengthLeft=length;
this.children.forEachAt(index,length,function(child,index,length){
if(criteria.blotName==null&&criteria(child)||
criteria.blotName!=null&&child instanceof criteria){
descendants.push(child);
}
if(child instanceof ContainerBlot){
descendants=descendants.concat(child.descendants(criteria,index,lengthLeft));
}
lengthLeft-=length;
});
return descendants;
};
ContainerBlot.prototype.detach=function(){
this.children.forEach(function(child){
child.detach();
});
_super.prototype.detach.call(this);
};
ContainerBlot.prototype.formatAt=function(index,length,name,value){
this.children.forEachAt(index,length,function(child,offset,length){
child.formatAt(offset,length,name,value);
});
};
ContainerBlot.prototype.insertAt=function(index,value,def){
var _a=this.children.find(index),child=_a[0],offset=_a[1];
if(child){
child.insertAt(offset,value,def);
}else
{
var blot=def==null?Registry.create('text',value):Registry.create(value,def);
this.appendChild(blot);
}
};
ContainerBlot.prototype.insertBefore=function(childBlot,refBlot){
if(this.statics.allowedChildren!=null&&
!this.statics.allowedChildren.some(function(child){
return childBlot instanceof child;
})){
throw new Registry.ParchmentError("Cannot insert "+childBlot.statics.blotName+" into "+this.statics.blotName);
}
childBlot.insertInto(this,refBlot);
};
ContainerBlot.prototype.length=function(){
return this.children.reduce(function(memo,child){
return memo+child.length();
},0);
};
ContainerBlot.prototype.moveChildren=function(targetParent,refNode){
this.children.forEach(function(child){
targetParent.insertBefore(child,refNode);
});
};
ContainerBlot.prototype.optimize=function(context){
_super.prototype.optimize.call(this,context);
if(this.children.length===0){
if(this.statics.defaultChild!=null){
var child=Registry.create(this.statics.defaultChild);
this.appendChild(child);
child.optimize(context);
}else
{
this.remove();
}
}
};
ContainerBlot.prototype.path=function(index,inclusive){
if(inclusive===void 0){inclusive=false;}
var _a=this.children.find(index,inclusive),child=_a[0],offset=_a[1];
var position=[[this,index]];
if(child instanceof ContainerBlot){
return position.concat(child.path(offset,inclusive));
}else
if(child!=null){
position.push([child,offset]);
}
return position;
};
ContainerBlot.prototype.removeChild=function(child){
this.children.remove(child);
};
ContainerBlot.prototype.replace=function(target){
if(target instanceof ContainerBlot){
target.moveChildren(this);
}
_super.prototype.replace.call(this,target);
};
ContainerBlot.prototype.split=function(index,force){
if(force===void 0){force=false;}
if(!force){
if(index===0)
return this;
if(index===this.length())
return this.next;
}
var after=this.clone();
this.parent.insertBefore(after,this.next);
this.children.forEachAt(index,this.length(),function(child,offset,length){
child=child.split(offset,force);
after.appendChild(child);
});
return after;
};
ContainerBlot.prototype.unwrap=function(){
this.moveChildren(this.parent,this.next);
this.remove();
};
ContainerBlot.prototype.update=function(mutations,context){
var _this=this;
var addedNodes=[];
var removedNodes=[];
mutations.forEach(function(mutation){
if(mutation.target===_this.domNode&&mutation.type==='childList'){
addedNodes.push.apply(addedNodes,mutation.addedNodes);
removedNodes.push.apply(removedNodes,mutation.removedNodes);
}
});
removedNodes.forEach(function(node){
// Check node has actually been removed
// One exception is Chrome does not immediately remove IFRAMEs
// from DOM but MutationRecord is correct in its reported removal
if(node.parentNode!=null&&
// @ts-ignore
node.tagName!=='IFRAME'&&
document.body.compareDocumentPosition(node)&Node.DOCUMENT_POSITION_CONTAINED_BY){
return;
}
var blot=Registry.find(node);
if(blot==null)
return;
if(blot.domNode.parentNode==null||blot.domNode.parentNode===_this.domNode){
blot.detach();
}
});
addedNodes.
filter(function(node){
return node.parentNode==_this.domNode;
}).
sort(function(a,b){
if(a===b)
return 0;
if(a.compareDocumentPosition(b)&Node.DOCUMENT_POSITION_FOLLOWING){
return 1;
}
return -1;
}).
forEach(function(node){
var refBlot=null;
if(node.nextSibling!=null){
refBlot=Registry.find(node.nextSibling);
}
var blot=makeBlot(node);
if(blot.next!=refBlot||blot.next==null){
if(blot.parent!=null){
blot.parent.removeChild(_this);
}
_this.insertBefore(blot,refBlot||undefined);
}
});
};
return ContainerBlot;
}(shadow_1.default);
function makeBlot(node){
var blot=Registry.find(node);
if(blot==null){
try{
blot=Registry.create(node);
}
catch(e){
blot=Registry.create(Registry.Scope.INLINE);
[].slice.call(node.childNodes).forEach(function(child){
// @ts-ignore
blot.domNode.appendChild(child);
});
if(node.parentNode){
node.parentNode.replaceChild(blot.domNode,node);
}
blot.attach();
}
}
return blot;
}
exports.default=ContainerBlot;


/***/},
/* 18 */
/***/function(module,exports,__webpack_require__){

var __extends=this&&this.__extends||function(){
var extendStatics=Object.setPrototypeOf||
{__proto__:[]}instanceof Array&&function(d,b){d.__proto__=b;}||
function(d,b){for(var p in b){if(b.hasOwnProperty(p))d[p]=b[p];}};
return function(d,b){
extendStatics(d,b);
function __(){this.constructor=d;}
d.prototype=b===null?Object.create(b):(__.prototype=b.prototype,new __());
};
}();
Object.defineProperty(exports,"__esModule",{value:true});
var attributor_1=__webpack_require__(12);
var store_1=__webpack_require__(31);
var container_1=__webpack_require__(17);
var Registry=__webpack_require__(1);
var FormatBlot=/** @class */function(_super){
__extends(FormatBlot,_super);
function FormatBlot(domNode){
var _this=_super.call(this,domNode)||this;
_this.attributes=new store_1.default(_this.domNode);
return _this;
}
FormatBlot.formats=function(domNode){
if(typeof this.tagName==='string'){
return true;
}else
if(Array.isArray(this.tagName)){
return domNode.tagName.toLowerCase();
}
return undefined;
};
FormatBlot.prototype.format=function(name,value){
var format=Registry.query(name);
if(format instanceof attributor_1.default){
this.attributes.attribute(format,value);
}else
if(value){
if(format!=null&&(name!==this.statics.blotName||this.formats()[name]!==value)){
this.replaceWith(name,value);
}
}
};
FormatBlot.prototype.formats=function(){
var formats=this.attributes.values();
var format=this.statics.formats(this.domNode);
if(format!=null){
formats[this.statics.blotName]=format;
}
return formats;
};
FormatBlot.prototype.replaceWith=function(name,value){
var replacement=_super.prototype.replaceWith.call(this,name,value);
this.attributes.copy(replacement);
return replacement;
};
FormatBlot.prototype.update=function(mutations,context){
var _this=this;
_super.prototype.update.call(this,mutations,context);
if(mutations.some(function(mutation){
return mutation.target===_this.domNode&&mutation.type==='attributes';
})){
this.attributes.build();
}
};
FormatBlot.prototype.wrap=function(name,value){
var wrapper=_super.prototype.wrap.call(this,name,value);
if(wrapper instanceof FormatBlot&&wrapper.statics.scope===this.statics.scope){
this.attributes.move(wrapper);
}
return wrapper;
};
return FormatBlot;
}(container_1.default);
exports.default=FormatBlot;


/***/},
/* 19 */
/***/function(module,exports,__webpack_require__){

var __extends=this&&this.__extends||function(){
var extendStatics=Object.setPrototypeOf||
{__proto__:[]}instanceof Array&&function(d,b){d.__proto__=b;}||
function(d,b){for(var p in b){if(b.hasOwnProperty(p))d[p]=b[p];}};
return function(d,b){
extendStatics(d,b);
function __(){this.constructor=d;}
d.prototype=b===null?Object.create(b):(__.prototype=b.prototype,new __());
};
}();
Object.defineProperty(exports,"__esModule",{value:true});
var shadow_1=__webpack_require__(30);
var Registry=__webpack_require__(1);
var LeafBlot=/** @class */function(_super){
__extends(LeafBlot,_super);
function LeafBlot(){
return _super!==null&&_super.apply(this,arguments)||this;
}
LeafBlot.value=function(domNode){
return true;
};
LeafBlot.prototype.index=function(node,offset){
if(this.domNode===node||
this.domNode.compareDocumentPosition(node)&Node.DOCUMENT_POSITION_CONTAINED_BY){
return Math.min(offset,1);
}
return -1;
};
LeafBlot.prototype.position=function(index,inclusive){
var offset=[].indexOf.call(this.parent.domNode.childNodes,this.domNode);
if(index>0)
offset+=1;
return [this.parent.domNode,offset];
};
LeafBlot.prototype.value=function(){
var _a;
return _a={},_a[this.statics.blotName]=this.statics.value(this.domNode)||true,_a;
};
LeafBlot.scope=Registry.Scope.INLINE_BLOT;
return LeafBlot;
}(shadow_1.default);
exports.default=LeafBlot;


/***/},
/* 20 */
/***/function(module,exports,__webpack_require__){

var equal=__webpack_require__(11);
var extend=__webpack_require__(3);


var lib={
attributes:{
compose:function compose(a,b,keepNull){
if(typeof a!=='object')a={};
if(typeof b!=='object')b={};
var attributes=extend(true,{},b);
if(!keepNull){
attributes=Object.keys(attributes).reduce(function(copy,key){
if(attributes[key]!=null){
copy[key]=attributes[key];
}
return copy;
},{});
}
for(var key in a){
if(a[key]!==undefined&&b[key]===undefined){
attributes[key]=a[key];
}
}
return Object.keys(attributes).length>0?attributes:undefined;
},

diff:function diff(a,b){
if(typeof a!=='object')a={};
if(typeof b!=='object')b={};
var attributes=Object.keys(a).concat(Object.keys(b)).reduce(function(attributes,key){
if(!equal(a[key],b[key])){
attributes[key]=b[key]===undefined?null:b[key];
}
return attributes;
},{});
return Object.keys(attributes).length>0?attributes:undefined;
},

transform:function transform(a,b,priority){
if(typeof a!=='object')return b;
if(typeof b!=='object')return undefined;
if(!priority)return b;// b simply overwrites us without priority
var attributes=Object.keys(b).reduce(function(attributes,key){
if(a[key]===undefined)attributes[key]=b[key];// null is a valid value
return attributes;
},{});
return Object.keys(attributes).length>0?attributes:undefined;
}},


iterator:function iterator(ops){
return new Iterator(ops);
},

length:function length(op){
if(typeof op['delete']==='number'){
return op['delete'];
}else if(typeof op.retain==='number'){
return op.retain;
}else {
return typeof op.insert==='string'?op.insert.length:1;
}
}};



function Iterator(ops){
this.ops=ops;
this.index=0;
this.offset=0;
}
Iterator.prototype.hasNext=function(){
return this.peekLength()<Infinity;
};

Iterator.prototype.next=function(length){
if(!length)length=Infinity;
var nextOp=this.ops[this.index];
if(nextOp){
var offset=this.offset;
var opLength=lib.length(nextOp);
if(length>=opLength-offset){
length=opLength-offset;
this.index+=1;
this.offset=0;
}else {
this.offset+=length;
}
if(typeof nextOp['delete']==='number'){
return {'delete':length};
}else {
var retOp={};
if(nextOp.attributes){
retOp.attributes=nextOp.attributes;
}
if(typeof nextOp.retain==='number'){
retOp.retain=length;
}else if(typeof nextOp.insert==='string'){
retOp.insert=nextOp.insert.substr(offset,length);
}else {
// offset should === 0, length should === 1
retOp.insert=nextOp.insert;
}
return retOp;
}
}else {
return {retain:Infinity};
}
};

Iterator.prototype.peek=function(){
return this.ops[this.index];
};

Iterator.prototype.peekLength=function(){
if(this.ops[this.index]){
// Should never return 0 if our index is being managed correctly
return lib.length(this.ops[this.index])-this.offset;
}else {
return Infinity;
}
};

Iterator.prototype.peekType=function(){
if(this.ops[this.index]){
if(typeof this.ops[this.index]['delete']==='number'){
return 'delete';
}else if(typeof this.ops[this.index].retain==='number'){
return 'retain';
}else {
return 'insert';
}
}
return 'retain';
};

Iterator.prototype.rest=function(){
if(!this.hasNext()){
return [];
}else if(this.offset===0){
return this.ops.slice(this.index);
}else {
var offset=this.offset;
var index=this.index;
var next=this.next();
var rest=this.ops.slice(this.index);
this.offset=offset;
this.index=index;
return [next].concat(rest);
}
};


module.exports=lib;


/***/},
/* 21 */
/***/function(module,exports){

var clone=function(){

function _instanceof(obj,type){
return type!=null&&obj instanceof type;
}

var nativeMap;
try{
nativeMap=Map;
}catch(_){
// maybe a reference error because no `Map`. Give it a dummy value that no
// value will ever be an instanceof.
nativeMap=function nativeMap(){};
}

var nativeSet;
try{
nativeSet=Set;
}catch(_){
nativeSet=function nativeSet(){};
}

var nativePromise;
try{
nativePromise=Promise;
}catch(_){
nativePromise=function nativePromise(){};
}

/**
 * Clones (copies) an Object using deep copying.
 *
 * This function supports circular references by default, but if you are certain
 * there are no circular references in your object, you can save some CPU time
 * by calling clone(obj, false).
 *
 * Caution: if `circular` is false and `parent` contains circular references,
 * your program may enter an infinite loop and crash.
 *
 * @param `parent` - the object to be cloned
 * @param `circular` - set to true if the object to be cloned may contain
 *    circular references. (optional - true by default)
 * @param `depth` - set to a number if the object is only to be cloned to
 *    a particular depth. (optional - defaults to Infinity)
 * @param `prototype` - sets the prototype to be used when cloning an object.
 *    (optional - defaults to parent prototype).
 * @param `includeNonEnumerable` - set to true if the non-enumerable properties
 *    should be cloned as well. Non-enumerable properties on the prototype
 *    chain will be ignored. (optional - false by default)
*/
function clone(parent,circular,depth,prototype,includeNonEnumerable){
if(typeof circular==='object'){
depth=circular.depth;
prototype=circular.prototype;
includeNonEnumerable=circular.includeNonEnumerable;
circular=circular.circular;
}
// maintain two arrays for circular references, where corresponding parents
// and children have the same index
var allParents=[];
var allChildren=[];

var useBuffer=typeof Buffer!='undefined';

if(typeof circular=='undefined')
circular=true;

if(typeof depth=='undefined')
depth=Infinity;

// recurse this function so we don't reset allParents and allChildren
function _clone(parent,depth){
// cloning null always returns null
if(parent===null)
return null;

if(depth===0)
return parent;

var child;
var proto;
if(typeof parent!='object'){
return parent;
}

if(_instanceof(parent,nativeMap)){
child=new nativeMap();
}else if(_instanceof(parent,nativeSet)){
child=new nativeSet();
}else if(_instanceof(parent,nativePromise)){
child=new nativePromise(function(resolve,reject){
parent.then(function(value){
resolve(_clone(value,depth-1));
},function(err){
reject(_clone(err,depth-1));
});
});
}else if(clone.__isArray(parent)){
child=[];
}else if(clone.__isRegExp(parent)){
child=new RegExp(parent.source,__getRegExpFlags(parent));
if(parent.lastIndex)child.lastIndex=parent.lastIndex;
}else if(clone.__isDate(parent)){
child=new Date(parent.getTime());
}else if(useBuffer&&Buffer.isBuffer(parent)){
if(Buffer.allocUnsafe){
// Node.js >= 4.5.0
child=Buffer.allocUnsafe(parent.length);
}else {
// Older Node.js versions
child=new Buffer(parent.length);
}
parent.copy(child);
return child;
}else if(_instanceof(parent,Error)){
child=Object.create(parent);
}else {
if(typeof prototype=='undefined'){
proto=Object.getPrototypeOf(parent);
child=Object.create(proto);
}else
{
child=Object.create(prototype);
proto=prototype;
}
}

if(circular){
var index=allParents.indexOf(parent);

if(index!=-1){
return allChildren[index];
}
allParents.push(parent);
allChildren.push(child);
}

if(_instanceof(parent,nativeMap)){
parent.forEach(function(value,key){
var keyChild=_clone(key,depth-1);
var valueChild=_clone(value,depth-1);
child.set(keyChild,valueChild);
});
}
if(_instanceof(parent,nativeSet)){
parent.forEach(function(value){
var entryChild=_clone(value,depth-1);
child.add(entryChild);
});
}

for(var i in parent){
var attrs;
if(proto){
attrs=Object.getOwnPropertyDescriptor(proto,i);
}

if(attrs&&attrs.set==null){
continue;
}
child[i]=_clone(parent[i],depth-1);
}

if(Object.getOwnPropertySymbols){
var symbols=Object.getOwnPropertySymbols(parent);
for(var i=0;i<symbols.length;i++){
// Don't need to worry about cloning a symbol because it is a primitive,
// like a number or string.
var symbol=symbols[i];
var descriptor=Object.getOwnPropertyDescriptor(parent,symbol);
if(descriptor&&!descriptor.enumerable&&!includeNonEnumerable){
continue;
}
child[symbol]=_clone(parent[symbol],depth-1);
if(!descriptor.enumerable){
Object.defineProperty(child,symbol,{
enumerable:false});

}
}
}

if(includeNonEnumerable){
var allPropertyNames=Object.getOwnPropertyNames(parent);
for(var i=0;i<allPropertyNames.length;i++){
var propertyName=allPropertyNames[i];
var descriptor=Object.getOwnPropertyDescriptor(parent,propertyName);
if(descriptor&&descriptor.enumerable){
continue;
}
child[propertyName]=_clone(parent[propertyName],depth-1);
Object.defineProperty(child,propertyName,{
enumerable:false});

}
}

return child;
}

return _clone(parent,depth);
}

/**
 * Simple flat clone using prototype, accepts only objects, usefull for property
 * override on FLAT configuration object (no nested props).
 *
 * USE WITH CAUTION! This may not behave as you wish if you do not know how this
 * works.
 */
clone.clonePrototype=function clonePrototype(parent){
if(parent===null)
return null;

var c=function c(){};
c.prototype=parent;
return new c();
};

// private utility functions

function __objToStr(o){
return Object.prototype.toString.call(o);
}
clone.__objToStr=__objToStr;

function __isDate(o){
return typeof o==='object'&&__objToStr(o)==='[object Date]';
}
clone.__isDate=__isDate;

function __isArray(o){
return typeof o==='object'&&__objToStr(o)==='[object Array]';
}
clone.__isArray=__isArray;

function __isRegExp(o){
return typeof o==='object'&&__objToStr(o)==='[object RegExp]';
}
clone.__isRegExp=__isRegExp;

function __getRegExpFlags(re){
var flags='';
if(re.global)flags+='g';
if(re.ignoreCase)flags+='i';
if(re.multiline)flags+='m';
return flags;
}
clone.__getRegExpFlags=__getRegExpFlags;

return clone;
}();

if(typeof module==='object'&&module.exports){
module.exports=clone;
}


/***/},
/* 22 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _slicedToArray=function(){function sliceIterator(arr,i){var _arr=[];var _n=true;var _d=false;var _e=undefined;try{for(var _i=arr[Symbol.iterator](),_s;!(_n=(_s=_i.next()).done);_n=true){_arr.push(_s.value);if(i&&_arr.length===i)break;}}catch(err){_d=true;_e=err;}finally{try{if(!_n&&_i["return"])_i["return"]();}finally{if(_d)throw _e;}}return _arr;}return function(arr,i){if(Array.isArray(arr)){return arr;}else if(Symbol.iterator in Object(arr)){return sliceIterator(arr,i);}else {throw new TypeError("Invalid attempt to destructure non-iterable instance");}};}();

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

var _emitter=__webpack_require__(8);

var _emitter2=_interopRequireDefault(_emitter);

var _block=__webpack_require__(4);

var _block2=_interopRequireDefault(_block);

var _break=__webpack_require__(16);

var _break2=_interopRequireDefault(_break);

var _code=__webpack_require__(13);

var _code2=_interopRequireDefault(_code);

var _container=__webpack_require__(25);

var _container2=_interopRequireDefault(_container);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

function isLine(blot){
return blot instanceof _block2.default||blot instanceof _block.BlockEmbed;
}

var Scroll=function(_Parchment$Scroll){
_inherits(Scroll,_Parchment$Scroll);

function Scroll(domNode,config){
_classCallCheck(this,Scroll);

var _this=_possibleConstructorReturn(this,(Scroll.__proto__||Object.getPrototypeOf(Scroll)).call(this,domNode));

_this.emitter=config.emitter;
if(Array.isArray(config.whitelist)){
_this.whitelist=config.whitelist.reduce(function(whitelist,format){
whitelist[format]=true;
return whitelist;
},{});
}
// Some reason fixes composition issues with character languages in Windows/Chrome, Safari
_this.domNode.addEventListener('DOMNodeInserted',function(){});
_this.optimize();
_this.enable();
return _this;
}

_createClass(Scroll,[{
key:'batchStart',
value:function batchStart(){
this.batch=true;
}},
{
key:'batchEnd',
value:function batchEnd(){
this.batch=false;
this.optimize();
}},
{
key:'deleteAt',
value:function deleteAt(index,length){
var _line=this.line(index),
_line2=_slicedToArray(_line,2),
first=_line2[0],
offset=_line2[1];

var _line3=this.line(index+length),
_line4=_slicedToArray(_line3,1),
last=_line4[0];

_get(Scroll.prototype.__proto__||Object.getPrototypeOf(Scroll.prototype),'deleteAt',this).call(this,index,length);
if(last!=null&&first!==last&&offset>0){
if(first instanceof _block.BlockEmbed||last instanceof _block.BlockEmbed){
this.optimize();
return;
}
if(first instanceof _code2.default){
var newlineIndex=first.newlineIndex(first.length(),true);
if(newlineIndex>-1){
first=first.split(newlineIndex+1);
if(first===last){
this.optimize();
return;
}
}
}else if(last instanceof _code2.default){
var _newlineIndex=last.newlineIndex(0);
if(_newlineIndex>-1){
last.split(_newlineIndex+1);
}
}
var ref=last.children.head instanceof _break2.default?null:last.children.head;
first.moveChildren(last,ref);
first.remove();
}
this.optimize();
}},
{
key:'enable',
value:function enable(){
var enabled=arguments.length>0&&arguments[0]!==undefined?arguments[0]:true;

this.domNode.setAttribute('contenteditable',enabled);
}},
{
key:'formatAt',
value:function formatAt(index,length,format,value){
if(this.whitelist!=null&&!this.whitelist[format])return;
_get(Scroll.prototype.__proto__||Object.getPrototypeOf(Scroll.prototype),'formatAt',this).call(this,index,length,format,value);
this.optimize();
}},
{
key:'insertAt',
value:function insertAt(index,value,def){
if(def!=null&&this.whitelist!=null&&!this.whitelist[value])return;
if(index>=this.length()){
if(def==null||_parchment2.default.query(value,_parchment2.default.Scope.BLOCK)==null){
var blot=_parchment2.default.create(this.statics.defaultChild);
this.appendChild(blot);
if(def==null&&value.endsWith('\n')){
value=value.slice(0,-1);
}
blot.insertAt(0,value,def);
}else {
var embed=_parchment2.default.create(value,def);
this.appendChild(embed);
}
}else {
_get(Scroll.prototype.__proto__||Object.getPrototypeOf(Scroll.prototype),'insertAt',this).call(this,index,value,def);
}
this.optimize();
}},
{
key:'insertBefore',
value:function insertBefore(blot,ref){
if(blot.statics.scope===_parchment2.default.Scope.INLINE_BLOT){
var wrapper=_parchment2.default.create(this.statics.defaultChild);
wrapper.appendChild(blot);
blot=wrapper;
}
_get(Scroll.prototype.__proto__||Object.getPrototypeOf(Scroll.prototype),'insertBefore',this).call(this,blot,ref);
}},
{
key:'leaf',
value:function leaf(index){
return this.path(index).pop()||[null,-1];
}},
{
key:'line',
value:function line(index){
if(index===this.length()){
return this.line(index-1);
}
return this.descendant(isLine,index);
}},
{
key:'lines',
value:function lines(){
var index=arguments.length>0&&arguments[0]!==undefined?arguments[0]:0;
var length=arguments.length>1&&arguments[1]!==undefined?arguments[1]:Number.MAX_VALUE;

var getLines=function getLines(blot,index,length){
var lines=[],
lengthLeft=length;
blot.children.forEachAt(index,length,function(child,index,length){
if(isLine(child)){
lines.push(child);
}else if(child instanceof _parchment2.default.Container){
lines=lines.concat(getLines(child,index,lengthLeft));
}
lengthLeft-=length;
});
return lines;
};
return getLines(this,index,length);
}},
{
key:'optimize',
value:function optimize(){
var mutations=arguments.length>0&&arguments[0]!==undefined?arguments[0]:[];
var context=arguments.length>1&&arguments[1]!==undefined?arguments[1]:{};

if(this.batch===true)return;
_get(Scroll.prototype.__proto__||Object.getPrototypeOf(Scroll.prototype),'optimize',this).call(this,mutations,context);
if(mutations.length>0){
this.emitter.emit(_emitter2.default.events.SCROLL_OPTIMIZE,mutations,context);
}
}},
{
key:'path',
value:function path(index){
return _get(Scroll.prototype.__proto__||Object.getPrototypeOf(Scroll.prototype),'path',this).call(this,index).slice(1);// Exclude self
}},
{
key:'update',
value:function update(mutations){
if(this.batch===true)return;
var source=_emitter2.default.sources.USER;
if(typeof mutations==='string'){
source=mutations;
}
if(!Array.isArray(mutations)){
mutations=this.observer.takeRecords();
}
if(mutations.length>0){
this.emitter.emit(_emitter2.default.events.SCROLL_BEFORE_UPDATE,source,mutations);
}
_get(Scroll.prototype.__proto__||Object.getPrototypeOf(Scroll.prototype),'update',this).call(this,mutations.concat([]));// pass copy
if(mutations.length>0){
this.emitter.emit(_emitter2.default.events.SCROLL_UPDATE,source,mutations);
}
}}]);


return Scroll;
}(_parchment2.default.Scroll);

Scroll.blotName='scroll';
Scroll.className='ql-editor';
Scroll.tagName='DIV';
Scroll.defaultChild='block';
Scroll.allowedChildren=[_block2.default,_block.BlockEmbed,_container2.default];

exports.default=Scroll;

/***/},
/* 23 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.SHORTKEY=exports.default=undefined;

var _typeof=typeof Symbol==="function"&&typeof Symbol.iterator==="symbol"?function(obj){return typeof obj;}:function(obj){return obj&&typeof Symbol==="function"&&obj.constructor===Symbol&&obj!==Symbol.prototype?"symbol":typeof obj;};

var _slicedToArray=function(){function sliceIterator(arr,i){var _arr=[];var _n=true;var _d=false;var _e=undefined;try{for(var _i=arr[Symbol.iterator](),_s;!(_n=(_s=_i.next()).done);_n=true){_arr.push(_s.value);if(i&&_arr.length===i)break;}}catch(err){_d=true;_e=err;}finally{try{if(!_n&&_i["return"])_i["return"]();}finally{if(_d)throw _e;}}return _arr;}return function(arr,i){if(Array.isArray(arr)){return arr;}else if(Symbol.iterator in Object(arr)){return sliceIterator(arr,i);}else {throw new TypeError("Invalid attempt to destructure non-iterable instance");}};}();

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _clone=__webpack_require__(21);

var _clone2=_interopRequireDefault(_clone);

var _deepEqual=__webpack_require__(11);

var _deepEqual2=_interopRequireDefault(_deepEqual);

var _extend=__webpack_require__(3);

var _extend2=_interopRequireDefault(_extend);

var _quillDelta=__webpack_require__(2);

var _quillDelta2=_interopRequireDefault(_quillDelta);

var _op=__webpack_require__(20);

var _op2=_interopRequireDefault(_op);

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

var _quill=__webpack_require__(5);

var _quill2=_interopRequireDefault(_quill);

var _logger=__webpack_require__(10);

var _logger2=_interopRequireDefault(_logger);

var _module=__webpack_require__(9);

var _module2=_interopRequireDefault(_module);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _defineProperty(obj,key,value){if(key in obj){Object.defineProperty(obj,key,{value:value,enumerable:true,configurable:true,writable:true});}else {obj[key]=value;}return obj;}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var debug=(0, _logger2.default)('quill:keyboard');

var SHORTKEY=/Mac/i.test(navigator.platform)?'metaKey':'ctrlKey';

var Keyboard=function(_Module){
_inherits(Keyboard,_Module);

_createClass(Keyboard,null,[{
key:'match',
value:function match(evt,binding){
binding=normalize(binding);
if(['altKey','ctrlKey','metaKey','shiftKey'].some(function(key){
return !!binding[key]!==evt[key]&&binding[key]!==null;
})){
return false;
}
return binding.key===(evt.which||evt.keyCode);
}}]);


function Keyboard(quill,options){
_classCallCheck(this,Keyboard);

var _this=_possibleConstructorReturn(this,(Keyboard.__proto__||Object.getPrototypeOf(Keyboard)).call(this,quill,options));

_this.bindings={};
Object.keys(_this.options.bindings).forEach(function(name){
if(name==='list autofill'&&quill.scroll.whitelist!=null&&!quill.scroll.whitelist['list']){
return;
}
if(_this.options.bindings[name]){
_this.addBinding(_this.options.bindings[name]);
}
});
_this.addBinding({key:Keyboard.keys.ENTER,shiftKey:null},handleEnter);
_this.addBinding({key:Keyboard.keys.ENTER,metaKey:null,ctrlKey:null,altKey:null},function(){});
if(/Firefox/i.test(navigator.userAgent)){
// Need to handle delete and backspace for Firefox in the general case #1171
_this.addBinding({key:Keyboard.keys.BACKSPACE},{collapsed:true},handleBackspace);
_this.addBinding({key:Keyboard.keys.DELETE},{collapsed:true},handleDelete);
}else {
_this.addBinding({key:Keyboard.keys.BACKSPACE},{collapsed:true,prefix:/^.?$/},handleBackspace);
_this.addBinding({key:Keyboard.keys.DELETE},{collapsed:true,suffix:/^.?$/},handleDelete);
}
_this.addBinding({key:Keyboard.keys.BACKSPACE},{collapsed:false},handleDeleteRange);
_this.addBinding({key:Keyboard.keys.DELETE},{collapsed:false},handleDeleteRange);
_this.addBinding({key:Keyboard.keys.BACKSPACE,altKey:null,ctrlKey:null,metaKey:null,shiftKey:null},{collapsed:true,offset:0},handleBackspace);
_this.listen();
return _this;
}

_createClass(Keyboard,[{
key:'addBinding',
value:function addBinding(key){
var context=arguments.length>1&&arguments[1]!==undefined?arguments[1]:{};
var handler=arguments.length>2&&arguments[2]!==undefined?arguments[2]:{};

var binding=normalize(key);
if(binding==null||binding.key==null){
return debug.warn('Attempted to add invalid keyboard binding',binding);
}
if(typeof context==='function'){
context={handler:context};
}
if(typeof handler==='function'){
handler={handler:handler};
}
binding=(0, _extend2.default)(binding,context,handler);
this.bindings[binding.key]=this.bindings[binding.key]||[];
this.bindings[binding.key].push(binding);
}},
{
key:'listen',
value:function listen(){
var _this2=this;

this.quill.root.addEventListener('keydown',function(evt){
if(evt.defaultPrevented)return;
var which=evt.which||evt.keyCode;
var bindings=(_this2.bindings[which]||[]).filter(function(binding){
return Keyboard.match(evt,binding);
});
if(bindings.length===0)return;
var range=_this2.quill.getSelection();
if(range==null||!_this2.quill.hasFocus())return;

var _quill$getLine=_this2.quill.getLine(range.index),
_quill$getLine2=_slicedToArray(_quill$getLine,2),
line=_quill$getLine2[0],
offset=_quill$getLine2[1];

var _quill$getLeaf=_this2.quill.getLeaf(range.index),
_quill$getLeaf2=_slicedToArray(_quill$getLeaf,2),
leafStart=_quill$getLeaf2[0],
offsetStart=_quill$getLeaf2[1];

var _ref=range.length===0?[leafStart,offsetStart]:_this2.quill.getLeaf(range.index+range.length),
_ref2=_slicedToArray(_ref,2),
leafEnd=_ref2[0],
offsetEnd=_ref2[1];

var prefixText=leafStart instanceof _parchment2.default.Text?leafStart.value().slice(0,offsetStart):'';
var suffixText=leafEnd instanceof _parchment2.default.Text?leafEnd.value().slice(offsetEnd):'';
var curContext={
collapsed:range.length===0,
empty:range.length===0&&line.length()<=1,
format:_this2.quill.getFormat(range),
offset:offset,
prefix:prefixText,
suffix:suffixText};

var prevented=bindings.some(function(binding){
if(binding.collapsed!=null&&binding.collapsed!==curContext.collapsed)return false;
if(binding.empty!=null&&binding.empty!==curContext.empty)return false;
if(binding.offset!=null&&binding.offset!==curContext.offset)return false;
if(Array.isArray(binding.format)){
// any format is present
if(binding.format.every(function(name){
return curContext.format[name]==null;
})){
return false;
}
}else if(_typeof(binding.format)==='object'){
// all formats must match
if(!Object.keys(binding.format).every(function(name){
if(binding.format[name]===true)return curContext.format[name]!=null;
if(binding.format[name]===false)return curContext.format[name]==null;
return (0, _deepEqual2.default)(binding.format[name],curContext.format[name]);
})){
return false;
}
}
if(binding.prefix!=null&&!binding.prefix.test(curContext.prefix))return false;
if(binding.suffix!=null&&!binding.suffix.test(curContext.suffix))return false;
return binding.handler.call(_this2,range,curContext)!==true;
});
if(prevented){
evt.preventDefault();
}
});
}}]);


return Keyboard;
}(_module2.default);

Keyboard.keys={
BACKSPACE:8,
TAB:9,
ENTER:13,
ESCAPE:27,
LEFT:37,
UP:38,
RIGHT:39,
DOWN:40,
DELETE:46};


Keyboard.DEFAULTS={
bindings:{
'bold':makeFormatHandler('bold'),
'italic':makeFormatHandler('italic'),
'underline':makeFormatHandler('underline'),
'indent':{
// highlight tab or tab at beginning of list, indent or blockquote
key:Keyboard.keys.TAB,
format:['blockquote','indent','list'],
handler:function handler(range,context){
if(context.collapsed&&context.offset!==0)return true;
this.quill.format('indent','+1',_quill2.default.sources.USER);
}},

'outdent':{
key:Keyboard.keys.TAB,
shiftKey:true,
format:['blockquote','indent','list'],
// highlight tab or tab at beginning of list, indent or blockquote
handler:function handler(range,context){
if(context.collapsed&&context.offset!==0)return true;
this.quill.format('indent','-1',_quill2.default.sources.USER);
}},

'outdent backspace':{
key:Keyboard.keys.BACKSPACE,
collapsed:true,
shiftKey:null,
metaKey:null,
ctrlKey:null,
altKey:null,
format:['indent','list'],
offset:0,
handler:function handler(range,context){
if(context.format.indent!=null){
this.quill.format('indent','-1',_quill2.default.sources.USER);
}else if(context.format.list!=null){
this.quill.format('list',false,_quill2.default.sources.USER);
}
}},

'indent code-block':makeCodeBlockHandler(true),
'outdent code-block':makeCodeBlockHandler(false),
'remove tab':{
key:Keyboard.keys.TAB,
shiftKey:true,
collapsed:true,
prefix:/\t$/,
handler:function handler(range){
this.quill.deleteText(range.index-1,1,_quill2.default.sources.USER);
}},

'tab':{
key:Keyboard.keys.TAB,
handler:function handler(range){
this.quill.history.cutoff();
var delta=new _quillDelta2.default().retain(range.index).delete(range.length).insert('\t');
this.quill.updateContents(delta,_quill2.default.sources.USER);
this.quill.history.cutoff();
this.quill.setSelection(range.index+1,_quill2.default.sources.SILENT);
}},

'list empty enter':{
key:Keyboard.keys.ENTER,
collapsed:true,
format:['list'],
empty:true,
handler:function handler(range,context){
this.quill.format('list',false,_quill2.default.sources.USER);
if(context.format.indent){
this.quill.format('indent',false,_quill2.default.sources.USER);
}
}},

'checklist enter':{
key:Keyboard.keys.ENTER,
collapsed:true,
format:{list:'checked'},
handler:function handler(range){
var _quill$getLine3=this.quill.getLine(range.index),
_quill$getLine4=_slicedToArray(_quill$getLine3,2),
line=_quill$getLine4[0],
offset=_quill$getLine4[1];

var formats=(0, _extend2.default)({},line.formats(),{list:'checked'});
var delta=new _quillDelta2.default().retain(range.index).insert('\n',formats).retain(line.length()-offset-1).retain(1,{list:'unchecked'});
this.quill.updateContents(delta,_quill2.default.sources.USER);
this.quill.setSelection(range.index+1,_quill2.default.sources.SILENT);
this.quill.scrollIntoView();
}},

'header enter':{
key:Keyboard.keys.ENTER,
collapsed:true,
format:['header'],
suffix:/^$/,
handler:function handler(range,context){
var _quill$getLine5=this.quill.getLine(range.index),
_quill$getLine6=_slicedToArray(_quill$getLine5,2),
line=_quill$getLine6[0],
offset=_quill$getLine6[1];

var delta=new _quillDelta2.default().retain(range.index).insert('\n',context.format).retain(line.length()-offset-1).retain(1,{header:null});
this.quill.updateContents(delta,_quill2.default.sources.USER);
this.quill.setSelection(range.index+1,_quill2.default.sources.SILENT);
this.quill.scrollIntoView();
}},

'list autofill':{
key:' ',
collapsed:true,
format:{list:false},
prefix:/^\s*?(\d+\.|-|\*|\[ ?\]|\[x\])$/,
handler:function handler(range,context){
var length=context.prefix.length;

var _quill$getLine7=this.quill.getLine(range.index),
_quill$getLine8=_slicedToArray(_quill$getLine7,2),
line=_quill$getLine8[0],
offset=_quill$getLine8[1];

if(offset>length)return true;
var value=void 0;
switch(context.prefix.trim()){
case'[]':case'[ ]':
value='unchecked';
break;
case'[x]':
value='checked';
break;
case'-':case'*':
value='bullet';
break;
default:
value='ordered';}

this.quill.insertText(range.index,' ',_quill2.default.sources.USER);
this.quill.history.cutoff();
var delta=new _quillDelta2.default().retain(range.index-offset).delete(length+1).retain(line.length()-2-offset).retain(1,{list:value});
this.quill.updateContents(delta,_quill2.default.sources.USER);
this.quill.history.cutoff();
this.quill.setSelection(range.index-length,_quill2.default.sources.SILENT);
}},

'code exit':{
key:Keyboard.keys.ENTER,
collapsed:true,
format:['code-block'],
prefix:/\n\n$/,
suffix:/^\s+$/,
handler:function handler(range){
var _quill$getLine9=this.quill.getLine(range.index),
_quill$getLine10=_slicedToArray(_quill$getLine9,2),
line=_quill$getLine10[0],
offset=_quill$getLine10[1];

var delta=new _quillDelta2.default().retain(range.index+line.length()-offset-2).retain(1,{'code-block':null}).delete(1);
this.quill.updateContents(delta,_quill2.default.sources.USER);
}},

'embed left':makeEmbedArrowHandler(Keyboard.keys.LEFT,false),
'embed left shift':makeEmbedArrowHandler(Keyboard.keys.LEFT,true),
'embed right':makeEmbedArrowHandler(Keyboard.keys.RIGHT,false),
'embed right shift':makeEmbedArrowHandler(Keyboard.keys.RIGHT,true)}};



function makeEmbedArrowHandler(key,shiftKey){
var _ref3;

var where=key===Keyboard.keys.LEFT?'prefix':'suffix';
return _ref3={
key:key,
shiftKey:shiftKey,
altKey:null},
_defineProperty(_ref3,where,/^$/),_defineProperty(_ref3,'handler',function handler(range){
var index=range.index;
if(key===Keyboard.keys.RIGHT){
index+=range.length+1;
}

var _quill$getLeaf3=this.quill.getLeaf(index),
_quill$getLeaf4=_slicedToArray(_quill$getLeaf3,1),
leaf=_quill$getLeaf4[0];

if(!(leaf instanceof _parchment2.default.Embed))return true;
if(key===Keyboard.keys.LEFT){
if(shiftKey){
this.quill.setSelection(range.index-1,range.length+1,_quill2.default.sources.USER);
}else {
this.quill.setSelection(range.index-1,_quill2.default.sources.USER);
}
}else {
if(shiftKey){
this.quill.setSelection(range.index,range.length+1,_quill2.default.sources.USER);
}else {
this.quill.setSelection(range.index+range.length+1,_quill2.default.sources.USER);
}
}
return false;
}),_ref3;
}

function handleBackspace(range,context){
if(range.index===0||this.quill.getLength()<=1)return;

var _quill$getLine11=this.quill.getLine(range.index),
_quill$getLine12=_slicedToArray(_quill$getLine11,1),
line=_quill$getLine12[0];

var formats={};
if(context.offset===0){
var _quill$getLine13=this.quill.getLine(range.index-1),
_quill$getLine14=_slicedToArray(_quill$getLine13,1),
prev=_quill$getLine14[0];

if(prev!=null&&prev.length()>1){
var curFormats=line.formats();
var prevFormats=this.quill.getFormat(range.index-1,1);
formats=_op2.default.attributes.diff(curFormats,prevFormats)||{};
}
}
// Check for astral symbols
var length=/[\uD800-\uDBFF][\uDC00-\uDFFF]$/.test(context.prefix)?2:1;
this.quill.deleteText(range.index-length,length,_quill2.default.sources.USER);
if(Object.keys(formats).length>0){
this.quill.formatLine(range.index-length,length,formats,_quill2.default.sources.USER);
}
this.quill.focus();
}

function handleDelete(range,context){
// Check for astral symbols
var length=/^[\uD800-\uDBFF][\uDC00-\uDFFF]/.test(context.suffix)?2:1;
if(range.index>=this.quill.getLength()-length)return;
var formats={},
nextLength=0;

var _quill$getLine15=this.quill.getLine(range.index),
_quill$getLine16=_slicedToArray(_quill$getLine15,1),
line=_quill$getLine16[0];

if(context.offset>=line.length()-1){
var _quill$getLine17=this.quill.getLine(range.index+1),
_quill$getLine18=_slicedToArray(_quill$getLine17,1),
next=_quill$getLine18[0];

if(next){
var curFormats=line.formats();
var nextFormats=this.quill.getFormat(range.index,1);
formats=_op2.default.attributes.diff(curFormats,nextFormats)||{};
nextLength=next.length();
}
}
this.quill.deleteText(range.index,length,_quill2.default.sources.USER);
if(Object.keys(formats).length>0){
this.quill.formatLine(range.index+nextLength-1,length,formats,_quill2.default.sources.USER);
}
}

function handleDeleteRange(range){
var lines=this.quill.getLines(range);
var formats={};
if(lines.length>1){
var firstFormats=lines[0].formats();
var lastFormats=lines[lines.length-1].formats();
formats=_op2.default.attributes.diff(lastFormats,firstFormats)||{};
}
this.quill.deleteText(range,_quill2.default.sources.USER);
if(Object.keys(formats).length>0){
this.quill.formatLine(range.index,1,formats,_quill2.default.sources.USER);
}
this.quill.setSelection(range.index,_quill2.default.sources.SILENT);
this.quill.focus();
}

function handleEnter(range,context){
var _this3=this;

if(range.length>0){
this.quill.scroll.deleteAt(range.index,range.length);// So we do not trigger text-change
}
var lineFormats=Object.keys(context.format).reduce(function(lineFormats,format){
if(_parchment2.default.query(format,_parchment2.default.Scope.BLOCK)&&!Array.isArray(context.format[format])){
lineFormats[format]=context.format[format];
}
return lineFormats;
},{});
this.quill.insertText(range.index,'\n',lineFormats,_quill2.default.sources.USER);
// Earlier scroll.deleteAt might have messed up our selection,
// so insertText's built in selection preservation is not reliable
this.quill.setSelection(range.index+1,_quill2.default.sources.SILENT);
this.quill.focus();
Object.keys(context.format).forEach(function(name){
if(lineFormats[name]!=null)return;
if(Array.isArray(context.format[name]))return;
if(name==='link')return;
_this3.quill.format(name,context.format[name],_quill2.default.sources.USER);
});
}

function makeCodeBlockHandler(indent){
return {
key:Keyboard.keys.TAB,
shiftKey:!indent,
format:{'code-block':true},
handler:function handler(range){
var CodeBlock=_parchment2.default.query('code-block');
var index=range.index,
length=range.length;

var _quill$scroll$descend=this.quill.scroll.descendant(CodeBlock,index),
_quill$scroll$descend2=_slicedToArray(_quill$scroll$descend,2),
block=_quill$scroll$descend2[0],
offset=_quill$scroll$descend2[1];

if(block==null)return;
var scrollIndex=this.quill.getIndex(block);
var start=block.newlineIndex(offset,true)+1;
var end=block.newlineIndex(scrollIndex+offset+length);
var lines=block.domNode.textContent.slice(start,end).split('\n');
offset=0;
lines.forEach(function(line,i){
if(indent){
block.insertAt(start+offset,CodeBlock.TAB);
offset+=CodeBlock.TAB.length;
if(i===0){
index+=CodeBlock.TAB.length;
}else {
length+=CodeBlock.TAB.length;
}
}else if(line.startsWith(CodeBlock.TAB)){
block.deleteAt(start+offset,CodeBlock.TAB.length);
offset-=CodeBlock.TAB.length;
if(i===0){
index-=CodeBlock.TAB.length;
}else {
length-=CodeBlock.TAB.length;
}
}
offset+=line.length+1;
});
this.quill.update(_quill2.default.sources.USER);
this.quill.setSelection(index,length,_quill2.default.sources.SILENT);
}};

}

function makeFormatHandler(format){
return {
key:format[0].toUpperCase(),
shortKey:true,
handler:function handler(range,context){
this.quill.format(format,!context.format[format],_quill2.default.sources.USER);
}};

}

function normalize(binding){
if(typeof binding==='string'||typeof binding==='number'){
return normalize({key:binding});
}
if((typeof binding==='undefined'?'undefined':_typeof(binding))==='object'){
binding=(0, _clone2.default)(binding,false);
}
if(typeof binding.key==='string'){
if(Keyboard.keys[binding.key.toUpperCase()]!=null){
binding.key=Keyboard.keys[binding.key.toUpperCase()];
}else if(binding.key.length===1){
binding.key=binding.key.toUpperCase().charCodeAt(0);
}else {
return null;
}
}
if(binding.shortKey){
binding[SHORTKEY]=binding.shortKey;
delete binding.shortKey;
}
return binding;
}

exports.default=Keyboard;
exports.SHORTKEY=SHORTKEY;

/***/},
/* 24 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _slicedToArray=function(){function sliceIterator(arr,i){var _arr=[];var _n=true;var _d=false;var _e=undefined;try{for(var _i=arr[Symbol.iterator](),_s;!(_n=(_s=_i.next()).done);_n=true){_arr.push(_s.value);if(i&&_arr.length===i)break;}}catch(err){_d=true;_e=err;}finally{try{if(!_n&&_i["return"])_i["return"]();}finally{if(_d)throw _e;}}return _arr;}return function(arr,i){if(Array.isArray(arr)){return arr;}else if(Symbol.iterator in Object(arr)){return sliceIterator(arr,i);}else {throw new TypeError("Invalid attempt to destructure non-iterable instance");}};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

var _text=__webpack_require__(7);

var _text2=_interopRequireDefault(_text);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var Cursor=function(_Parchment$Embed){
_inherits(Cursor,_Parchment$Embed);

_createClass(Cursor,null,[{
key:'value',
value:function value(){
return undefined;
}}]);


function Cursor(domNode,selection){
_classCallCheck(this,Cursor);

var _this=_possibleConstructorReturn(this,(Cursor.__proto__||Object.getPrototypeOf(Cursor)).call(this,domNode));

_this.selection=selection;
_this.textNode=document.createTextNode(Cursor.CONTENTS);
_this.domNode.appendChild(_this.textNode);
_this._length=0;
return _this;
}

_createClass(Cursor,[{
key:'detach',
value:function detach(){
// super.detach() will also clear domNode.__blot
if(this.parent!=null)this.parent.removeChild(this);
}},
{
key:'format',
value:function format(name,value){
if(this._length!==0){
return _get(Cursor.prototype.__proto__||Object.getPrototypeOf(Cursor.prototype),'format',this).call(this,name,value);
}
var target=this,
index=0;
while(target!=null&&target.statics.scope!==_parchment2.default.Scope.BLOCK_BLOT){
index+=target.offset(target.parent);
target=target.parent;
}
if(target!=null){
this._length=Cursor.CONTENTS.length;
target.optimize();
target.formatAt(index,Cursor.CONTENTS.length,name,value);
this._length=0;
}
}},
{
key:'index',
value:function index(node,offset){
if(node===this.textNode)return 0;
return _get(Cursor.prototype.__proto__||Object.getPrototypeOf(Cursor.prototype),'index',this).call(this,node,offset);
}},
{
key:'length',
value:function length(){
return this._length;
}},
{
key:'position',
value:function position(){
return [this.textNode,this.textNode.data.length];
}},
{
key:'remove',
value:function remove(){
_get(Cursor.prototype.__proto__||Object.getPrototypeOf(Cursor.prototype),'remove',this).call(this);
this.parent=null;
}},
{
key:'restore',
value:function restore(){
if(this.selection.composing||this.parent==null)return;
var textNode=this.textNode;
var range=this.selection.getNativeRange();
var restoreText=void 0,
start=void 0,
end=void 0;
if(range!=null&&range.start.node===textNode&&range.end.node===textNode){
var _ref=[textNode,range.start.offset,range.end.offset];
restoreText=_ref[0];
start=_ref[1];
end=_ref[2];
}
// Link format will insert text outside of anchor tag
while(this.domNode.lastChild!=null&&this.domNode.lastChild!==this.textNode){
this.domNode.parentNode.insertBefore(this.domNode.lastChild,this.domNode);
}
if(this.textNode.data!==Cursor.CONTENTS){
var text=this.textNode.data.split(Cursor.CONTENTS).join('');
if(this.next instanceof _text2.default){
restoreText=this.next.domNode;
this.next.insertAt(0,text);
this.textNode.data=Cursor.CONTENTS;
}else {
this.textNode.data=text;
this.parent.insertBefore(_parchment2.default.create(this.textNode),this);
this.textNode=document.createTextNode(Cursor.CONTENTS);
this.domNode.appendChild(this.textNode);
}
}
this.remove();
if(start!=null){
var _map=[start,end].map(function(offset){
return Math.max(0,Math.min(restoreText.data.length,offset-1));
});

var _map2=_slicedToArray(_map,2);

start=_map2[0];
end=_map2[1];

return {
startNode:restoreText,
startOffset:start,
endNode:restoreText,
endOffset:end};

}
}},
{
key:'update',
value:function update(mutations,context){
var _this2=this;

if(mutations.some(function(mutation){
return mutation.type==='characterData'&&mutation.target===_this2.textNode;
})){
var range=this.restore();
if(range)context.range=range;
}
}},
{
key:'value',
value:function value(){
return '';
}}]);


return Cursor;
}(_parchment2.default.Embed);

Cursor.blotName='cursor';
Cursor.className='ql-cursor';
Cursor.tagName='span';
Cursor.CONTENTS="\uFEFF";// Zero width no break space


exports.default=Cursor;

/***/},
/* 25 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

var _block=__webpack_require__(4);

var _block2=_interopRequireDefault(_block);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var Container=function(_Parchment$Container){
_inherits(Container,_Parchment$Container);

function Container(){
_classCallCheck(this,Container);

return _possibleConstructorReturn(this,(Container.__proto__||Object.getPrototypeOf(Container)).apply(this,arguments));
}

return Container;
}(_parchment2.default.Container);

Container.allowedChildren=[_block2.default,_block.BlockEmbed,Container];

exports.default=Container;

/***/},
/* 26 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.ColorStyle=exports.ColorClass=exports.ColorAttributor=undefined;

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var ColorAttributor=function(_Parchment$Attributor){
_inherits(ColorAttributor,_Parchment$Attributor);

function ColorAttributor(){
_classCallCheck(this,ColorAttributor);

return _possibleConstructorReturn(this,(ColorAttributor.__proto__||Object.getPrototypeOf(ColorAttributor)).apply(this,arguments));
}

_createClass(ColorAttributor,[{
key:'value',
value:function value(domNode){
var value=_get(ColorAttributor.prototype.__proto__||Object.getPrototypeOf(ColorAttributor.prototype),'value',this).call(this,domNode);
if(!value.startsWith('rgb('))return value;
value=value.replace(/^[^\d]+/,'').replace(/[^\d]+$/,'');
return '#'+value.split(',').map(function(component){
return ('00'+parseInt(component).toString(16)).slice(-2);
}).join('');
}}]);


return ColorAttributor;
}(_parchment2.default.Attributor.Style);

var ColorClass=new _parchment2.default.Attributor.Class('color','ql-color',{
scope:_parchment2.default.Scope.INLINE});

var ColorStyle=new ColorAttributor('color','color',{
scope:_parchment2.default.Scope.INLINE});


exports.ColorAttributor=ColorAttributor;
exports.ColorClass=ColorClass;
exports.ColorStyle=ColorStyle;

/***/},
/* 27 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.sanitize=exports.default=undefined;

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _inline=__webpack_require__(6);

var _inline2=_interopRequireDefault(_inline);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var Link=function(_Inline){
_inherits(Link,_Inline);

function Link(){
_classCallCheck(this,Link);

return _possibleConstructorReturn(this,(Link.__proto__||Object.getPrototypeOf(Link)).apply(this,arguments));
}

_createClass(Link,[{
key:'format',
value:function format(name,value){
if(name!==this.statics.blotName||!value)return _get(Link.prototype.__proto__||Object.getPrototypeOf(Link.prototype),'format',this).call(this,name,value);
value=this.constructor.sanitize(value);
this.domNode.setAttribute('href',value);
}}],
[{
key:'create',
value:function create(value){
var node=_get(Link.__proto__||Object.getPrototypeOf(Link),'create',this).call(this,value);
value=this.sanitize(value);
node.setAttribute('href',value);
node.setAttribute('rel','noopener noreferrer');
node.setAttribute('target','_blank');
return node;
}},
{
key:'formats',
value:function formats(domNode){
return domNode.getAttribute('href');
}},
{
key:'sanitize',
value:function sanitize(url){
return _sanitize(url,this.PROTOCOL_WHITELIST)?url:this.SANITIZED_URL;
}}]);


return Link;
}(_inline2.default);

Link.blotName='link';
Link.tagName='A';
Link.SANITIZED_URL='about:blank';
Link.PROTOCOL_WHITELIST=['http','https','mailto','tel'];

function _sanitize(url,protocols){
var anchor=document.createElement('a');
anchor.href=url;
var protocol=anchor.href.slice(0,anchor.href.indexOf(':'));
return protocols.indexOf(protocol)>-1;
}

exports.default=Link;
exports.sanitize=_sanitize;

/***/},
/* 28 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _typeof=typeof Symbol==="function"&&typeof Symbol.iterator==="symbol"?function(obj){return typeof obj;}:function(obj){return obj&&typeof Symbol==="function"&&obj.constructor===Symbol&&obj!==Symbol.prototype?"symbol":typeof obj;};

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _keyboard=__webpack_require__(23);

var _keyboard2=_interopRequireDefault(_keyboard);

var _dropdown=__webpack_require__(107);

var _dropdown2=_interopRequireDefault(_dropdown);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

var optionsCounter=0;

function toggleAriaAttribute(element,attribute){
element.setAttribute(attribute,!(element.getAttribute(attribute)==='true'));
}

var Picker=function(){
function Picker(select){
var _this=this;

_classCallCheck(this,Picker);

this.select=select;
this.container=document.createElement('span');
this.buildPicker();
this.select.style.display='none';
this.select.parentNode.insertBefore(this.container,this.select);

this.label.addEventListener('mousedown',function(){
_this.togglePicker();
});
this.label.addEventListener('keydown',function(event){
switch(event.keyCode){
// Allows the "Enter" key to open the picker
case _keyboard2.default.keys.ENTER:
_this.togglePicker();
break;

// Allows the "Escape" key to close the picker
case _keyboard2.default.keys.ESCAPE:
_this.escape();
event.preventDefault();
break;
}

});
this.select.addEventListener('change',this.update.bind(this));
}

_createClass(Picker,[{
key:'togglePicker',
value:function togglePicker(){
this.container.classList.toggle('ql-expanded');
// Toggle aria-expanded and aria-hidden to make the picker accessible
toggleAriaAttribute(this.label,'aria-expanded');
toggleAriaAttribute(this.options,'aria-hidden');
}},
{
key:'buildItem',
value:function buildItem(option){
var _this2=this;

var item=document.createElement('span');
item.tabIndex='0';
item.setAttribute('role','button');

item.classList.add('ql-picker-item');
if(option.hasAttribute('value')){
item.setAttribute('data-value',option.getAttribute('value'));
}
if(option.textContent){
item.setAttribute('data-label',option.textContent);
}
item.addEventListener('click',function(){
_this2.selectItem(item,true);
});
item.addEventListener('keydown',function(event){
switch(event.keyCode){
// Allows the "Enter" key to select an item
case _keyboard2.default.keys.ENTER:
_this2.selectItem(item,true);
event.preventDefault();
break;

// Allows the "Escape" key to close the picker
case _keyboard2.default.keys.ESCAPE:
_this2.escape();
event.preventDefault();
break;
}

});

return item;
}},
{
key:'buildLabel',
value:function buildLabel(){
var label=document.createElement('span');
label.classList.add('ql-picker-label');
label.innerHTML=_dropdown2.default;
label.tabIndex='0';
label.setAttribute('role','button');
label.setAttribute('aria-expanded','false');
this.container.appendChild(label);
return label;
}},
{
key:'buildOptions',
value:function buildOptions(){
var _this3=this;

var options=document.createElement('span');
options.classList.add('ql-picker-options');

// Don't want screen readers to read this until options are visible
options.setAttribute('aria-hidden','true');
options.tabIndex='-1';

// Need a unique id for aria-controls
options.id='ql-picker-options-'+optionsCounter;
optionsCounter+=1;
this.label.setAttribute('aria-controls',options.id);

this.options=options;

[].slice.call(this.select.options).forEach(function(option){
var item=_this3.buildItem(option);
options.appendChild(item);
if(option.selected===true){
_this3.selectItem(item);
}
});
this.container.appendChild(options);
}},
{
key:'buildPicker',
value:function buildPicker(){
var _this4=this;

[].slice.call(this.select.attributes).forEach(function(item){
_this4.container.setAttribute(item.name,item.value);
});
this.container.classList.add('ql-picker');
this.label=this.buildLabel();
this.buildOptions();
}},
{
key:'escape',
value:function escape(){
var _this5=this;

// Close menu and return focus to trigger label
this.close();
// Need setTimeout for accessibility to ensure that the browser executes
// focus on the next process thread and after any DOM content changes
setTimeout(function(){
return _this5.label.focus();
},1);
}},
{
key:'close',
value:function close(){
this.container.classList.remove('ql-expanded');
this.label.setAttribute('aria-expanded','false');
this.options.setAttribute('aria-hidden','true');
}},
{
key:'selectItem',
value:function selectItem(item){
var trigger=arguments.length>1&&arguments[1]!==undefined?arguments[1]:false;

var selected=this.container.querySelector('.ql-selected');
if(item===selected)return;
if(selected!=null){
selected.classList.remove('ql-selected');
}
if(item==null)return;
item.classList.add('ql-selected');
this.select.selectedIndex=[].indexOf.call(item.parentNode.children,item);
if(item.hasAttribute('data-value')){
this.label.setAttribute('data-value',item.getAttribute('data-value'));
}else {
this.label.removeAttribute('data-value');
}
if(item.hasAttribute('data-label')){
this.label.setAttribute('data-label',item.getAttribute('data-label'));
}else {
this.label.removeAttribute('data-label');
}
if(trigger){
if(typeof Event==='function'){
this.select.dispatchEvent(new Event('change'));
}else if((typeof Event==='undefined'?'undefined':_typeof(Event))==='object'){
// IE11
var event=document.createEvent('Event');
event.initEvent('change',true,true);
this.select.dispatchEvent(event);
}
this.close();
}
}},
{
key:'update',
value:function update(){
var option=void 0;
if(this.select.selectedIndex>-1){
var item=this.container.querySelector('.ql-picker-options').children[this.select.selectedIndex];
option=this.select.options[this.select.selectedIndex];
this.selectItem(item);
}else {
this.selectItem(null);
}
var isActive=option!=null&&option!==this.select.querySelector('option[selected]');
this.label.classList.toggle('ql-active',isActive);
}}]);


return Picker;
}();

exports.default=Picker;

/***/},
/* 29 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

var _quill=__webpack_require__(5);

var _quill2=_interopRequireDefault(_quill);

var _block=__webpack_require__(4);

var _block2=_interopRequireDefault(_block);

var _break=__webpack_require__(16);

var _break2=_interopRequireDefault(_break);

var _container=__webpack_require__(25);

var _container2=_interopRequireDefault(_container);

var _cursor=__webpack_require__(24);

var _cursor2=_interopRequireDefault(_cursor);

var _embed=__webpack_require__(35);

var _embed2=_interopRequireDefault(_embed);

var _inline=__webpack_require__(6);

var _inline2=_interopRequireDefault(_inline);

var _scroll=__webpack_require__(22);

var _scroll2=_interopRequireDefault(_scroll);

var _text=__webpack_require__(7);

var _text2=_interopRequireDefault(_text);

var _clipboard=__webpack_require__(55);

var _clipboard2=_interopRequireDefault(_clipboard);

var _history=__webpack_require__(42);

var _history2=_interopRequireDefault(_history);

var _keyboard=__webpack_require__(23);

var _keyboard2=_interopRequireDefault(_keyboard);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

_quill2.default.register({
'blots/block':_block2.default,
'blots/block/embed':_block.BlockEmbed,
'blots/break':_break2.default,
'blots/container':_container2.default,
'blots/cursor':_cursor2.default,
'blots/embed':_embed2.default,
'blots/inline':_inline2.default,
'blots/scroll':_scroll2.default,
'blots/text':_text2.default,

'modules/clipboard':_clipboard2.default,
'modules/history':_history2.default,
'modules/keyboard':_keyboard2.default});


_parchment2.default.register(_block2.default,_break2.default,_cursor2.default,_inline2.default,_scroll2.default,_text2.default);

exports.default=_quill2.default;

/***/},
/* 30 */
/***/function(module,exports,__webpack_require__){

Object.defineProperty(exports,"__esModule",{value:true});
var Registry=__webpack_require__(1);
var ShadowBlot=/** @class */function(){
function ShadowBlot(domNode){
this.domNode=domNode;
// @ts-ignore
this.domNode[Registry.DATA_KEY]={blot:this};
}
Object.defineProperty(ShadowBlot.prototype,"statics",{
// Hack for accessing inherited static methods
get:function get(){
return this.constructor;
},
enumerable:true,
configurable:true});

ShadowBlot.create=function(value){
if(this.tagName==null){
throw new Registry.ParchmentError('Blot definition missing tagName');
}
var node;
if(Array.isArray(this.tagName)){
if(typeof value==='string'){
value=value.toUpperCase();
if(parseInt(value).toString()===value){
value=parseInt(value);
}
}
if(typeof value==='number'){
node=document.createElement(this.tagName[value-1]);
}else
if(this.tagName.indexOf(value)>-1){
node=document.createElement(value);
}else
{
node=document.createElement(this.tagName[0]);
}
}else
{
node=document.createElement(this.tagName);
}
if(this.className){
node.classList.add(this.className);
}
return node;
};
ShadowBlot.prototype.attach=function(){
if(this.parent!=null){
this.scroll=this.parent.scroll;
}
};
ShadowBlot.prototype.clone=function(){
var domNode=this.domNode.cloneNode(false);
return Registry.create(domNode);
};
ShadowBlot.prototype.detach=function(){
if(this.parent!=null)
this.parent.removeChild(this);
// @ts-ignore
delete this.domNode[Registry.DATA_KEY];
};
ShadowBlot.prototype.deleteAt=function(index,length){
var blot=this.isolate(index,length);
blot.remove();
};
ShadowBlot.prototype.formatAt=function(index,length,name,value){
var blot=this.isolate(index,length);
if(Registry.query(name,Registry.Scope.BLOT)!=null&&value){
blot.wrap(name,value);
}else
if(Registry.query(name,Registry.Scope.ATTRIBUTE)!=null){
var parent=Registry.create(this.statics.scope);
blot.wrap(parent);
parent.format(name,value);
}
};
ShadowBlot.prototype.insertAt=function(index,value,def){
var blot=def==null?Registry.create('text',value):Registry.create(value,def);
var ref=this.split(index);
this.parent.insertBefore(blot,ref);
};
ShadowBlot.prototype.insertInto=function(parentBlot,refBlot){
if(refBlot===void 0){refBlot=null;}
if(this.parent!=null){
this.parent.children.remove(this);
}
var refDomNode=null;
parentBlot.children.insertBefore(this,refBlot);
if(refBlot!=null){
refDomNode=refBlot.domNode;
}
if(this.domNode.parentNode!=parentBlot.domNode||
this.domNode.nextSibling!=refDomNode){
parentBlot.domNode.insertBefore(this.domNode,refDomNode);
}
this.parent=parentBlot;
this.attach();
};
ShadowBlot.prototype.isolate=function(index,length){
var target=this.split(index);
target.split(length);
return target;
};
ShadowBlot.prototype.length=function(){
return 1;
};
ShadowBlot.prototype.offset=function(root){
if(root===void 0){root=this.parent;}
if(this.parent==null||this==root)
return 0;
return this.parent.children.offset(this)+this.parent.offset(root);
};
ShadowBlot.prototype.optimize=function(context){
// TODO clean up once we use WeakMap
// @ts-ignore
if(this.domNode[Registry.DATA_KEY]!=null){
// @ts-ignore
delete this.domNode[Registry.DATA_KEY].mutations;
}
};
ShadowBlot.prototype.remove=function(){
if(this.domNode.parentNode!=null){
this.domNode.parentNode.removeChild(this.domNode);
}
this.detach();
};
ShadowBlot.prototype.replace=function(target){
if(target.parent==null)
return;
target.parent.insertBefore(this,target.next);
target.remove();
};
ShadowBlot.prototype.replaceWith=function(name,value){
var replacement=typeof name==='string'?Registry.create(name,value):name;
replacement.replace(this);
return replacement;
};
ShadowBlot.prototype.split=function(index,force){
return index===0?this:this.next;
};
ShadowBlot.prototype.update=function(mutations,context){
// Nothing to do by default
};
ShadowBlot.prototype.wrap=function(name,value){
var wrapper=typeof name==='string'?Registry.create(name,value):name;
if(this.parent!=null){
this.parent.insertBefore(wrapper,this.next);
}
wrapper.appendChild(this);
return wrapper;
};
ShadowBlot.blotName='abstract';
return ShadowBlot;
}();
exports.default=ShadowBlot;


/***/},
/* 31 */
/***/function(module,exports,__webpack_require__){

Object.defineProperty(exports,"__esModule",{value:true});
var attributor_1=__webpack_require__(12);
var class_1=__webpack_require__(32);
var style_1=__webpack_require__(33);
var Registry=__webpack_require__(1);
var AttributorStore=/** @class */function(){
function AttributorStore(domNode){
this.attributes={};
this.domNode=domNode;
this.build();
}
AttributorStore.prototype.attribute=function(attribute,value){
// verb
if(value){
if(attribute.add(this.domNode,value)){
if(attribute.value(this.domNode)!=null){
this.attributes[attribute.attrName]=attribute;
}else
{
delete this.attributes[attribute.attrName];
}
}
}else
{
attribute.remove(this.domNode);
delete this.attributes[attribute.attrName];
}
};
AttributorStore.prototype.build=function(){
var _this=this;
this.attributes={};
var attributes=attributor_1.default.keys(this.domNode);
var classes=class_1.default.keys(this.domNode);
var styles=style_1.default.keys(this.domNode);
attributes.
concat(classes).
concat(styles).
forEach(function(name){
var attr=Registry.query(name,Registry.Scope.ATTRIBUTE);
if(attr instanceof attributor_1.default){
_this.attributes[attr.attrName]=attr;
}
});
};
AttributorStore.prototype.copy=function(target){
var _this=this;
Object.keys(this.attributes).forEach(function(key){
var value=_this.attributes[key].value(_this.domNode);
target.format(key,value);
});
};
AttributorStore.prototype.move=function(target){
var _this=this;
this.copy(target);
Object.keys(this.attributes).forEach(function(key){
_this.attributes[key].remove(_this.domNode);
});
this.attributes={};
};
AttributorStore.prototype.values=function(){
var _this=this;
return Object.keys(this.attributes).reduce(function(attributes,name){
attributes[name]=_this.attributes[name].value(_this.domNode);
return attributes;
},{});
};
return AttributorStore;
}();
exports.default=AttributorStore;


/***/},
/* 32 */
/***/function(module,exports,__webpack_require__){

var __extends=this&&this.__extends||function(){
var extendStatics=Object.setPrototypeOf||
{__proto__:[]}instanceof Array&&function(d,b){d.__proto__=b;}||
function(d,b){for(var p in b){if(b.hasOwnProperty(p))d[p]=b[p];}};
return function(d,b){
extendStatics(d,b);
function __(){this.constructor=d;}
d.prototype=b===null?Object.create(b):(__.prototype=b.prototype,new __());
};
}();
Object.defineProperty(exports,"__esModule",{value:true});
var attributor_1=__webpack_require__(12);
function match(node,prefix){
var className=node.getAttribute('class')||'';
return className.split(/\s+/).filter(function(name){
return name.indexOf(prefix+"-")===0;
});
}
var ClassAttributor=/** @class */function(_super){
__extends(ClassAttributor,_super);
function ClassAttributor(){
return _super!==null&&_super.apply(this,arguments)||this;
}
ClassAttributor.keys=function(node){
return (node.getAttribute('class')||'').split(/\s+/).map(function(name){
return name.
split('-').
slice(0,-1).
join('-');
});
};
ClassAttributor.prototype.add=function(node,value){
if(!this.canAdd(node,value))
return false;
this.remove(node);
node.classList.add(this.keyName+"-"+value);
return true;
};
ClassAttributor.prototype.remove=function(node){
var matches=match(node,this.keyName);
matches.forEach(function(name){
node.classList.remove(name);
});
if(node.classList.length===0){
node.removeAttribute('class');
}
};
ClassAttributor.prototype.value=function(node){
var result=match(node,this.keyName)[0]||'';
var value=result.slice(this.keyName.length+1);// +1 for hyphen
return this.canAdd(node,value)?value:'';
};
return ClassAttributor;
}(attributor_1.default);
exports.default=ClassAttributor;


/***/},
/* 33 */
/***/function(module,exports,__webpack_require__){

var __extends=this&&this.__extends||function(){
var extendStatics=Object.setPrototypeOf||
{__proto__:[]}instanceof Array&&function(d,b){d.__proto__=b;}||
function(d,b){for(var p in b){if(b.hasOwnProperty(p))d[p]=b[p];}};
return function(d,b){
extendStatics(d,b);
function __(){this.constructor=d;}
d.prototype=b===null?Object.create(b):(__.prototype=b.prototype,new __());
};
}();
Object.defineProperty(exports,"__esModule",{value:true});
var attributor_1=__webpack_require__(12);
function camelize(name){
var parts=name.split('-');
var rest=parts.
slice(1).
map(function(part){
return part[0].toUpperCase()+part.slice(1);
}).
join('');
return parts[0]+rest;
}
var StyleAttributor=/** @class */function(_super){
__extends(StyleAttributor,_super);
function StyleAttributor(){
return _super!==null&&_super.apply(this,arguments)||this;
}
StyleAttributor.keys=function(node){
return (node.getAttribute('style')||'').split(';').map(function(value){
var arr=value.split(':');
return arr[0].trim();
});
};
StyleAttributor.prototype.add=function(node,value){
if(!this.canAdd(node,value))
return false;
// @ts-ignore
node.style[camelize(this.keyName)]=value;
return true;
};
StyleAttributor.prototype.remove=function(node){
// @ts-ignore
node.style[camelize(this.keyName)]='';
if(!node.getAttribute('style')){
node.removeAttribute('style');
}
};
StyleAttributor.prototype.value=function(node){
// @ts-ignore
var value=node.style[camelize(this.keyName)];
return this.canAdd(node,value)?value:'';
};
return StyleAttributor;
}(attributor_1.default);
exports.default=StyleAttributor;


/***/},
/* 34 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

var Theme=function(){
function Theme(quill,options){
_classCallCheck(this,Theme);

this.quill=quill;
this.options=options;
this.modules={};
}

_createClass(Theme,[{
key:'init',
value:function init(){
var _this=this;

Object.keys(this.options.modules).forEach(function(name){
if(_this.modules[name]==null){
_this.addModule(name);
}
});
}},
{
key:'addModule',
value:function addModule(name){
var moduleClass=this.quill.constructor.import('modules/'+name);
this.modules[name]=new moduleClass(this.quill,this.options.modules[name]||{});
return this.modules[name];
}}]);


return Theme;
}();

Theme.DEFAULTS={
modules:{}};

Theme.themes={
'default':Theme};


exports.default=Theme;

/***/},
/* 35 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

var _text=__webpack_require__(7);

var _text2=_interopRequireDefault(_text);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var GUARD_TEXT="\uFEFF";

var Embed=function(_Parchment$Embed){
_inherits(Embed,_Parchment$Embed);

function Embed(node){
_classCallCheck(this,Embed);

var _this=_possibleConstructorReturn(this,(Embed.__proto__||Object.getPrototypeOf(Embed)).call(this,node));

_this.contentNode=document.createElement('span');
_this.contentNode.setAttribute('contenteditable',false);
[].slice.call(_this.domNode.childNodes).forEach(function(childNode){
_this.contentNode.appendChild(childNode);
});
_this.leftGuard=document.createTextNode(GUARD_TEXT);
_this.rightGuard=document.createTextNode(GUARD_TEXT);
_this.domNode.appendChild(_this.leftGuard);
_this.domNode.appendChild(_this.contentNode);
_this.domNode.appendChild(_this.rightGuard);
return _this;
}

_createClass(Embed,[{
key:'index',
value:function index(node,offset){
if(node===this.leftGuard)return 0;
if(node===this.rightGuard)return 1;
return _get(Embed.prototype.__proto__||Object.getPrototypeOf(Embed.prototype),'index',this).call(this,node,offset);
}},
{
key:'restore',
value:function restore(node){
var range=void 0,
textNode=void 0;
var text=node.data.split(GUARD_TEXT).join('');
if(node===this.leftGuard){
if(this.prev instanceof _text2.default){
var prevLength=this.prev.length();
this.prev.insertAt(prevLength,text);
range={
startNode:this.prev.domNode,
startOffset:prevLength+text.length};

}else {
textNode=document.createTextNode(text);
this.parent.insertBefore(_parchment2.default.create(textNode),this);
range={
startNode:textNode,
startOffset:text.length};

}
}else if(node===this.rightGuard){
if(this.next instanceof _text2.default){
this.next.insertAt(0,text);
range={
startNode:this.next.domNode,
startOffset:text.length};

}else {
textNode=document.createTextNode(text);
this.parent.insertBefore(_parchment2.default.create(textNode),this.next);
range={
startNode:textNode,
startOffset:text.length};

}
}
node.data=GUARD_TEXT;
return range;
}},
{
key:'update',
value:function update(mutations,context){
var _this2=this;

mutations.forEach(function(mutation){
if(mutation.type==='characterData'&&(mutation.target===_this2.leftGuard||mutation.target===_this2.rightGuard)){
var range=_this2.restore(mutation.target);
if(range)context.range=range;
}
});
}}]);


return Embed;
}(_parchment2.default.Embed);

exports.default=Embed;

/***/},
/* 36 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.AlignStyle=exports.AlignClass=exports.AlignAttribute=undefined;

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

var config={
scope:_parchment2.default.Scope.BLOCK,
whitelist:['right','center','justify']};


var AlignAttribute=new _parchment2.default.Attributor.Attribute('align','align',config);
var AlignClass=new _parchment2.default.Attributor.Class('align','ql-align',config);
var AlignStyle=new _parchment2.default.Attributor.Style('align','text-align',config);

exports.AlignAttribute=AlignAttribute;
exports.AlignClass=AlignClass;
exports.AlignStyle=AlignStyle;

/***/},
/* 37 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.BackgroundStyle=exports.BackgroundClass=undefined;

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

var _color=__webpack_require__(26);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

var BackgroundClass=new _parchment2.default.Attributor.Class('background','ql-bg',{
scope:_parchment2.default.Scope.INLINE});

var BackgroundStyle=new _color.ColorAttributor('background','background-color',{
scope:_parchment2.default.Scope.INLINE});


exports.BackgroundClass=BackgroundClass;
exports.BackgroundStyle=BackgroundStyle;

/***/},
/* 38 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.DirectionStyle=exports.DirectionClass=exports.DirectionAttribute=undefined;

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

var config={
scope:_parchment2.default.Scope.BLOCK,
whitelist:['rtl']};


var DirectionAttribute=new _parchment2.default.Attributor.Attribute('direction','dir',config);
var DirectionClass=new _parchment2.default.Attributor.Class('direction','ql-direction',config);
var DirectionStyle=new _parchment2.default.Attributor.Style('direction','direction',config);

exports.DirectionAttribute=DirectionAttribute;
exports.DirectionClass=DirectionClass;
exports.DirectionStyle=DirectionStyle;

/***/},
/* 39 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.FontClass=exports.FontStyle=undefined;

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var config={
scope:_parchment2.default.Scope.INLINE,
whitelist:['serif','monospace']};


var FontClass=new _parchment2.default.Attributor.Class('font','ql-font',config);

var FontStyleAttributor=function(_Parchment$Attributor){
_inherits(FontStyleAttributor,_Parchment$Attributor);

function FontStyleAttributor(){
_classCallCheck(this,FontStyleAttributor);

return _possibleConstructorReturn(this,(FontStyleAttributor.__proto__||Object.getPrototypeOf(FontStyleAttributor)).apply(this,arguments));
}

_createClass(FontStyleAttributor,[{
key:'value',
value:function value(node){
return _get(FontStyleAttributor.prototype.__proto__||Object.getPrototypeOf(FontStyleAttributor.prototype),'value',this).call(this,node).replace(/["']/g,'');
}}]);


return FontStyleAttributor;
}(_parchment2.default.Attributor.Style);

var FontStyle=new FontStyleAttributor('font','font-family',config);

exports.FontStyle=FontStyle;
exports.FontClass=FontClass;

/***/},
/* 40 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.SizeStyle=exports.SizeClass=undefined;

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

var SizeClass=new _parchment2.default.Attributor.Class('size','ql-size',{
scope:_parchment2.default.Scope.INLINE,
whitelist:['small','large','huge']});

var SizeStyle=new _parchment2.default.Attributor.Style('size','font-size',{
scope:_parchment2.default.Scope.INLINE,
whitelist:['10px','18px','32px']});


exports.SizeClass=SizeClass;
exports.SizeStyle=SizeStyle;

/***/},
/* 41 */
/***/function(module,exports,__webpack_require__){


module.exports={
'align':{
'':__webpack_require__(76),
'center':__webpack_require__(77),
'right':__webpack_require__(78),
'justify':__webpack_require__(79)},

'background':__webpack_require__(80),
'blockquote':__webpack_require__(81),
'bold':__webpack_require__(82),
'clean':__webpack_require__(83),
'code':__webpack_require__(58),
'code-block':__webpack_require__(58),
'color':__webpack_require__(84),
'direction':{
'':__webpack_require__(85),
'rtl':__webpack_require__(86)},

'float':{
'center':__webpack_require__(87),
'full':__webpack_require__(88),
'left':__webpack_require__(89),
'right':__webpack_require__(90)},

'formula':__webpack_require__(91),
'header':{
'1':__webpack_require__(92),
'2':__webpack_require__(93)},

'italic':__webpack_require__(94),
'image':__webpack_require__(95),
'indent':{
'+1':__webpack_require__(96),
'-1':__webpack_require__(97)},

'link':__webpack_require__(98),
'list':{
'ordered':__webpack_require__(99),
'bullet':__webpack_require__(100),
'check':__webpack_require__(101)},

'script':{
'sub':__webpack_require__(102),
'super':__webpack_require__(103)},

'strike':__webpack_require__(104),
'underline':__webpack_require__(105),
'video':__webpack_require__(106)};


/***/},
/* 42 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.getLastChangeIndex=exports.default=undefined;

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

var _quill=__webpack_require__(5);

var _quill2=_interopRequireDefault(_quill);

var _module=__webpack_require__(9);

var _module2=_interopRequireDefault(_module);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var History=function(_Module){
_inherits(History,_Module);

function History(quill,options){
_classCallCheck(this,History);

var _this=_possibleConstructorReturn(this,(History.__proto__||Object.getPrototypeOf(History)).call(this,quill,options));

_this.lastRecorded=0;
_this.ignoreChange=false;
_this.clear();
_this.quill.on(_quill2.default.events.EDITOR_CHANGE,function(eventName,delta,oldDelta,source){
if(eventName!==_quill2.default.events.TEXT_CHANGE||_this.ignoreChange)return;
if(!_this.options.userOnly||source===_quill2.default.sources.USER){
_this.record(delta,oldDelta);
}else {
_this.transform(delta);
}
});
_this.quill.keyboard.addBinding({key:'Z',shortKey:true},_this.undo.bind(_this));
_this.quill.keyboard.addBinding({key:'Z',shortKey:true,shiftKey:true},_this.redo.bind(_this));
if(/Win/i.test(navigator.platform)){
_this.quill.keyboard.addBinding({key:'Y',shortKey:true},_this.redo.bind(_this));
}
return _this;
}

_createClass(History,[{
key:'change',
value:function change(source,dest){
if(this.stack[source].length===0)return;
var delta=this.stack[source].pop();
this.stack[dest].push(delta);
this.lastRecorded=0;
this.ignoreChange=true;
this.quill.updateContents(delta[source],_quill2.default.sources.USER);
this.ignoreChange=false;
var index=getLastChangeIndex(delta[source]);
this.quill.setSelection(index);
}},
{
key:'clear',
value:function clear(){
this.stack={undo:[],redo:[]};
}},
{
key:'cutoff',
value:function cutoff(){
this.lastRecorded=0;
}},
{
key:'record',
value:function record(changeDelta,oldDelta){
if(changeDelta.ops.length===0)return;
this.stack.redo=[];
var undoDelta=this.quill.getContents().diff(oldDelta);
var timestamp=Date.now();
if(this.lastRecorded+this.options.delay>timestamp&&this.stack.undo.length>0){
var delta=this.stack.undo.pop();
undoDelta=undoDelta.compose(delta.undo);
changeDelta=delta.redo.compose(changeDelta);
}else {
this.lastRecorded=timestamp;
}
this.stack.undo.push({
redo:changeDelta,
undo:undoDelta});

if(this.stack.undo.length>this.options.maxStack){
this.stack.undo.shift();
}
}},
{
key:'redo',
value:function redo(){
this.change('redo','undo');
}},
{
key:'transform',
value:function transform(delta){
this.stack.undo.forEach(function(change){
change.undo=delta.transform(change.undo,true);
change.redo=delta.transform(change.redo,true);
});
this.stack.redo.forEach(function(change){
change.undo=delta.transform(change.undo,true);
change.redo=delta.transform(change.redo,true);
});
}},
{
key:'undo',
value:function undo(){
this.change('undo','redo');
}}]);


return History;
}(_module2.default);

History.DEFAULTS={
delay:1000,
maxStack:100,
userOnly:false};


function endsWithNewlineChange(delta){
var lastOp=delta.ops[delta.ops.length-1];
if(lastOp==null)return false;
if(lastOp.insert!=null){
return typeof lastOp.insert==='string'&&lastOp.insert.endsWith('\n');
}
if(lastOp.attributes!=null){
return Object.keys(lastOp.attributes).some(function(attr){
return _parchment2.default.query(attr,_parchment2.default.Scope.BLOCK)!=null;
});
}
return false;
}

function getLastChangeIndex(delta){
var deleteLength=delta.reduce(function(length,op){
length+=op.delete||0;
return length;
},0);
var changeIndex=delta.length()-deleteLength;
if(endsWithNewlineChange(delta)){
changeIndex-=1;
}
return changeIndex;
}

exports.default=History;
exports.getLastChangeIndex=getLastChangeIndex;

/***/},
/* 43 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.default=exports.BaseTooltip=undefined;

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _extend=__webpack_require__(3);

var _extend2=_interopRequireDefault(_extend);

var _quillDelta=__webpack_require__(2);

var _quillDelta2=_interopRequireDefault(_quillDelta);

var _emitter=__webpack_require__(8);

var _emitter2=_interopRequireDefault(_emitter);

var _keyboard=__webpack_require__(23);

var _keyboard2=_interopRequireDefault(_keyboard);

var _theme=__webpack_require__(34);

var _theme2=_interopRequireDefault(_theme);

var _colorPicker=__webpack_require__(59);

var _colorPicker2=_interopRequireDefault(_colorPicker);

var _iconPicker=__webpack_require__(60);

var _iconPicker2=_interopRequireDefault(_iconPicker);

var _picker=__webpack_require__(28);

var _picker2=_interopRequireDefault(_picker);

var _tooltip=__webpack_require__(61);

var _tooltip2=_interopRequireDefault(_tooltip);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var ALIGNS=[false,'center','right','justify'];

var COLORS=["#000000","#e60000","#ff9900","#ffff00","#008a00","#0066cc","#9933ff","#ffffff","#facccc","#ffebcc","#ffffcc","#cce8cc","#cce0f5","#ebd6ff","#bbbbbb","#f06666","#ffc266","#ffff66","#66b966","#66a3e0","#c285ff","#888888","#a10000","#b26b00","#b2b200","#006100","#0047b2","#6b24b2","#444444","#5c0000","#663d00","#666600","#003700","#002966","#3d1466"];

var FONTS=[false,'serif','monospace'];

var HEADERS=['1','2','3',false];

var SIZES=['small',false,'large','huge'];

var BaseTheme=function(_Theme){
_inherits(BaseTheme,_Theme);

function BaseTheme(quill,options){
_classCallCheck(this,BaseTheme);

var _this=_possibleConstructorReturn(this,(BaseTheme.__proto__||Object.getPrototypeOf(BaseTheme)).call(this,quill,options));

var listener=function listener(e){
if(!document.body.contains(quill.root)){
return document.body.removeEventListener('click',listener);
}
if(_this.tooltip!=null&&!_this.tooltip.root.contains(e.target)&&document.activeElement!==_this.tooltip.textbox&&!_this.quill.hasFocus()){
_this.tooltip.hide();
}
if(_this.pickers!=null){
_this.pickers.forEach(function(picker){
if(!picker.container.contains(e.target)){
picker.close();
}
});
}
};
quill.emitter.listenDOM('click',document.body,listener);
return _this;
}

_createClass(BaseTheme,[{
key:'addModule',
value:function addModule(name){
var module=_get(BaseTheme.prototype.__proto__||Object.getPrototypeOf(BaseTheme.prototype),'addModule',this).call(this,name);
if(name==='toolbar'){
this.extendToolbar(module);
}
return module;
}},
{
key:'buildButtons',
value:function buildButtons(buttons,icons){
buttons.forEach(function(button){
var className=button.getAttribute('class')||'';
className.split(/\s+/).forEach(function(name){
if(!name.startsWith('ql-'))return;
name=name.slice('ql-'.length);
if(icons[name]==null)return;
if(name==='direction'){
button.innerHTML=icons[name]['']+icons[name]['rtl'];
}else if(typeof icons[name]==='string'){
button.innerHTML=icons[name];
}else {
var value=button.value||'';
if(value!=null&&icons[name][value]){
button.innerHTML=icons[name][value];
}
}
});
});
}},
{
key:'buildPickers',
value:function buildPickers(selects,icons){
var _this2=this;

this.pickers=selects.map(function(select){
if(select.classList.contains('ql-align')){
if(select.querySelector('option')==null){
fillSelect(select,ALIGNS);
}
return new _iconPicker2.default(select,icons.align);
}else if(select.classList.contains('ql-background')||select.classList.contains('ql-color')){
var format=select.classList.contains('ql-background')?'background':'color';
if(select.querySelector('option')==null){
fillSelect(select,COLORS,format==='background'?'#ffffff':'#000000');
}
return new _colorPicker2.default(select,icons[format]);
}else {
if(select.querySelector('option')==null){
if(select.classList.contains('ql-font')){
fillSelect(select,FONTS);
}else if(select.classList.contains('ql-header')){
fillSelect(select,HEADERS);
}else if(select.classList.contains('ql-size')){
fillSelect(select,SIZES);
}
}
return new _picker2.default(select);
}
});
var update=function update(){
_this2.pickers.forEach(function(picker){
picker.update();
});
};
this.quill.on(_emitter2.default.events.EDITOR_CHANGE,update);
}}]);


return BaseTheme;
}(_theme2.default);

BaseTheme.DEFAULTS=(0, _extend2.default)(true,{},_theme2.default.DEFAULTS,{
modules:{
toolbar:{
handlers:{
formula:function formula(){
this.quill.theme.tooltip.edit('formula');
},
image:function image(){
var _this3=this;

var fileInput=this.container.querySelector('input.ql-image[type=file]');
if(fileInput==null){
fileInput=document.createElement('input');
fileInput.setAttribute('type','file');
fileInput.setAttribute('accept','image/png, image/gif, image/jpeg, image/bmp, image/x-icon');
fileInput.classList.add('ql-image');
fileInput.addEventListener('change',function(){
if(fileInput.files!=null&&fileInput.files[0]!=null){
var reader=new FileReader();
reader.onload=function(e){
var range=_this3.quill.getSelection(true);
_this3.quill.updateContents(new _quillDelta2.default().retain(range.index).delete(range.length).insert({image:e.target.result}),_emitter2.default.sources.USER);
_this3.quill.setSelection(range.index+1,_emitter2.default.sources.SILENT);
fileInput.value="";
};
reader.readAsDataURL(fileInput.files[0]);
}
});
this.container.appendChild(fileInput);
}
fileInput.click();
},
video:function video(){
this.quill.theme.tooltip.edit('video');
}}}}});





var BaseTooltip=function(_Tooltip){
_inherits(BaseTooltip,_Tooltip);

function BaseTooltip(quill,boundsContainer){
_classCallCheck(this,BaseTooltip);

var _this4=_possibleConstructorReturn(this,(BaseTooltip.__proto__||Object.getPrototypeOf(BaseTooltip)).call(this,quill,boundsContainer));

_this4.textbox=_this4.root.querySelector('input[type="text"]');
_this4.listen();
return _this4;
}

_createClass(BaseTooltip,[{
key:'listen',
value:function listen(){
var _this5=this;

this.textbox.addEventListener('keydown',function(event){
if(_keyboard2.default.match(event,'enter')){
_this5.save();
event.preventDefault();
}else if(_keyboard2.default.match(event,'escape')){
_this5.cancel();
event.preventDefault();
}
});
}},
{
key:'cancel',
value:function cancel(){
this.hide();
}},
{
key:'edit',
value:function edit(){
var mode=arguments.length>0&&arguments[0]!==undefined?arguments[0]:'link';
var preview=arguments.length>1&&arguments[1]!==undefined?arguments[1]:null;

this.root.classList.remove('ql-hidden');
this.root.classList.add('ql-editing');
if(preview!=null){
this.textbox.value=preview;
}else if(mode!==this.root.getAttribute('data-mode')){
this.textbox.value='';
}
this.position(this.quill.getBounds(this.quill.selection.savedRange));
this.textbox.select();
this.textbox.setAttribute('placeholder',this.textbox.getAttribute('data-'+mode)||'');
this.root.setAttribute('data-mode',mode);
}},
{
key:'restoreFocus',
value:function restoreFocus(){
var scrollTop=this.quill.scrollingContainer.scrollTop;
this.quill.focus();
this.quill.scrollingContainer.scrollTop=scrollTop;
}},
{
key:'save',
value:function save(){
var value=this.textbox.value;
switch(this.root.getAttribute('data-mode')){
case'link':
{
var scrollTop=this.quill.root.scrollTop;
if(this.linkRange){
this.quill.formatText(this.linkRange,'link',value,_emitter2.default.sources.USER);
delete this.linkRange;
}else {
this.restoreFocus();
this.quill.format('link',value,_emitter2.default.sources.USER);
}
this.quill.root.scrollTop=scrollTop;
break;
}
case'video':
{
value=extractVideoUrl(value);
}// eslint-disable-next-line no-fallthrough
case'formula':
{
if(!value)break;
var range=this.quill.getSelection(true);
if(range!=null){
var index=range.index+range.length;
this.quill.insertEmbed(index,this.root.getAttribute('data-mode'),value,_emitter2.default.sources.USER);
if(this.root.getAttribute('data-mode')==='formula'){
this.quill.insertText(index+1,' ',_emitter2.default.sources.USER);
}
this.quill.setSelection(index+2,_emitter2.default.sources.USER);
}
break;
}
}

this.textbox.value='';
this.hide();
}}]);


return BaseTooltip;
}(_tooltip2.default);

function extractVideoUrl(url){
var match=url.match(/^(?:(https?):\/\/)?(?:(?:www|m)\.)?youtube\.com\/watch.*v=([a-zA-Z0-9_-]+)/)||url.match(/^(?:(https?):\/\/)?(?:(?:www|m)\.)?youtu\.be\/([a-zA-Z0-9_-]+)/);
if(match){
return (match[1]||'https')+'://www.youtube.com/embed/'+match[2]+'?showinfo=0';
}
if(match=url.match(/^(?:(https?):\/\/)?(?:www\.)?vimeo\.com\/(\d+)/)){
// eslint-disable-line no-cond-assign
return (match[1]||'https')+'://player.vimeo.com/video/'+match[2]+'/';
}
return url;
}

function fillSelect(select,values){
var defaultValue=arguments.length>2&&arguments[2]!==undefined?arguments[2]:false;

values.forEach(function(value){
var option=document.createElement('option');
if(value===defaultValue){
option.setAttribute('selected','selected');
}else {
option.setAttribute('value',value);
}
select.appendChild(option);
});
}

exports.BaseTooltip=BaseTooltip;
exports.default=BaseTheme;

/***/},
/* 44 */
/***/function(module,exports,__webpack_require__){

Object.defineProperty(exports,"__esModule",{value:true});
var LinkedList=/** @class */function(){
function LinkedList(){
this.head=this.tail=null;
this.length=0;
}
LinkedList.prototype.append=function(){
var nodes=[];
for(var _i=0;_i<arguments.length;_i++){
nodes[_i]=arguments[_i];
}
this.insertBefore(nodes[0],null);
if(nodes.length>1){
this.append.apply(this,nodes.slice(1));
}
};
LinkedList.prototype.contains=function(node){
var cur,next=this.iterator();
while(cur=next()){
if(cur===node)
return true;
}
return false;
};
LinkedList.prototype.insertBefore=function(node,refNode){
if(!node)
return;
node.next=refNode;
if(refNode!=null){
node.prev=refNode.prev;
if(refNode.prev!=null){
refNode.prev.next=node;
}
refNode.prev=node;
if(refNode===this.head){
this.head=node;
}
}else
if(this.tail!=null){
this.tail.next=node;
node.prev=this.tail;
this.tail=node;
}else
{
node.prev=null;
this.head=this.tail=node;
}
this.length+=1;
};
LinkedList.prototype.offset=function(target){
var index=0,cur=this.head;
while(cur!=null){
if(cur===target)
return index;
index+=cur.length();
cur=cur.next;
}
return -1;
};
LinkedList.prototype.remove=function(node){
if(!this.contains(node))
return;
if(node.prev!=null)
node.prev.next=node.next;
if(node.next!=null)
node.next.prev=node.prev;
if(node===this.head)
this.head=node.next;
if(node===this.tail)
this.tail=node.prev;
this.length-=1;
};
LinkedList.prototype.iterator=function(curNode){
if(curNode===void 0){curNode=this.head;}
// TODO use yield when we can
return function(){
var ret=curNode;
if(curNode!=null)
curNode=curNode.next;
return ret;
};
};
LinkedList.prototype.find=function(index,inclusive){
if(inclusive===void 0){inclusive=false;}
var cur,next=this.iterator();
while(cur=next()){
var length=cur.length();
if(index<length||
inclusive&&index===length&&(cur.next==null||cur.next.length()!==0)){
return [cur,index];
}
index-=length;
}
return [null,0];
};
LinkedList.prototype.forEach=function(callback){
var cur,next=this.iterator();
while(cur=next()){
callback(cur);
}
};
LinkedList.prototype.forEachAt=function(index,length,callback){
if(length<=0)
return;
var _a=this.find(index),startNode=_a[0],offset=_a[1];
var cur,curIndex=index-offset,next=this.iterator(startNode);
while((cur=next())&&curIndex<index+length){
var curLength=cur.length();
if(index>curIndex){
callback(cur,index-curIndex,Math.min(length,curIndex+curLength-index));
}else
{
callback(cur,0,Math.min(curLength,index+length-curIndex));
}
curIndex+=curLength;
}
};
LinkedList.prototype.map=function(callback){
return this.reduce(function(memo,cur){
memo.push(callback(cur));
return memo;
},[]);
};
LinkedList.prototype.reduce=function(callback,memo){
var cur,next=this.iterator();
while(cur=next()){
memo=callback(memo,cur);
}
return memo;
};
return LinkedList;
}();
exports.default=LinkedList;


/***/},
/* 45 */
/***/function(module,exports,__webpack_require__){

var __extends=this&&this.__extends||function(){
var extendStatics=Object.setPrototypeOf||
{__proto__:[]}instanceof Array&&function(d,b){d.__proto__=b;}||
function(d,b){for(var p in b){if(b.hasOwnProperty(p))d[p]=b[p];}};
return function(d,b){
extendStatics(d,b);
function __(){this.constructor=d;}
d.prototype=b===null?Object.create(b):(__.prototype=b.prototype,new __());
};
}();
Object.defineProperty(exports,"__esModule",{value:true});
var container_1=__webpack_require__(17);
var Registry=__webpack_require__(1);
var OBSERVER_CONFIG={
attributes:true,
characterData:true,
characterDataOldValue:true,
childList:true,
subtree:true};

var MAX_OPTIMIZE_ITERATIONS=100;
var ScrollBlot=/** @class */function(_super){
__extends(ScrollBlot,_super);
function ScrollBlot(node){
var _this=_super.call(this,node)||this;
_this.scroll=_this;
_this.observer=new MutationObserver(function(mutations){
_this.update(mutations);
});
_this.observer.observe(_this.domNode,OBSERVER_CONFIG);
_this.attach();
return _this;
}
ScrollBlot.prototype.detach=function(){
_super.prototype.detach.call(this);
this.observer.disconnect();
};
ScrollBlot.prototype.deleteAt=function(index,length){
this.update();
if(index===0&&length===this.length()){
this.children.forEach(function(child){
child.remove();
});
}else
{
_super.prototype.deleteAt.call(this,index,length);
}
};
ScrollBlot.prototype.formatAt=function(index,length,name,value){
this.update();
_super.prototype.formatAt.call(this,index,length,name,value);
};
ScrollBlot.prototype.insertAt=function(index,value,def){
this.update();
_super.prototype.insertAt.call(this,index,value,def);
};
ScrollBlot.prototype.optimize=function(mutations,context){
var _this=this;
if(mutations===void 0){mutations=[];}
if(context===void 0){context={};}
_super.prototype.optimize.call(this,context);
// We must modify mutations directly, cannot make copy and then modify
var records=[].slice.call(this.observer.takeRecords());
// Array.push currently seems to be implemented by a non-tail recursive function
// so we cannot just mutations.push.apply(mutations, this.observer.takeRecords());
while(records.length>0){
mutations.push(records.pop());}
// TODO use WeakMap
var mark=function mark(blot,markParent){
if(markParent===void 0){markParent=true;}
if(blot==null||blot===_this)
return;
if(blot.domNode.parentNode==null)
return;
// @ts-ignore
if(blot.domNode[Registry.DATA_KEY].mutations==null){
// @ts-ignore
blot.domNode[Registry.DATA_KEY].mutations=[];
}
if(markParent)
mark(blot.parent);
};
var optimize=function optimize(blot){
// Post-order traversal
if(
// @ts-ignore
blot.domNode[Registry.DATA_KEY]==null||
// @ts-ignore
blot.domNode[Registry.DATA_KEY].mutations==null){
return;
}
if(blot instanceof container_1.default){
blot.children.forEach(optimize);
}
blot.optimize(context);
};
var remaining=mutations;
for(var i=0;remaining.length>0;i+=1){
if(i>=MAX_OPTIMIZE_ITERATIONS){
throw new Error('[Parchment] Maximum optimize iterations reached');
}
remaining.forEach(function(mutation){
var blot=Registry.find(mutation.target,true);
if(blot==null)
return;
if(blot.domNode===mutation.target){
if(mutation.type==='childList'){
mark(Registry.find(mutation.previousSibling,false));
[].forEach.call(mutation.addedNodes,function(node){
var child=Registry.find(node,false);
mark(child,false);
if(child instanceof container_1.default){
child.children.forEach(function(grandChild){
mark(grandChild,false);
});
}
});
}else
if(mutation.type==='attributes'){
mark(blot.prev);
}
}
mark(blot);
});
this.children.forEach(optimize);
remaining=[].slice.call(this.observer.takeRecords());
records=remaining.slice();
while(records.length>0){
mutations.push(records.pop());}
}
};
ScrollBlot.prototype.update=function(mutations,context){
var _this=this;
if(context===void 0){context={};}
mutations=mutations||this.observer.takeRecords();
// TODO use WeakMap
mutations.
map(function(mutation){
var blot=Registry.find(mutation.target,true);
if(blot==null)
return null;
// @ts-ignore
if(blot.domNode[Registry.DATA_KEY].mutations==null){
// @ts-ignore
blot.domNode[Registry.DATA_KEY].mutations=[mutation];
return blot;
}else
{
// @ts-ignore
blot.domNode[Registry.DATA_KEY].mutations.push(mutation);
return null;
}
}).
forEach(function(blot){
if(blot==null||
blot===_this||
//@ts-ignore
blot.domNode[Registry.DATA_KEY]==null)
return;
// @ts-ignore
blot.update(blot.domNode[Registry.DATA_KEY].mutations||[],context);
});
// @ts-ignore
if(this.domNode[Registry.DATA_KEY].mutations!=null){
// @ts-ignore
_super.prototype.update.call(this,this.domNode[Registry.DATA_KEY].mutations,context);
}
this.optimize(mutations,context);
};
ScrollBlot.blotName='scroll';
ScrollBlot.defaultChild='block';
ScrollBlot.scope=Registry.Scope.BLOCK_BLOT;
ScrollBlot.tagName='DIV';
return ScrollBlot;
}(container_1.default);
exports.default=ScrollBlot;


/***/},
/* 46 */
/***/function(module,exports,__webpack_require__){

var __extends=this&&this.__extends||function(){
var extendStatics=Object.setPrototypeOf||
{__proto__:[]}instanceof Array&&function(d,b){d.__proto__=b;}||
function(d,b){for(var p in b){if(b.hasOwnProperty(p))d[p]=b[p];}};
return function(d,b){
extendStatics(d,b);
function __(){this.constructor=d;}
d.prototype=b===null?Object.create(b):(__.prototype=b.prototype,new __());
};
}();
Object.defineProperty(exports,"__esModule",{value:true});
var format_1=__webpack_require__(18);
var Registry=__webpack_require__(1);
// Shallow object comparison
function isEqual(obj1,obj2){
if(Object.keys(obj1).length!==Object.keys(obj2).length)
return false;
// @ts-ignore
for(var prop in obj1){
// @ts-ignore
if(obj1[prop]!==obj2[prop])
return false;
}
return true;
}
var InlineBlot=/** @class */function(_super){
__extends(InlineBlot,_super);
function InlineBlot(){
return _super!==null&&_super.apply(this,arguments)||this;
}
InlineBlot.formats=function(domNode){
if(domNode.tagName===InlineBlot.tagName)
return undefined;
return _super.formats.call(this,domNode);
};
InlineBlot.prototype.format=function(name,value){
var _this=this;
if(name===this.statics.blotName&&!value){
this.children.forEach(function(child){
if(!(child instanceof format_1.default)){
child=child.wrap(InlineBlot.blotName,true);
}
_this.attributes.copy(child);
});
this.unwrap();
}else
{
_super.prototype.format.call(this,name,value);
}
};
InlineBlot.prototype.formatAt=function(index,length,name,value){
if(this.formats()[name]!=null||Registry.query(name,Registry.Scope.ATTRIBUTE)){
var blot=this.isolate(index,length);
blot.format(name,value);
}else
{
_super.prototype.formatAt.call(this,index,length,name,value);
}
};
InlineBlot.prototype.optimize=function(context){
_super.prototype.optimize.call(this,context);
var formats=this.formats();
if(Object.keys(formats).length===0){
return this.unwrap();// unformatted span
}
var next=this.next;
if(next instanceof InlineBlot&&next.prev===this&&isEqual(formats,next.formats())){
next.moveChildren(this);
next.remove();
}
};
InlineBlot.blotName='inline';
InlineBlot.scope=Registry.Scope.INLINE_BLOT;
InlineBlot.tagName='SPAN';
return InlineBlot;
}(format_1.default);
exports.default=InlineBlot;


/***/},
/* 47 */
/***/function(module,exports,__webpack_require__){

var __extends=this&&this.__extends||function(){
var extendStatics=Object.setPrototypeOf||
{__proto__:[]}instanceof Array&&function(d,b){d.__proto__=b;}||
function(d,b){for(var p in b){if(b.hasOwnProperty(p))d[p]=b[p];}};
return function(d,b){
extendStatics(d,b);
function __(){this.constructor=d;}
d.prototype=b===null?Object.create(b):(__.prototype=b.prototype,new __());
};
}();
Object.defineProperty(exports,"__esModule",{value:true});
var format_1=__webpack_require__(18);
var Registry=__webpack_require__(1);
var BlockBlot=/** @class */function(_super){
__extends(BlockBlot,_super);
function BlockBlot(){
return _super!==null&&_super.apply(this,arguments)||this;
}
BlockBlot.formats=function(domNode){
var tagName=Registry.query(BlockBlot.blotName).tagName;
if(domNode.tagName===tagName)
return undefined;
return _super.formats.call(this,domNode);
};
BlockBlot.prototype.format=function(name,value){
if(Registry.query(name,Registry.Scope.BLOCK)==null){
return;
}else
if(name===this.statics.blotName&&!value){
this.replaceWith(BlockBlot.blotName);
}else
{
_super.prototype.format.call(this,name,value);
}
};
BlockBlot.prototype.formatAt=function(index,length,name,value){
if(Registry.query(name,Registry.Scope.BLOCK)!=null){
this.format(name,value);
}else
{
_super.prototype.formatAt.call(this,index,length,name,value);
}
};
BlockBlot.prototype.insertAt=function(index,value,def){
if(def==null||Registry.query(value,Registry.Scope.INLINE)!=null){
// Insert text or inline
_super.prototype.insertAt.call(this,index,value,def);
}else
{
var after=this.split(index);
var blot=Registry.create(value,def);
after.parent.insertBefore(blot,after);
}
};
BlockBlot.prototype.update=function(mutations,context){
if(navigator.userAgent.match(/Trident/)){
this.build();
}else
{
_super.prototype.update.call(this,mutations,context);
}
};
BlockBlot.blotName='block';
BlockBlot.scope=Registry.Scope.BLOCK_BLOT;
BlockBlot.tagName='P';
return BlockBlot;
}(format_1.default);
exports.default=BlockBlot;


/***/},
/* 48 */
/***/function(module,exports,__webpack_require__){

var __extends=this&&this.__extends||function(){
var extendStatics=Object.setPrototypeOf||
{__proto__:[]}instanceof Array&&function(d,b){d.__proto__=b;}||
function(d,b){for(var p in b){if(b.hasOwnProperty(p))d[p]=b[p];}};
return function(d,b){
extendStatics(d,b);
function __(){this.constructor=d;}
d.prototype=b===null?Object.create(b):(__.prototype=b.prototype,new __());
};
}();
Object.defineProperty(exports,"__esModule",{value:true});
var leaf_1=__webpack_require__(19);
var EmbedBlot=/** @class */function(_super){
__extends(EmbedBlot,_super);
function EmbedBlot(){
return _super!==null&&_super.apply(this,arguments)||this;
}
EmbedBlot.formats=function(domNode){
return undefined;
};
EmbedBlot.prototype.format=function(name,value){
// super.formatAt wraps, which is what we want in general,
// but this allows subclasses to overwrite for formats
// that just apply to particular embeds
_super.prototype.formatAt.call(this,0,this.length(),name,value);
};
EmbedBlot.prototype.formatAt=function(index,length,name,value){
if(index===0&&length===this.length()){
this.format(name,value);
}else
{
_super.prototype.formatAt.call(this,index,length,name,value);
}
};
EmbedBlot.prototype.formats=function(){
return this.statics.formats(this.domNode);
};
return EmbedBlot;
}(leaf_1.default);
exports.default=EmbedBlot;


/***/},
/* 49 */
/***/function(module,exports,__webpack_require__){

var __extends=this&&this.__extends||function(){
var extendStatics=Object.setPrototypeOf||
{__proto__:[]}instanceof Array&&function(d,b){d.__proto__=b;}||
function(d,b){for(var p in b){if(b.hasOwnProperty(p))d[p]=b[p];}};
return function(d,b){
extendStatics(d,b);
function __(){this.constructor=d;}
d.prototype=b===null?Object.create(b):(__.prototype=b.prototype,new __());
};
}();
Object.defineProperty(exports,"__esModule",{value:true});
var leaf_1=__webpack_require__(19);
var Registry=__webpack_require__(1);
var TextBlot=/** @class */function(_super){
__extends(TextBlot,_super);
function TextBlot(node){
var _this=_super.call(this,node)||this;
_this.text=_this.statics.value(_this.domNode);
return _this;
}
TextBlot.create=function(value){
return document.createTextNode(value);
};
TextBlot.value=function(domNode){
var text=domNode.data;
// @ts-ignore
if(text['normalize'])
text=text['normalize']();
return text;
};
TextBlot.prototype.deleteAt=function(index,length){
this.domNode.data=this.text=this.text.slice(0,index)+this.text.slice(index+length);
};
TextBlot.prototype.index=function(node,offset){
if(this.domNode===node){
return offset;
}
return -1;
};
TextBlot.prototype.insertAt=function(index,value,def){
if(def==null){
this.text=this.text.slice(0,index)+value+this.text.slice(index);
this.domNode.data=this.text;
}else
{
_super.prototype.insertAt.call(this,index,value,def);
}
};
TextBlot.prototype.length=function(){
return this.text.length;
};
TextBlot.prototype.optimize=function(context){
_super.prototype.optimize.call(this,context);
this.text=this.statics.value(this.domNode);
if(this.text.length===0){
this.remove();
}else
if(this.next instanceof TextBlot&&this.next.prev===this){
this.insertAt(this.length(),this.next.value());
this.next.remove();
}
};
TextBlot.prototype.position=function(index,inclusive){
return [this.domNode,index];
};
TextBlot.prototype.split=function(index,force){
if(force===void 0){force=false;}
if(!force){
if(index===0)
return this;
if(index===this.length())
return this.next;
}
var after=Registry.create(this.domNode.splitText(index));
this.parent.insertBefore(after,this.next);
this.text=this.statics.value(this.domNode);
return after;
};
TextBlot.prototype.update=function(mutations,context){
var _this=this;
if(mutations.some(function(mutation){
return mutation.type==='characterData'&&mutation.target===_this.domNode;
})){
this.text=this.statics.value(this.domNode);
}
};
TextBlot.prototype.value=function(){
return this.text;
};
TextBlot.blotName='text';
TextBlot.scope=Registry.Scope.INLINE_BLOT;
return TextBlot;
}(leaf_1.default);
exports.default=TextBlot;


/***/},
/* 50 */
/***/function(module,exports,__webpack_require__){


var elem=document.createElement('div');
elem.classList.toggle('test-class',false);
if(elem.classList.contains('test-class')){
var _toggle=DOMTokenList.prototype.toggle;
DOMTokenList.prototype.toggle=function(token,force){
if(arguments.length>1&&!this.contains(token)===!force){
return force;
}else {
return _toggle.call(this,token);
}
};
}

if(!String.prototype.startsWith){
String.prototype.startsWith=function(searchString,position){
position=position||0;
return this.substr(position,searchString.length)===searchString;
};
}

if(!String.prototype.endsWith){
String.prototype.endsWith=function(searchString,position){
var subjectString=this.toString();
if(typeof position!=='number'||!isFinite(position)||Math.floor(position)!==position||position>subjectString.length){
position=subjectString.length;
}
position-=searchString.length;
var lastIndex=subjectString.indexOf(searchString,position);
return lastIndex!==-1&&lastIndex===position;
};
}

if(!Array.prototype.find){
Object.defineProperty(Array.prototype,"find",{
value:function value(predicate){
if(this===null){
throw new TypeError('Array.prototype.find called on null or undefined');
}
if(typeof predicate!=='function'){
throw new TypeError('predicate must be a function');
}
var list=Object(this);
var length=list.length>>>0;
var thisArg=arguments[1];
var value;

for(var i=0;i<length;i++){
value=list[i];
if(predicate.call(thisArg,value,i,list)){
return value;
}
}
return undefined;
}});

}

document.addEventListener("DOMContentLoaded",function(){
// Disable resizing in Firefox
document.execCommand("enableObjectResizing",false,false);
// Disable automatic linkifying in IE11
document.execCommand("autoUrlDetect",false,false);
});

/***/},
/* 51 */
/***/function(module,exports){

/**
 * This library modifies the diff-patch-match library by Neil Fraser
 * by removing the patch and match functionality and certain advanced
 * options in the diff function. The original license is as follows:
 *
 * ===
 *
 * Diff Match and Patch
 *
 * Copyright 2006 Google Inc.
 * http://code.google.com/p/google-diff-match-patch/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * The data structure representing a diff is an array of tuples:
 * [[DIFF_DELETE, 'Hello'], [DIFF_INSERT, 'Goodbye'], [DIFF_EQUAL, ' world.']]
 * which means: delete 'Hello', add 'Goodbye' and keep ' world.'
 */
var DIFF_DELETE=-1;
var DIFF_INSERT=1;
var DIFF_EQUAL=0;


/**
 * Find the differences between two texts.  Simplifies the problem by stripping
 * any common prefix or suffix off the texts before diffing.
 * @param {string} text1 Old string to be diffed.
 * @param {string} text2 New string to be diffed.
 * @param {Int} cursor_pos Expected edit position in text1 (optional)
 * @return {Array} Array of diff tuples.
 */
function diff_main(text1,text2,cursor_pos){
// Check for equality (speedup).
if(text1==text2){
if(text1){
return [[DIFF_EQUAL,text1]];
}
return [];
}

// Check cursor_pos within bounds
if(cursor_pos<0||text1.length<cursor_pos){
cursor_pos=null;
}

// Trim off common prefix (speedup).
var commonlength=diff_commonPrefix(text1,text2);
var commonprefix=text1.substring(0,commonlength);
text1=text1.substring(commonlength);
text2=text2.substring(commonlength);

// Trim off common suffix (speedup).
commonlength=diff_commonSuffix(text1,text2);
var commonsuffix=text1.substring(text1.length-commonlength);
text1=text1.substring(0,text1.length-commonlength);
text2=text2.substring(0,text2.length-commonlength);

// Compute the diff on the middle block.
var diffs=diff_compute_(text1,text2);

// Restore the prefix and suffix.
if(commonprefix){
diffs.unshift([DIFF_EQUAL,commonprefix]);
}
if(commonsuffix){
diffs.push([DIFF_EQUAL,commonsuffix]);
}
diff_cleanupMerge(diffs);
if(cursor_pos!=null){
diffs=fix_cursor(diffs,cursor_pos);
}
diffs=fix_emoji(diffs);
return diffs;
}

/**
 * Find the differences between two texts.  Assumes that the texts do not
 * have any common prefix or suffix.
 * @param {string} text1 Old string to be diffed.
 * @param {string} text2 New string to be diffed.
 * @return {Array} Array of diff tuples.
 */
function diff_compute_(text1,text2){
var diffs;

if(!text1){
// Just add some text (speedup).
return [[DIFF_INSERT,text2]];
}

if(!text2){
// Just delete some text (speedup).
return [[DIFF_DELETE,text1]];
}

var longtext=text1.length>text2.length?text1:text2;
var shorttext=text1.length>text2.length?text2:text1;
var i=longtext.indexOf(shorttext);
if(i!=-1){
// Shorter text is inside the longer text (speedup).
diffs=[[DIFF_INSERT,longtext.substring(0,i)],
[DIFF_EQUAL,shorttext],
[DIFF_INSERT,longtext.substring(i+shorttext.length)]];
// Swap insertions for deletions if diff is reversed.
if(text1.length>text2.length){
diffs[0][0]=diffs[2][0]=DIFF_DELETE;
}
return diffs;
}

if(shorttext.length==1){
// Single character string.
// After the previous speedup, the character can't be an equality.
return [[DIFF_DELETE,text1],[DIFF_INSERT,text2]];
}

// Check to see if the problem can be split in two.
var hm=diff_halfMatch_(text1,text2);
if(hm){
// A half-match was found, sort out the return data.
var text1_a=hm[0];
var text1_b=hm[1];
var text2_a=hm[2];
var text2_b=hm[3];
var mid_common=hm[4];
// Send both pairs off for separate processing.
var diffs_a=diff_main(text1_a,text2_a);
var diffs_b=diff_main(text1_b,text2_b);
// Merge the results.
return diffs_a.concat([[DIFF_EQUAL,mid_common]],diffs_b);
}

return diff_bisect_(text1,text2);
}

/**
 * Find the 'middle snake' of a diff, split the problem in two
 * and return the recursively constructed diff.
 * See Myers 1986 paper: An O(ND) Difference Algorithm and Its Variations.
 * @param {string} text1 Old string to be diffed.
 * @param {string} text2 New string to be diffed.
 * @return {Array} Array of diff tuples.
 * @private
 */
function diff_bisect_(text1,text2){
// Cache the text lengths to prevent multiple calls.
var text1_length=text1.length;
var text2_length=text2.length;
var max_d=Math.ceil((text1_length+text2_length)/2);
var v_offset=max_d;
var v_length=2*max_d;
var v1=new Array(v_length);
var v2=new Array(v_length);
// Setting all elements to -1 is faster in Chrome & Firefox than mixing
// integers and undefined.
for(var x=0;x<v_length;x++){
v1[x]=-1;
v2[x]=-1;
}
v1[v_offset+1]=0;
v2[v_offset+1]=0;
var delta=text1_length-text2_length;
// If the total number of characters is odd, then the front path will collide
// with the reverse path.
var front=delta%2!=0;
// Offsets for start and end of k loop.
// Prevents mapping of space beyond the grid.
var k1start=0;
var k1end=0;
var k2start=0;
var k2end=0;
for(var d=0;d<max_d;d++){
// Walk the front path one step.
for(var k1=-d+k1start;k1<=d-k1end;k1+=2){
var k1_offset=v_offset+k1;
var x1;
if(k1==-d||k1!=d&&v1[k1_offset-1]<v1[k1_offset+1]){
x1=v1[k1_offset+1];
}else {
x1=v1[k1_offset-1]+1;
}
var y1=x1-k1;
while(x1<text1_length&&y1<text2_length&&
text1.charAt(x1)==text2.charAt(y1)){
x1++;
y1++;
}
v1[k1_offset]=x1;
if(x1>text1_length){
// Ran off the right of the graph.
k1end+=2;
}else if(y1>text2_length){
// Ran off the bottom of the graph.
k1start+=2;
}else if(front){
var k2_offset=v_offset+delta-k1;
if(k2_offset>=0&&k2_offset<v_length&&v2[k2_offset]!=-1){
// Mirror x2 onto top-left coordinate system.
var x2=text1_length-v2[k2_offset];
if(x1>=x2){
// Overlap detected.
return diff_bisectSplit_(text1,text2,x1,y1);
}
}
}
}

// Walk the reverse path one step.
for(var k2=-d+k2start;k2<=d-k2end;k2+=2){
var k2_offset=v_offset+k2;
var x2;
if(k2==-d||k2!=d&&v2[k2_offset-1]<v2[k2_offset+1]){
x2=v2[k2_offset+1];
}else {
x2=v2[k2_offset-1]+1;
}
var y2=x2-k2;
while(x2<text1_length&&y2<text2_length&&
text1.charAt(text1_length-x2-1)==
text2.charAt(text2_length-y2-1)){
x2++;
y2++;
}
v2[k2_offset]=x2;
if(x2>text1_length){
// Ran off the left of the graph.
k2end+=2;
}else if(y2>text2_length){
// Ran off the top of the graph.
k2start+=2;
}else if(!front){
var k1_offset=v_offset+delta-k2;
if(k1_offset>=0&&k1_offset<v_length&&v1[k1_offset]!=-1){
var x1=v1[k1_offset];
var y1=v_offset+x1-k1_offset;
// Mirror x2 onto top-left coordinate system.
x2=text1_length-x2;
if(x1>=x2){
// Overlap detected.
return diff_bisectSplit_(text1,text2,x1,y1);
}
}
}
}
}
// Diff took too long and hit the deadline or
// number of diffs equals number of characters, no commonality at all.
return [[DIFF_DELETE,text1],[DIFF_INSERT,text2]];
}

/**
 * Given the location of the 'middle snake', split the diff in two parts
 * and recurse.
 * @param {string} text1 Old string to be diffed.
 * @param {string} text2 New string to be diffed.
 * @param {number} x Index of split point in text1.
 * @param {number} y Index of split point in text2.
 * @return {Array} Array of diff tuples.
 */
function diff_bisectSplit_(text1,text2,x,y){
var text1a=text1.substring(0,x);
var text2a=text2.substring(0,y);
var text1b=text1.substring(x);
var text2b=text2.substring(y);

// Compute both diffs serially.
var diffs=diff_main(text1a,text2a);
var diffsb=diff_main(text1b,text2b);

return diffs.concat(diffsb);
}

/**
 * Determine the common prefix of two strings.
 * @param {string} text1 First string.
 * @param {string} text2 Second string.
 * @return {number} The number of characters common to the start of each
 *     string.
 */
function diff_commonPrefix(text1,text2){
// Quick check for common null cases.
if(!text1||!text2||text1.charAt(0)!=text2.charAt(0)){
return 0;
}
// Binary search.
// Performance analysis: http://neil.fraser.name/news/2007/10/09/
var pointermin=0;
var pointermax=Math.min(text1.length,text2.length);
var pointermid=pointermax;
var pointerstart=0;
while(pointermin<pointermid){
if(text1.substring(pointerstart,pointermid)==
text2.substring(pointerstart,pointermid)){
pointermin=pointermid;
pointerstart=pointermin;
}else {
pointermax=pointermid;
}
pointermid=Math.floor((pointermax-pointermin)/2+pointermin);
}
return pointermid;
}

/**
 * Determine the common suffix of two strings.
 * @param {string} text1 First string.
 * @param {string} text2 Second string.
 * @return {number} The number of characters common to the end of each string.
 */
function diff_commonSuffix(text1,text2){
// Quick check for common null cases.
if(!text1||!text2||
text1.charAt(text1.length-1)!=text2.charAt(text2.length-1)){
return 0;
}
// Binary search.
// Performance analysis: http://neil.fraser.name/news/2007/10/09/
var pointermin=0;
var pointermax=Math.min(text1.length,text2.length);
var pointermid=pointermax;
var pointerend=0;
while(pointermin<pointermid){
if(text1.substring(text1.length-pointermid,text1.length-pointerend)==
text2.substring(text2.length-pointermid,text2.length-pointerend)){
pointermin=pointermid;
pointerend=pointermin;
}else {
pointermax=pointermid;
}
pointermid=Math.floor((pointermax-pointermin)/2+pointermin);
}
return pointermid;
}

/**
 * Do the two texts share a substring which is at least half the length of the
 * longer text?
 * This speedup can produce non-minimal diffs.
 * @param {string} text1 First string.
 * @param {string} text2 Second string.
 * @return {Array.<string>} Five element Array, containing the prefix of
 *     text1, the suffix of text1, the prefix of text2, the suffix of
 *     text2 and the common middle.  Or null if there was no match.
 */
function diff_halfMatch_(text1,text2){
var longtext=text1.length>text2.length?text1:text2;
var shorttext=text1.length>text2.length?text2:text1;
if(longtext.length<4||shorttext.length*2<longtext.length){
return null;// Pointless.
}

/**
   * Does a substring of shorttext exist within longtext such that the substring
   * is at least half the length of longtext?
   * Closure, but does not reference any external variables.
   * @param {string} longtext Longer string.
   * @param {string} shorttext Shorter string.
   * @param {number} i Start index of quarter length substring within longtext.
   * @return {Array.<string>} Five element Array, containing the prefix of
   *     longtext, the suffix of longtext, the prefix of shorttext, the suffix
   *     of shorttext and the common middle.  Or null if there was no match.
   * @private
   */
function diff_halfMatchI_(longtext,shorttext,i){
// Start with a 1/4 length substring at position i as a seed.
var seed=longtext.substring(i,i+Math.floor(longtext.length/4));
var j=-1;
var best_common='';
var best_longtext_a,best_longtext_b,best_shorttext_a,best_shorttext_b;
while((j=shorttext.indexOf(seed,j+1))!=-1){
var prefixLength=diff_commonPrefix(longtext.substring(i),
shorttext.substring(j));
var suffixLength=diff_commonSuffix(longtext.substring(0,i),
shorttext.substring(0,j));
if(best_common.length<suffixLength+prefixLength){
best_common=shorttext.substring(j-suffixLength,j)+
shorttext.substring(j,j+prefixLength);
best_longtext_a=longtext.substring(0,i-suffixLength);
best_longtext_b=longtext.substring(i+prefixLength);
best_shorttext_a=shorttext.substring(0,j-suffixLength);
best_shorttext_b=shorttext.substring(j+prefixLength);
}
}
if(best_common.length*2>=longtext.length){
return [best_longtext_a,best_longtext_b,
best_shorttext_a,best_shorttext_b,best_common];
}else {
return null;
}
}

// First check if the second quarter is the seed for a half-match.
var hm1=diff_halfMatchI_(longtext,shorttext,
Math.ceil(longtext.length/4));
// Check again based on the third quarter.
var hm2=diff_halfMatchI_(longtext,shorttext,
Math.ceil(longtext.length/2));
var hm;
if(!hm1&&!hm2){
return null;
}else if(!hm2){
hm=hm1;
}else if(!hm1){
hm=hm2;
}else {
// Both matched.  Select the longest.
hm=hm1[4].length>hm2[4].length?hm1:hm2;
}

// A half-match was found, sort out the return data.
var text1_a,text1_b,text2_a,text2_b;
if(text1.length>text2.length){
text1_a=hm[0];
text1_b=hm[1];
text2_a=hm[2];
text2_b=hm[3];
}else {
text2_a=hm[0];
text2_b=hm[1];
text1_a=hm[2];
text1_b=hm[3];
}
var mid_common=hm[4];
return [text1_a,text1_b,text2_a,text2_b,mid_common];
}

/**
 * Reorder and merge like edit sections.  Merge equalities.
 * Any edit section can move as long as it doesn't cross an equality.
 * @param {Array} diffs Array of diff tuples.
 */
function diff_cleanupMerge(diffs){
diffs.push([DIFF_EQUAL,'']);// Add a dummy entry at the end.
var pointer=0;
var count_delete=0;
var count_insert=0;
var text_delete='';
var text_insert='';
var commonlength;
while(pointer<diffs.length){
switch(diffs[pointer][0]){
case DIFF_INSERT:
count_insert++;
text_insert+=diffs[pointer][1];
pointer++;
break;
case DIFF_DELETE:
count_delete++;
text_delete+=diffs[pointer][1];
pointer++;
break;
case DIFF_EQUAL:
// Upon reaching an equality, check for prior redundancies.
if(count_delete+count_insert>1){
if(count_delete!==0&&count_insert!==0){
// Factor out any common prefixies.
commonlength=diff_commonPrefix(text_insert,text_delete);
if(commonlength!==0){
if(pointer-count_delete-count_insert>0&&
diffs[pointer-count_delete-count_insert-1][0]==
DIFF_EQUAL){
diffs[pointer-count_delete-count_insert-1][1]+=
text_insert.substring(0,commonlength);
}else {
diffs.splice(0,0,[DIFF_EQUAL,
text_insert.substring(0,commonlength)]);
pointer++;
}
text_insert=text_insert.substring(commonlength);
text_delete=text_delete.substring(commonlength);
}
// Factor out any common suffixies.
commonlength=diff_commonSuffix(text_insert,text_delete);
if(commonlength!==0){
diffs[pointer][1]=text_insert.substring(text_insert.length-
commonlength)+diffs[pointer][1];
text_insert=text_insert.substring(0,text_insert.length-
commonlength);
text_delete=text_delete.substring(0,text_delete.length-
commonlength);
}
}
// Delete the offending records and add the merged ones.
if(count_delete===0){
diffs.splice(pointer-count_insert,
count_delete+count_insert,[DIFF_INSERT,text_insert]);
}else if(count_insert===0){
diffs.splice(pointer-count_delete,
count_delete+count_insert,[DIFF_DELETE,text_delete]);
}else {
diffs.splice(pointer-count_delete-count_insert,
count_delete+count_insert,[DIFF_DELETE,text_delete],
[DIFF_INSERT,text_insert]);
}
pointer=pointer-count_delete-count_insert+(
count_delete?1:0)+(count_insert?1:0)+1;
}else if(pointer!==0&&diffs[pointer-1][0]==DIFF_EQUAL){
// Merge this equality with the previous one.
diffs[pointer-1][1]+=diffs[pointer][1];
diffs.splice(pointer,1);
}else {
pointer++;
}
count_insert=0;
count_delete=0;
text_delete='';
text_insert='';
break;}

}
if(diffs[diffs.length-1][1]===''){
diffs.pop();// Remove the dummy entry at the end.
}

// Second pass: look for single edits surrounded on both sides by equalities
// which can be shifted sideways to eliminate an equality.
// e.g: A<ins>BA</ins>C -> <ins>AB</ins>AC
var changes=false;
pointer=1;
// Intentionally ignore the first and last element (don't need checking).
while(pointer<diffs.length-1){
if(diffs[pointer-1][0]==DIFF_EQUAL&&
diffs[pointer+1][0]==DIFF_EQUAL){
// This is a single edit surrounded by equalities.
if(diffs[pointer][1].substring(diffs[pointer][1].length-
diffs[pointer-1][1].length)==diffs[pointer-1][1]){
// Shift the edit over the previous equality.
diffs[pointer][1]=diffs[pointer-1][1]+
diffs[pointer][1].substring(0,diffs[pointer][1].length-
diffs[pointer-1][1].length);
diffs[pointer+1][1]=diffs[pointer-1][1]+diffs[pointer+1][1];
diffs.splice(pointer-1,1);
changes=true;
}else if(diffs[pointer][1].substring(0,diffs[pointer+1][1].length)==
diffs[pointer+1][1]){
// Shift the edit over the next equality.
diffs[pointer-1][1]+=diffs[pointer+1][1];
diffs[pointer][1]=
diffs[pointer][1].substring(diffs[pointer+1][1].length)+
diffs[pointer+1][1];
diffs.splice(pointer+1,1);
changes=true;
}
}
pointer++;
}
// If shifts were made, the diff needs reordering and another shift sweep.
if(changes){
diff_cleanupMerge(diffs);
}
}

var diff=diff_main;
diff.INSERT=DIFF_INSERT;
diff.DELETE=DIFF_DELETE;
diff.EQUAL=DIFF_EQUAL;

module.exports=diff;

/*
 * Modify a diff such that the cursor position points to the start of a change:
 * E.g.
 *   cursor_normalize_diff([[DIFF_EQUAL, 'abc']], 1)
 *     => [1, [[DIFF_EQUAL, 'a'], [DIFF_EQUAL, 'bc']]]
 *   cursor_normalize_diff([[DIFF_INSERT, 'new'], [DIFF_DELETE, 'xyz']], 2)
 *     => [2, [[DIFF_INSERT, 'new'], [DIFF_DELETE, 'xy'], [DIFF_DELETE, 'z']]]
 *
 * @param {Array} diffs Array of diff tuples
 * @param {Int} cursor_pos Suggested edit position. Must not be out of bounds!
 * @return {Array} A tuple [cursor location in the modified diff, modified diff]
 */
function cursor_normalize_diff(diffs,cursor_pos){
if(cursor_pos===0){
return [DIFF_EQUAL,diffs];
}
for(var current_pos=0,i=0;i<diffs.length;i++){
var d=diffs[i];
if(d[0]===DIFF_DELETE||d[0]===DIFF_EQUAL){
var next_pos=current_pos+d[1].length;
if(cursor_pos===next_pos){
return [i+1,diffs];
}else if(cursor_pos<next_pos){
// copy to prevent side effects
diffs=diffs.slice();
// split d into two diff changes
var split_pos=cursor_pos-current_pos;
var d_left=[d[0],d[1].slice(0,split_pos)];
var d_right=[d[0],d[1].slice(split_pos)];
diffs.splice(i,1,d_left,d_right);
return [i+1,diffs];
}else {
current_pos=next_pos;
}
}
}
throw new Error('cursor_pos is out of bounds!');
}

/*
 * Modify a diff such that the edit position is "shifted" to the proposed edit location (cursor_position).
 *
 * Case 1)
 *   Check if a naive shift is possible:
 *     [0, X], [ 1, Y] -> [ 1, Y], [0, X]    (if X + Y === Y + X)
 *     [0, X], [-1, Y] -> [-1, Y], [0, X]    (if X + Y === Y + X) - holds same result
 * Case 2)
 *   Check if the following shifts are possible:
 *     [0, 'pre'], [ 1, 'prefix'] -> [ 1, 'pre'], [0, 'pre'], [ 1, 'fix']
 *     [0, 'pre'], [-1, 'prefix'] -> [-1, 'pre'], [0, 'pre'], [-1, 'fix']
 *         ^            ^
 *         d          d_next
 *
 * @param {Array} diffs Array of diff tuples
 * @param {Int} cursor_pos Suggested edit position. Must not be out of bounds!
 * @return {Array} Array of diff tuples
 */
function fix_cursor(diffs,cursor_pos){
var norm=cursor_normalize_diff(diffs,cursor_pos);
var ndiffs=norm[1];
var cursor_pointer=norm[0];
var d=ndiffs[cursor_pointer];
var d_next=ndiffs[cursor_pointer+1];

if(d==null){
// Text was deleted from end of original string,
// cursor is now out of bounds in new string
return diffs;
}else if(d[0]!==DIFF_EQUAL){
// A modification happened at the cursor location.
// This is the expected outcome, so we can return the original diff.
return diffs;
}else {
if(d_next!=null&&d[1]+d_next[1]===d_next[1]+d[1]){
// Case 1)
// It is possible to perform a naive shift
ndiffs.splice(cursor_pointer,2,d_next,d);
return merge_tuples(ndiffs,cursor_pointer,2);
}else if(d_next!=null&&d_next[1].indexOf(d[1])===0){
// Case 2)
// d[1] is a prefix of d_next[1]
// We can assume that d_next[0] !== 0, since d[0] === 0
// Shift edit locations..
ndiffs.splice(cursor_pointer,2,[d_next[0],d[1]],[0,d[1]]);
var suffix=d_next[1].slice(d[1].length);
if(suffix.length>0){
ndiffs.splice(cursor_pointer+2,0,[d_next[0],suffix]);
}
return merge_tuples(ndiffs,cursor_pointer,3);
}else {
// Not possible to perform any modification
return diffs;
}
}
}

/*
 * Check diff did not split surrogate pairs.
 * Ex. [0, '\uD83D'], [-1, '\uDC36'], [1, '\uDC2F'] -> [-1, '\uD83D\uDC36'], [1, '\uD83D\uDC2F']
 *     '\uD83D\uDC36' === '🐶', '\uD83D\uDC2F' === '🐯'
 *
 * @param {Array} diffs Array of diff tuples
 * @return {Array} Array of diff tuples
 */
function fix_emoji(diffs){
var compact=false;
var starts_with_pair_end=function starts_with_pair_end(str){
return str.charCodeAt(0)>=0xDC00&&str.charCodeAt(0)<=0xDFFF;
};
var ends_with_pair_start=function ends_with_pair_start(str){
return str.charCodeAt(str.length-1)>=0xD800&&str.charCodeAt(str.length-1)<=0xDBFF;
};
for(var i=2;i<diffs.length;i+=1){
if(diffs[i-2][0]===DIFF_EQUAL&&ends_with_pair_start(diffs[i-2][1])&&
diffs[i-1][0]===DIFF_DELETE&&starts_with_pair_end(diffs[i-1][1])&&
diffs[i][0]===DIFF_INSERT&&starts_with_pair_end(diffs[i][1])){
compact=true;

diffs[i-1][1]=diffs[i-2][1].slice(-1)+diffs[i-1][1];
diffs[i][1]=diffs[i-2][1].slice(-1)+diffs[i][1];

diffs[i-2][1]=diffs[i-2][1].slice(0,-1);
}
}
if(!compact){
return diffs;
}
var fixed_diffs=[];
for(var i=0;i<diffs.length;i+=1){
if(diffs[i][1].length>0){
fixed_diffs.push(diffs[i]);
}
}
return fixed_diffs;
}

/*
 * Try to merge tuples with their neigbors in a given range.
 * E.g. [0, 'a'], [0, 'b'] -> [0, 'ab']
 *
 * @param {Array} diffs Array of diff tuples.
 * @param {Int} start Position of the first element to merge (diffs[start] is also merged with diffs[start - 1]).
 * @param {Int} length Number of consecutive elements to check.
 * @return {Array} Array of merged diff tuples.
 */
function merge_tuples(diffs,start,length){
// Check from (start-1) to (start+length).
for(var i=start+length-1;i>=0&&i>=start-1;i--){
if(i+1<diffs.length){
var left_d=diffs[i];
var right_d=diffs[i+1];
if(left_d[0]===right_d[1]){
diffs.splice(i,2,[left_d[0],left_d[1]+right_d[1]]);
}
}
}
return diffs;
}


/***/},
/* 52 */
/***/function(module,exports){

exports=module.exports=typeof Object.keys==='function'?
Object.keys:shim;

exports.shim=shim;
function shim(obj){
var keys=[];
for(var key in obj){keys.push(key);}
return keys;
}


/***/},
/* 53 */
/***/function(module,exports){

var supportsArgumentsClass=function(){
return Object.prototype.toString.call(arguments);
}()=='[object Arguments]';

exports=module.exports=supportsArgumentsClass?supported:unsupported;

exports.supported=supported;
function supported(object){
return Object.prototype.toString.call(object)=='[object Arguments]';
}
exports.unsupported=unsupported;
function unsupported(object){
return object&&
typeof object=='object'&&
typeof object.length=='number'&&
Object.prototype.hasOwnProperty.call(object,'callee')&&
!Object.prototype.propertyIsEnumerable.call(object,'callee')||
false;
}

/***/},
/* 54 */
/***/function(module,exports){

var has=Object.prototype.hasOwnProperty,
prefix='~';

/**
 * Constructor to create a storage for our `EE` objects.
 * An `Events` instance is a plain object whose properties are event names.
 *
 * @constructor
 * @api private
 */
function Events(){}

//
// We try to not inherit from `Object.prototype`. In some engines creating an
// instance in this way is faster than calling `Object.create(null)` directly.
// If `Object.create(null)` is not supported we prefix the event names with a
// character to make sure that the built-in object properties are not
// overridden or used as an attack vector.
//
if(Object.create){
Events.prototype=Object.create(null);

//
// This hack is needed because the `__proto__` property is still inherited in
// some old browsers like Android 4, iPhone 5.1, Opera 11 and Safari 5.
//
if(!new Events().__proto__)prefix=false;
}

/**
 * Representation of a single event listener.
 *
 * @param {Function} fn The listener function.
 * @param {Mixed} context The context to invoke the listener with.
 * @param {Boolean} [once=false] Specify if the listener is a one-time listener.
 * @constructor
 * @api private
 */
function EE(fn,context,once){
this.fn=fn;
this.context=context;
this.once=once||false;
}

/**
 * Minimal `EventEmitter` interface that is molded against the Node.js
 * `EventEmitter` interface.
 *
 * @constructor
 * @api public
 */
function EventEmitter(){
this._events=new Events();
this._eventsCount=0;
}

/**
 * Return an array listing the events for which the emitter has registered
 * listeners.
 *
 * @returns {Array}
 * @api public
 */
EventEmitter.prototype.eventNames=function eventNames(){
var names=[],
events,
name;

if(this._eventsCount===0)return names;

for(name in events=this._events){
if(has.call(events,name))names.push(prefix?name.slice(1):name);
}

if(Object.getOwnPropertySymbols){
return names.concat(Object.getOwnPropertySymbols(events));
}

return names;
};

/**
 * Return the listeners registered for a given event.
 *
 * @param {String|Symbol} event The event name.
 * @param {Boolean} exists Only check if there are listeners.
 * @returns {Array|Boolean}
 * @api public
 */
EventEmitter.prototype.listeners=function listeners(event,exists){
var evt=prefix?prefix+event:event,
available=this._events[evt];

if(exists)return !!available;
if(!available)return [];
if(available.fn)return [available.fn];

for(var i=0,l=available.length,ee=new Array(l);i<l;i++){
ee[i]=available[i].fn;
}

return ee;
};

/**
 * Calls each of the listeners registered for a given event.
 *
 * @param {String|Symbol} event The event name.
 * @returns {Boolean} `true` if the event had listeners, else `false`.
 * @api public
 */
EventEmitter.prototype.emit=function emit(event,a1,a2,a3,a4,a5){
var evt=prefix?prefix+event:event;

if(!this._events[evt])return false;

var listeners=this._events[evt],
len=arguments.length,
args,
i;

if(listeners.fn){
if(listeners.once)this.removeListener(event,listeners.fn,undefined,true);

switch(len){
case 1:return listeners.fn.call(listeners.context),true;
case 2:return listeners.fn.call(listeners.context,a1),true;
case 3:return listeners.fn.call(listeners.context,a1,a2),true;
case 4:return listeners.fn.call(listeners.context,a1,a2,a3),true;
case 5:return listeners.fn.call(listeners.context,a1,a2,a3,a4),true;
case 6:return listeners.fn.call(listeners.context,a1,a2,a3,a4,a5),true;}


for(i=1,args=new Array(len-1);i<len;i++){
args[i-1]=arguments[i];
}

listeners.fn.apply(listeners.context,args);
}else {
var length=listeners.length,
j;

for(i=0;i<length;i++){
if(listeners[i].once)this.removeListener(event,listeners[i].fn,undefined,true);

switch(len){
case 1:listeners[i].fn.call(listeners[i].context);break;
case 2:listeners[i].fn.call(listeners[i].context,a1);break;
case 3:listeners[i].fn.call(listeners[i].context,a1,a2);break;
case 4:listeners[i].fn.call(listeners[i].context,a1,a2,a3);break;
default:
if(!args)for(j=1,args=new Array(len-1);j<len;j++){
args[j-1]=arguments[j];
}

listeners[i].fn.apply(listeners[i].context,args);}

}
}

return true;
};

/**
 * Add a listener for a given event.
 *
 * @param {String|Symbol} event The event name.
 * @param {Function} fn The listener function.
 * @param {Mixed} [context=this] The context to invoke the listener with.
 * @returns {EventEmitter} `this`.
 * @api public
 */
EventEmitter.prototype.on=function on(event,fn,context){
var listener=new EE(fn,context||this),
evt=prefix?prefix+event:event;

if(!this._events[evt])this._events[evt]=listener,this._eventsCount++;else
if(!this._events[evt].fn)this._events[evt].push(listener);else
this._events[evt]=[this._events[evt],listener];

return this;
};

/**
 * Add a one-time listener for a given event.
 *
 * @param {String|Symbol} event The event name.
 * @param {Function} fn The listener function.
 * @param {Mixed} [context=this] The context to invoke the listener with.
 * @returns {EventEmitter} `this`.
 * @api public
 */
EventEmitter.prototype.once=function once(event,fn,context){
var listener=new EE(fn,context||this,true),
evt=prefix?prefix+event:event;

if(!this._events[evt])this._events[evt]=listener,this._eventsCount++;else
if(!this._events[evt].fn)this._events[evt].push(listener);else
this._events[evt]=[this._events[evt],listener];

return this;
};

/**
 * Remove the listeners of a given event.
 *
 * @param {String|Symbol} event The event name.
 * @param {Function} fn Only remove the listeners that match this function.
 * @param {Mixed} context Only remove the listeners that have this context.
 * @param {Boolean} once Only remove one-time listeners.
 * @returns {EventEmitter} `this`.
 * @api public
 */
EventEmitter.prototype.removeListener=function removeListener(event,fn,context,once){
var evt=prefix?prefix+event:event;

if(!this._events[evt])return this;
if(!fn){
if(--this._eventsCount===0)this._events=new Events();else
delete this._events[evt];
return this;
}

var listeners=this._events[evt];

if(listeners.fn){
if(
listeners.fn===fn&&(
!once||listeners.once)&&(
!context||listeners.context===context))
{
if(--this._eventsCount===0)this._events=new Events();else
delete this._events[evt];
}
}else {
for(var i=0,events=[],length=listeners.length;i<length;i++){
if(
listeners[i].fn!==fn||
once&&!listeners[i].once||
context&&listeners[i].context!==context)
{
events.push(listeners[i]);
}
}

//
// Reset the array, or remove it completely if we have no more listeners.
//
if(events.length)this._events[evt]=events.length===1?events[0]:events;else
if(--this._eventsCount===0)this._events=new Events();else
delete this._events[evt];
}

return this;
};

/**
 * Remove all listeners, or those of the specified event.
 *
 * @param {String|Symbol} [event] The event name.
 * @returns {EventEmitter} `this`.
 * @api public
 */
EventEmitter.prototype.removeAllListeners=function removeAllListeners(event){
var evt;

if(event){
evt=prefix?prefix+event:event;
if(this._events[evt]){
if(--this._eventsCount===0)this._events=new Events();else
delete this._events[evt];
}
}else {
this._events=new Events();
this._eventsCount=0;
}

return this;
};

//
// Alias methods names because people roll like that.
//
EventEmitter.prototype.off=EventEmitter.prototype.removeListener;
EventEmitter.prototype.addListener=EventEmitter.prototype.on;

//
// This function doesn't apply anymore.
//
EventEmitter.prototype.setMaxListeners=function setMaxListeners(){
return this;
};

//
// Expose the prefix.
//
EventEmitter.prefixed=prefix;

//
// Allow `EventEmitter` to be imported as module namespace.
//
EventEmitter.EventEmitter=EventEmitter;

//
// Expose the module.
//
if('undefined'!==typeof module){
module.exports=EventEmitter;
}


/***/},
/* 55 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.matchText=exports.matchSpacing=exports.matchNewline=exports.matchBlot=exports.matchAttributor=exports.default=undefined;

var _typeof=typeof Symbol==="function"&&typeof Symbol.iterator==="symbol"?function(obj){return typeof obj;}:function(obj){return obj&&typeof Symbol==="function"&&obj.constructor===Symbol&&obj!==Symbol.prototype?"symbol":typeof obj;};

var _slicedToArray=function(){function sliceIterator(arr,i){var _arr=[];var _n=true;var _d=false;var _e=undefined;try{for(var _i=arr[Symbol.iterator](),_s;!(_n=(_s=_i.next()).done);_n=true){_arr.push(_s.value);if(i&&_arr.length===i)break;}}catch(err){_d=true;_e=err;}finally{try{if(!_n&&_i["return"])_i["return"]();}finally{if(_d)throw _e;}}return _arr;}return function(arr,i){if(Array.isArray(arr)){return arr;}else if(Symbol.iterator in Object(arr)){return sliceIterator(arr,i);}else {throw new TypeError("Invalid attempt to destructure non-iterable instance");}};}();

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _extend2=__webpack_require__(3);

var _extend3=_interopRequireDefault(_extend2);

var _quillDelta=__webpack_require__(2);

var _quillDelta2=_interopRequireDefault(_quillDelta);

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

var _quill=__webpack_require__(5);

var _quill2=_interopRequireDefault(_quill);

var _logger=__webpack_require__(10);

var _logger2=_interopRequireDefault(_logger);

var _module=__webpack_require__(9);

var _module2=_interopRequireDefault(_module);

var _align=__webpack_require__(36);

var _background=__webpack_require__(37);

var _code=__webpack_require__(13);

var _code2=_interopRequireDefault(_code);

var _color=__webpack_require__(26);

var _direction=__webpack_require__(38);

var _font=__webpack_require__(39);

var _size=__webpack_require__(40);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _defineProperty(obj,key,value){if(key in obj){Object.defineProperty(obj,key,{value:value,enumerable:true,configurable:true,writable:true});}else {obj[key]=value;}return obj;}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var debug=(0, _logger2.default)('quill:clipboard');

var DOM_KEY='__ql-matcher';

var CLIPBOARD_CONFIG=[[Node.TEXT_NODE,matchText],[Node.TEXT_NODE,matchNewline],['br',matchBreak],[Node.ELEMENT_NODE,matchNewline],[Node.ELEMENT_NODE,matchBlot],[Node.ELEMENT_NODE,matchSpacing],[Node.ELEMENT_NODE,matchAttributor],[Node.ELEMENT_NODE,matchStyles],['li',matchIndent],['b',matchAlias.bind(matchAlias,'bold')],['i',matchAlias.bind(matchAlias,'italic')],['style',matchIgnore]];

var ATTRIBUTE_ATTRIBUTORS=[_align.AlignAttribute,_direction.DirectionAttribute].reduce(function(memo,attr){
memo[attr.keyName]=attr;
return memo;
},{});

var STYLE_ATTRIBUTORS=[_align.AlignStyle,_background.BackgroundStyle,_color.ColorStyle,_direction.DirectionStyle,_font.FontStyle,_size.SizeStyle].reduce(function(memo,attr){
memo[attr.keyName]=attr;
return memo;
},{});

var Clipboard=function(_Module){
_inherits(Clipboard,_Module);

function Clipboard(quill,options){
_classCallCheck(this,Clipboard);

var _this=_possibleConstructorReturn(this,(Clipboard.__proto__||Object.getPrototypeOf(Clipboard)).call(this,quill,options));

_this.quill.root.addEventListener('paste',_this.onPaste.bind(_this));
_this.container=_this.quill.addContainer('ql-clipboard');
_this.container.setAttribute('contenteditable',true);
_this.container.setAttribute('tabindex',-1);
_this.matchers=[];
CLIPBOARD_CONFIG.concat(_this.options.matchers).forEach(function(_ref){
var _ref2=_slicedToArray(_ref,2),
selector=_ref2[0],
matcher=_ref2[1];

if(!options.matchVisual&&matcher===matchSpacing)return;
_this.addMatcher(selector,matcher);
});
return _this;
}

_createClass(Clipboard,[{
key:'addMatcher',
value:function addMatcher(selector,matcher){
this.matchers.push([selector,matcher]);
}},
{
key:'convert',
value:function convert(html){
if(typeof html==='string'){
this.container.innerHTML=html.replace(/\>\r?\n +\</g,'><');// Remove spaces between tags
return this.convert();
}
var formats=this.quill.getFormat(this.quill.selection.savedRange.index);
if(formats[_code2.default.blotName]){
var text=this.container.innerText;
this.container.innerHTML='';
return new _quillDelta2.default().insert(text,_defineProperty({},_code2.default.blotName,formats[_code2.default.blotName]));
}

var _prepareMatching=this.prepareMatching(),
_prepareMatching2=_slicedToArray(_prepareMatching,2),
elementMatchers=_prepareMatching2[0],
textMatchers=_prepareMatching2[1];

var delta=traverse(this.container,elementMatchers,textMatchers);
// Remove trailing newline
if(deltaEndsWith(delta,'\n')&&delta.ops[delta.ops.length-1].attributes==null){
delta=delta.compose(new _quillDelta2.default().retain(delta.length()-1).delete(1));
}
debug.log('convert',this.container.innerHTML,delta);
this.container.innerHTML='';
return delta;
}},
{
key:'dangerouslyPasteHTML',
value:function dangerouslyPasteHTML(index,html){
var source=arguments.length>2&&arguments[2]!==undefined?arguments[2]:_quill2.default.sources.API;

if(typeof index==='string'){
this.quill.setContents(this.convert(index),html);
this.quill.setSelection(0,_quill2.default.sources.SILENT);
}else {
var paste=this.convert(html);
this.quill.updateContents(new _quillDelta2.default().retain(index).concat(paste),source);
this.quill.setSelection(index+paste.length(),_quill2.default.sources.SILENT);
}
}},
{
key:'onPaste',
value:function onPaste(e){
var _this2=this;

if(e.defaultPrevented||!this.quill.isEnabled())return;
var range=this.quill.getSelection();
var delta=new _quillDelta2.default().retain(range.index);
var scrollTop=this.quill.scrollingContainer.scrollTop;
this.container.focus();
this.quill.selection.update(_quill2.default.sources.SILENT);
setTimeout(function(){
delta=delta.concat(_this2.convert()).delete(range.length);
_this2.quill.updateContents(delta,_quill2.default.sources.USER);
// range.length contributes to delta.length()
_this2.quill.setSelection(delta.length()-range.length,_quill2.default.sources.SILENT);
_this2.quill.scrollingContainer.scrollTop=scrollTop;
_this2.quill.focus();
},1);
}},
{
key:'prepareMatching',
value:function prepareMatching(){
var _this3=this;

var elementMatchers=[],
textMatchers=[];
this.matchers.forEach(function(pair){
var _pair=_slicedToArray(pair,2),
selector=_pair[0],
matcher=_pair[1];

switch(selector){
case Node.TEXT_NODE:
textMatchers.push(matcher);
break;
case Node.ELEMENT_NODE:
elementMatchers.push(matcher);
break;
default:
[].forEach.call(_this3.container.querySelectorAll(selector),function(node){
// TODO use weakmap
node[DOM_KEY]=node[DOM_KEY]||[];
node[DOM_KEY].push(matcher);
});
break;}

});
return [elementMatchers,textMatchers];
}}]);


return Clipboard;
}(_module2.default);

Clipboard.DEFAULTS={
matchers:[],
matchVisual:true};


function applyFormat(delta,format,value){
if((typeof format==='undefined'?'undefined':_typeof(format))==='object'){
return Object.keys(format).reduce(function(delta,key){
return applyFormat(delta,key,format[key]);
},delta);
}else {
return delta.reduce(function(delta,op){
if(op.attributes&&op.attributes[format]){
return delta.push(op);
}else {
return delta.insert(op.insert,(0, _extend3.default)({},_defineProperty({},format,value),op.attributes));
}
},new _quillDelta2.default());
}
}

function computeStyle(node){
if(node.nodeType!==Node.ELEMENT_NODE)return {};
var DOM_KEY='__ql-computed-style';
return node[DOM_KEY]||(node[DOM_KEY]=window.getComputedStyle(node));
}

function deltaEndsWith(delta,text){
var endText="";
for(var i=delta.ops.length-1;i>=0&&endText.length<text.length;--i){
var op=delta.ops[i];
if(typeof op.insert!=='string')break;
endText=op.insert+endText;
}
return endText.slice(-1*text.length)===text;
}

function isLine(node){
if(node.childNodes.length===0)return false;// Exclude embed blocks
var style=computeStyle(node);
return ['block','list-item'].indexOf(style.display)>-1;
}

function traverse(node,elementMatchers,textMatchers){
// Post-order
if(node.nodeType===node.TEXT_NODE){
return textMatchers.reduce(function(delta,matcher){
return matcher(node,delta);
},new _quillDelta2.default());
}else if(node.nodeType===node.ELEMENT_NODE){
return [].reduce.call(node.childNodes||[],function(delta,childNode){
var childrenDelta=traverse(childNode,elementMatchers,textMatchers);
if(childNode.nodeType===node.ELEMENT_NODE){
childrenDelta=elementMatchers.reduce(function(childrenDelta,matcher){
return matcher(childNode,childrenDelta);
},childrenDelta);
childrenDelta=(childNode[DOM_KEY]||[]).reduce(function(childrenDelta,matcher){
return matcher(childNode,childrenDelta);
},childrenDelta);
}
return delta.concat(childrenDelta);
},new _quillDelta2.default());
}else {
return new _quillDelta2.default();
}
}

function matchAlias(format,node,delta){
return applyFormat(delta,format,true);
}

function matchAttributor(node,delta){
var attributes=_parchment2.default.Attributor.Attribute.keys(node);
var classes=_parchment2.default.Attributor.Class.keys(node);
var styles=_parchment2.default.Attributor.Style.keys(node);
var formats={};
attributes.concat(classes).concat(styles).forEach(function(name){
var attr=_parchment2.default.query(name,_parchment2.default.Scope.ATTRIBUTE);
if(attr!=null){
formats[attr.attrName]=attr.value(node);
if(formats[attr.attrName])return;
}
attr=ATTRIBUTE_ATTRIBUTORS[name];
if(attr!=null&&(attr.attrName===name||attr.keyName===name)){
formats[attr.attrName]=attr.value(node)||undefined;
}
attr=STYLE_ATTRIBUTORS[name];
if(attr!=null&&(attr.attrName===name||attr.keyName===name)){
attr=STYLE_ATTRIBUTORS[name];
formats[attr.attrName]=attr.value(node)||undefined;
}
});
if(Object.keys(formats).length>0){
delta=applyFormat(delta,formats);
}
return delta;
}

function matchBlot(node,delta){
var match=_parchment2.default.query(node);
if(match==null)return delta;
if(match.prototype instanceof _parchment2.default.Embed){
var embed={};
var value=match.value(node);
if(value!=null){
embed[match.blotName]=value;
delta=new _quillDelta2.default().insert(embed,match.formats(node));
}
}else if(typeof match.formats==='function'){
delta=applyFormat(delta,match.blotName,match.formats(node));
}
return delta;
}

function matchBreak(node,delta){
if(!deltaEndsWith(delta,'\n')){
delta.insert('\n');
}
return delta;
}

function matchIgnore(){
return new _quillDelta2.default();
}

function matchIndent(node,delta){
var match=_parchment2.default.query(node);
if(match==null||match.blotName!=='list-item'||!deltaEndsWith(delta,'\n')){
return delta;
}
var indent=-1,
parent=node.parentNode;
while(!parent.classList.contains('ql-clipboard')){
if((_parchment2.default.query(parent)||{}).blotName==='list'){
indent+=1;
}
parent=parent.parentNode;
}
if(indent<=0)return delta;
return delta.compose(new _quillDelta2.default().retain(delta.length()-1).retain(1,{indent:indent}));
}

function matchNewline(node,delta){
if(!deltaEndsWith(delta,'\n')){
if(isLine(node)||delta.length()>0&&node.nextSibling&&isLine(node.nextSibling)){
delta.insert('\n');
}
}
return delta;
}

function matchSpacing(node,delta){
if(isLine(node)&&node.nextElementSibling!=null&&!deltaEndsWith(delta,'\n\n')){
var nodeHeight=node.offsetHeight+parseFloat(computeStyle(node).marginTop)+parseFloat(computeStyle(node).marginBottom);
if(node.nextElementSibling.offsetTop>node.offsetTop+nodeHeight*1.5){
delta.insert('\n');
}
}
return delta;
}

function matchStyles(node,delta){
var formats={};
var style=node.style||{};
if(style.fontStyle&&computeStyle(node).fontStyle==='italic'){
formats.italic=true;
}
if(style.fontWeight&&(computeStyle(node).fontWeight.startsWith('bold')||parseInt(computeStyle(node).fontWeight)>=700)){
formats.bold=true;
}
if(Object.keys(formats).length>0){
delta=applyFormat(delta,formats);
}
if(parseFloat(style.textIndent||0)>0){
// Could be 0.5in
delta=new _quillDelta2.default().insert('\t').concat(delta);
}
return delta;
}

function matchText(node,delta){
var text=node.data;
// Word represents empty line with <o:p>&nbsp;</o:p>
if(node.parentNode.tagName==='O:P'){
return delta.insert(text.trim());
}
if(text.trim().length===0&&node.parentNode.classList.contains('ql-clipboard')){
return delta;
}
if(!computeStyle(node.parentNode).whiteSpace.startsWith('pre')){
// eslint-disable-next-line func-style
var replacer=function replacer(collapse,match){
match=match.replace(/[^\u00a0]/g,'');// \u00a0 is nbsp;
return match.length<1&&collapse?' ':match;
};
text=text.replace(/\r\n/g,' ').replace(/\n/g,' ');
text=text.replace(/\s\s+/g,replacer.bind(replacer,true));// collapse whitespace
if(node.previousSibling==null&&isLine(node.parentNode)||node.previousSibling!=null&&isLine(node.previousSibling)){
text=text.replace(/^\s+/,replacer.bind(replacer,false));
}
if(node.nextSibling==null&&isLine(node.parentNode)||node.nextSibling!=null&&isLine(node.nextSibling)){
text=text.replace(/\s+$/,replacer.bind(replacer,false));
}
}
return delta.insert(text);
}

exports.default=Clipboard;
exports.matchAttributor=matchAttributor;
exports.matchBlot=matchBlot;
exports.matchNewline=matchNewline;
exports.matchSpacing=matchSpacing;
exports.matchText=matchText;

/***/},
/* 56 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _inline=__webpack_require__(6);

var _inline2=_interopRequireDefault(_inline);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var Bold=function(_Inline){
_inherits(Bold,_Inline);

function Bold(){
_classCallCheck(this,Bold);

return _possibleConstructorReturn(this,(Bold.__proto__||Object.getPrototypeOf(Bold)).apply(this,arguments));
}

_createClass(Bold,[{
key:'optimize',
value:function optimize(context){
_get(Bold.prototype.__proto__||Object.getPrototypeOf(Bold.prototype),'optimize',this).call(this,context);
if(this.domNode.tagName!==this.statics.tagName[0]){
this.replaceWith(this.statics.blotName);
}
}}],
[{
key:'create',
value:function create(){
return _get(Bold.__proto__||Object.getPrototypeOf(Bold),'create',this).call(this);
}},
{
key:'formats',
value:function formats(){
return true;
}}]);


return Bold;
}(_inline2.default);

Bold.blotName='bold';
Bold.tagName=['STRONG','B'];

exports.default=Bold;

/***/},
/* 57 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.addControls=exports.default=undefined;

var _slicedToArray=function(){function sliceIterator(arr,i){var _arr=[];var _n=true;var _d=false;var _e=undefined;try{for(var _i=arr[Symbol.iterator](),_s;!(_n=(_s=_i.next()).done);_n=true){_arr.push(_s.value);if(i&&_arr.length===i)break;}}catch(err){_d=true;_e=err;}finally{try{if(!_n&&_i["return"])_i["return"]();}finally{if(_d)throw _e;}}return _arr;}return function(arr,i){if(Array.isArray(arr)){return arr;}else if(Symbol.iterator in Object(arr)){return sliceIterator(arr,i);}else {throw new TypeError("Invalid attempt to destructure non-iterable instance");}};}();

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _quillDelta=__webpack_require__(2);

var _quillDelta2=_interopRequireDefault(_quillDelta);

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

var _quill=__webpack_require__(5);

var _quill2=_interopRequireDefault(_quill);

var _logger=__webpack_require__(10);

var _logger2=_interopRequireDefault(_logger);

var _module=__webpack_require__(9);

var _module2=_interopRequireDefault(_module);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _defineProperty(obj,key,value){if(key in obj){Object.defineProperty(obj,key,{value:value,enumerable:true,configurable:true,writable:true});}else {obj[key]=value;}return obj;}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var debug=(0, _logger2.default)('quill:toolbar');

var Toolbar=function(_Module){
_inherits(Toolbar,_Module);

function Toolbar(quill,options){
_classCallCheck(this,Toolbar);

var _this=_possibleConstructorReturn(this,(Toolbar.__proto__||Object.getPrototypeOf(Toolbar)).call(this,quill,options));

if(Array.isArray(_this.options.container)){
var container=document.createElement('div');
addControls(container,_this.options.container);
quill.container.parentNode.insertBefore(container,quill.container);
_this.container=container;
}else if(typeof _this.options.container==='string'){
_this.container=document.querySelector(_this.options.container);
}else {
_this.container=_this.options.container;
}
if(!(_this.container instanceof HTMLElement)){
var _ret;

return _ret=debug.error('Container required for toolbar',_this.options),_possibleConstructorReturn(_this,_ret);
}
_this.container.classList.add('ql-toolbar');
_this.controls=[];
_this.handlers={};
Object.keys(_this.options.handlers).forEach(function(format){
_this.addHandler(format,_this.options.handlers[format]);
});
[].forEach.call(_this.container.querySelectorAll('button, select'),function(input){
_this.attach(input);
});
_this.quill.on(_quill2.default.events.EDITOR_CHANGE,function(type,range){
if(type===_quill2.default.events.SELECTION_CHANGE){
_this.update(range);
}
});
_this.quill.on(_quill2.default.events.SCROLL_OPTIMIZE,function(){
var _this$quill$selection=_this.quill.selection.getRange(),
_this$quill$selection2=_slicedToArray(_this$quill$selection,1),
range=_this$quill$selection2[0];// quill.getSelection triggers update


_this.update(range);
});
return _this;
}

_createClass(Toolbar,[{
key:'addHandler',
value:function addHandler(format,handler){
this.handlers[format]=handler;
}},
{
key:'attach',
value:function attach(input){
var _this2=this;

var format=[].find.call(input.classList,function(className){
return className.indexOf('ql-')===0;
});
if(!format)return;
format=format.slice('ql-'.length);
if(input.tagName==='BUTTON'){
input.setAttribute('type','button');
}
if(this.handlers[format]==null){
if(this.quill.scroll.whitelist!=null&&this.quill.scroll.whitelist[format]==null){
debug.warn('ignoring attaching to disabled format',format,input);
return;
}
if(_parchment2.default.query(format)==null){
debug.warn('ignoring attaching to nonexistent format',format,input);
return;
}
}
var eventName=input.tagName==='SELECT'?'change':'click';
input.addEventListener(eventName,function(e){
var value=void 0;
if(input.tagName==='SELECT'){
if(input.selectedIndex<0)return;
var selected=input.options[input.selectedIndex];
if(selected.hasAttribute('selected')){
value=false;
}else {
value=selected.value||false;
}
}else {
if(input.classList.contains('ql-active')){
value=false;
}else {
value=input.value||!input.hasAttribute('value');
}
e.preventDefault();
}
_this2.quill.focus();

var _quill$selection$getR=_this2.quill.selection.getRange(),
_quill$selection$getR2=_slicedToArray(_quill$selection$getR,1),
range=_quill$selection$getR2[0];

if(_this2.handlers[format]!=null){
_this2.handlers[format].call(_this2,value);
}else if(_parchment2.default.query(format).prototype instanceof _parchment2.default.Embed){
value=prompt('Enter '+format);
if(!value)return;
_this2.quill.updateContents(new _quillDelta2.default().retain(range.index).delete(range.length).insert(_defineProperty({},format,value)),_quill2.default.sources.USER);
}else {
_this2.quill.format(format,value,_quill2.default.sources.USER);
}
_this2.update(range);
});
// TODO use weakmap
this.controls.push([format,input]);
}},
{
key:'update',
value:function update(range){
var formats=range==null?{}:this.quill.getFormat(range);
this.controls.forEach(function(pair){
var _pair=_slicedToArray(pair,2),
format=_pair[0],
input=_pair[1];

if(input.tagName==='SELECT'){
var option=void 0;
if(range==null){
option=null;
}else if(formats[format]==null){
option=input.querySelector('option[selected]');
}else if(!Array.isArray(formats[format])){
var value=formats[format];
if(typeof value==='string'){
value=value.replace(/\"/g,'\\"');
}
option=input.querySelector('option[value="'+value+'"]');
}
if(option==null){
input.value='';// TODO make configurable?
input.selectedIndex=-1;
}else {
option.selected=true;
}
}else {
if(range==null){
input.classList.remove('ql-active');
}else if(input.hasAttribute('value')){
// both being null should match (default values)
// '1' should match with 1 (headers)
var isActive=formats[format]===input.getAttribute('value')||formats[format]!=null&&formats[format].toString()===input.getAttribute('value')||formats[format]==null&&!input.getAttribute('value');
input.classList.toggle('ql-active',isActive);
}else {
input.classList.toggle('ql-active',formats[format]!=null);
}
}
});
}}]);


return Toolbar;
}(_module2.default);

Toolbar.DEFAULTS={};

function addButton(container,format,value){
var input=document.createElement('button');
input.setAttribute('type','button');
input.classList.add('ql-'+format);
if(value!=null){
input.value=value;
}
container.appendChild(input);
}

function addControls(container,groups){
if(!Array.isArray(groups[0])){
groups=[groups];
}
groups.forEach(function(controls){
var group=document.createElement('span');
group.classList.add('ql-formats');
controls.forEach(function(control){
if(typeof control==='string'){
addButton(group,control);
}else {
var format=Object.keys(control)[0];
var value=control[format];
if(Array.isArray(value)){
addSelect(group,format,value);
}else {
addButton(group,format,value);
}
}
});
container.appendChild(group);
});
}

function addSelect(container,format,values){
var input=document.createElement('select');
input.classList.add('ql-'+format);
values.forEach(function(value){
var option=document.createElement('option');
if(value!==false){
option.setAttribute('value',value);
}else {
option.setAttribute('selected','selected');
}
input.appendChild(option);
});
container.appendChild(input);
}

Toolbar.DEFAULTS={
container:null,
handlers:{
clean:function clean(){
var _this3=this;

var range=this.quill.getSelection();
if(range==null)return;
if(range.length==0){
var formats=this.quill.getFormat();
Object.keys(formats).forEach(function(name){
// Clean functionality in existing apps only clean inline formats
if(_parchment2.default.query(name,_parchment2.default.Scope.INLINE)!=null){
_this3.quill.format(name,false);
}
});
}else {
this.quill.removeFormat(range,_quill2.default.sources.USER);
}
},
direction:function direction(value){
var align=this.quill.getFormat()['align'];
if(value==='rtl'&&align==null){
this.quill.format('align','right',_quill2.default.sources.USER);
}else if(!value&&align==='right'){
this.quill.format('align',false,_quill2.default.sources.USER);
}
this.quill.format('direction',value,_quill2.default.sources.USER);
},
indent:function indent(value){
var range=this.quill.getSelection();
var formats=this.quill.getFormat(range);
var indent=parseInt(formats.indent||0);
if(value==='+1'||value==='-1'){
var modifier=value==='+1'?1:-1;
if(formats.direction==='rtl')modifier*=-1;
this.quill.format('indent',indent+modifier,_quill2.default.sources.USER);
}
},
link:function link(value){
if(value===true){
value=prompt('Enter link URL:');
}
this.quill.format('link',value,_quill2.default.sources.USER);
},
list:function list(value){
var range=this.quill.getSelection();
var formats=this.quill.getFormat(range);
if(value==='check'){
if(formats['list']==='checked'||formats['list']==='unchecked'){
this.quill.format('list',false,_quill2.default.sources.USER);
}else {
this.quill.format('list','unchecked',_quill2.default.sources.USER);
}
}else {
this.quill.format('list',value,_quill2.default.sources.USER);
}
}}};



exports.default=Toolbar;
exports.addControls=addControls;

/***/},
/* 58 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <polyline class=\"ql-even ql-stroke\" points=\"5 7 3 9 5 11\"></polyline> <polyline class=\"ql-even ql-stroke\" points=\"13 7 15 9 13 11\"></polyline> <line class=ql-stroke x1=10 x2=8 y1=5 y2=13></line> </svg>";

/***/},
/* 59 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _picker=__webpack_require__(28);

var _picker2=_interopRequireDefault(_picker);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var ColorPicker=function(_Picker){
_inherits(ColorPicker,_Picker);

function ColorPicker(select,label){
_classCallCheck(this,ColorPicker);

var _this=_possibleConstructorReturn(this,(ColorPicker.__proto__||Object.getPrototypeOf(ColorPicker)).call(this,select));

_this.label.innerHTML=label;
_this.container.classList.add('ql-color-picker');
[].slice.call(_this.container.querySelectorAll('.ql-picker-item'),0,7).forEach(function(item){
item.classList.add('ql-primary');
});
return _this;
}

_createClass(ColorPicker,[{
key:'buildItem',
value:function buildItem(option){
var item=_get(ColorPicker.prototype.__proto__||Object.getPrototypeOf(ColorPicker.prototype),'buildItem',this).call(this,option);
item.style.backgroundColor=option.getAttribute('value')||'';
return item;
}},
{
key:'selectItem',
value:function selectItem(item,trigger){
_get(ColorPicker.prototype.__proto__||Object.getPrototypeOf(ColorPicker.prototype),'selectItem',this).call(this,item,trigger);
var colorLabel=this.label.querySelector('.ql-color-label');
var value=item?item.getAttribute('data-value')||'':'';
if(colorLabel){
if(colorLabel.tagName==='line'){
colorLabel.style.stroke=value;
}else {
colorLabel.style.fill=value;
}
}
}}]);


return ColorPicker;
}(_picker2.default);

exports.default=ColorPicker;

/***/},
/* 60 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _picker=__webpack_require__(28);

var _picker2=_interopRequireDefault(_picker);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var IconPicker=function(_Picker){
_inherits(IconPicker,_Picker);

function IconPicker(select,icons){
_classCallCheck(this,IconPicker);

var _this=_possibleConstructorReturn(this,(IconPicker.__proto__||Object.getPrototypeOf(IconPicker)).call(this,select));

_this.container.classList.add('ql-icon-picker');
[].forEach.call(_this.container.querySelectorAll('.ql-picker-item'),function(item){
item.innerHTML=icons[item.getAttribute('data-value')||''];
});
_this.defaultItem=_this.container.querySelector('.ql-selected');
_this.selectItem(_this.defaultItem);
return _this;
}

_createClass(IconPicker,[{
key:'selectItem',
value:function selectItem(item,trigger){
_get(IconPicker.prototype.__proto__||Object.getPrototypeOf(IconPicker.prototype),'selectItem',this).call(this,item,trigger);
item=item||this.defaultItem;
this.label.innerHTML=item.innerHTML;
}}]);


return IconPicker;
}(_picker2.default);

exports.default=IconPicker;

/***/},
/* 61 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

var Tooltip=function(){
function Tooltip(quill,boundsContainer){
var _this=this;

_classCallCheck(this,Tooltip);

this.quill=quill;
this.boundsContainer=boundsContainer||document.body;
this.root=quill.addContainer('ql-tooltip');
this.root.innerHTML=this.constructor.TEMPLATE;
if(this.quill.root===this.quill.scrollingContainer){
this.quill.root.addEventListener('scroll',function(){
_this.root.style.marginTop=-1*_this.quill.root.scrollTop+'px';
});
}
this.hide();
}

_createClass(Tooltip,[{
key:'hide',
value:function hide(){
this.root.classList.add('ql-hidden');
}},
{
key:'position',
value:function position(reference){
var left=reference.left+reference.width/2-this.root.offsetWidth/2;
// root.scrollTop should be 0 if scrollContainer !== root
var top=reference.bottom+this.quill.root.scrollTop;
this.root.style.left=left+'px';
this.root.style.top=top+'px';
this.root.classList.remove('ql-flip');
var containerBounds=this.boundsContainer.getBoundingClientRect();
var rootBounds=this.root.getBoundingClientRect();
var shift=0;
if(rootBounds.right>containerBounds.right){
shift=containerBounds.right-rootBounds.right;
this.root.style.left=left+shift+'px';
}
if(rootBounds.left<containerBounds.left){
shift=containerBounds.left-rootBounds.left;
this.root.style.left=left+shift+'px';
}
if(rootBounds.bottom>containerBounds.bottom){
var height=rootBounds.bottom-rootBounds.top;
var verticalShift=reference.bottom-reference.top+height;
this.root.style.top=top-verticalShift+'px';
this.root.classList.add('ql-flip');
}
return shift;
}},
{
key:'show',
value:function show(){
this.root.classList.remove('ql-editing');
this.root.classList.remove('ql-hidden');
}}]);


return Tooltip;
}();

exports.default=Tooltip;

/***/},
/* 62 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _slicedToArray=function(){function sliceIterator(arr,i){var _arr=[];var _n=true;var _d=false;var _e=undefined;try{for(var _i=arr[Symbol.iterator](),_s;!(_n=(_s=_i.next()).done);_n=true){_arr.push(_s.value);if(i&&_arr.length===i)break;}}catch(err){_d=true;_e=err;}finally{try{if(!_n&&_i["return"])_i["return"]();}finally{if(_d)throw _e;}}return _arr;}return function(arr,i){if(Array.isArray(arr)){return arr;}else if(Symbol.iterator in Object(arr)){return sliceIterator(arr,i);}else {throw new TypeError("Invalid attempt to destructure non-iterable instance");}};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _extend=__webpack_require__(3);

var _extend2=_interopRequireDefault(_extend);

var _emitter=__webpack_require__(8);

var _emitter2=_interopRequireDefault(_emitter);

var _base=__webpack_require__(43);

var _base2=_interopRequireDefault(_base);

var _link=__webpack_require__(27);

var _link2=_interopRequireDefault(_link);

var _selection=__webpack_require__(15);

var _icons=__webpack_require__(41);

var _icons2=_interopRequireDefault(_icons);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var TOOLBAR_CONFIG=[[{header:['1','2','3',false]}],['bold','italic','underline','link'],[{list:'ordered'},{list:'bullet'}],['clean']];

var SnowTheme=function(_BaseTheme){
_inherits(SnowTheme,_BaseTheme);

function SnowTheme(quill,options){
_classCallCheck(this,SnowTheme);

if(options.modules.toolbar!=null&&options.modules.toolbar.container==null){
options.modules.toolbar.container=TOOLBAR_CONFIG;
}

var _this=_possibleConstructorReturn(this,(SnowTheme.__proto__||Object.getPrototypeOf(SnowTheme)).call(this,quill,options));

_this.quill.container.classList.add('ql-snow');
return _this;
}

_createClass(SnowTheme,[{
key:'extendToolbar',
value:function extendToolbar(toolbar){
toolbar.container.classList.add('ql-snow');
this.buildButtons([].slice.call(toolbar.container.querySelectorAll('button')),_icons2.default);
this.buildPickers([].slice.call(toolbar.container.querySelectorAll('select')),_icons2.default);
this.tooltip=new SnowTooltip(this.quill,this.options.bounds);
if(toolbar.container.querySelector('.ql-link')){
this.quill.keyboard.addBinding({key:'K',shortKey:true},function(range,context){
toolbar.handlers['link'].call(toolbar,!context.format.link);
});
}
}}]);


return SnowTheme;
}(_base2.default);

SnowTheme.DEFAULTS=(0, _extend2.default)(true,{},_base2.default.DEFAULTS,{
modules:{
toolbar:{
handlers:{
link:function link(value){
if(value){
var range=this.quill.getSelection();
if(range==null||range.length==0)return;
var preview=this.quill.getText(range);
if(/^\S+@\S+\.\S+$/.test(preview)&&preview.indexOf('mailto:')!==0){
preview='mailto:'+preview;
}
var tooltip=this.quill.theme.tooltip;
tooltip.edit('link',preview);
}else {
this.quill.format('link',false);
}
}}}}});





var SnowTooltip=function(_BaseTooltip){
_inherits(SnowTooltip,_BaseTooltip);

function SnowTooltip(quill,bounds){
_classCallCheck(this,SnowTooltip);

var _this2=_possibleConstructorReturn(this,(SnowTooltip.__proto__||Object.getPrototypeOf(SnowTooltip)).call(this,quill,bounds));

_this2.preview=_this2.root.querySelector('a.ql-preview');
return _this2;
}

_createClass(SnowTooltip,[{
key:'listen',
value:function listen(){
var _this3=this;

_get(SnowTooltip.prototype.__proto__||Object.getPrototypeOf(SnowTooltip.prototype),'listen',this).call(this);
this.root.querySelector('a.ql-action').addEventListener('click',function(event){
if(_this3.root.classList.contains('ql-editing')){
_this3.save();
}else {
_this3.edit('link',_this3.preview.textContent);
}
event.preventDefault();
});
this.root.querySelector('a.ql-remove').addEventListener('click',function(event){
if(_this3.linkRange!=null){
var range=_this3.linkRange;
_this3.restoreFocus();
_this3.quill.formatText(range,'link',false,_emitter2.default.sources.USER);
delete _this3.linkRange;
}
event.preventDefault();
_this3.hide();
});
this.quill.on(_emitter2.default.events.SELECTION_CHANGE,function(range,oldRange,source){
if(range==null)return;
if(range.length===0&&source===_emitter2.default.sources.USER){
var _quill$scroll$descend=_this3.quill.scroll.descendant(_link2.default,range.index),
_quill$scroll$descend2=_slicedToArray(_quill$scroll$descend,2),
link=_quill$scroll$descend2[0],
offset=_quill$scroll$descend2[1];

if(link!=null){
_this3.linkRange=new _selection.Range(range.index-offset,link.length());
var preview=_link2.default.formats(link.domNode);
_this3.preview.textContent=preview;
_this3.preview.setAttribute('href',preview);
_this3.show();
_this3.position(_this3.quill.getBounds(_this3.linkRange));
return;
}
}else {
delete _this3.linkRange;
}
_this3.hide();
});
}},
{
key:'show',
value:function show(){
_get(SnowTooltip.prototype.__proto__||Object.getPrototypeOf(SnowTooltip.prototype),'show',this).call(this);
this.root.removeAttribute('data-mode');
}}]);


return SnowTooltip;
}(_base.BaseTooltip);

SnowTooltip.TEMPLATE=['<a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a>','<input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL">','<a class="ql-action"></a>','<a class="ql-remove"></a>'].join('');

exports.default=SnowTheme;

/***/},
/* 63 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _core=__webpack_require__(29);

var _core2=_interopRequireDefault(_core);

var _align=__webpack_require__(36);

var _direction=__webpack_require__(38);

var _indent=__webpack_require__(64);

var _blockquote=__webpack_require__(65);

var _blockquote2=_interopRequireDefault(_blockquote);

var _header=__webpack_require__(66);

var _header2=_interopRequireDefault(_header);

var _list=__webpack_require__(67);

var _list2=_interopRequireDefault(_list);

var _background=__webpack_require__(37);

var _color=__webpack_require__(26);

var _font=__webpack_require__(39);

var _size=__webpack_require__(40);

var _bold=__webpack_require__(56);

var _bold2=_interopRequireDefault(_bold);

var _italic=__webpack_require__(68);

var _italic2=_interopRequireDefault(_italic);

var _link=__webpack_require__(27);

var _link2=_interopRequireDefault(_link);

var _script=__webpack_require__(69);

var _script2=_interopRequireDefault(_script);

var _strike=__webpack_require__(70);

var _strike2=_interopRequireDefault(_strike);

var _underline=__webpack_require__(71);

var _underline2=_interopRequireDefault(_underline);

var _image=__webpack_require__(72);

var _image2=_interopRequireDefault(_image);

var _video=__webpack_require__(73);

var _video2=_interopRequireDefault(_video);

var _code=__webpack_require__(13);

var _code2=_interopRequireDefault(_code);

var _formula=__webpack_require__(74);

var _formula2=_interopRequireDefault(_formula);

var _syntax=__webpack_require__(75);

var _syntax2=_interopRequireDefault(_syntax);

var _toolbar=__webpack_require__(57);

var _toolbar2=_interopRequireDefault(_toolbar);

var _icons=__webpack_require__(41);

var _icons2=_interopRequireDefault(_icons);

var _picker=__webpack_require__(28);

var _picker2=_interopRequireDefault(_picker);

var _colorPicker=__webpack_require__(59);

var _colorPicker2=_interopRequireDefault(_colorPicker);

var _iconPicker=__webpack_require__(60);

var _iconPicker2=_interopRequireDefault(_iconPicker);

var _tooltip=__webpack_require__(61);

var _tooltip2=_interopRequireDefault(_tooltip);

var _bubble=__webpack_require__(108);

var _bubble2=_interopRequireDefault(_bubble);

var _snow=__webpack_require__(62);

var _snow2=_interopRequireDefault(_snow);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

_core2.default.register({
'attributors/attribute/direction':_direction.DirectionAttribute,

'attributors/class/align':_align.AlignClass,
'attributors/class/background':_background.BackgroundClass,
'attributors/class/color':_color.ColorClass,
'attributors/class/direction':_direction.DirectionClass,
'attributors/class/font':_font.FontClass,
'attributors/class/size':_size.SizeClass,

'attributors/style/align':_align.AlignStyle,
'attributors/style/background':_background.BackgroundStyle,
'attributors/style/color':_color.ColorStyle,
'attributors/style/direction':_direction.DirectionStyle,
'attributors/style/font':_font.FontStyle,
'attributors/style/size':_size.SizeStyle},
true);

_core2.default.register({
'formats/align':_align.AlignClass,
'formats/direction':_direction.DirectionClass,
'formats/indent':_indent.IndentClass,

'formats/background':_background.BackgroundStyle,
'formats/color':_color.ColorStyle,
'formats/font':_font.FontClass,
'formats/size':_size.SizeClass,

'formats/blockquote':_blockquote2.default,
'formats/code-block':_code2.default,
'formats/header':_header2.default,
'formats/list':_list2.default,

'formats/bold':_bold2.default,
'formats/code':_code.Code,
'formats/italic':_italic2.default,
'formats/link':_link2.default,
'formats/script':_script2.default,
'formats/strike':_strike2.default,
'formats/underline':_underline2.default,

'formats/image':_image2.default,
'formats/video':_video2.default,

'formats/list/item':_list.ListItem,

'modules/formula':_formula2.default,
'modules/syntax':_syntax2.default,
'modules/toolbar':_toolbar2.default,

'themes/bubble':_bubble2.default,
'themes/snow':_snow2.default,

'ui/icons':_icons2.default,
'ui/picker':_picker2.default,
'ui/icon-picker':_iconPicker2.default,
'ui/color-picker':_colorPicker2.default,
'ui/tooltip':_tooltip2.default},
true);

exports.default=_core2.default;

/***/},
/* 64 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.IndentClass=undefined;

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var IdentAttributor=function(_Parchment$Attributor){
_inherits(IdentAttributor,_Parchment$Attributor);

function IdentAttributor(){
_classCallCheck(this,IdentAttributor);

return _possibleConstructorReturn(this,(IdentAttributor.__proto__||Object.getPrototypeOf(IdentAttributor)).apply(this,arguments));
}

_createClass(IdentAttributor,[{
key:'add',
value:function add(node,value){
if(value==='+1'||value==='-1'){
var indent=this.value(node)||0;
value=value==='+1'?indent+1:indent-1;
}
if(value===0){
this.remove(node);
return true;
}else {
return _get(IdentAttributor.prototype.__proto__||Object.getPrototypeOf(IdentAttributor.prototype),'add',this).call(this,node,value);
}
}},
{
key:'canAdd',
value:function canAdd(node,value){
return _get(IdentAttributor.prototype.__proto__||Object.getPrototypeOf(IdentAttributor.prototype),'canAdd',this).call(this,node,value)||_get(IdentAttributor.prototype.__proto__||Object.getPrototypeOf(IdentAttributor.prototype),'canAdd',this).call(this,node,parseInt(value));
}},
{
key:'value',
value:function value(node){
return parseInt(_get(IdentAttributor.prototype.__proto__||Object.getPrototypeOf(IdentAttributor.prototype),'value',this).call(this,node))||undefined;// Don't return NaN
}}]);


return IdentAttributor;
}(_parchment2.default.Attributor.Class);

var IndentClass=new IdentAttributor('indent','ql-indent',{
scope:_parchment2.default.Scope.BLOCK,
whitelist:[1,2,3,4,5,6,7,8]});


exports.IndentClass=IndentClass;

/***/},
/* 65 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _block=__webpack_require__(4);

var _block2=_interopRequireDefault(_block);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var Blockquote=function(_Block){
_inherits(Blockquote,_Block);

function Blockquote(){
_classCallCheck(this,Blockquote);

return _possibleConstructorReturn(this,(Blockquote.__proto__||Object.getPrototypeOf(Blockquote)).apply(this,arguments));
}

return Blockquote;
}(_block2.default);

Blockquote.blotName='blockquote';
Blockquote.tagName='blockquote';

exports.default=Blockquote;

/***/},
/* 66 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _block=__webpack_require__(4);

var _block2=_interopRequireDefault(_block);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var Header=function(_Block){
_inherits(Header,_Block);

function Header(){
_classCallCheck(this,Header);

return _possibleConstructorReturn(this,(Header.__proto__||Object.getPrototypeOf(Header)).apply(this,arguments));
}

_createClass(Header,null,[{
key:'formats',
value:function formats(domNode){
return this.tagName.indexOf(domNode.tagName)+1;
}}]);


return Header;
}(_block2.default);

Header.blotName='header';
Header.tagName=['H1','H2','H3','H4','H5','H6'];

exports.default=Header;

/***/},
/* 67 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.default=exports.ListItem=undefined;

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

var _block=__webpack_require__(4);

var _block2=_interopRequireDefault(_block);

var _container=__webpack_require__(25);

var _container2=_interopRequireDefault(_container);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _defineProperty(obj,key,value){if(key in obj){Object.defineProperty(obj,key,{value:value,enumerable:true,configurable:true,writable:true});}else {obj[key]=value;}return obj;}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var ListItem=function(_Block){
_inherits(ListItem,_Block);

function ListItem(){
_classCallCheck(this,ListItem);

return _possibleConstructorReturn(this,(ListItem.__proto__||Object.getPrototypeOf(ListItem)).apply(this,arguments));
}

_createClass(ListItem,[{
key:'format',
value:function format(name,value){
if(name===List.blotName&&!value){
this.replaceWith(_parchment2.default.create(this.statics.scope));
}else {
_get(ListItem.prototype.__proto__||Object.getPrototypeOf(ListItem.prototype),'format',this).call(this,name,value);
}
}},
{
key:'remove',
value:function remove(){
if(this.prev==null&&this.next==null){
this.parent.remove();
}else {
_get(ListItem.prototype.__proto__||Object.getPrototypeOf(ListItem.prototype),'remove',this).call(this);
}
}},
{
key:'replaceWith',
value:function replaceWith(name,value){
this.parent.isolate(this.offset(this.parent),this.length());
if(name===this.parent.statics.blotName){
this.parent.replaceWith(name,value);
return this;
}else {
this.parent.unwrap();
return _get(ListItem.prototype.__proto__||Object.getPrototypeOf(ListItem.prototype),'replaceWith',this).call(this,name,value);
}
}}],
[{
key:'formats',
value:function formats(domNode){
return domNode.tagName===this.tagName?undefined:_get(ListItem.__proto__||Object.getPrototypeOf(ListItem),'formats',this).call(this,domNode);
}}]);


return ListItem;
}(_block2.default);

ListItem.blotName='list-item';
ListItem.tagName='LI';

var List=function(_Container){
_inherits(List,_Container);

_createClass(List,null,[{
key:'create',
value:function create(value){
var tagName=value==='ordered'?'OL':'UL';
var node=_get(List.__proto__||Object.getPrototypeOf(List),'create',this).call(this,tagName);
if(value==='checked'||value==='unchecked'){
node.setAttribute('data-checked',value==='checked');
}
return node;
}},
{
key:'formats',
value:function formats(domNode){
if(domNode.tagName==='OL')return 'ordered';
if(domNode.tagName==='UL'){
if(domNode.hasAttribute('data-checked')){
return domNode.getAttribute('data-checked')==='true'?'checked':'unchecked';
}else {
return 'bullet';
}
}
return undefined;
}}]);


function List(domNode){
_classCallCheck(this,List);

var _this2=_possibleConstructorReturn(this,(List.__proto__||Object.getPrototypeOf(List)).call(this,domNode));

var listEventHandler=function listEventHandler(e){
if(e.target.parentNode!==domNode)return;
var format=_this2.statics.formats(domNode);
var blot=_parchment2.default.find(e.target);
if(format==='checked'){
blot.format('list','unchecked');
}else if(format==='unchecked'){
blot.format('list','checked');
}
};

domNode.addEventListener('touchstart',listEventHandler);
domNode.addEventListener('mousedown',listEventHandler);
return _this2;
}

_createClass(List,[{
key:'format',
value:function format(name,value){
if(this.children.length>0){
this.children.tail.format(name,value);
}
}},
{
key:'formats',
value:function formats(){
// We don't inherit from FormatBlot
return _defineProperty({},this.statics.blotName,this.statics.formats(this.domNode));
}},
{
key:'insertBefore',
value:function insertBefore(blot,ref){
if(blot instanceof ListItem){
_get(List.prototype.__proto__||Object.getPrototypeOf(List.prototype),'insertBefore',this).call(this,blot,ref);
}else {
var index=ref==null?this.length():ref.offset(this);
var after=this.split(index);
after.parent.insertBefore(blot,after);
}
}},
{
key:'optimize',
value:function optimize(context){
_get(List.prototype.__proto__||Object.getPrototypeOf(List.prototype),'optimize',this).call(this,context);
var next=this.next;
if(next!=null&&next.prev===this&&next.statics.blotName===this.statics.blotName&&next.domNode.tagName===this.domNode.tagName&&next.domNode.getAttribute('data-checked')===this.domNode.getAttribute('data-checked')){
next.moveChildren(this);
next.remove();
}
}},
{
key:'replace',
value:function replace(target){
if(target.statics.blotName!==this.statics.blotName){
var item=_parchment2.default.create(this.statics.defaultChild);
target.moveChildren(item);
this.appendChild(item);
}
_get(List.prototype.__proto__||Object.getPrototypeOf(List.prototype),'replace',this).call(this,target);
}}]);


return List;
}(_container2.default);

List.blotName='list';
List.scope=_parchment2.default.Scope.BLOCK_BLOT;
List.tagName=['OL','UL'];
List.defaultChild='list-item';
List.allowedChildren=[ListItem];

exports.ListItem=ListItem;
exports.default=List;

/***/},
/* 68 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _bold=__webpack_require__(56);

var _bold2=_interopRequireDefault(_bold);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var Italic=function(_Bold){
_inherits(Italic,_Bold);

function Italic(){
_classCallCheck(this,Italic);

return _possibleConstructorReturn(this,(Italic.__proto__||Object.getPrototypeOf(Italic)).apply(this,arguments));
}

return Italic;
}(_bold2.default);

Italic.blotName='italic';
Italic.tagName=['EM','I'];

exports.default=Italic;

/***/},
/* 69 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _inline=__webpack_require__(6);

var _inline2=_interopRequireDefault(_inline);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var Script=function(_Inline){
_inherits(Script,_Inline);

function Script(){
_classCallCheck(this,Script);

return _possibleConstructorReturn(this,(Script.__proto__||Object.getPrototypeOf(Script)).apply(this,arguments));
}

_createClass(Script,null,[{
key:'create',
value:function create(value){
if(value==='super'){
return document.createElement('sup');
}else if(value==='sub'){
return document.createElement('sub');
}else {
return _get(Script.__proto__||Object.getPrototypeOf(Script),'create',this).call(this,value);
}
}},
{
key:'formats',
value:function formats(domNode){
if(domNode.tagName==='SUB')return 'sub';
if(domNode.tagName==='SUP')return 'super';
return undefined;
}}]);


return Script;
}(_inline2.default);

Script.blotName='script';
Script.tagName=['SUB','SUP'];

exports.default=Script;

/***/},
/* 70 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _inline=__webpack_require__(6);

var _inline2=_interopRequireDefault(_inline);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var Strike=function(_Inline){
_inherits(Strike,_Inline);

function Strike(){
_classCallCheck(this,Strike);

return _possibleConstructorReturn(this,(Strike.__proto__||Object.getPrototypeOf(Strike)).apply(this,arguments));
}

return Strike;
}(_inline2.default);

Strike.blotName='strike';
Strike.tagName='S';

exports.default=Strike;

/***/},
/* 71 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _inline=__webpack_require__(6);

var _inline2=_interopRequireDefault(_inline);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var Underline=function(_Inline){
_inherits(Underline,_Inline);

function Underline(){
_classCallCheck(this,Underline);

return _possibleConstructorReturn(this,(Underline.__proto__||Object.getPrototypeOf(Underline)).apply(this,arguments));
}

return Underline;
}(_inline2.default);

Underline.blotName='underline';
Underline.tagName='U';

exports.default=Underline;

/***/},
/* 72 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

var _link=__webpack_require__(27);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var ATTRIBUTES=['alt','height','width'];

var Image=function(_Parchment$Embed){
_inherits(Image,_Parchment$Embed);

function Image(){
_classCallCheck(this,Image);

return _possibleConstructorReturn(this,(Image.__proto__||Object.getPrototypeOf(Image)).apply(this,arguments));
}

_createClass(Image,[{
key:'format',
value:function format(name,value){
if(ATTRIBUTES.indexOf(name)>-1){
if(value){
this.domNode.setAttribute(name,value);
}else {
this.domNode.removeAttribute(name);
}
}else {
_get(Image.prototype.__proto__||Object.getPrototypeOf(Image.prototype),'format',this).call(this,name,value);
}
}}],
[{
key:'create',
value:function create(value){
var node=_get(Image.__proto__||Object.getPrototypeOf(Image),'create',this).call(this,value);
if(typeof value==='string'){
node.setAttribute('src',this.sanitize(value));
}
return node;
}},
{
key:'formats',
value:function formats(domNode){
return ATTRIBUTES.reduce(function(formats,attribute){
if(domNode.hasAttribute(attribute)){
formats[attribute]=domNode.getAttribute(attribute);
}
return formats;
},{});
}},
{
key:'match',
value:function match(url){
return /\.(jpe?g|gif|png)$/.test(url)||/^data:image\/.+;base64/.test(url);

}},
{
key:'sanitize',
value:function sanitize(url){
return (0, _link.sanitize)(url,['http','https','data'])?url:'//:0';
}},
{
key:'value',
value:function value(domNode){
return domNode.getAttribute('src');
}}]);


return Image;
}(_parchment2.default.Embed);

Image.blotName='image';
Image.tagName='IMG';

exports.default=Image;

/***/},
/* 73 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});


var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _block=__webpack_require__(4);

var _link=__webpack_require__(27);

var _link2=_interopRequireDefault(_link);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var ATTRIBUTES=['height','width'];

var Video=function(_BlockEmbed){
_inherits(Video,_BlockEmbed);

function Video(){
_classCallCheck(this,Video);

return _possibleConstructorReturn(this,(Video.__proto__||Object.getPrototypeOf(Video)).apply(this,arguments));
}

_createClass(Video,[{
key:'format',
value:function format(name,value){
if(ATTRIBUTES.indexOf(name)>-1){
if(value){
this.domNode.setAttribute(name,value);
}else {
this.domNode.removeAttribute(name);
}
}else {
_get(Video.prototype.__proto__||Object.getPrototypeOf(Video.prototype),'format',this).call(this,name,value);
}
}}],
[{
key:'create',
value:function create(value){
var node=_get(Video.__proto__||Object.getPrototypeOf(Video),'create',this).call(this,value);
node.setAttribute('frameborder','0');
node.setAttribute('allowfullscreen',true);
node.setAttribute('src',this.sanitize(value));
return node;
}},
{
key:'formats',
value:function formats(domNode){
return ATTRIBUTES.reduce(function(formats,attribute){
if(domNode.hasAttribute(attribute)){
formats[attribute]=domNode.getAttribute(attribute);
}
return formats;
},{});
}},
{
key:'sanitize',
value:function sanitize(url){
return _link2.default.sanitize(url);
}},
{
key:'value',
value:function value(domNode){
return domNode.getAttribute('src');
}}]);


return Video;
}(_block.BlockEmbed);

Video.blotName='video';
Video.className='ql-video';
Video.tagName='IFRAME';

exports.default=Video;

/***/},
/* 74 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.default=exports.FormulaBlot=undefined;

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _embed=__webpack_require__(35);

var _embed2=_interopRequireDefault(_embed);

var _quill=__webpack_require__(5);

var _quill2=_interopRequireDefault(_quill);

var _module=__webpack_require__(9);

var _module2=_interopRequireDefault(_module);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var FormulaBlot=function(_Embed){
_inherits(FormulaBlot,_Embed);

function FormulaBlot(){
_classCallCheck(this,FormulaBlot);

return _possibleConstructorReturn(this,(FormulaBlot.__proto__||Object.getPrototypeOf(FormulaBlot)).apply(this,arguments));
}

_createClass(FormulaBlot,null,[{
key:'create',
value:function create(value){
var node=_get(FormulaBlot.__proto__||Object.getPrototypeOf(FormulaBlot),'create',this).call(this,value);
if(typeof value==='string'){
window.katex.render(value,node,{
throwOnError:false,
errorColor:'#f00'});

node.setAttribute('data-value',value);
}
return node;
}},
{
key:'value',
value:function value(domNode){
return domNode.getAttribute('data-value');
}}]);


return FormulaBlot;
}(_embed2.default);

FormulaBlot.blotName='formula';
FormulaBlot.className='ql-formula';
FormulaBlot.tagName='SPAN';

var Formula=function(_Module){
_inherits(Formula,_Module);

_createClass(Formula,null,[{
key:'register',
value:function register(){
_quill2.default.register(FormulaBlot,true);
}}]);


function Formula(){
_classCallCheck(this,Formula);

var _this2=_possibleConstructorReturn(this,(Formula.__proto__||Object.getPrototypeOf(Formula)).call(this));

if(window.katex==null){
throw new Error('Formula module requires KaTeX.');
}
return _this2;
}

return Formula;
}(_module2.default);

exports.FormulaBlot=FormulaBlot;
exports.default=Formula;

/***/},
/* 75 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.default=exports.CodeToken=exports.CodeBlock=undefined;

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _parchment=__webpack_require__(0);

var _parchment2=_interopRequireDefault(_parchment);

var _quill=__webpack_require__(5);

var _quill2=_interopRequireDefault(_quill);

var _module=__webpack_require__(9);

var _module2=_interopRequireDefault(_module);

var _code=__webpack_require__(13);

var _code2=_interopRequireDefault(_code);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var SyntaxCodeBlock=function(_CodeBlock){
_inherits(SyntaxCodeBlock,_CodeBlock);

function SyntaxCodeBlock(){
_classCallCheck(this,SyntaxCodeBlock);

return _possibleConstructorReturn(this,(SyntaxCodeBlock.__proto__||Object.getPrototypeOf(SyntaxCodeBlock)).apply(this,arguments));
}

_createClass(SyntaxCodeBlock,[{
key:'replaceWith',
value:function replaceWith(block){
this.domNode.textContent=this.domNode.textContent;
this.attach();
_get(SyntaxCodeBlock.prototype.__proto__||Object.getPrototypeOf(SyntaxCodeBlock.prototype),'replaceWith',this).call(this,block);
}},
{
key:'highlight',
value:function highlight(_highlight){
var text=this.domNode.textContent;
if(this.cachedText!==text){
if(text.trim().length>0||this.cachedText==null){
this.domNode.innerHTML=_highlight(text);
this.domNode.normalize();
this.attach();
}
this.cachedText=text;
}
}}]);


return SyntaxCodeBlock;
}(_code2.default);

SyntaxCodeBlock.className='ql-syntax';

var CodeToken=new _parchment2.default.Attributor.Class('token','hljs',{
scope:_parchment2.default.Scope.INLINE});


var Syntax=function(_Module){
_inherits(Syntax,_Module);

_createClass(Syntax,null,[{
key:'register',
value:function register(){
_quill2.default.register(CodeToken,true);
_quill2.default.register(SyntaxCodeBlock,true);
}}]);


function Syntax(quill,options){
_classCallCheck(this,Syntax);

var _this2=_possibleConstructorReturn(this,(Syntax.__proto__||Object.getPrototypeOf(Syntax)).call(this,quill,options));

if(typeof _this2.options.highlight!=='function'){
throw new Error('Syntax module requires highlight.js. Please include the library on the page before Quill.');
}
var timer=null;
_this2.quill.on(_quill2.default.events.SCROLL_OPTIMIZE,function(){
clearTimeout(timer);
timer=setTimeout(function(){
_this2.highlight();
timer=null;
},_this2.options.interval);
});
_this2.highlight();
return _this2;
}

_createClass(Syntax,[{
key:'highlight',
value:function highlight(){
var _this3=this;

if(this.quill.selection.composing)return;
this.quill.update(_quill2.default.sources.USER);
var range=this.quill.getSelection();
this.quill.scroll.descendants(SyntaxCodeBlock).forEach(function(code){
code.highlight(_this3.options.highlight);
});
this.quill.update(_quill2.default.sources.SILENT);
if(range!=null){
this.quill.setSelection(range,_quill2.default.sources.SILENT);
}
}}]);


return Syntax;
}(_module2.default);

Syntax.DEFAULTS={
highlight:function(){
if(window.hljs==null)return null;
return function(text){
var result=window.hljs.highlightAuto(text);
return result.value;
};
}(),
interval:1000};


exports.CodeBlock=SyntaxCodeBlock;
exports.CodeToken=CodeToken;
exports.default=Syntax;

/***/},
/* 76 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <line class=ql-stroke x1=3 x2=15 y1=9 y2=9></line> <line class=ql-stroke x1=3 x2=13 y1=14 y2=14></line> <line class=ql-stroke x1=3 x2=9 y1=4 y2=4></line> </svg>";

/***/},
/* 77 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <line class=ql-stroke x1=15 x2=3 y1=9 y2=9></line> <line class=ql-stroke x1=14 x2=4 y1=14 y2=14></line> <line class=ql-stroke x1=12 x2=6 y1=4 y2=4></line> </svg>";

/***/},
/* 78 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <line class=ql-stroke x1=15 x2=3 y1=9 y2=9></line> <line class=ql-stroke x1=15 x2=5 y1=14 y2=14></line> <line class=ql-stroke x1=15 x2=9 y1=4 y2=4></line> </svg>";

/***/},
/* 79 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <line class=ql-stroke x1=15 x2=3 y1=9 y2=9></line> <line class=ql-stroke x1=15 x2=3 y1=14 y2=14></line> <line class=ql-stroke x1=15 x2=3 y1=4 y2=4></line> </svg>";

/***/},
/* 80 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <g class=\"ql-fill ql-color-label\"> <polygon points=\"6 6.868 6 6 5 6 5 7 5.942 7 6 6.868\"></polygon> <rect height=1 width=1 x=4 y=4></rect> <polygon points=\"6.817 5 6 5 6 6 6.38 6 6.817 5\"></polygon> <rect height=1 width=1 x=2 y=6></rect> <rect height=1 width=1 x=3 y=5></rect> <rect height=1 width=1 x=4 y=7></rect> <polygon points=\"4 11.439 4 11 3 11 3 12 3.755 12 4 11.439\"></polygon> <rect height=1 width=1 x=2 y=12></rect> <rect height=1 width=1 x=2 y=9></rect> <rect height=1 width=1 x=2 y=15></rect> <polygon points=\"4.63 10 4 10 4 11 4.192 11 4.63 10\"></polygon> <rect height=1 width=1 x=3 y=8></rect> <path d=M10.832,4.2L11,4.582V4H10.708A1.948,1.948,0,0,1,10.832,4.2Z></path> <path d=M7,4.582L7.168,4.2A1.929,1.929,0,0,1,7.292,4H7V4.582Z></path> <path d=M8,13H7.683l-0.351.8a1.933,1.933,0,0,1-.124.2H8V13Z></path> <rect height=1 width=1 x=12 y=2></rect> <rect height=1 width=1 x=11 y=3></rect> <path d=M9,3H8V3.282A1.985,1.985,0,0,1,9,3Z></path> <rect height=1 width=1 x=2 y=3></rect> <rect height=1 width=1 x=6 y=2></rect> <rect height=1 width=1 x=3 y=2></rect> <rect height=1 width=1 x=5 y=3></rect> <rect height=1 width=1 x=9 y=2></rect> <rect height=1 width=1 x=15 y=14></rect> <polygon points=\"13.447 10.174 13.469 10.225 13.472 10.232 13.808 11 14 11 14 10 13.37 10 13.447 10.174\"></polygon> <rect height=1 width=1 x=13 y=7></rect> <rect height=1 width=1 x=15 y=5></rect> <rect height=1 width=1 x=14 y=6></rect> <rect height=1 width=1 x=15 y=8></rect> <rect height=1 width=1 x=14 y=9></rect> <path d=M3.775,14H3v1H4V14.314A1.97,1.97,0,0,1,3.775,14Z></path> <rect height=1 width=1 x=14 y=3></rect> <polygon points=\"12 6.868 12 6 11.62 6 12 6.868\"></polygon> <rect height=1 width=1 x=15 y=2></rect> <rect height=1 width=1 x=12 y=5></rect> <rect height=1 width=1 x=13 y=4></rect> <polygon points=\"12.933 9 13 9 13 8 12.495 8 12.933 9\"></polygon> <rect height=1 width=1 x=9 y=14></rect> <rect height=1 width=1 x=8 y=15></rect> <path d=M6,14.926V15H7V14.316A1.993,1.993,0,0,1,6,14.926Z></path> <rect height=1 width=1 x=5 y=15></rect> <path d=M10.668,13.8L10.317,13H10v1h0.792A1.947,1.947,0,0,1,10.668,13.8Z></path> <rect height=1 width=1 x=11 y=15></rect> <path d=M14.332,12.2a1.99,1.99,0,0,1,.166.8H15V12H14.245Z></path> <rect height=1 width=1 x=14 y=15></rect> <rect height=1 width=1 x=15 y=11></rect> </g> <polyline class=ql-stroke points=\"5.5 13 9 5 12.5 13\"></polyline> <line class=ql-stroke x1=11.63 x2=6.38 y1=11 y2=11></line> </svg>";

/***/},
/* 81 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <rect class=\"ql-fill ql-stroke\" height=3 width=3 x=4 y=5></rect> <rect class=\"ql-fill ql-stroke\" height=3 width=3 x=11 y=5></rect> <path class=\"ql-even ql-fill ql-stroke\" d=M7,8c0,4.031-3,5-3,5></path> <path class=\"ql-even ql-fill ql-stroke\" d=M14,8c0,4.031-3,5-3,5></path> </svg>";

/***/},
/* 82 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <path class=ql-stroke d=M5,4H9.5A2.5,2.5,0,0,1,12,6.5v0A2.5,2.5,0,0,1,9.5,9H5A0,0,0,0,1,5,9V4A0,0,0,0,1,5,4Z></path> <path class=ql-stroke d=M5,9h5.5A2.5,2.5,0,0,1,13,11.5v0A2.5,2.5,0,0,1,10.5,14H5a0,0,0,0,1,0,0V9A0,0,0,0,1,5,9Z></path> </svg>";

/***/},
/* 83 */
/***/function(module,exports){

module.exports="<svg class=\"\" viewbox=\"0 0 18 18\"> <line class=ql-stroke x1=5 x2=13 y1=3 y2=3></line> <line class=ql-stroke x1=6 x2=9.35 y1=12 y2=3></line> <line class=ql-stroke x1=11 x2=15 y1=11 y2=15></line> <line class=ql-stroke x1=15 x2=11 y1=11 y2=15></line> <rect class=ql-fill height=1 rx=0.5 ry=0.5 width=7 x=2 y=14></rect> </svg>";

/***/},
/* 84 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <line class=\"ql-color-label ql-stroke ql-transparent\" x1=3 x2=15 y1=15 y2=15></line> <polyline class=ql-stroke points=\"5.5 11 9 3 12.5 11\"></polyline> <line class=ql-stroke x1=11.63 x2=6.38 y1=9 y2=9></line> </svg>";

/***/},
/* 85 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <polygon class=\"ql-stroke ql-fill\" points=\"3 11 5 9 3 7 3 11\"></polygon> <line class=\"ql-stroke ql-fill\" x1=15 x2=11 y1=4 y2=4></line> <path class=ql-fill d=M11,3a3,3,0,0,0,0,6h1V3H11Z></path> <rect class=ql-fill height=11 width=1 x=11 y=4></rect> <rect class=ql-fill height=11 width=1 x=13 y=4></rect> </svg>";

/***/},
/* 86 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <polygon class=\"ql-stroke ql-fill\" points=\"15 12 13 10 15 8 15 12\"></polygon> <line class=\"ql-stroke ql-fill\" x1=9 x2=5 y1=4 y2=4></line> <path class=ql-fill d=M5,3A3,3,0,0,0,5,9H6V3H5Z></path> <rect class=ql-fill height=11 width=1 x=5 y=4></rect> <rect class=ql-fill height=11 width=1 x=7 y=4></rect> </svg>";

/***/},
/* 87 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <path class=ql-fill d=M14,16H4a1,1,0,0,1,0-2H14A1,1,0,0,1,14,16Z /> <path class=ql-fill d=M14,4H4A1,1,0,0,1,4,2H14A1,1,0,0,1,14,4Z /> <rect class=ql-fill x=3 y=6 width=12 height=6 rx=1 ry=1 /> </svg>";

/***/},
/* 88 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <path class=ql-fill d=M13,16H5a1,1,0,0,1,0-2h8A1,1,0,0,1,13,16Z /> <path class=ql-fill d=M13,4H5A1,1,0,0,1,5,2h8A1,1,0,0,1,13,4Z /> <rect class=ql-fill x=2 y=6 width=14 height=6 rx=1 ry=1 /> </svg>";

/***/},
/* 89 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <path class=ql-fill d=M15,8H13a1,1,0,0,1,0-2h2A1,1,0,0,1,15,8Z /> <path class=ql-fill d=M15,12H13a1,1,0,0,1,0-2h2A1,1,0,0,1,15,12Z /> <path class=ql-fill d=M15,16H5a1,1,0,0,1,0-2H15A1,1,0,0,1,15,16Z /> <path class=ql-fill d=M15,4H5A1,1,0,0,1,5,2H15A1,1,0,0,1,15,4Z /> <rect class=ql-fill x=2 y=6 width=8 height=6 rx=1 ry=1 /> </svg>";

/***/},
/* 90 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <path class=ql-fill d=M5,8H3A1,1,0,0,1,3,6H5A1,1,0,0,1,5,8Z /> <path class=ql-fill d=M5,12H3a1,1,0,0,1,0-2H5A1,1,0,0,1,5,12Z /> <path class=ql-fill d=M13,16H3a1,1,0,0,1,0-2H13A1,1,0,0,1,13,16Z /> <path class=ql-fill d=M13,4H3A1,1,0,0,1,3,2H13A1,1,0,0,1,13,4Z /> <rect class=ql-fill x=8 y=6 width=8 height=6 rx=1 ry=1 transform=\"translate(24 18) rotate(-180)\"/> </svg>";

/***/},
/* 91 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <path class=ql-fill d=M11.759,2.482a2.561,2.561,0,0,0-3.53.607A7.656,7.656,0,0,0,6.8,6.2C6.109,9.188,5.275,14.677,4.15,14.927a1.545,1.545,0,0,0-1.3-.933A0.922,0.922,0,0,0,2,15.036S1.954,16,4.119,16s3.091-2.691,3.7-5.553c0.177-.826.36-1.726,0.554-2.6L8.775,6.2c0.381-1.421.807-2.521,1.306-2.676a1.014,1.014,0,0,0,1.02.56A0.966,0.966,0,0,0,11.759,2.482Z></path> <rect class=ql-fill height=1.6 rx=0.8 ry=0.8 width=5 x=5.15 y=6.2></rect> <path class=ql-fill d=M13.663,12.027a1.662,1.662,0,0,1,.266-0.276q0.193,0.069.456,0.138a2.1,2.1,0,0,0,.535.069,1.075,1.075,0,0,0,.767-0.3,1.044,1.044,0,0,0,.314-0.8,0.84,0.84,0,0,0-.238-0.619,0.8,0.8,0,0,0-.594-0.239,1.154,1.154,0,0,0-.781.3,4.607,4.607,0,0,0-.781,1q-0.091.15-.218,0.346l-0.246.38c-0.068-.288-0.137-0.582-0.212-0.885-0.459-1.847-2.494-.984-2.941-0.8-0.482.2-.353,0.647-0.094,0.529a0.869,0.869,0,0,1,1.281.585c0.217,0.751.377,1.436,0.527,2.038a5.688,5.688,0,0,1-.362.467,2.69,2.69,0,0,1-.264.271q-0.221-.08-0.471-0.147a2.029,2.029,0,0,0-.522-0.066,1.079,1.079,0,0,0-.768.3A1.058,1.058,0,0,0,9,15.131a0.82,0.82,0,0,0,.832.852,1.134,1.134,0,0,0,.787-0.3,5.11,5.11,0,0,0,.776-0.993q0.141-.219.215-0.34c0.046-.076.122-0.194,0.223-0.346a2.786,2.786,0,0,0,.918,1.726,2.582,2.582,0,0,0,2.376-.185c0.317-.181.212-0.565,0-0.494A0.807,0.807,0,0,1,14.176,15a5.159,5.159,0,0,1-.913-2.446l0,0Q13.487,12.24,13.663,12.027Z></path> </svg>";

/***/},
/* 92 */
/***/function(module,exports){

module.exports="<svg viewBox=\"0 0 18 18\"> <path class=ql-fill d=M10,4V14a1,1,0,0,1-2,0V10H3v4a1,1,0,0,1-2,0V4A1,1,0,0,1,3,4V8H8V4a1,1,0,0,1,2,0Zm6.06787,9.209H14.98975V7.59863a.54085.54085,0,0,0-.605-.60547h-.62744a1.01119,1.01119,0,0,0-.748.29688L11.645,8.56641a.5435.5435,0,0,0-.022.8584l.28613.30762a.53861.53861,0,0,0,.84717.0332l.09912-.08789a1.2137,1.2137,0,0,0,.2417-.35254h.02246s-.01123.30859-.01123.60547V13.209H12.041a.54085.54085,0,0,0-.605.60547v.43945a.54085.54085,0,0,0,.605.60547h4.02686a.54085.54085,0,0,0,.605-.60547v-.43945A.54085.54085,0,0,0,16.06787,13.209Z /> </svg>";

/***/},
/* 93 */
/***/function(module,exports){

module.exports="<svg viewBox=\"0 0 18 18\"> <path class=ql-fill d=M16.73975,13.81445v.43945a.54085.54085,0,0,1-.605.60547H11.855a.58392.58392,0,0,1-.64893-.60547V14.0127c0-2.90527,3.39941-3.42187,3.39941-4.55469a.77675.77675,0,0,0-.84717-.78125,1.17684,1.17684,0,0,0-.83594.38477c-.2749.26367-.561.374-.85791.13184l-.4292-.34082c-.30811-.24219-.38525-.51758-.1543-.81445a2.97155,2.97155,0,0,1,2.45361-1.17676,2.45393,2.45393,0,0,1,2.68408,2.40918c0,2.45312-3.1792,2.92676-3.27832,3.93848h2.79443A.54085.54085,0,0,1,16.73975,13.81445ZM9,3A.99974.99974,0,0,0,8,4V8H3V4A1,1,0,0,0,1,4V14a1,1,0,0,0,2,0V10H8v4a1,1,0,0,0,2,0V4A.99974.99974,0,0,0,9,3Z /> </svg>";

/***/},
/* 94 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <line class=ql-stroke x1=7 x2=13 y1=4 y2=4></line> <line class=ql-stroke x1=5 x2=11 y1=14 y2=14></line> <line class=ql-stroke x1=8 x2=10 y1=14 y2=4></line> </svg>";

/***/},
/* 95 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <rect class=ql-stroke height=10 width=12 x=3 y=4></rect> <circle class=ql-fill cx=6 cy=7 r=1></circle> <polyline class=\"ql-even ql-fill\" points=\"5 12 5 11 7 9 8 10 11 7 13 9 13 12 5 12\"></polyline> </svg>";

/***/},
/* 96 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <line class=ql-stroke x1=3 x2=15 y1=14 y2=14></line> <line class=ql-stroke x1=3 x2=15 y1=4 y2=4></line> <line class=ql-stroke x1=9 x2=15 y1=9 y2=9></line> <polyline class=\"ql-fill ql-stroke\" points=\"3 7 3 11 5 9 3 7\"></polyline> </svg>";

/***/},
/* 97 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <line class=ql-stroke x1=3 x2=15 y1=14 y2=14></line> <line class=ql-stroke x1=3 x2=15 y1=4 y2=4></line> <line class=ql-stroke x1=9 x2=15 y1=9 y2=9></line> <polyline class=ql-stroke points=\"5 7 5 11 3 9 5 7\"></polyline> </svg>";

/***/},
/* 98 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <line class=ql-stroke x1=7 x2=11 y1=7 y2=11></line> <path class=\"ql-even ql-stroke\" d=M8.9,4.577a3.476,3.476,0,0,1,.36,4.679A3.476,3.476,0,0,1,4.577,8.9C3.185,7.5,2.035,6.4,4.217,4.217S7.5,3.185,8.9,4.577Z></path> <path class=\"ql-even ql-stroke\" d=M13.423,9.1a3.476,3.476,0,0,0-4.679-.36,3.476,3.476,0,0,0,.36,4.679c1.392,1.392,2.5,2.542,4.679.36S14.815,10.5,13.423,9.1Z></path> </svg>";

/***/},
/* 99 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <line class=ql-stroke x1=7 x2=15 y1=4 y2=4></line> <line class=ql-stroke x1=7 x2=15 y1=9 y2=9></line> <line class=ql-stroke x1=7 x2=15 y1=14 y2=14></line> <line class=\"ql-stroke ql-thin\" x1=2.5 x2=4.5 y1=5.5 y2=5.5></line> <path class=ql-fill d=M3.5,6A0.5,0.5,0,0,1,3,5.5V3.085l-0.276.138A0.5,0.5,0,0,1,2.053,3c-0.124-.247-0.023-0.324.224-0.447l1-.5A0.5,0.5,0,0,1,4,2.5v3A0.5,0.5,0,0,1,3.5,6Z></path> <path class=\"ql-stroke ql-thin\" d=M4.5,10.5h-2c0-.234,1.85-1.076,1.85-2.234A0.959,0.959,0,0,0,2.5,8.156></path> <path class=\"ql-stroke ql-thin\" d=M2.5,14.846a0.959,0.959,0,0,0,1.85-.109A0.7,0.7,0,0,0,3.75,14a0.688,0.688,0,0,0,.6-0.736,0.959,0.959,0,0,0-1.85-.109></path> </svg>";

/***/},
/* 100 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <line class=ql-stroke x1=6 x2=15 y1=4 y2=4></line> <line class=ql-stroke x1=6 x2=15 y1=9 y2=9></line> <line class=ql-stroke x1=6 x2=15 y1=14 y2=14></line> <line class=ql-stroke x1=3 x2=3 y1=4 y2=4></line> <line class=ql-stroke x1=3 x2=3 y1=9 y2=9></line> <line class=ql-stroke x1=3 x2=3 y1=14 y2=14></line> </svg>";

/***/},
/* 101 */
/***/function(module,exports){

module.exports="<svg class=\"\" viewbox=\"0 0 18 18\"> <line class=ql-stroke x1=9 x2=15 y1=4 y2=4></line> <polyline class=ql-stroke points=\"3 4 4 5 6 3\"></polyline> <line class=ql-stroke x1=9 x2=15 y1=14 y2=14></line> <polyline class=ql-stroke points=\"3 14 4 15 6 13\"></polyline> <line class=ql-stroke x1=9 x2=15 y1=9 y2=9></line> <polyline class=ql-stroke points=\"3 9 4 10 6 8\"></polyline> </svg>";

/***/},
/* 102 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <path class=ql-fill d=M15.5,15H13.861a3.858,3.858,0,0,0,1.914-2.975,1.8,1.8,0,0,0-1.6-1.751A1.921,1.921,0,0,0,12.021,11.7a0.50013,0.50013,0,1,0,.957.291h0a0.914,0.914,0,0,1,1.053-.725,0.81,0.81,0,0,1,.744.762c0,1.076-1.16971,1.86982-1.93971,2.43082A1.45639,1.45639,0,0,0,12,15.5a0.5,0.5,0,0,0,.5.5h3A0.5,0.5,0,0,0,15.5,15Z /> <path class=ql-fill d=M9.65,5.241a1,1,0,0,0-1.409.108L6,7.964,3.759,5.349A1,1,0,0,0,2.192,6.59178Q2.21541,6.6213,2.241,6.649L4.684,9.5,2.241,12.35A1,1,0,0,0,3.71,13.70722q0.02557-.02768.049-0.05722L6,11.036,8.241,13.65a1,1,0,1,0,1.567-1.24277Q9.78459,12.3777,9.759,12.35L7.316,9.5,9.759,6.651A1,1,0,0,0,9.65,5.241Z /> </svg>";

/***/},
/* 103 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <path class=ql-fill d=M15.5,7H13.861a4.015,4.015,0,0,0,1.914-2.975,1.8,1.8,0,0,0-1.6-1.751A1.922,1.922,0,0,0,12.021,3.7a0.5,0.5,0,1,0,.957.291,0.917,0.917,0,0,1,1.053-.725,0.81,0.81,0,0,1,.744.762c0,1.077-1.164,1.925-1.934,2.486A1.423,1.423,0,0,0,12,7.5a0.5,0.5,0,0,0,.5.5h3A0.5,0.5,0,0,0,15.5,7Z /> <path class=ql-fill d=M9.651,5.241a1,1,0,0,0-1.41.108L6,7.964,3.759,5.349a1,1,0,1,0-1.519,1.3L4.683,9.5,2.241,12.35a1,1,0,1,0,1.519,1.3L6,11.036,8.241,13.65a1,1,0,0,0,1.519-1.3L7.317,9.5,9.759,6.651A1,1,0,0,0,9.651,5.241Z /> </svg>";

/***/},
/* 104 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <line class=\"ql-stroke ql-thin\" x1=15.5 x2=2.5 y1=8.5 y2=9.5></line> <path class=ql-fill d=M9.007,8C6.542,7.791,6,7.519,6,6.5,6,5.792,7.283,5,9,5c1.571,0,2.765.679,2.969,1.309a1,1,0,0,0,1.9-.617C13.356,4.106,11.354,3,9,3,6.2,3,4,4.538,4,6.5a3.2,3.2,0,0,0,.5,1.843Z></path> <path class=ql-fill d=M8.984,10C11.457,10.208,12,10.479,12,11.5c0,0.708-1.283,1.5-3,1.5-1.571,0-2.765-.679-2.969-1.309a1,1,0,1,0-1.9.617C4.644,13.894,6.646,15,9,15c2.8,0,5-1.538,5-3.5a3.2,3.2,0,0,0-.5-1.843Z></path> </svg>";

/***/},
/* 105 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <path class=ql-stroke d=M5,3V9a4.012,4.012,0,0,0,4,4H9a4.012,4.012,0,0,0,4-4V3></path> <rect class=ql-fill height=1 rx=0.5 ry=0.5 width=12 x=3 y=15></rect> </svg>";

/***/},
/* 106 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <rect class=ql-stroke height=12 width=12 x=3 y=3></rect> <rect class=ql-fill height=12 width=1 x=5 y=3></rect> <rect class=ql-fill height=12 width=1 x=12 y=3></rect> <rect class=ql-fill height=2 width=8 x=5 y=8></rect> <rect class=ql-fill height=1 width=3 x=3 y=5></rect> <rect class=ql-fill height=1 width=3 x=3 y=7></rect> <rect class=ql-fill height=1 width=3 x=3 y=10></rect> <rect class=ql-fill height=1 width=3 x=3 y=12></rect> <rect class=ql-fill height=1 width=3 x=12 y=5></rect> <rect class=ql-fill height=1 width=3 x=12 y=7></rect> <rect class=ql-fill height=1 width=3 x=12 y=10></rect> <rect class=ql-fill height=1 width=3 x=12 y=12></rect> </svg>";

/***/},
/* 107 */
/***/function(module,exports){

module.exports="<svg viewbox=\"0 0 18 18\"> <polygon class=ql-stroke points=\"7 11 9 13 11 11 7 11\"></polygon> <polygon class=ql-stroke points=\"7 7 9 5 11 7 7 7\"></polygon> </svg>";

/***/},
/* 108 */
/***/function(module,exports,__webpack_require__){


Object.defineProperty(exports,"__esModule",{
value:true});

exports.default=exports.BubbleTooltip=undefined;

var _get=function get(object,property,receiver){if(object===null)object=Function.prototype;var desc=Object.getOwnPropertyDescriptor(object,property);if(desc===undefined){var parent=Object.getPrototypeOf(object);if(parent===null){return undefined;}else {return get(parent,property,receiver);}}else if("value"in desc){return desc.value;}else {var getter=desc.get;if(getter===undefined){return undefined;}return getter.call(receiver);}};

var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();

var _extend=__webpack_require__(3);

var _extend2=_interopRequireDefault(_extend);

var _emitter=__webpack_require__(8);

var _emitter2=_interopRequireDefault(_emitter);

var _base=__webpack_require__(43);

var _base2=_interopRequireDefault(_base);

var _selection=__webpack_require__(15);

var _icons=__webpack_require__(41);

var _icons2=_interopRequireDefault(_icons);

function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}

function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}

var TOOLBAR_CONFIG=[['bold','italic','link'],[{header:1},{header:2},'blockquote']];

var BubbleTheme=function(_BaseTheme){
_inherits(BubbleTheme,_BaseTheme);

function BubbleTheme(quill,options){
_classCallCheck(this,BubbleTheme);

if(options.modules.toolbar!=null&&options.modules.toolbar.container==null){
options.modules.toolbar.container=TOOLBAR_CONFIG;
}

var _this=_possibleConstructorReturn(this,(BubbleTheme.__proto__||Object.getPrototypeOf(BubbleTheme)).call(this,quill,options));

_this.quill.container.classList.add('ql-bubble');
return _this;
}

_createClass(BubbleTheme,[{
key:'extendToolbar',
value:function extendToolbar(toolbar){
this.tooltip=new BubbleTooltip(this.quill,this.options.bounds);
this.tooltip.root.appendChild(toolbar.container);
this.buildButtons([].slice.call(toolbar.container.querySelectorAll('button')),_icons2.default);
this.buildPickers([].slice.call(toolbar.container.querySelectorAll('select')),_icons2.default);
}}]);


return BubbleTheme;
}(_base2.default);

BubbleTheme.DEFAULTS=(0, _extend2.default)(true,{},_base2.default.DEFAULTS,{
modules:{
toolbar:{
handlers:{
link:function link(value){
if(!value){
this.quill.format('link',false);
}else {
this.quill.theme.tooltip.edit();
}
}}}}});





var BubbleTooltip=function(_BaseTooltip){
_inherits(BubbleTooltip,_BaseTooltip);

function BubbleTooltip(quill,bounds){
_classCallCheck(this,BubbleTooltip);

var _this2=_possibleConstructorReturn(this,(BubbleTooltip.__proto__||Object.getPrototypeOf(BubbleTooltip)).call(this,quill,bounds));

_this2.quill.on(_emitter2.default.events.EDITOR_CHANGE,function(type,range,oldRange,source){
if(type!==_emitter2.default.events.SELECTION_CHANGE)return;
if(range!=null&&range.length>0&&source===_emitter2.default.sources.USER){
_this2.show();
// Lock our width so we will expand beyond our offsetParent boundaries
_this2.root.style.left='0px';
_this2.root.style.width='';
_this2.root.style.width=_this2.root.offsetWidth+'px';
var lines=_this2.quill.getLines(range.index,range.length);
if(lines.length===1){
_this2.position(_this2.quill.getBounds(range));
}else {
var lastLine=lines[lines.length-1];
var index=_this2.quill.getIndex(lastLine);
var length=Math.min(lastLine.length()-1,range.index+range.length-index);
var _bounds=_this2.quill.getBounds(new _selection.Range(index,length));
_this2.position(_bounds);
}
}else if(document.activeElement!==_this2.textbox&&_this2.quill.hasFocus()){
_this2.hide();
}
});
return _this2;
}

_createClass(BubbleTooltip,[{
key:'listen',
value:function listen(){
var _this3=this;

_get(BubbleTooltip.prototype.__proto__||Object.getPrototypeOf(BubbleTooltip.prototype),'listen',this).call(this);
this.root.querySelector('.ql-close').addEventListener('click',function(){
_this3.root.classList.remove('ql-editing');
});
this.quill.on(_emitter2.default.events.SCROLL_OPTIMIZE,function(){
// Let selection be restored by toolbar handlers before repositioning
setTimeout(function(){
if(_this3.root.classList.contains('ql-hidden'))return;
var range=_this3.quill.getSelection();
if(range!=null){
_this3.position(_this3.quill.getBounds(range));
}
},1);
});
}},
{
key:'cancel',
value:function cancel(){
this.show();
}},
{
key:'position',
value:function position(reference){
var shift=_get(BubbleTooltip.prototype.__proto__||Object.getPrototypeOf(BubbleTooltip.prototype),'position',this).call(this,reference);
var arrow=this.root.querySelector('.ql-tooltip-arrow');
arrow.style.marginLeft='';
if(shift===0)return shift;
arrow.style.marginLeft=-1*shift-arrow.offsetWidth/2+'px';
}}]);


return BubbleTooltip;
}(_base.BaseTooltip);

BubbleTooltip.TEMPLATE=['<span class="ql-tooltip-arrow"></span>','<div class="ql-tooltip-editor">','<input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL">','<a class="ql-close"></a>','</div>'].join('');

exports.BubbleTooltip=BubbleTooltip;
exports.default=BubbleTheme;

/***/},
/* 109 */
/***/function(module,exports,__webpack_require__){

module.exports=__webpack_require__(63);


/***/}
/******/])["default"]);
});
/* WEBPACK VAR INJECTION */}).call(this,__webpack_require__(/*! ./../../buffer/index.js */"./node_modules/buffer/index.js").Buffer);

/***/},

/***/"./node_modules/vue2-editor/dist/vue2-editor.esm.js":
/*!**********************************************************!*\
  !*** ./node_modules/vue2-editor/dist/vue2-editor.esm.js ***!
  \**********************************************************/
/*! exports provided: Quill, default, VueEditor, install */
/***/function node_modulesVue2EditorDistVue2EditorEsmJs(module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global){/* harmony export (binding) */__webpack_require__.d(__webpack_exports__,"VueEditor",function(){return VueEditor;});
/* harmony export (binding) */__webpack_require__.d(__webpack_exports__,"install",function(){return install;});
/* harmony import */var quill__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! quill */"./node_modules/quill/dist/quill.js");
/* harmony import */var quill__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(quill__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (default from non-harmony) */__webpack_require__.d(__webpack_exports__,"Quill",function(){return quill__WEBPACK_IMPORTED_MODULE_0___default.a;});
/*!
 * vue2-editor v2.10.2 
 * (c) 2019 David Royer
 * Released under the MIT License.
 */



var defaultToolbar=[[{
header:[false,1,2,3,4,5,6]}],
["bold","italic","underline","strike"],// toggled buttons
[{
align:""},
{
align:"center"},
{
align:"right"},
{
align:"justify"}],
["blockquote","code-block"],[{
list:"ordered"},
{
list:"bullet"},
{
list:"check"}],
[{
indent:"-1"},
{
indent:"+1"}],
// outdent/indent
[{
color:[]},
{
background:[]}],
// dropdown with defaults from theme
["link","image","video"],["clean"]// remove formatting button
];

var oldApi={
props:{
customModules:Array},

methods:{
registerCustomModules:function registerCustomModules(Quill){
if(this.customModules!==undefined){
this.customModules.forEach(function(customModule){
Quill.register("modules/"+customModule.alias,customModule.module);
});
}
}}};



function _typeof(obj){
if(typeof Symbol==="function"&&typeof Symbol.iterator==="symbol"){
_typeof=function _typeof(obj){
return typeof obj;
};
}else {
_typeof=function _typeof(obj){
return obj&&typeof Symbol==="function"&&obj.constructor===Symbol&&obj!==Symbol.prototype?"symbol":typeof obj;
};
}

return _typeof(obj);
}

function _classCallCheck(instance,Constructor){
if(!(instance instanceof Constructor)){
throw new TypeError("Cannot call a class as a function");
}
}

function _defineProperties(target,props){
for(var i=0;i<props.length;i++){
var descriptor=props[i];
descriptor.enumerable=descriptor.enumerable||false;
descriptor.configurable=true;
if("value"in descriptor)descriptor.writable=true;
Object.defineProperty(target,descriptor.key,descriptor);
}
}

function _createClass(Constructor,protoProps,staticProps){
if(protoProps)_defineProperties(Constructor.prototype,protoProps);
if(staticProps)_defineProperties(Constructor,staticProps);
return Constructor;
}

function _inherits(subClass,superClass){
if(typeof superClass!=="function"&&superClass!==null){
throw new TypeError("Super expression must either be null or a function");
}

subClass.prototype=Object.create(superClass&&superClass.prototype,{
constructor:{
value:subClass,
writable:true,
configurable:true}});


if(superClass)_setPrototypeOf(subClass,superClass);
}

function _getPrototypeOf(o){
_getPrototypeOf=Object.setPrototypeOf?Object.getPrototypeOf:function _getPrototypeOf(o){
return o.__proto__||Object.getPrototypeOf(o);
};
return _getPrototypeOf(o);
}

function _setPrototypeOf(o,p){
_setPrototypeOf=Object.setPrototypeOf||function _setPrototypeOf(o,p){
o.__proto__=p;
return o;
};

return _setPrototypeOf(o,p);
}

function _assertThisInitialized(self){
if(self===void 0){
throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
}

return self;
}

function _possibleConstructorReturn(self,call){
if(call&&(typeof call==="object"||typeof call==="function")){
return call;
}

return _assertThisInitialized(self);
}

function _slicedToArray(arr,i){
return _arrayWithHoles(arr)||_iterableToArrayLimit(arr,i)||_nonIterableRest();
}

function _arrayWithHoles(arr){
if(Array.isArray(arr))return arr;
}

function _iterableToArrayLimit(arr,i){
var _arr=[];
var _n=true;
var _d=false;
var _e=undefined;

try{
for(var _i=arr[Symbol.iterator](),_s;!(_n=(_s=_i.next()).done);_n=true){
_arr.push(_s.value);

if(i&&_arr.length===i)break;
}
}catch(err){
_d=true;
_e=err;
}finally{
try{
if(!_n&&_i["return"]!=null)_i["return"]();
}finally{
if(_d)throw _e;
}
}

return _arr;
}

function _nonIterableRest(){
throw new TypeError("Invalid attempt to destructure non-iterable instance");
}

/**
 * Performs a deep merge of `source` into `target`.
 * Mutates `target` only but not its objects and arrays.
 *
 */
function mergeDeep(target,source){
var isObject=function isObject(obj){
return obj&&_typeof(obj)==="object";
};

if(!isObject(target)||!isObject(source)){
return source;
}

Object.keys(source).forEach(function(key){
var targetValue=target[key];
var sourceValue=source[key];

if(Array.isArray(targetValue)&&Array.isArray(sourceValue)){
target[key]=targetValue.concat(sourceValue);
}else if(isObject(targetValue)&&isObject(sourceValue)){
target[key]=mergeDeep(Object.assign({},targetValue),sourceValue);
}else {
target[key]=sourceValue;
}
});
return target;
}

var BlockEmbed=quill__WEBPACK_IMPORTED_MODULE_0___default.a.import("blots/block/embed");

var HorizontalRule=
/*#__PURE__*/
function(_BlockEmbed){
_inherits(HorizontalRule,_BlockEmbed);

function HorizontalRule(){
_classCallCheck(this,HorizontalRule);

return _possibleConstructorReturn(this,_getPrototypeOf(HorizontalRule).apply(this,arguments));
}

return HorizontalRule;
}(BlockEmbed);

HorizontalRule.blotName="hr";
HorizontalRule.tagName="hr";
quill__WEBPACK_IMPORTED_MODULE_0___default.a.register("formats/horizontal",HorizontalRule);

var MarkdownShortcuts=
/*#__PURE__*/
function(){
function MarkdownShortcuts(quill,options){
var _this=this;

_classCallCheck(this,MarkdownShortcuts);

this.quill=quill;
this.options=options;
this.ignoreTags=["PRE"];
this.matches=[{
name:"header",
pattern:/^(#){1,6}\s/g,
action:function action(text,selection,pattern){
var match=pattern.exec(text);
if(!match)return;
var size=match[0].length;// Need to defer this action https://github.com/quilljs/quill/issues/1134

setTimeout(function(){
_this.quill.formatLine(selection.index,0,"header",size-1);

_this.quill.deleteText(selection.index-size,size);
},0);
}},
{
name:"blockquote",
pattern:/^(>)\s/g,
action:function action(_text,selection){
// Need to defer this action https://github.com/quilljs/quill/issues/1134
setTimeout(function(){
_this.quill.formatLine(selection.index,1,"blockquote",true);

_this.quill.deleteText(selection.index-2,2);
},0);
}},
{
name:"code-block",
pattern:/^`{3}(?:\s|\n)/g,
action:function action(_text,selection){
// Need to defer this action https://github.com/quilljs/quill/issues/1134
setTimeout(function(){
_this.quill.formatLine(selection.index,1,"code-block",true);

_this.quill.deleteText(selection.index-4,4);
},0);
}},
{
name:"bolditalic",
pattern:/(?:\*|_){3}(.+?)(?:\*|_){3}/g,
action:function action(text,_selection,pattern,lineStart){
var match=pattern.exec(text);
var annotatedText=match[0];
var matchedText=match[1];
var startIndex=lineStart+match.index;
if(text.match(/^([*_ \n]+)$/g))return;
setTimeout(function(){
_this.quill.deleteText(startIndex,annotatedText.length);

_this.quill.insertText(startIndex,matchedText,{
bold:true,
italic:true});


_this.quill.format("bold",false);
},0);
}},
{
name:"bold",
pattern:/(?:\*|_){2}(.+?)(?:\*|_){2}/g,
action:function action(text,_selection,pattern,lineStart){
var match=pattern.exec(text);
var annotatedText=match[0];
var matchedText=match[1];
var startIndex=lineStart+match.index;
if(text.match(/^([*_ \n]+)$/g))return;
setTimeout(function(){
_this.quill.deleteText(startIndex,annotatedText.length);

_this.quill.insertText(startIndex,matchedText,{
bold:true});


_this.quill.format("bold",false);
},0);
}},
{
name:"italic",
pattern:/(?:\*|_){1}(.+?)(?:\*|_){1}/g,
action:function action(text,_selection,pattern,lineStart){
var match=pattern.exec(text);
var annotatedText=match[0];
var matchedText=match[1];
var startIndex=lineStart+match.index;
if(text.match(/^([*_ \n]+)$/g))return;
setTimeout(function(){
_this.quill.deleteText(startIndex,annotatedText.length);

_this.quill.insertText(startIndex,matchedText,{
italic:true});


_this.quill.format("italic",false);
},0);
}},
{
name:"strikethrough",
pattern:/(?:~~)(.+?)(?:~~)/g,
action:function action(text,_selection,pattern,lineStart){
var match=pattern.exec(text);
var annotatedText=match[0];
var matchedText=match[1];
var startIndex=lineStart+match.index;
if(text.match(/^([*_ \n]+)$/g))return;
setTimeout(function(){
_this.quill.deleteText(startIndex,annotatedText.length);

_this.quill.insertText(startIndex,matchedText,{
strike:true});


_this.quill.format("strike",false);
},0);
}},
{
name:"code",
pattern:/(?:`)(.+?)(?:`)/g,
action:function action(text,_selection,pattern,lineStart){
var match=pattern.exec(text);
var annotatedText=match[0];
var matchedText=match[1];
var startIndex=lineStart+match.index;
if(text.match(/^([*_ \n]+)$/g))return;
setTimeout(function(){
_this.quill.deleteText(startIndex,annotatedText.length);

_this.quill.insertText(startIndex,matchedText,{
code:true});


_this.quill.format("code",false);

_this.quill.insertText(_this.quill.getSelection()," ");
},0);
}},
{
name:"hr",
pattern:/^([-*]\s?){3}/g,
action:function action(text,selection){
var startIndex=selection.index-text.length;
setTimeout(function(){
_this.quill.deleteText(startIndex,text.length);

_this.quill.insertEmbed(startIndex+1,"hr",true,quill__WEBPACK_IMPORTED_MODULE_0___default.a.sources.USER);

_this.quill.insertText(startIndex+2,"\n",quill__WEBPACK_IMPORTED_MODULE_0___default.a.sources.SILENT);

_this.quill.setSelection(startIndex+2,quill__WEBPACK_IMPORTED_MODULE_0___default.a.sources.SILENT);
},0);
}},
{
name:"asterisk-ul",
pattern:/^(\*|\+)\s$/g,
action:function action(_text,selection,_pattern){
setTimeout(function(){
_this.quill.formatLine(selection.index,1,"list","unordered");

_this.quill.deleteText(selection.index-2,2);
},0);
}},
{
name:"image",
pattern:/(?:!\[(.+?)\])(?:\((.+?)\))/g,
action:function action(text,selection,pattern){
var startIndex=text.search(pattern);
var matchedText=text.match(pattern)[0];// const hrefText = text.match(/(?:!\[(.*?)\])/g)[0]

var hrefLink=text.match(/(?:\((.*?)\))/g)[0];
var start=selection.index-matchedText.length-1;

if(startIndex!==-1){
setTimeout(function(){
_this.quill.deleteText(start,matchedText.length);

_this.quill.insertEmbed(start,"image",hrefLink.slice(1,hrefLink.length-1));
},0);
}
}},
{
name:"link",
pattern:/(?:\[(.+?)\])(?:\((.+?)\))/g,
action:function action(text,selection,pattern){
var startIndex=text.search(pattern);
var matchedText=text.match(pattern)[0];
var hrefText=text.match(/(?:\[(.*?)\])/g)[0];
var hrefLink=text.match(/(?:\((.*?)\))/g)[0];
var start=selection.index-matchedText.length-1;

if(startIndex!==-1){
setTimeout(function(){
_this.quill.deleteText(start,matchedText.length);

_this.quill.insertText(start,hrefText.slice(1,hrefText.length-1),"link",hrefLink.slice(1,hrefLink.length-1));
},0);
}
}}];
// Handler that looks for insert deltas that match specific characters

this.quill.on("text-change",function(delta,_oldContents,_source){
for(var i=0;i<delta.ops.length;i++){
if(delta.ops[i].hasOwnProperty("insert")){
if(delta.ops[i].insert===" "){
_this.onSpace();
}else if(delta.ops[i].insert==="\n"){
_this.onEnter();
}
}
}
});
}

_createClass(MarkdownShortcuts,[{
key:"isValid",
value:function isValid(text,tagName){
return typeof text!=="undefined"&&text&&this.ignoreTags.indexOf(tagName)===-1;
}},
{
key:"onSpace",
value:function onSpace(){
var selection=this.quill.getSelection();
if(!selection)return;

var _this$quill$getLine=this.quill.getLine(selection.index),
_this$quill$getLine2=_slicedToArray(_this$quill$getLine,2),
line=_this$quill$getLine2[0],
offset=_this$quill$getLine2[1];

var text=line.domNode.textContent;
var lineStart=selection.index-offset;

if(this.isValid(text,line.domNode.tagName)){
var _iteratorNormalCompletion=true;
var _didIteratorError=false;
var _iteratorError=undefined;

try{
for(var _iterator=this.matches[Symbol.iterator](),_step;!(_iteratorNormalCompletion=(_step=_iterator.next()).done);_iteratorNormalCompletion=true){
var match=_step.value;
var matchedText=text.match(match.pattern);

if(matchedText){
// We need to replace only matched text not the whole line
console.log("matched:",match.name,text);
match.action(text,selection,match.pattern,lineStart);
return;
}
}
}catch(err){
_didIteratorError=true;
_iteratorError=err;
}finally{
try{
if(!_iteratorNormalCompletion&&_iterator.return!=null){
_iterator.return();
}
}finally{
if(_didIteratorError){
throw _iteratorError;
}
}
}
}
}},
{
key:"onEnter",
value:function onEnter(){
var selection=this.quill.getSelection();
if(!selection)return;

var _this$quill$getLine3=this.quill.getLine(selection.index),
_this$quill$getLine4=_slicedToArray(_this$quill$getLine3,2),
line=_this$quill$getLine4[0],
offset=_this$quill$getLine4[1];

var text=line.domNode.textContent+" ";
var lineStart=selection.index-offset;
selection.length=selection.index++;

if(this.isValid(text,line.domNode.tagName)){
var _iteratorNormalCompletion2=true;
var _didIteratorError2=false;
var _iteratorError2=undefined;

try{
for(var _iterator2=this.matches[Symbol.iterator](),_step2;!(_iteratorNormalCompletion2=(_step2=_iterator2.next()).done);_iteratorNormalCompletion2=true){
var match=_step2.value;
var matchedText=text.match(match.pattern);

if(matchedText){
console.log("matched",match.name,text);
match.action(text,selection,match.pattern,lineStart);
return;
}
}
}catch(err){
_didIteratorError2=true;
_iteratorError2=err;
}finally{
try{
if(!_iteratorNormalCompletion2&&_iterator2.return!=null){
_iterator2.return();
}
}finally{
if(_didIteratorError2){
throw _iteratorError2;
}
}
}
}
}}]);


return MarkdownShortcuts;
}();// module.exports = MarkdownShortcuts;

//
var script={
name:"VueEditor",
mixins:[oldApi],
props:{
id:{
type:String,
default:"quill-container"},

placeholder:{
type:String,
default:""},

value:{
type:String,
default:""},

disabled:{
type:Boolean},

editorToolbar:{
type:Array,
default:function _default(){
return [];
}},

editorOptions:{
type:Object,
required:false,
default:function _default(){
return {};
}},

useCustomImageHandler:{
type:Boolean,
default:false},

useMarkdownShortcuts:{
type:Boolean,
default:false}},


data:function data(){
return {
quill:null};

},
watch:{
value:function value(val){
if(val!=this.quill.root.innerHTML&&!this.quill.hasFocus()){
this.quill.root.innerHTML=val;
}
},
disabled:function disabled(status){
this.quill.enable(!status);
}},

mounted:function mounted(){
this.registerCustomModules(quill__WEBPACK_IMPORTED_MODULE_0___default.a);
this.registerPrototypes();
this.initializeEditor();
},
beforeDestroy:function beforeDestroy(){
this.quill=null;
delete this.quill;
},
methods:{
initializeEditor:function initializeEditor(){
this.setupQuillEditor();
this.checkForCustomImageHandler();
this.handleInitialContent();
this.registerEditorEventListeners();
this.$emit("ready",this.quill);
},
setupQuillEditor:function setupQuillEditor(){
var editorConfig={
debug:false,
modules:this.setModules(),
theme:"snow",
placeholder:this.placeholder?this.placeholder:"",
readOnly:this.disabled?this.disabled:false};

this.prepareEditorConfig(editorConfig);
this.quill=new quill__WEBPACK_IMPORTED_MODULE_0___default.a(this.$refs.quillContainer,editorConfig);
},
setModules:function setModules(){
var modules={
toolbar:this.editorToolbar.length?this.editorToolbar:defaultToolbar};


if(this.useMarkdownShortcuts){
quill__WEBPACK_IMPORTED_MODULE_0___default.a.register("modules/markdownShortcuts",MarkdownShortcuts,true);
modules["markdownShortcuts"]={};
}

return modules;
},
prepareEditorConfig:function prepareEditorConfig(editorConfig){
if(Object.keys(this.editorOptions).length>0&&this.editorOptions.constructor===Object){
if(this.editorOptions.modules&&typeof this.editorOptions.modules.toolbar!=="undefined"){
// We don't want to merge default toolbar with provided toolbar.
delete editorConfig.modules.toolbar;
}

mergeDeep(editorConfig,this.editorOptions);
}
},
registerPrototypes:function registerPrototypes(){
quill__WEBPACK_IMPORTED_MODULE_0___default.a.prototype.getHTML=function(){
return this.container.querySelector(".ql-editor").innerHTML;
};

quill__WEBPACK_IMPORTED_MODULE_0___default.a.prototype.getWordCount=function(){
return this.container.querySelector(".ql-editor").innerText.length;
};
},
registerEditorEventListeners:function registerEditorEventListeners(){
this.quill.on("text-change",this.handleTextChange);
this.quill.on("selection-change",this.handleSelectionChange);
this.listenForEditorEvent("text-change");
this.listenForEditorEvent("selection-change");
this.listenForEditorEvent("editor-change");
},
listenForEditorEvent:function listenForEditorEvent(type){
var _this=this;

this.quill.on(type,function(){
for(var _len=arguments.length,args=new Array(_len),_key=0;_key<_len;_key++){
args[_key]=arguments[_key];
}

_this.$emit.apply(_this,[type].concat(args));
});
},
handleInitialContent:function handleInitialContent(){
if(this.value)this.quill.root.innerHTML=this.value;// Set initial editor content
},
handleSelectionChange:function handleSelectionChange(range,oldRange){
if(!range&&oldRange)this.$emit("blur",this.quill);else if(range&&!oldRange)this.$emit("focus",this.quill);
},
handleTextChange:function handleTextChange(delta,oldContents){
var editorContent=this.quill.getHTML()==="<p><br></p>"?"":this.quill.getHTML();
this.$emit("input",editorContent);
if(this.useCustomImageHandler)this.handleImageRemoved(delta,oldContents);
},
handleImageRemoved:function handleImageRemoved(delta,oldContents){
var _this2=this;

var currrentContents=this.quill.getContents();
var deletedContents=currrentContents.diff(oldContents);
var operations=deletedContents.ops;
operations.map(function(operation){
if(operation.insert&&operation.insert.hasOwnProperty("image")){
var image=operation.insert.image;

_this2.$emit("image-removed",image);
}
});
},
checkForCustomImageHandler:function checkForCustomImageHandler(){
this.useCustomImageHandler===true?this.setupCustomImageHandler():"";
},
setupCustomImageHandler:function setupCustomImageHandler(){
var toolbar=this.quill.getModule("toolbar");
toolbar.addHandler("image",this.customImageHandler);
},
customImageHandler:function customImageHandler(image,callback){
this.$refs.fileInput.click();
},
emitImageInfo:function emitImageInfo($event){
var resetUploader=function resetUploader(){
var uploader=document.getElementById("file-upload");
uploader.value="";
};

var file=$event.target.files[0];
var Editor=this.quill;
var range=Editor.getSelection();
var cursorLocation=range.index;
this.$emit("image-added",file,Editor,cursorLocation,resetUploader);
}}};



function normalizeComponent(template,style,script,scopeId,isFunctionalTemplate,moduleIdentifier
/* server only */,
shadowMode,createInjector,createInjectorSSR,createInjectorShadow){
if(typeof shadowMode!=='boolean'){
createInjectorSSR=createInjector;
createInjector=shadowMode;
shadowMode=false;
}// Vue.extend constructor export interop.


var options=typeof script==='function'?script.options:script;// render functions

if(template&&template.render){
options.render=template.render;
options.staticRenderFns=template.staticRenderFns;
options._compiled=true;// functional template

if(isFunctionalTemplate){
options.functional=true;
}
}// scopedId


if(scopeId){
options._scopeId=scopeId;
}

var hook;

if(moduleIdentifier){
// server build
hook=function hook(context){
// 2.3 injection
context=context||// cached call
this.$vnode&&this.$vnode.ssrContext||// stateful
this.parent&&this.parent.$vnode&&this.parent.$vnode.ssrContext;// functional
// 2.2 with runInNewContext: true

if(!context&&typeof __VUE_SSR_CONTEXT__!=='undefined'){
context=__VUE_SSR_CONTEXT__;
}// inject component styles


if(style){
style.call(this,createInjectorSSR(context));
}// register component module identifier for async chunk inference


if(context&&context._registeredComponents){
context._registeredComponents.add(moduleIdentifier);
}
};// used by ssr in case component is cached and beforeCreate
// never gets called


options._ssrRegister=hook;
}else if(style){
hook=shadowMode?function(){
style.call(this,createInjectorShadow(this.$root.$options.shadowRoot));
}:function(context){
style.call(this,createInjector(context));
};
}

if(hook){
if(options.functional){
// register for functional component in vue file
var originalRender=options.render;

options.render=function renderWithStyleInjection(h,context){
hook.call(context);
return originalRender(h,context);
};
}else {
// inject component registration as beforeCreate hook
var existing=options.beforeCreate;
options.beforeCreate=existing?[].concat(existing,hook):[hook];
}
}

return script;
}

var normalizeComponent_1=normalizeComponent;

var isOldIE=typeof navigator!=='undefined'&&/msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());

function createInjector(context){
return function(id,style){
return addStyle(id,style);
};
}

var HEAD;
var styles={};

function addStyle(id,css){
var group=isOldIE?css.media||'default':id;
var style=styles[group]||(styles[group]={
ids:new Set(),
styles:[]});


if(!style.ids.has(id)){
style.ids.add(id);
var code=css.source;

if(css.map){
// https://developer.chrome.com/devtools/docs/javascript-debugging
// this makes source maps inside style tags work properly in Chrome
code+='\n/*# sourceURL='+css.map.sources[0]+' */';// http://stackoverflow.com/a/26603875

code+='\n/*# sourceMappingURL=data:application/json;base64,'+btoa(unescape(encodeURIComponent(JSON.stringify(css.map))))+' */';
}

if(!style.element){
style.element=document.createElement('style');
style.element.type='text/css';
if(css.media)style.element.setAttribute('media',css.media);

if(HEAD===undefined){
HEAD=document.head||document.getElementsByTagName('head')[0];
}

HEAD.appendChild(style.element);
}

if('styleSheet'in style.element){
style.styles.push(code);
style.element.styleSheet.cssText=style.styles.filter(Boolean).join('\n');
}else {
var index=style.ids.size-1;
var textNode=document.createTextNode(code);
var nodes=style.element.childNodes;
if(nodes[index])style.element.removeChild(nodes[index]);
if(nodes.length)style.element.insertBefore(textNode,nodes[index]);else style.element.appendChild(textNode);
}
}
}

var browser=createInjector;

/* script */
var __vue_script__=script;

/* template */
var __vue_render__=function __vue_render__(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"quillWrapper"},[_vm._t("toolbar"),_vm._v(" "),_c('div',{ref:"quillContainer",attrs:{"id":_vm.id}}),_vm._v(" "),_vm.useCustomImageHandler?_c('input',{ref:"fileInput",staticStyle:{"display":"none"},attrs:{"id":"file-upload","type":"file","accept":"image/*"},on:{"change":function change($event){return _vm.emitImageInfo($event);}}}):_vm._e()],2);};
var __vue_staticRenderFns__=[];

/* style */
var __vue_inject_styles__=function __vue_inject_styles__(inject){
if(!inject)return;
inject("data-v-59392418_0",{source:"/*!\n * Quill Editor v1.3.6\n * https://quilljs.com/\n * Copyright (c) 2014, Jason Chen\n * Copyright (c) 2013, salesforce.com\n */.ql-container{box-sizing:border-box;font-family:Helvetica,Arial,sans-serif;font-size:13px;height:100%;margin:0;position:relative}.ql-container.ql-disabled .ql-tooltip{visibility:hidden}.ql-container.ql-disabled .ql-editor ul[data-checked]>li::before{pointer-events:none}.ql-clipboard{left:-100000px;height:1px;overflow-y:hidden;position:absolute;top:50%}.ql-clipboard p{margin:0;padding:0}.ql-editor{box-sizing:border-box;line-height:1.42;height:100%;outline:0;overflow-y:auto;padding:12px 15px;tab-size:4;-moz-tab-size:4;text-align:left;white-space:pre-wrap;word-wrap:break-word}.ql-editor>*{cursor:text}.ql-editor blockquote,.ql-editor h1,.ql-editor h2,.ql-editor h3,.ql-editor h4,.ql-editor h5,.ql-editor h6,.ql-editor ol,.ql-editor p,.ql-editor pre,.ql-editor ul{margin:0;padding:0;counter-reset:list-1 list-2 list-3 list-4 list-5 list-6 list-7 list-8 list-9}.ql-editor ol,.ql-editor ul{padding-left:1.5em}.ql-editor ol>li,.ql-editor ul>li{list-style-type:none}.ql-editor ul>li::before{content:'\\2022'}.ql-editor ul[data-checked=false],.ql-editor ul[data-checked=true]{pointer-events:none}.ql-editor ul[data-checked=false]>li *,.ql-editor ul[data-checked=true]>li *{pointer-events:all}.ql-editor ul[data-checked=false]>li::before,.ql-editor ul[data-checked=true]>li::before{color:#777;cursor:pointer;pointer-events:all}.ql-editor ul[data-checked=true]>li::before{content:'\\2611'}.ql-editor ul[data-checked=false]>li::before{content:'\\2610'}.ql-editor li::before{display:inline-block;white-space:nowrap;width:1.2em}.ql-editor li:not(.ql-direction-rtl)::before{margin-left:-1.5em;margin-right:.3em;text-align:right}.ql-editor li.ql-direction-rtl::before{margin-left:.3em;margin-right:-1.5em}.ql-editor ol li:not(.ql-direction-rtl),.ql-editor ul li:not(.ql-direction-rtl){padding-left:1.5em}.ql-editor ol li.ql-direction-rtl,.ql-editor ul li.ql-direction-rtl{padding-right:1.5em}.ql-editor ol li{counter-reset:list-1 list-2 list-3 list-4 list-5 list-6 list-7 list-8 list-9;counter-increment:list-0}.ql-editor ol li:before{content:counter(list-0,decimal) '. '}.ql-editor ol li.ql-indent-1{counter-increment:list-1}.ql-editor ol li.ql-indent-1:before{content:counter(list-1,lower-alpha) '. '}.ql-editor ol li.ql-indent-1{counter-reset:list-2 list-3 list-4 list-5 list-6 list-7 list-8 list-9}.ql-editor ol li.ql-indent-2{counter-increment:list-2}.ql-editor ol li.ql-indent-2:before{content:counter(list-2,lower-roman) '. '}.ql-editor ol li.ql-indent-2{counter-reset:list-3 list-4 list-5 list-6 list-7 list-8 list-9}.ql-editor ol li.ql-indent-3{counter-increment:list-3}.ql-editor ol li.ql-indent-3:before{content:counter(list-3,decimal) '. '}.ql-editor ol li.ql-indent-3{counter-reset:list-4 list-5 list-6 list-7 list-8 list-9}.ql-editor ol li.ql-indent-4{counter-increment:list-4}.ql-editor ol li.ql-indent-4:before{content:counter(list-4,lower-alpha) '. '}.ql-editor ol li.ql-indent-4{counter-reset:list-5 list-6 list-7 list-8 list-9}.ql-editor ol li.ql-indent-5{counter-increment:list-5}.ql-editor ol li.ql-indent-5:before{content:counter(list-5,lower-roman) '. '}.ql-editor ol li.ql-indent-5{counter-reset:list-6 list-7 list-8 list-9}.ql-editor ol li.ql-indent-6{counter-increment:list-6}.ql-editor ol li.ql-indent-6:before{content:counter(list-6,decimal) '. '}.ql-editor ol li.ql-indent-6{counter-reset:list-7 list-8 list-9}.ql-editor ol li.ql-indent-7{counter-increment:list-7}.ql-editor ol li.ql-indent-7:before{content:counter(list-7,lower-alpha) '. '}.ql-editor ol li.ql-indent-7{counter-reset:list-8 list-9}.ql-editor ol li.ql-indent-8{counter-increment:list-8}.ql-editor ol li.ql-indent-8:before{content:counter(list-8,lower-roman) '. '}.ql-editor ol li.ql-indent-8{counter-reset:list-9}.ql-editor ol li.ql-indent-9{counter-increment:list-9}.ql-editor ol li.ql-indent-9:before{content:counter(list-9,decimal) '. '}.ql-editor .ql-indent-1:not(.ql-direction-rtl){padding-left:3em}.ql-editor li.ql-indent-1:not(.ql-direction-rtl){padding-left:4.5em}.ql-editor .ql-indent-1.ql-direction-rtl.ql-align-right{padding-right:3em}.ql-editor li.ql-indent-1.ql-direction-rtl.ql-align-right{padding-right:4.5em}.ql-editor .ql-indent-2:not(.ql-direction-rtl){padding-left:6em}.ql-editor li.ql-indent-2:not(.ql-direction-rtl){padding-left:7.5em}.ql-editor .ql-indent-2.ql-direction-rtl.ql-align-right{padding-right:6em}.ql-editor li.ql-indent-2.ql-direction-rtl.ql-align-right{padding-right:7.5em}.ql-editor .ql-indent-3:not(.ql-direction-rtl){padding-left:9em}.ql-editor li.ql-indent-3:not(.ql-direction-rtl){padding-left:10.5em}.ql-editor .ql-indent-3.ql-direction-rtl.ql-align-right{padding-right:9em}.ql-editor li.ql-indent-3.ql-direction-rtl.ql-align-right{padding-right:10.5em}.ql-editor .ql-indent-4:not(.ql-direction-rtl){padding-left:12em}.ql-editor li.ql-indent-4:not(.ql-direction-rtl){padding-left:13.5em}.ql-editor .ql-indent-4.ql-direction-rtl.ql-align-right{padding-right:12em}.ql-editor li.ql-indent-4.ql-direction-rtl.ql-align-right{padding-right:13.5em}.ql-editor .ql-indent-5:not(.ql-direction-rtl){padding-left:15em}.ql-editor li.ql-indent-5:not(.ql-direction-rtl){padding-left:16.5em}.ql-editor .ql-indent-5.ql-direction-rtl.ql-align-right{padding-right:15em}.ql-editor li.ql-indent-5.ql-direction-rtl.ql-align-right{padding-right:16.5em}.ql-editor .ql-indent-6:not(.ql-direction-rtl){padding-left:18em}.ql-editor li.ql-indent-6:not(.ql-direction-rtl){padding-left:19.5em}.ql-editor .ql-indent-6.ql-direction-rtl.ql-align-right{padding-right:18em}.ql-editor li.ql-indent-6.ql-direction-rtl.ql-align-right{padding-right:19.5em}.ql-editor .ql-indent-7:not(.ql-direction-rtl){padding-left:21em}.ql-editor li.ql-indent-7:not(.ql-direction-rtl){padding-left:22.5em}.ql-editor .ql-indent-7.ql-direction-rtl.ql-align-right{padding-right:21em}.ql-editor li.ql-indent-7.ql-direction-rtl.ql-align-right{padding-right:22.5em}.ql-editor .ql-indent-8:not(.ql-direction-rtl){padding-left:24em}.ql-editor li.ql-indent-8:not(.ql-direction-rtl){padding-left:25.5em}.ql-editor .ql-indent-8.ql-direction-rtl.ql-align-right{padding-right:24em}.ql-editor li.ql-indent-8.ql-direction-rtl.ql-align-right{padding-right:25.5em}.ql-editor .ql-indent-9:not(.ql-direction-rtl){padding-left:27em}.ql-editor li.ql-indent-9:not(.ql-direction-rtl){padding-left:28.5em}.ql-editor .ql-indent-9.ql-direction-rtl.ql-align-right{padding-right:27em}.ql-editor li.ql-indent-9.ql-direction-rtl.ql-align-right{padding-right:28.5em}.ql-editor .ql-video{display:block;max-width:100%}.ql-editor .ql-video.ql-align-center{margin:0 auto}.ql-editor .ql-video.ql-align-right{margin:0 0 0 auto}.ql-editor .ql-bg-black{background-color:#000}.ql-editor .ql-bg-red{background-color:#e60000}.ql-editor .ql-bg-orange{background-color:#f90}.ql-editor .ql-bg-yellow{background-color:#ff0}.ql-editor .ql-bg-green{background-color:#008a00}.ql-editor .ql-bg-blue{background-color:#06c}.ql-editor .ql-bg-purple{background-color:#93f}.ql-editor .ql-color-white{color:#fff}.ql-editor .ql-color-red{color:#e60000}.ql-editor .ql-color-orange{color:#f90}.ql-editor .ql-color-yellow{color:#ff0}.ql-editor .ql-color-green{color:#008a00}.ql-editor .ql-color-blue{color:#06c}.ql-editor .ql-color-purple{color:#93f}.ql-editor .ql-font-serif{font-family:Georgia,Times New Roman,serif}.ql-editor .ql-font-monospace{font-family:Monaco,Courier New,monospace}.ql-editor .ql-size-small{font-size:.75em}.ql-editor .ql-size-large{font-size:1.5em}.ql-editor .ql-size-huge{font-size:2.5em}.ql-editor .ql-direction-rtl{direction:rtl;text-align:inherit}.ql-editor .ql-align-center{text-align:center}.ql-editor .ql-align-justify{text-align:justify}.ql-editor .ql-align-right{text-align:right}.ql-editor.ql-blank::before{color:rgba(0,0,0,.6);content:attr(data-placeholder);font-style:italic;left:15px;pointer-events:none;position:absolute;right:15px}.ql-snow .ql-toolbar:after,.ql-snow.ql-toolbar:after{clear:both;content:'';display:table}.ql-snow .ql-toolbar button,.ql-snow.ql-toolbar button{background:0 0;border:none;cursor:pointer;display:inline-block;float:left;height:24px;padding:3px 5px;width:28px}.ql-snow .ql-toolbar button svg,.ql-snow.ql-toolbar button svg{float:left;height:100%}.ql-snow .ql-toolbar button:active:hover,.ql-snow.ql-toolbar button:active:hover{outline:0}.ql-snow .ql-toolbar input.ql-image[type=file],.ql-snow.ql-toolbar input.ql-image[type=file]{display:none}.ql-snow .ql-toolbar .ql-picker-item.ql-selected,.ql-snow .ql-toolbar .ql-picker-item:hover,.ql-snow .ql-toolbar .ql-picker-label.ql-active,.ql-snow .ql-toolbar .ql-picker-label:hover,.ql-snow .ql-toolbar button.ql-active,.ql-snow .ql-toolbar button:focus,.ql-snow .ql-toolbar button:hover,.ql-snow.ql-toolbar .ql-picker-item.ql-selected,.ql-snow.ql-toolbar .ql-picker-item:hover,.ql-snow.ql-toolbar .ql-picker-label.ql-active,.ql-snow.ql-toolbar .ql-picker-label:hover,.ql-snow.ql-toolbar button.ql-active,.ql-snow.ql-toolbar button:focus,.ql-snow.ql-toolbar button:hover{color:#06c}.ql-snow .ql-toolbar .ql-picker-item.ql-selected .ql-fill,.ql-snow .ql-toolbar .ql-picker-item.ql-selected .ql-stroke.ql-fill,.ql-snow .ql-toolbar .ql-picker-item:hover .ql-fill,.ql-snow .ql-toolbar .ql-picker-item:hover .ql-stroke.ql-fill,.ql-snow .ql-toolbar .ql-picker-label.ql-active .ql-fill,.ql-snow .ql-toolbar .ql-picker-label.ql-active .ql-stroke.ql-fill,.ql-snow .ql-toolbar .ql-picker-label:hover .ql-fill,.ql-snow .ql-toolbar .ql-picker-label:hover .ql-stroke.ql-fill,.ql-snow .ql-toolbar button.ql-active .ql-fill,.ql-snow .ql-toolbar button.ql-active .ql-stroke.ql-fill,.ql-snow .ql-toolbar button:focus .ql-fill,.ql-snow .ql-toolbar button:focus .ql-stroke.ql-fill,.ql-snow .ql-toolbar button:hover .ql-fill,.ql-snow .ql-toolbar button:hover .ql-stroke.ql-fill,.ql-snow.ql-toolbar .ql-picker-item.ql-selected .ql-fill,.ql-snow.ql-toolbar .ql-picker-item.ql-selected .ql-stroke.ql-fill,.ql-snow.ql-toolbar .ql-picker-item:hover .ql-fill,.ql-snow.ql-toolbar .ql-picker-item:hover .ql-stroke.ql-fill,.ql-snow.ql-toolbar .ql-picker-label.ql-active .ql-fill,.ql-snow.ql-toolbar .ql-picker-label.ql-active .ql-stroke.ql-fill,.ql-snow.ql-toolbar .ql-picker-label:hover .ql-fill,.ql-snow.ql-toolbar .ql-picker-label:hover .ql-stroke.ql-fill,.ql-snow.ql-toolbar button.ql-active .ql-fill,.ql-snow.ql-toolbar button.ql-active .ql-stroke.ql-fill,.ql-snow.ql-toolbar button:focus .ql-fill,.ql-snow.ql-toolbar button:focus .ql-stroke.ql-fill,.ql-snow.ql-toolbar button:hover .ql-fill,.ql-snow.ql-toolbar button:hover .ql-stroke.ql-fill{fill:#06c}.ql-snow .ql-toolbar .ql-picker-item.ql-selected .ql-stroke,.ql-snow .ql-toolbar .ql-picker-item.ql-selected .ql-stroke-miter,.ql-snow .ql-toolbar .ql-picker-item:hover .ql-stroke,.ql-snow .ql-toolbar .ql-picker-item:hover .ql-stroke-miter,.ql-snow .ql-toolbar .ql-picker-label.ql-active .ql-stroke,.ql-snow .ql-toolbar .ql-picker-label.ql-active .ql-stroke-miter,.ql-snow .ql-toolbar .ql-picker-label:hover .ql-stroke,.ql-snow .ql-toolbar .ql-picker-label:hover .ql-stroke-miter,.ql-snow .ql-toolbar button.ql-active .ql-stroke,.ql-snow .ql-toolbar button.ql-active .ql-stroke-miter,.ql-snow .ql-toolbar button:focus .ql-stroke,.ql-snow .ql-toolbar button:focus .ql-stroke-miter,.ql-snow .ql-toolbar button:hover .ql-stroke,.ql-snow .ql-toolbar button:hover .ql-stroke-miter,.ql-snow.ql-toolbar .ql-picker-item.ql-selected .ql-stroke,.ql-snow.ql-toolbar .ql-picker-item.ql-selected .ql-stroke-miter,.ql-snow.ql-toolbar .ql-picker-item:hover .ql-stroke,.ql-snow.ql-toolbar .ql-picker-item:hover .ql-stroke-miter,.ql-snow.ql-toolbar .ql-picker-label.ql-active .ql-stroke,.ql-snow.ql-toolbar .ql-picker-label.ql-active .ql-stroke-miter,.ql-snow.ql-toolbar .ql-picker-label:hover .ql-stroke,.ql-snow.ql-toolbar .ql-picker-label:hover .ql-stroke-miter,.ql-snow.ql-toolbar button.ql-active .ql-stroke,.ql-snow.ql-toolbar button.ql-active .ql-stroke-miter,.ql-snow.ql-toolbar button:focus .ql-stroke,.ql-snow.ql-toolbar button:focus .ql-stroke-miter,.ql-snow.ql-toolbar button:hover .ql-stroke,.ql-snow.ql-toolbar button:hover .ql-stroke-miter{stroke:#06c}@media (pointer:coarse){.ql-snow .ql-toolbar button:hover:not(.ql-active),.ql-snow.ql-toolbar button:hover:not(.ql-active){color:#444}.ql-snow .ql-toolbar button:hover:not(.ql-active) .ql-fill,.ql-snow .ql-toolbar button:hover:not(.ql-active) .ql-stroke.ql-fill,.ql-snow.ql-toolbar button:hover:not(.ql-active) .ql-fill,.ql-snow.ql-toolbar button:hover:not(.ql-active) .ql-stroke.ql-fill{fill:#444}.ql-snow .ql-toolbar button:hover:not(.ql-active) .ql-stroke,.ql-snow .ql-toolbar button:hover:not(.ql-active) .ql-stroke-miter,.ql-snow.ql-toolbar button:hover:not(.ql-active) .ql-stroke,.ql-snow.ql-toolbar button:hover:not(.ql-active) .ql-stroke-miter{stroke:#444}}.ql-snow{box-sizing:border-box}.ql-snow *{box-sizing:border-box}.ql-snow .ql-hidden{display:none}.ql-snow .ql-out-bottom,.ql-snow .ql-out-top{visibility:hidden}.ql-snow .ql-tooltip{position:absolute;transform:translateY(10px)}.ql-snow .ql-tooltip a{cursor:pointer;text-decoration:none}.ql-snow .ql-tooltip.ql-flip{transform:translateY(-10px)}.ql-snow .ql-formats{display:inline-block;vertical-align:middle}.ql-snow .ql-formats:after{clear:both;content:'';display:table}.ql-snow .ql-stroke{fill:none;stroke:#444;stroke-linecap:round;stroke-linejoin:round;stroke-width:2}.ql-snow .ql-stroke-miter{fill:none;stroke:#444;stroke-miterlimit:10;stroke-width:2}.ql-snow .ql-fill,.ql-snow .ql-stroke.ql-fill{fill:#444}.ql-snow .ql-empty{fill:none}.ql-snow .ql-even{fill-rule:evenodd}.ql-snow .ql-stroke.ql-thin,.ql-snow .ql-thin{stroke-width:1}.ql-snow .ql-transparent{opacity:.4}.ql-snow .ql-direction svg:last-child{display:none}.ql-snow .ql-direction.ql-active svg:last-child{display:inline}.ql-snow .ql-direction.ql-active svg:first-child{display:none}.ql-snow .ql-editor h1{font-size:2em}.ql-snow .ql-editor h2{font-size:1.5em}.ql-snow .ql-editor h3{font-size:1.17em}.ql-snow .ql-editor h4{font-size:1em}.ql-snow .ql-editor h5{font-size:.83em}.ql-snow .ql-editor h6{font-size:.67em}.ql-snow .ql-editor a{text-decoration:underline}.ql-snow .ql-editor blockquote{border-left:4px solid #ccc;margin-bottom:5px;margin-top:5px;padding-left:16px}.ql-snow .ql-editor code,.ql-snow .ql-editor pre{background-color:#f0f0f0;border-radius:3px}.ql-snow .ql-editor pre{white-space:pre-wrap;margin-bottom:5px;margin-top:5px;padding:5px 10px}.ql-snow .ql-editor code{font-size:85%;padding:2px 4px}.ql-snow .ql-editor pre.ql-syntax{background-color:#23241f;color:#f8f8f2;overflow:visible}.ql-snow .ql-editor img{max-width:100%}.ql-snow .ql-picker{color:#444;display:inline-block;float:left;font-size:14px;font-weight:500;height:24px;position:relative;vertical-align:middle}.ql-snow .ql-picker-label{cursor:pointer;display:inline-block;height:100%;padding-left:8px;padding-right:2px;position:relative;width:100%}.ql-snow .ql-picker-label::before{display:inline-block;line-height:22px}.ql-snow .ql-picker-options{background-color:#fff;display:none;min-width:100%;padding:4px 8px;position:absolute;white-space:nowrap}.ql-snow .ql-picker-options .ql-picker-item{cursor:pointer;display:block;padding-bottom:5px;padding-top:5px}.ql-snow .ql-picker.ql-expanded .ql-picker-label{color:#ccc;z-index:2}.ql-snow .ql-picker.ql-expanded .ql-picker-label .ql-fill{fill:#ccc}.ql-snow .ql-picker.ql-expanded .ql-picker-label .ql-stroke{stroke:#ccc}.ql-snow .ql-picker.ql-expanded .ql-picker-options{display:block;margin-top:-1px;top:100%;z-index:1}.ql-snow .ql-color-picker,.ql-snow .ql-icon-picker{width:28px}.ql-snow .ql-color-picker .ql-picker-label,.ql-snow .ql-icon-picker .ql-picker-label{padding:2px 4px}.ql-snow .ql-color-picker .ql-picker-label svg,.ql-snow .ql-icon-picker .ql-picker-label svg{right:4px}.ql-snow .ql-icon-picker .ql-picker-options{padding:4px 0}.ql-snow .ql-icon-picker .ql-picker-item{height:24px;width:24px;padding:2px 4px}.ql-snow .ql-color-picker .ql-picker-options{padding:3px 5px;width:152px}.ql-snow .ql-color-picker .ql-picker-item{border:1px solid transparent;float:left;height:16px;margin:2px;padding:0;width:16px}.ql-snow .ql-picker:not(.ql-color-picker):not(.ql-icon-picker) svg{position:absolute;margin-top:-9px;right:0;top:50%;width:18px}.ql-snow .ql-picker.ql-font .ql-picker-item[data-label]:not([data-label=''])::before,.ql-snow .ql-picker.ql-font .ql-picker-label[data-label]:not([data-label=''])::before,.ql-snow .ql-picker.ql-header .ql-picker-item[data-label]:not([data-label=''])::before,.ql-snow .ql-picker.ql-header .ql-picker-label[data-label]:not([data-label=''])::before,.ql-snow .ql-picker.ql-size .ql-picker-item[data-label]:not([data-label=''])::before,.ql-snow .ql-picker.ql-size .ql-picker-label[data-label]:not([data-label=''])::before{content:attr(data-label)}.ql-snow .ql-picker.ql-header{width:98px}.ql-snow .ql-picker.ql-header .ql-picker-item::before,.ql-snow .ql-picker.ql-header .ql-picker-label::before{content:'Normal'}.ql-snow .ql-picker.ql-header .ql-picker-item[data-value=\"1\"]::before,.ql-snow .ql-picker.ql-header .ql-picker-label[data-value=\"1\"]::before{content:'Heading 1'}.ql-snow .ql-picker.ql-header .ql-picker-item[data-value=\"2\"]::before,.ql-snow .ql-picker.ql-header .ql-picker-label[data-value=\"2\"]::before{content:'Heading 2'}.ql-snow .ql-picker.ql-header .ql-picker-item[data-value=\"3\"]::before,.ql-snow .ql-picker.ql-header .ql-picker-label[data-value=\"3\"]::before{content:'Heading 3'}.ql-snow .ql-picker.ql-header .ql-picker-item[data-value=\"4\"]::before,.ql-snow .ql-picker.ql-header .ql-picker-label[data-value=\"4\"]::before{content:'Heading 4'}.ql-snow .ql-picker.ql-header .ql-picker-item[data-value=\"5\"]::before,.ql-snow .ql-picker.ql-header .ql-picker-label[data-value=\"5\"]::before{content:'Heading 5'}.ql-snow .ql-picker.ql-header .ql-picker-item[data-value=\"6\"]::before,.ql-snow .ql-picker.ql-header .ql-picker-label[data-value=\"6\"]::before{content:'Heading 6'}.ql-snow .ql-picker.ql-header .ql-picker-item[data-value=\"1\"]::before{font-size:2em}.ql-snow .ql-picker.ql-header .ql-picker-item[data-value=\"2\"]::before{font-size:1.5em}.ql-snow .ql-picker.ql-header .ql-picker-item[data-value=\"3\"]::before{font-size:1.17em}.ql-snow .ql-picker.ql-header .ql-picker-item[data-value=\"4\"]::before{font-size:1em}.ql-snow .ql-picker.ql-header .ql-picker-item[data-value=\"5\"]::before{font-size:.83em}.ql-snow .ql-picker.ql-header .ql-picker-item[data-value=\"6\"]::before{font-size:.67em}.ql-snow .ql-picker.ql-font{width:108px}.ql-snow .ql-picker.ql-font .ql-picker-item::before,.ql-snow .ql-picker.ql-font .ql-picker-label::before{content:'Sans Serif'}.ql-snow .ql-picker.ql-font .ql-picker-item[data-value=serif]::before,.ql-snow .ql-picker.ql-font .ql-picker-label[data-value=serif]::before{content:'Serif'}.ql-snow .ql-picker.ql-font .ql-picker-item[data-value=monospace]::before,.ql-snow .ql-picker.ql-font .ql-picker-label[data-value=monospace]::before{content:'Monospace'}.ql-snow .ql-picker.ql-font .ql-picker-item[data-value=serif]::before{font-family:Georgia,Times New Roman,serif}.ql-snow .ql-picker.ql-font .ql-picker-item[data-value=monospace]::before{font-family:Monaco,Courier New,monospace}.ql-snow .ql-picker.ql-size{width:98px}.ql-snow .ql-picker.ql-size .ql-picker-item::before,.ql-snow .ql-picker.ql-size .ql-picker-label::before{content:'Normal'}.ql-snow .ql-picker.ql-size .ql-picker-item[data-value=small]::before,.ql-snow .ql-picker.ql-size .ql-picker-label[data-value=small]::before{content:'Small'}.ql-snow .ql-picker.ql-size .ql-picker-item[data-value=large]::before,.ql-snow .ql-picker.ql-size .ql-picker-label[data-value=large]::before{content:'Large'}.ql-snow .ql-picker.ql-size .ql-picker-item[data-value=huge]::before,.ql-snow .ql-picker.ql-size .ql-picker-label[data-value=huge]::before{content:'Huge'}.ql-snow .ql-picker.ql-size .ql-picker-item[data-value=small]::before{font-size:10px}.ql-snow .ql-picker.ql-size .ql-picker-item[data-value=large]::before{font-size:18px}.ql-snow .ql-picker.ql-size .ql-picker-item[data-value=huge]::before{font-size:32px}.ql-snow .ql-color-picker.ql-background .ql-picker-item{background-color:#fff}.ql-snow .ql-color-picker.ql-color .ql-picker-item{background-color:#000}.ql-toolbar.ql-snow{border:1px solid #ccc;box-sizing:border-box;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;padding:8px}.ql-toolbar.ql-snow .ql-formats{margin-right:15px}.ql-toolbar.ql-snow .ql-picker-label{border:1px solid transparent}.ql-toolbar.ql-snow .ql-picker-options{border:1px solid transparent;box-shadow:rgba(0,0,0,.2) 0 2px 8px}.ql-toolbar.ql-snow .ql-picker.ql-expanded .ql-picker-label{border-color:#ccc}.ql-toolbar.ql-snow .ql-picker.ql-expanded .ql-picker-options{border-color:#ccc}.ql-toolbar.ql-snow .ql-color-picker .ql-picker-item.ql-selected,.ql-toolbar.ql-snow .ql-color-picker .ql-picker-item:hover{border-color:#000}.ql-toolbar.ql-snow+.ql-container.ql-snow{border-top:0}.ql-snow .ql-tooltip{background-color:#fff;border:1px solid #ccc;box-shadow:0 0 5px #ddd;color:#444;padding:5px 12px;white-space:nowrap}.ql-snow .ql-tooltip::before{content:\"Visit URL:\";line-height:26px;margin-right:8px}.ql-snow .ql-tooltip input[type=text]{display:none;border:1px solid #ccc;font-size:13px;height:26px;margin:0;padding:3px 5px;width:170px}.ql-snow .ql-tooltip a.ql-preview{display:inline-block;max-width:200px;overflow-x:hidden;text-overflow:ellipsis;vertical-align:top}.ql-snow .ql-tooltip a.ql-action::after{border-right:1px solid #ccc;content:'Edit';margin-left:16px;padding-right:8px}.ql-snow .ql-tooltip a.ql-remove::before{content:'Remove';margin-left:8px}.ql-snow .ql-tooltip a{line-height:26px}.ql-snow .ql-tooltip.ql-editing a.ql-preview,.ql-snow .ql-tooltip.ql-editing a.ql-remove{display:none}.ql-snow .ql-tooltip.ql-editing input[type=text]{display:inline-block}.ql-snow .ql-tooltip.ql-editing a.ql-action::after{border-right:0;content:'Save';padding-right:0}.ql-snow .ql-tooltip[data-mode=link]::before{content:\"Enter link:\"}.ql-snow .ql-tooltip[data-mode=formula]::before{content:\"Enter formula:\"}.ql-snow .ql-tooltip[data-mode=video]::before{content:\"Enter video:\"}.ql-snow a{color:#06c}.ql-container.ql-snow{border:1px solid #ccc}",map:undefined,media:undefined}),
inject("data-v-59392418_1",{source:".ql-editor{min-height:200px;font-size:16px}.ql-snow .ql-stroke.ql-thin,.ql-snow .ql-thin{stroke-width:1px!important}.quillWrapper .ql-snow.ql-toolbar{padding-top:8px;padding-bottom:4px}.quillWrapper .ql-snow.ql-toolbar .ql-formats{margin-bottom:10px}.ql-snow .ql-toolbar button svg,.quillWrapper .ql-snow.ql-toolbar button svg{width:22px;height:22px}.quillWrapper .ql-editor ul[data-checked=false]>li::before,.quillWrapper .ql-editor ul[data-checked=true]>li::before{font-size:1.35em;vertical-align:baseline;bottom:-.065em;font-weight:900;color:#222}.quillWrapper .ql-snow .ql-stroke{stroke:rgba(63,63,63,.95);stroke-linecap:square;stroke-linejoin:initial;stroke-width:1.7px}.quillWrapper .ql-picker-label{font-size:15px}.quillWrapper .ql-snow .ql-active .ql-stroke{stroke-width:2.25px}.quillWrapper .ql-toolbar.ql-snow .ql-formats{vertical-align:top}.ql-picker:not(.ql-background){position:relative;top:2px}.ql-picker.ql-color-picker svg{width:22px!important;height:22px!important}.quillWrapper .imageResizeActive img{display:block;cursor:pointer}.quillWrapper .imageResizeActive~div svg{cursor:pointer}",map:undefined,media:undefined});

};
/* scoped */
var __vue_scope_id__=undefined;
/* module identifier */
var __vue_module_identifier__=undefined;
/* functional template */
var __vue_is_functional_template__=false;
/* style inject SSR */



var VueEditor=normalizeComponent_1(
{render:__vue_render__,staticRenderFns:__vue_staticRenderFns__},
__vue_inject_styles__,
__vue_script__,
__vue_scope_id__,
__vue_is_functional_template__,
__vue_module_identifier__,
browser,
undefined);


var version="2.10.2";// Declare install function executed by Vue.use()

function install(Vue){
if(install.installed)return;
install.installed=true;
Vue.component("VueEditor",VueEditor);
}
var VPlugin={
install:install,
version:version,
Quill:quill__WEBPACK_IMPORTED_MODULE_0___default.a,
VueEditor:VueEditor};
// Auto-install when vue is found (eg. in browser via <script> tag)

var GlobalVue=null;

if(typeof window!=="undefined"){
GlobalVue=window.Vue;
}else if(typeof global!=="undefined"){
GlobalVue=global.Vue;
}

if(GlobalVue){
GlobalVue.use(VPlugin);
}
/*************************************************/

/* harmony default export */__webpack_exports__["default"]=VPlugin;


/* WEBPACK VAR INJECTION */}).call(this,__webpack_require__(/*! ./../../webpack/buildin/global.js */"./node_modules/webpack/buildin/global.js"));

/***/}}]);

}());
